package cdc.issues.locations;

import cdc.util.lang.Checks;

/**
 * Default implementation of IssueLocation.
 * <p>
 * For example, it can be used to designate the attribute of an object.
 *
 * @author Damien Carbonne
 */
public class DefaultLocation extends AbstractLocation {
    private final String path;
    private final String anchor;

    public static final String TAG = "DefaultLocation";

    public DefaultLocation(String path,
                           String anchor) {
        this.path = Checks.isNotNull(path, "path");
        this.anchor = anchor;
    }

    public DefaultLocation(String path) {
        this(path,
             null);
    }

    public DefaultLocation(Location other) {
        this(other.getPath(),
             other.getAnchor());
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getAnchor() {
        return anchor;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String path;
        private String anchor = null;

        protected Builder() {
            super();
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder anchor(String anchor) {
            this.anchor = anchor;
            return this;
        }

        public DefaultLocation build() {
            return new DefaultLocation(path,
                                       anchor);
        }
    }
}