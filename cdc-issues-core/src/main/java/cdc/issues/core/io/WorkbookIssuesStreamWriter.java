package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import cdc.issues.Issue;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssueComment;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesIoFactoryFeatures.Hint;
import cdc.issues.io.IssuesStreamWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.SnapshotData;
import cdc.issues.locations.Location;
import cdc.issues.rules.Profile;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.tables.TableSection;
import cdc.util.encoding.Encoders;
import cdc.util.encoding.ExtensionEncoder;

/**
 * Implementation of {@link IssuesStreamWriter} dedicated to Workbooks.
 *
 * @author Damien Carbonne
 */
public class WorkbookIssuesStreamWriter extends WorkbookIssuesIo implements IssuesStreamWriter {
    private final File file;
    private final OutputStream out;
    private final IssuesFormat format;
    private final OutSettings settings;
    private final WorkbookWriter<?> writer;
    private final int maxDataRows;
    private int sheetIndex = 0;
    private int rowIndex = 0;
    private String projectName = null;
    private String snapshotName = null;

    private static String orElse(String s1,
                                 String s2) {
        return s1 == null ? s2 : s1;
    }

    private WorkbookIssuesStreamWriter(File file,
                                       OutputStream out,
                                       IssuesFormat format,
                                       OutSettings settings,
                                       IssuesIoFactoryFeatures features)
            throws IOException {
        super(features);
        this.file = file;
        this.out = out;
        this.format = format;
        this.settings = settings;
        this.writer = createWriter();
        this.maxDataRows = writer.getKind().getMaxRows();

        for (final OutSettings.Hint hint : OutSettings.Hint.values()) {
            if (hint.isAuto() && settings.isEnabled(hint)) {
                throw new IllegalArgumentException("Non supported hint: " + hint);
            }
        }
    }

    public WorkbookIssuesStreamWriter(File file,
                                      OutSettings settings,
                                      IssuesIoFactoryFeatures features)
            throws IOException {
        this(file, null, null, settings, features);
    }

    public WorkbookIssuesStreamWriter(OutputStream out,
                                      IssuesFormat format,
                                      OutSettings settings,
                                      IssuesIoFactoryFeatures features)
            throws IOException {
        this(null, out, format, settings, features);
    }

    private WorkbookWriter<?> createWriter() throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();

        // Pass matching hints to factory
        final ExtensionEncoder<Hint, WorkbookWriterFactory.Hint> hintEncoder =
                Encoders.sameNameEncoder(Hint.class,
                                         WorkbookWriterFactory.Hint.class);
        for (final Hint sourceHint : hintEncoder.getSourceValues()) {
            final WorkbookWriterFactory.Hint targetHint = hintEncoder.encode(sourceHint);
            factory.setEnabled(targetHint, features.isEnabled(sourceHint));
        }

        if (file == null) {
            return factory.create(out,
                                  format.getWorkbookKind(),
                                  features.getWorkbookWriterFeatures());
        } else {
            return factory.create(file,
                                  features.getWorkbookWriterFeatures());
        }
    }

    private static String format(Location location) {
        return Location.toString(location);
    }

    private static String format(Location[] locations) {
        if (locations.length == 0) {
            return "";
        } else {
            return Arrays.asList(locations)
                         .stream()
                         .map(WorkbookIssuesStreamWriter::format)
                         .collect(Collectors.joining("\n"));
        }
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    private static String toPrefix(String s) {
        return s == null || s.isEmpty() ? "" : s;
    }

    private void addHeader(String name,
                           String comment) throws IOException {
        writer.addCellAndComment(name, comment);
    }

    private void addHeader(String name,
                           String comment,
                           OutSettings.Hint hint) throws IOException {
        if (!settings.isEnabled(hint)) {
            addHeader(name, comment);
        }
    }

    private void addData(Object data) throws IOException {
        writer.addCell(data);
    }

    private void addData(Object data,
                         OutSettings.Hint hint) throws IOException {
        if (!settings.isEnabled(hint)) {
            writer.addCell(data);
        }
    }

    private void addData(Supplier<Object> supplier,
                         OutSettings.Hint hint) throws IOException {
        if (!settings.isEnabled(hint)) {
            writer.addCell(supplier.get());
        }
    }

    /**
     * Generates a sheet containing snapshot infos.
     *
     * @param snapshot The snapshot data.
     * @throws IOException When an IO error occurs.
     */
    private void saveInfos(SnapshotData snapshot) throws IOException {
        final Profile profile = snapshot.getProfile().orElse(null);
        writer.beginSheet(WorkbooksIo.INFOS);
        writer.addRow(TableSection.HEADER, "Property", "Value");
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROJECT,
                               "Project name.",
                               validate(snapshot.getProjectName()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROJECT_DESCRIPTION,
                               "Project description.",
                               validate(snapshot.getProjectDescription()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROJECT_METAS,
                               "Project meta data.",
                               WorkbooksIo.format(snapshot.getProjectMetas()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROJECT_LABELS,
                               "Project labels.",
                               WorkbooksIo.format(snapshot.getProjectLabels()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROFILE,
                               "Profile name.",
                               profile == null ? "" : validate(profile.getName()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROFILE_DESCRIPTION,
                               "Profile description.",
                               profile == null ? "" : validate(profile.getDescription()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROFILE_METAS,
                               "Profile meta data.",
                               profile == null ? "" : WorkbooksIo.format(profile.getMetas()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.PROFILE_LABELS,
                               "Profile labels.",
                               profile == null ? "" : WorkbooksIo.format(profile.getLabels()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.SNAPSHOT,
                               "Snapshot name.",
                               validate(snapshot.getSnapshotName()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.SNAPSHOT_DESCRIPTION,
                               "Snapshot description.",
                               validate(snapshot.getSnapshotDescription()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.SNAPSHOT_METAS,
                               "Snapshot meta data.",
                               WorkbooksIo.format(snapshot.getSnapshotMetas()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.SNAPSHOT_TIMESTAMP,
                               "Snapshot timestamp.",
                               toString(snapshot.getSnapshotTimestamp()));
        WorkbooksIo.addInfoRow(writer,
                               WorkbooksIo.ISSUES_HASH,
                               "Issues hash.",
                               snapshot.getIssuesHash());
    }

    /**
     * Generates a sheet containing profile.
     *
     * @param snapshot The snapshot data.
     * @throws IOException When an IO error occurs.
     */
    private void saveProfile(SnapshotData snapshot) throws IOException {
        WorkbooksIo.writeProfileRules(writer,
                                      WorkbooksIo.PROFILE,
                                      snapshot.getProfile().orElse(null));
    }

    private void startNewIssuesSheet() throws IOException {
        rowIndex = 0;
        sheetIndex++;
        writer.beginSheet(WorkbooksIo.ISSUES + "#" + sheetIndex);
        header();
        rowIndex++;
    }

    @Override
    public void startDocument() {
        // Ignore
    }

    @Override
    public void add(SnapshotData snapshot) throws IOException {
        this.projectName = snapshot.getProjectName();
        this.snapshotName = snapshot.getSnapshotName();
        saveInfos(snapshot);
        saveProfile(snapshot);
    }

    /**
     * Produces the header from settings.
     * <p>
     * Must be consistent with code used to generate issues.
     *
     * @throws IOException When an IO error occurs.
     */
    private void header() throws IOException {
        writer.beginRow(TableSection.HEADER);

        addHeader(WorkbooksIo.TIMESTAMP,
                  "The issue timestamp.");
        addHeader(WorkbooksIo.ID_HASH,
                  "The issue id hash (built from domain, name, params, project and locations.",
                  OutSettings.Hint.NO_ID_HASH_COL);
        addHeader(WorkbooksIo.DOMAIN,
                  "The issue rule domain.",
                  OutSettings.Hint.NO_DOMAIN_COL);
        addHeader(WorkbooksIo.NAME,
                  "The issue rule name.");
        addHeader(WorkbooksIo.TITLE,
                  "The issue rule title.");
        addHeader(WorkbooksIo.PARAMS,
                  "The issue rule effective parameters.",
                  OutSettings.Hint.NO_PARAMS_COL);
        for (final String s : settings.getParams()) {
            addHeader(toPrefix(settings.getParamPrefix()) + s, null);
        }

        addHeader(WorkbooksIo.PROJECT,
                  "The project (name of the dataset).",
                  OutSettings.Hint.NO_PROJECT_COL);
        addHeader(WorkbooksIo.SNAPSHOT,
                  "The snapshot (name of the dataset analysis).",
                  OutSettings.Hint.NO_SNAPSHOT_COL);
        addHeader(WorkbooksIo.SEVERITY,
                  "The issue rule severity.");
        addHeader(WorkbooksIo.DESCRIPTION,
                  "The issue description.");
        addHeader(WorkbooksIo.LOCATIONS,
                  """
                  The issue locations.
                  If the issue is related to a single place, then there should be one location.
                  If the issue is related to a consistency problem, then there should be at least two locations.""",
                  OutSettings.Hint.NO_LOCATIONS_COL);
        if (settings.getNumberOfLocations() > 0) {
            for (int index = 1; index <= settings.getNumberOfLocations(); index++) {
                final String prefix = WorkbooksIo.LOCATION + "#" + index + ".";
                addHeader(prefix + WorkbooksIo.TAG, null);
                addHeader(prefix + WorkbooksIo.PATH, null);
                addHeader(prefix + WorkbooksIo.ANCHOR, null);
            }
        }

        addHeader(WorkbooksIo.METAS,
                  """
                  Metadata associated to the issue (produced by the checker).
                  It is a set of (name, value) pairs that can be used to define non-standard information.
                  Some of them can be printed on their own column.""",
                  OutSettings.Hint.NO_METAS_COL);

        for (final String s : settings.getMetas()) {
            addHeader(toPrefix(settings.getMetaPrefix()) + s, null);
        }

        addHeader(WorkbooksIo.LABELS,
                  """
                  Labels associated to the issue (produced by the checker).
                  It is a set of names.
                  """,
                  OutSettings.Hint.NO_LABELS_COL);

        if (!settings.isEnabled(OutSettings.Hint.NO_ANSWERS)) {
            addHeader(WorkbooksIo.AUTHOR,
                      "Initrial author of the answer.");
            addHeader(WorkbooksIo.CREATED,
                      "Creation date of the answer.");
            addHeader(WorkbooksIo.MODIFIED,
                      "Last modification date of the answer.");
            addHeader(WorkbooksIo.STATUS,
                      "Status of the issue.");
            addHeader(WorkbooksIo.RESOLUTION,
                      "Resolution of the issue.");
            addHeader(WorkbooksIo.ASSIGNEE,
                      "Person to which the issue is assigned.");
            addHeader(WorkbooksIo.NEW_SEVERITY,
                      "User defined severity of the issue, when the default one associated to the rule is not correct.");

            addHeader(WorkbooksIo.COMMENTS,
                      "Some comments on the issue.",
                      OutSettings.Hint.NO_COMMENTS_COL);

            if (settings.getNumberOfComments() > 0) {
                for (int index = 1; index <= settings.getNumberOfComments(); index++) {
                    final String prefix = WorkbooksIo.COMMENT + "#" + index + ".";
                    addHeader(prefix + WorkbooksIo.AUTHOR, null);
                    addHeader(prefix + WorkbooksIo.DATE, null);
                    addHeader(prefix + WorkbooksIo.TEXT, null);
                }
            }
            addHeader(WorkbooksIo.USER_METAS,
                      "Metadata associated to the issue (produced by users).",
                      OutSettings.Hint.NO_USER_METAS_COL);
            for (final String s : settings.getUserMetas()) {
                addHeader(toPrefix(settings.getUserMetaPrefix()) + s, null);
            }
            addHeader(WorkbooksIo.USER_LABELS,
                      "Labels associated to the issue (produced by users).",
                      OutSettings.Hint.NO_USER_LABELS_COL);
        }
    }

    @Override
    public void add(Issue issue,
                    IssueAnswer answer) throws IOException {
        if (sheetIndex == 0 || rowIndex == maxDataRows) {
            startNewIssuesSheet();
        }

        writer.beginRow(TableSection.DATA);
        addData(toString(issue.getTimestamp()));
        addData(issue::getIdHash, OutSettings.Hint.NO_ID_HASH_COL); // Avoid hash computation if not needed
        addData(issue.getDomain(), OutSettings.Hint.NO_DOMAIN_COL);
        addData(issue.getName());
        addData(issue.getTitle());
        addData(WorkbooksIo.format(issue.getParams()), OutSettings.Hint.NO_PARAMS_COL);

        // Write specifically selected param data
        for (final String name : settings.getParams()) {
            final String value = issue.getParams().getValue(name, "");
            addData(value);
        }

        addData(orElse(issue.getProject(), projectName), OutSettings.Hint.NO_PROJECT_COL);
        addData(orElse(issue.getSnapshot(), snapshotName), OutSettings.Hint.NO_SNAPSHOT_COL);
        addData(issue.getSeverity());
        addData(issue.getDescription());
        addData(format(issue.getLocations()), OutSettings.Hint.NO_LOCATIONS_COL);

        // Write specifically expanded location columns
        for (int index = 0; index < settings.getNumberOfLocations(); index++) {
            if (index < issue.getNumberOfLocations()) {
                final Location location = issue.getLocationAt(index);
                addData(location.getTag());
                addData(location.getPath());
                addData(location.getAnchor());
            } else {
                writer.addEmptyCell();
                writer.addEmptyCell();
                writer.addEmptyCell();
            }
        }

        addData(WorkbooksIo.format(issue.getMetas(), settings.getMetaComparator()), OutSettings.Hint.NO_METAS_COL);

        // Write specifically selected meta data
        for (final String name : settings.getMetas()) {
            final String value = issue.getMetas().getValue(name, "");
            addData(value);
        }
        addData(WorkbooksIo.format(issue.getLabels()), OutSettings.Hint.NO_LABELS_COL);

        if (answer != null && !settings.isEnabled(OutSettings.Hint.NO_ANSWERS)) {
            addData(answer.getAuthor());
            addData(toString(answer.getCreationDate()));
            addData(toString(answer.getModificationDate()));
            addData(answer.getStatus());
            addData(answer.getResolution());
            addData(answer.getAssignee());
            addData(answer.getNewSeverity());
            addData(format(answer.getComments()), OutSettings.Hint.NO_COMMENTS_COL);
            // Write specifically expanded comments columns
            for (int index = 0; index < settings.getNumberOfComments(); index++) {
                if (index < answer.getComments().size()) {
                    final IssueComment comment = answer.getComments().get(index);
                    addData(comment.getAuthor());
                    addData(toString(comment.getDate()));
                    addData(comment.getText());
                } else {
                    writer.addEmptyCell();
                    writer.addEmptyCell();
                    writer.addEmptyCell();
                }
            }
            addData(WorkbooksIo.format(answer.getMetas()), OutSettings.Hint.NO_USER_METAS_COL);
            // Write specifically selected user meta data
            for (final String name : settings.getUserMetas()) {
                final String value = answer.getMetas().getValue(name, "");
                addData(value);
            }

            addData(WorkbooksIo.format(answer.getLabels()), OutSettings.Hint.NO_USER_LABELS_COL);
        }

        rowIndex++;
    }

    @Override
    public void endDocument() throws IOException {
        // Do this to ensure at least one header is generated
        if (sheetIndex == 0) {
            startNewIssuesSheet();
        }
    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}