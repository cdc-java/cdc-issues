package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.impl.ProfileConfigImpl;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleConfig;
import cdc.issues.rules.RuleId;
import cdc.office.ss.SheetLoader;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.Header;
import cdc.office.tables.HeaderCell;
import cdc.office.tables.HeaderMapper;
import cdc.office.tables.Row;
import cdc.office.tables.TableSection;
import cdc.util.lang.CollectionUtils;
import cdc.util.strings.StringUtils;

public final class WorkbooksIo {
    private static final Pattern PARAM_PATTERN = Pattern.compile("(?m)^([^:]+):");

    private WorkbooksIo() {
    }

    static String toCase(String s) {
        return Names.WORKBOOKS_CASE_CONVERTER.splitAndConvert(s);
    }

    static final String ANCHOR = toCase(Names.ANCHOR);
    static final String ASSIGNEE = toCase(Names.ASSIGNEE);
    static final String AUTHOR = toCase(Names.AUTHOR);
    static final String COMMENT = toCase(Names.COMMENT);
    static final String COMMENTS = toCase(Names.COMMENTS);
    static final String CREATED = toCase(Names.CREATED);
    static final String CUSTOMIZED_SEVERITY = toCase(Names.CUSTOMIZED_SEVERITY);
    static final String DATE = toCase(Names.DATE);
    static final String DESCRIPTION = toCase(Names.DESCRIPTION);
    static final String DOMAIN = toCase(Names.DOMAIN);
    static final String ENABLED = toCase(Names.ENABLED);
    static final String ID_HASH = toCase(Names.ID_HASH);
    static final String INFOS = toCase(Names.INFOS);
    static final String ISSUES = toCase(Names.ISSUES);
    static final String ISSUES_HASH = toCase(Names.ISSUES_HASH);
    static final String LABELS = toCase(Names.LABELS);
    static final String LOCATION = toCase(Names.LOCATION);
    static final String LOCATIONS = toCase(Names.LOCATIONS);
    static final String METAS = toCase(Names.METAS);
    static final String MODIFIED = toCase(Names.MODIFIED);
    static final String NAME = toCase(Names.NAME);
    static final String NEW_SEVERITY = toCase(Names.NEW_SEVERITY);
    static final String PARAMS = toCase(Names.PARAMS);
    static final String PATH = toCase(Names.PATH);
    static final String PROFILE = toCase(Names.PROFILE);
    static final String PROFILE_CONFIGURATION = toCase(Names.PROFILE_CONFIGURATION);
    static final String PROFILE_DESCRIPTION = toCase(Names.PROFILE_DESCRIPTION);
    static final String PROFILE_LABELS = toCase(Names.PROFILE_LABELS);
    static final String PROFILE_METAS = toCase(Names.PROFILE_METAS);
    static final String PROJECT = toCase(Names.PROJECT);
    static final String PROJECT_DESCRIPTION = toCase(Names.PROJECT_DESCRIPTION);
    static final String PROJECT_LABELS = toCase(Names.PROJECT_LABELS);
    static final String PROJECT_METAS = toCase(Names.PROJECT_METAS);
    static final String RESOLUTION = toCase(Names.RESOLUTION);
    static final String RULES = toCase(Names.RULES);
    static final String SEVERITY = toCase(Names.SEVERITY);
    static final String SNAPSHOT = toCase(Names.SNAPSHOT);
    static final String SNAPSHOT_DESCRIPTION = toCase(Names.SNAPSHOT_DESCRIPTION);
    static final String SNAPSHOT_METAS = toCase(Names.SNAPSHOT_METAS);
    static final String SNAPSHOT_TIMESTAMP = toCase(Names.SNAPSHOT_TIMESTAMP);
    static final String STATUS = toCase(Names.STATUS);
    static final String TAG = toCase(Names.TAG);
    static final String TEXT = toCase(Names.TEXT);
    static final String TIMESTAMP = toCase(Names.TIMESTAMP);
    static final String TITLE = toCase(Names.TITLE);
    static final String USER_LABELS = toCase(Names.USER_LABELS);
    static final String USER_METAS = toCase(Names.USER_METAS);

    private static final Header PROFILE_CONFIG_HEADER =
            Header.builder()
                  .name(DOMAIN)
                  .name(NAME)
                  .name(PARAMS)
                  .name(CUSTOMIZED_SEVERITY)
                  .name(ENABLED)
                  .build();

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    public static String format(NameValueMap map,
                                Comparator<? super String> comparator) {
        if (map.isEmpty()) {
            return "";
        } else {
            return map.getNames()
                      .stream()
                      .sorted(comparator)
                      .map(n -> n + ":" + validate(map.getValue(n)))
                      .collect(Collectors.joining("\n"));
        }
    }

    public static String format(NameValueMap map) {
        return format(map, Comparator.naturalOrder());
    }

    public static String format(Labels labels) {
        return labels.getSorted().stream().collect(Collectors.joining("\n"));
    }

    public static Params parseParams(String s) {
        // Note: if a value contains ':' after a NL, parsing will probably fail
        if (StringUtils.isNullOrEmpty(s)) {
            return Params.NO_PARAMS;
        } else {
            final Params.Builder params = Params.builder();
            final Matcher m = PARAM_PATTERN.matcher(s);
            String name = null;
            int valueStart = -1;
            while (m.find()) {
                final int valueEnd = m.start() - 1;
                if (valueStart >= 0) {
                    final String value = s.substring(valueStart, valueEnd);
                    params.param(name, value);
                }

                // Prepare next loop
                valueStart = m.end();
                name = m.group(1);
            }
            if (name != null) {
                final String value = s.substring(valueStart);
                params.param(name, value);
            }
            return params.build();
        }
    }

    public static void addInfoRow(WorkbookWriter<?> writer,
                                  String property,
                                  String comment,
                                  String value) throws IOException {
        writer.beginRow(TableSection.DATA);
        writer.addCellAndComment(property, comment);
        writer.addCell(value == null ? "" : value);
    }

    public static void writeProfileInfos(WorkbookWriter<?> writer,
                                         String sheetName,
                                         Profile profile) throws IOException {
        writer.beginSheet(sheetName);
        addInfoRow(writer,
                   WorkbooksIo.NAME,
                   "Profile name.",
                   profile == null ? "" : profile.getName());
        addInfoRow(writer,
                   WorkbooksIo.DESCRIPTION,
                   "Profile description.",
                   profile == null ? "" : profile.getDescription());
        addInfoRow(writer,
                   WorkbooksIo.METAS,
                   "Profile meta data.",
                   profile == null ? "" : WorkbooksIo.format(profile.getMetas()));
        addInfoRow(writer,
                   WorkbooksIo.LABELS,
                   "Profile labels.",
                   profile == null ? "" : WorkbooksIo.format(profile.getLabels()));
    }

    private static void writeProfileHeader(WorkbookWriter<?> writer,
                                           boolean full) throws IOException {
        writer.beginRow(TableSection.HEADER);
        writer.addCellAndComment(DOMAIN, "Rule domain.");
        writer.addCellAndComment(NAME, "Rule name.");
        if (full) {
            writer.addCellAndComment(TITLE, "Rule title.");
        }
        writer.addCellAndComment(PARAMS,
                                 """
                                 Rule effective parameters (User defined).
                                 List of <name>:<value> pairs, each on its line.
                                 Specific to each rule.""");
        if (full) {
            writer.addCellAndComment(METAS, "Rule meta data.");
            writer.addCellAndComment(LABELS, "Rule labels.");
            writer.addCellAndComment(DESCRIPTION, "Rule description.");
            writer.addCellAndComment(SEVERITY, "Rule default severity.");
        }
        writer.addCellAndComment(CUSTOMIZED_SEVERITY,
                                 "Optional customized severity of the rule (User defined)."
                                         + "\nOne of: "
                                         + List.of(IssueSeverity.values())
                                               .stream()
                                               .map(IssueSeverity::name)
                                               .collect(Collectors.joining(", "))
                                         + "\nCan be used if the default rule severity is not suitable.");
        writer.addCellAndComment(ENABLED,
                                 """
                                 Rule enabling (User defined).
                                 If missing, considered as true (enabled).""");
    }

    public static void writeProfileRules(WorkbookWriter<?> writer,
                                         String sheetName,
                                         Profile profile) throws IOException {
        writer.beginSheet(sheetName);
        writeProfileHeader(writer, true);

        if (profile != null) {
            for (final Rule rule : CollectionUtils.toSortedList(profile.getRules(), Rule.ID_COMPARATOR)) {
                writer.beginRow(TableSection.DATA);
                writer.addCell(rule.getDomain());
                writer.addCell(rule.getName());
                writer.addCell(rule.getTitle());
                writer.addCell(WorkbooksIo.format(profile.getParams(rule)));
                writer.addCell(WorkbooksIo.format(rule.getMetas()));
                writer.addCell(WorkbooksIo.format(rule.getLabels()));
                writer.addCell(rule.getDescription());
                writer.addCell(rule.getSeverity());
                if (profile.getCustomizedSeverity(rule).isPresent()) {
                    writer.addCell(profile.getCustomizedSeverity(rule).orElseThrow());
                } else {
                    writer.addEmptyCell();
                }
                writer.addCell(profile.isEnabled(rule));
            }
        }
    }

    public static void writeProfileConfig(WorkbookWriter<?> writer,
                                          String sheetName,
                                          ProfileConfig config) throws IOException {
        writer.beginSheet(sheetName);
        writeProfileHeader(writer, false);

        for (final RuleId ruleId : CollectionUtils.toSortedList(config.getRuleIds())) {
            final RuleConfig ruleConfig = config.getRuleConfig(ruleId);
            writer.beginRow(TableSection.DATA);
            writer.addCell(ruleId.getDomain());
            writer.addCell(ruleId.getName());
            writer.addCell(WorkbooksIo.format(ruleConfig.getParams()));
            if (ruleConfig.getCustomizedSeverity().isPresent()) {
                writer.addCell(ruleConfig.getCustomizedSeverity().orElseThrow());
            } else {
                writer.addEmptyCell();
            }
            writer.addCell(ruleConfig.isEnabled());
        }
    }

    public static ProfileConfig loadProfileConfig(File file,
                                                  String sheetName) throws IOException {
        final SheetLoader loader = new SheetLoader();
        final List<Row> rows;
        if (sheetName == null) {
            rows = loader.load(file, null, 0);
        } else {
            rows = loader.load(file, null, sheetName);
        }

        final ProfileConfigImpl config = new ProfileConfigImpl();
        if (!rows.isEmpty()) {
            final Header actual = Header.builder().names(rows.get(0)).build();
            final HeaderMapper mapper = HeaderMapper.builder()
                                                    .mandatory(PROFILE_CONFIG_HEADER)
                                                    .actual(actual)
                                                    .build();
            if (!mapper.hasAllMandatoryCells()) {
                throw new IOException("Missing keys: "
                        + mapper.getMissingMandatoryCells().stream().map(HeaderCell::toString).sorted()
                                .collect(Collectors.joining(",", "[", "]"))
                        + " in file header: " + actual);
            }
            rows.remove(0);

            for (final Row row : rows) {
                final String domain = row.getValue(actual.getMatchingIndex(DOMAIN));
                final String name = row.getValue(actual.getMatchingIndex(NAME));
                final Params params = parseParams(row.getValue(actual.getMatchingIndex(PARAMS), null));
                final IssueSeverity customizedSeverity =
                        row.getValueAsEnum(actual.getMatchingIndex(CUSTOMIZED_SEVERITY), IssueSeverity.class, null);
                final boolean enabled = row.getValueAsBoolean(actual.getMatchingIndex(ENABLED), true);

                final RuleId ruleId = new RuleId(domain, name);
                config.addIfMissing(ruleId);
                config.setCustomizedSeverity(ruleId, customizedSeverity);
                config.setEnabled(ruleId, enabled);
                config.setParams(ruleId, params);
            }
        }

        return config;
    }
}