package cdc.issues.answers;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.issues.Issue;
import cdc.issues.IssueId;

/**
 * Interface giving access to issues and answers.
 *
 * @author Damien Carbonne
 */
public interface IssuesAndAnswers {
    /**
     * @return The managed issues.
     */
    public List<Issue> getIssues();

    /**
     * @return The managed answers.
     */
    public Set<? extends IssueAnswer> getAnswers();

    /**
     * @param id The issue id.
     * @return The answer having an {@code id} or {@code null}.
     */
    public Optional<? extends IssueAnswer> getAnswer(IssueId id);
}