package cdc.issues;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * A collector of issues that can transmit collected issues to a delegate.
 *
 * @author Damien Carbonne
 */
public class IssuesCollector implements IssuesHandler {
    private final List<Issue> issues = new ArrayList<>();
    private final IssuesHandler delegate;

    public IssuesCollector(IssuesHandler delegate) {
        this.delegate = Checks.isNotNull(delegate, "delegate");
    }

    public IssuesCollector() {
        this(IssuesHandler.VOID);
    }

    /**
     * @return The delegate (possibly {@code null}).
     */
    public IssuesHandler getDelegate() {
        return delegate;
    }

    /**
     * Clears collected issues.
     */
    public void clear() {
        issues.clear();
    }

    /**
     * @return The list of collected issues.
     */
    public List<Issue> getIssues() {
        return issues;
    }

    /**
     * Returns a list of issues having a severity.
     *
     * @param severity The severity.
     * @return A list of issues that have a certain {@code severity}.
     */
    public List<Issue> getIssues(IssueSeverity severity) {
        return issues.stream()
                     .filter(i -> i.getSeverity() == severity)
                     .toList();
    }

    @Override
    public void issue(Issue issue) {
        issues.add(issue);
        delegate.issue(issue);
    }
}