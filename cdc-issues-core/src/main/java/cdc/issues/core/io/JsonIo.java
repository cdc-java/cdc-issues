package cdc.issues.core.io;

import javax.json.stream.JsonGenerator;

import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.rules.Profile;
import cdc.issues.rules.Rule;
import cdc.util.lang.CollectionUtils;
import cdc.util.strings.StringUtils;

final class JsonIo {
    private JsonIo() {
    }

    private static String toCase(String s) {
        return Names.JSON_CASE_CONVERTER.splitAndConvert(s);
    }

    static final String ANCHOR = toCase(Names.ANCHOR);
    static final String ANSWER = toCase(Names.ANSWER);
    static final String ASSIGNEE = toCase(Names.ASSIGNEE);
    static final String AUTHOR = toCase(Names.AUTHOR);
    static final String COMMENTS = toCase(Names.COMMENTS);
    static final String CREATED = toCase(Names.CREATED);
    static final String CUSTOMIZED_SEVERITY = toCase(Names.CUSTOMIZED_SEVERITY);
    static final String DATE = toCase(Names.DATE);
    static final String DESCRIPTION = toCase(Names.DESCRIPTION);
    static final String DOMAIN = toCase(Names.DOMAIN);
    static final String ENABLED = toCase(Names.ENABLED);
    static final String ID_HASH = toCase(Names.ID_HASH);
    static final String INFOS = toCase(Names.INFOS);
    static final String ISSUES = toCase(Names.ISSUES);
    static final String ISSUES_HASH = toCase(Names.ISSUES_HASH);
    static final String LABELS = toCase(Names.LABELS);
    static final String LOCATIONS = toCase(Names.LOCATIONS);
    static final String METAS = toCase(Names.METAS);
    static final String MODIFIED = toCase(Names.MODIFIED);
    static final String NAME = toCase(Names.NAME);
    static final String NEW_SEVERITY = toCase(Names.NEW_SEVERITY);
    static final String PARAMS = toCase(Names.PARAMS);
    static final String PATH = toCase(Names.PATH);
    static final String PROFILE = toCase(Names.PROFILE);
    static final String PROJECT = toCase(Names.PROJECT);
    static final String RESOLUTION = toCase(Names.RESOLUTION);
    static final String RULES = toCase(Names.RULES);
    static final String SEVERITY = toCase(Names.SEVERITY);
    static final String SNAPSHOT = toCase(Names.SNAPSHOT);
    static final String STATUS = toCase(Names.STATUS);
    static final String TAG = toCase(Names.TAG);
    static final String TEXT = toCase(Names.TEXT);
    static final String TIMESTAMP = toCase(Names.TIMESTAMP);
    static final String TITLE = toCase(Names.TITLE);
    static final String VALUE = toCase(Names.VALUE);

    private static void writeNameValueMap(JsonGenerator generator,
                                          NameValueMap map,
                                          String title) {
        if (!map.isEmpty()) {
            generator.writeStartArray(title);
            for (final String name : map.getSortedNames()) {
                generator.writeStartObject();
                generator.write(NAME, name);
                generator.write(VALUE, validate(map.getValue(name)));
                generator.writeEnd();
            }
            generator.writeEnd();
        }
    }

    static void writeMetas(JsonGenerator generator,
                           Metas metas) {
        writeNameValueMap(generator, metas, METAS);
    }

    static void writeParams(JsonGenerator generator,
                            Params params) {
        writeNameValueMap(generator, params, PARAMS);
    }

    static void writeLabels(JsonGenerator generator,
                            Labels labels) {
        if (!labels.isEmpty()) {
            generator.writeStartArray(LABELS);
            for (final String label : labels.getSorted()) {
                generator.write(label);
            }
            generator.writeEnd();
        }
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    static void writeProfile(JsonGenerator generator,
                             Profile profile) {
        generator.writeStartObject(PROFILE);
        if (profile != null) {
            generator.write(NAME, validate(profile.getName()));
            generator.write(DESCRIPTION, validate(profile.getDescription()));
            writeMetas(generator, profile.getMetas());
            writeLabels(generator, profile.getLabels());
            generator.writeStartArray(RULES);
            for (final Rule rule : CollectionUtils.toSortedList(profile.getRules(), Rule.ID_COMPARATOR)) {
                generator.writeStartObject(); // RULE
                generator.write(DOMAIN, validate(rule.getDomain()));
                generator.write(NAME, validate(rule.getName()));
                if (!StringUtils.isNullOrEmpty(rule.getTitle())) {
                    generator.write(TITLE, validate(rule.getTitle()));
                }
                generator.write(SEVERITY, rule.getSeverity().name());
                if (profile.getCustomizedSeverity(rule).isPresent()) {
                    generator.write(CUSTOMIZED_SEVERITY, profile.getCustomizedSeverity(rule).orElseThrow().name());
                }
                writeMetas(generator, rule.getMetas());
                writeLabels(generator, rule.getLabels());
                writeParams(generator, profile.getParams(rule));
                generator.write(DESCRIPTION, validate(rule.getDescription()));
                generator.write(ENABLED, profile.isEnabled(rule));
                generator.writeEnd(); // RULE
            }
            generator.writeEnd(); // RULES
        }
        generator.writeEnd(); // PROFILE
    }
}