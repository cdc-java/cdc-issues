package cdc.issues.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Params;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.issues.impl.IssueAnswerImpl;
import cdc.issues.impl.IssueCommentImpl;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.impl.ProjectImpl;
import cdc.issues.impl.SnapshotImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesReader;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.locations.DefaultLocation;
import cdc.issues.rules.Rule;
import cdc.util.events.ProgressController;
import cdc.util.lang.UnexpectedValueException;

class IssuesIoTest {
    private static final Logger LOGGER = LogManager.getLogger(IssuesIoTest.class);
    private static final String DOMAIN = "Domain1";
    private static final String PROFILE = "Profile1";
    private static final String PROJECT = "Project1";
    private static final String SNAPSHOT = "Snapshot1";
    private static final String RULE1 = "Rule1";
    private static final String RULE2 = "Rule2";

    private enum Mode {
        ANSWERS,
        NO_ANSWERS,
        SPECIAL,
        SPECIAL_AUTO;

        public String code() {
            return name().toLowerCase().replace('_', '-');
        }
    }

    private static IssuesAndAnswersImpl build(Mode mode,
                                              int count) {
        final IssuesAndAnswersImpl data = new IssuesAndAnswersImpl();
        final IssueResolution[] resolutions = IssueResolution.values();
        final IssueStatus[] statuses = IssueStatus.values();
        for (int index = 0; index < count; index++) {
            final Metas metas;
            final Labels labels;
            if (index % 2 == 0) {
                metas = Metas.NO_METAS;
                labels = Labels.NO_LABELS;
            } else {
                metas = Metas.builder()
                             .meta("meta1", "value1")
                             .meta("meta2", "value2")
                             .meta("meta3", null)
                             .build();
                labels = Labels.of("Label1", "Label2");
            }

            final Params params;
            if (index % 2 == 0) {
                params = Params.NO_PARAMS;
            } else {
                params = Params.builder()
                               .param("param1", "value1")
                               .param("param2", "value2")
                               .param("param3", null)
                               .build();
            }

            final Issue issue =
                    Issue.builder()
                         .domain(DOMAIN)
                         .name(index % 2 == 0 ? RULE1 : RULE2)
                         .params(params)
                         .snapshot(index % 2 == 0 ? null : SNAPSHOT)
                         .project(index % 2 == 0 ? null : PROJECT)
                         .severity(IssueSeverity.INFO)
                         .description("Issue Description")
                         .addLocation(new DefaultLocation("Target " + index, "Path 1"))
                         .addLocation(new DefaultLocation("Target " + index))
                         .metas(metas)
                         .labels(labels)
                         .build();
            data.addIssue(issue);
            if (mode == Mode.ANSWERS) {
                final IssueAnswerImpl.Builder answer =
                        IssueAnswerImpl.builder()
                                       .issueId(issue.getId())
                                       .assignee("Joe")
                                       .author("Jack");
                final int resolutionIndex = index % (resolutions.length + 1);
                answer.resolution(resolutionIndex == resolutions.length
                        ? IssueResolution.UNRESOLVED
                        : resolutions[resolutionIndex]);
                final int statusIndex = index % (statuses.length + 1);
                answer.status(statusIndex == statuses.length
                        ? IssueStatus.OPEN
                        : statuses[statusIndex]);
                if (index % 2 == 0) {
                    answer.newSeverity(IssueSeverity.MINOR);
                }

                if (index % 2 == 0) {
                    answer.comment(IssueCommentImpl.builder().author("Jude").text("A comment").build())
                          .comment(IssueCommentImpl.builder().author("Jack").text("Another comment").build());
                    answer.metas(Metas.builder().meta("p1", "v1").meta("p2", "v2").build());
                    answer.labels(Labels.of("Label1", "Label2"));
                }

                data.addAnswer(answer.build());
            }
        }
        return data;
    }

    private static void test(String ext,
                             Mode mode,
                             int count) throws IOException {
        final String as = mode.code();
        final File file = new File("target/issues-io-test-" + as
                + "-" + count
                + "." + ext);
        final IssuesFormat format = IssuesFormat.from(file);
        final IssuesAndAnswersImpl data1 = build(mode, count);

        final OutSettings settings;
        switch (mode) {
        case ANSWERS:
            settings = OutSettings.builder()
                                  .hint(OutSettings.Hint.AUTO_METAS)
                                  .hint(OutSettings.Hint.AUTO_PARAMS)
                                  .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                  .hint(OutSettings.Hint.AUTO_COMMENTS)
                                  .hint(OutSettings.Hint.AUTO_USER_METAS)
                                  .build();
            break;
        case NO_ANSWERS:
            settings = OutSettings.builder()
                                  .hint(OutSettings.Hint.NO_ANSWERS)
                                  .hint(OutSettings.Hint.AUTO_METAS)
                                  .hint(OutSettings.Hint.AUTO_PARAMS)
                                  .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                  .build();
            break;
        case SPECIAL:
            settings = OutSettings.builder()
                                  .hint(OutSettings.Hint.NO_DOMAIN_COL)
                                  .hint(OutSettings.Hint.NO_METAS_COL)
                                  .hint(OutSettings.Hint.NO_LABELS_COL)
                                  .hint(OutSettings.Hint.NO_PROJECT_COL)
                                  .hint(OutSettings.Hint.NO_SNAPSHOT_COL)
                                  .hint(OutSettings.Hint.NO_LOCATIONS_COL)
                                  .hint(OutSettings.Hint.NO_ANSWERS)
                                  .meta("meta1")
                                  .meta("meta2")
                                  .meta("meta3")
                                  .userMeta("user-meta1")
                                  .build();
            break;
        case SPECIAL_AUTO:
            settings = OutSettings.builder()
                                  .hint(OutSettings.Hint.NO_DOMAIN_COL)
                                  .hint(OutSettings.Hint.NO_METAS_COL)
                                  .hint(OutSettings.Hint.NO_LABELS_COL)
                                  .hint(OutSettings.Hint.NO_PROJECT_COL)
                                  .hint(OutSettings.Hint.NO_SNAPSHOT_COL)
                                  .hint(OutSettings.Hint.NO_PARAMS_COL)
                                  .hint(OutSettings.Hint.NO_LOCATIONS_COL)
                                  .hint(OutSettings.Hint.NO_ANSWERS)
                                  .hint(OutSettings.Hint.AUTO_METAS)
                                  .hint(OutSettings.Hint.AUTO_PARAMS)
                                  .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                  .build();
            break;
        default:
            throw new UnexpectedValueException(mode);
        }

        LOGGER.info("save {}", file);
        IssuesWriter.save(data1,
                          settings,
                          file,
                          ProgressController.VOID,
                          data1.getIssues().size() < 100
                                  ? IssuesIoFactoryFeatures.UTC_BEST
                                  : IssuesIoFactoryFeatures.UTC_FASTEST);

        // try (OutputStream out = new BufferedOutputStream(new FileOutputStream(fileos))) {
        // IssuesWriter.save(data1,
        // settings,
        // out,
        // format,
        // ProgressController.VOID,
        // data1.getIssues().size() < 100
        // ? IssuesIoFactoryFeatures.UTC_BEST
        // : IssuesIoFactoryFeatures.UTC_FASTEST);
        // }

        // Reload saved data and save it again for comparison
        if ((mode == Mode.ANSWERS || mode == Mode.NO_ANSWERS) && format.isImportFormat()) {
            final IssuesAndAnswers data2 =
                    IssuesReader.load(file,
                                      IssuesReader.ANSWERS,
                                      ProgressController.VOID,
                                      IssuesIoFactoryFeatures.UTC_FASTEST);
            final File file2 = new File("target/issues-io-test-" + as
                    + "-" + count
                    + "-2." + format.name().toLowerCase());
            LOGGER.info("save {}", file2);
            IssuesWriter.save(data2,
                              settings,
                              file2,
                              ProgressController.VOID,
                              IssuesIoFactoryFeatures.UTC_FASTEST);
            assertEquals(data1.getIssues().size(), data2.getIssues().size(), "Number of issues");
            assertEquals(data1.getAnswers().size(), data2.getAnswers().size(), "Number of answers");
            for (int index = 0; index < data1.getIssues().size(); index++) {
                final Issue issue1 = data1.getIssues().get(index);
                final Issue issue2 = data2.getIssues().get(index);
                assertEquals(issue1, issue2, "issue " + index);
                final Optional<IssueAnswerImpl> answer1 = data1.getAnswer(issue1.getId());
                final Optional<? extends IssueAnswer> answer2 = data2.getAnswer(issue2.getId());
                assertEquals(answer1, answer2, "answer " + index);
            }

            assertEquals(data1.getIssues(), data2.getIssues(), "Issues");
            assertEquals(data1.getAnswers(), data2.getAnswers(), "Answers");
        }
    }

    private static void testFull(String ext,
                                 int count) throws IOException {
        final File file = new File("target/issues-io-test-full-" + count
                + "." + ext);
        final IssuesFormat format = IssuesFormat.from(file);

        final IssuesAndAnswersImpl data1 = build(Mode.ANSWERS, count);
        final ProjectImpl project =
                new ProjectImpl(PROJECT).setDescription("Project description")
                                        .setMetas(Metas.builder()
                                                       .meta("meta1", "value1")
                                                       .meta("meta2", "value2")
                                                       .build());
        final SnapshotImpl snapshot =
                project.createSnapshot().setName(SNAPSHOT)
                       .setDescription("Snapshot description")
                       .setMetas(Metas.builder()
                                      .meta("meta1", "value1")
                                      .meta("meta2", "value2")
                                      .build())
                       .setLabels(Labels.of("Label1"));
        final ProfileImpl profile = new ProfileImpl(PROFILE).setDescription("Profile description");
        final Rule rule1 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE1)
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description of Rule 1")
                               .build();
        final Rule rule2 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE2)
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description of Rule 2")
                               .build();
        profile.add(rule1);
        profile.add(rule2);
        profile.setParams(rule2,
                          Params.builder()
                                .param("param1", "value1")
                                .param("param2", "value2")
                                .build());
        profile.setEnabled(rule2, true);
        profile.setMetas(Metas.builder()
                              .meta("meta1", "value1")
                              .meta("meta2", "value2")
                              .build());
        profile.setLabels(Labels.of("Label1"));
        snapshot.addIssues(data1.getIssues());
        project.addAnswers(data1.getAnswers());
        project.setProfile(profile);

        final OutSettings settings = OutSettings.ALL_DATA_ANSWERS;

        LOGGER.info("save {}", file);
        IssuesWriter.save(snapshot,
                          settings,
                          file,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_FASTEST);

        // Check
        if (format != IssuesFormat.CSV) {
            LOGGER.info("load {}", file);
            final IssuesAndAnswers data2 =
                    IssuesReader.load(file,
                                      IssuesReader.ANSWERS,
                                      ProgressController.VOID,
                                      IssuesIoFactoryFeatures.UTC_FASTEST);

            assertEquals(data1.getIssues().size(), data2.getIssues().size());
        }
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xlsx",
            "xls",
            "csv"
    })
    void testLarge(String ext) throws IOException {
        // For XLSX, it seems something went wrong:
        // DocumentSummaryInformation property set came back as null
        // SummaryInformation property set came back as null

        test(ext, Mode.NO_ANSWERS, 100_000);
        test(ext, Mode.ANSWERS, 100_000);
        test(ext, Mode.SPECIAL, 100_000);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testSpecial0(String ext) throws IOException {
        test(ext, Mode.SPECIAL, 0);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testSpecial10(String ext) throws IOException {
        test(ext, Mode.SPECIAL, 10);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testAnswers0(String ext) throws IOException {
        test(ext, Mode.ANSWERS, 0);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testAnswers10(String ext) throws IOException {
        test(ext, Mode.ANSWERS, 10);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testNoAnswers0(String ext) throws IOException {
        test(ext, Mode.NO_ANSWERS, 0);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testNoAnswers10(String ext) throws IOException {
        test(ext, Mode.NO_ANSWERS, 10);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "json"
    })
    void testFull(String ext) throws IOException {
        testFull(ext, 100);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xlsx"
    })
    void testSpecialAuto0(String ext) throws IOException {
        test(ext, Mode.SPECIAL_AUTO, 0);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xlsx"
    })
    void testSpecialAuto10(String ext) throws IOException {
        test(ext, Mode.SPECIAL_AUTO, 10);
    }
}