package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;

import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;

class WorkbookProfileIo {
    private final ProfileIoFeatures features;

    WorkbookProfileIo(ProfileIoFeatures features) {
        this.features = features;
    }

    public void save(Profile profile,
                     File file) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);
        try (final WorkbookWriter<?> writer = factory.create(file, features.getWorkbookWriterFeatures())) {
            WorkbooksIo.writeProfileInfos(writer,
                                          WorkbooksIo.PROFILE,
                                          profile);
            WorkbooksIo.writeProfileRules(writer,
                                          WorkbooksIo.RULES,
                                          profile);
        }
    }

    public void save(ProfileConfig config,
                     File file) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);
        try (final WorkbookWriter<?> writer = factory.create(file, features.getWorkbookWriterFeatures())) {
            WorkbooksIo.writeProfileConfig(writer,
                                           WorkbooksIo.PROFILE_CONFIGURATION,
                                           config);
        }
    }

    public ProfileConfig loadProfileConfig(File file) throws IOException {
        return WorkbooksIo.loadProfileConfig(file,
                                             WorkbooksIo.PROFILE_CONFIGURATION);
    }
}