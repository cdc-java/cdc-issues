package cdc.issues.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.issues.locations.LocatedObject;
import cdc.util.lang.Checks;

/**
 * Calling context passed to checkers.
 */
public final class CheckContext {
    private final SnapshotManager manager;
    /** The stack of checked located objects. */
    private final List<LocatedObject<?>> stack;

    private CheckContext(SnapshotManager manager,
                         List<LocatedObject<?>> elements) {
        this.manager = Checks.isNotNull(manager, "manager");
        this.stack = elements;
    }

    private CheckContext(SnapshotManager manager) {
        this(manager,
             Collections.emptyList());
    }

    public SnapshotManager getManager() {
        return manager;
    }

    /**
     * @param <M> The snapshot manager type.
     * @param managerClass The snapshot manager class.
     * @return The {@link SnapshotManager} as an instance of {@code snapshotManagerClass}.
     */
    public <M extends SnapshotManager> M getManager(Class<M> managerClass) {
        return managerClass.cast(manager);
    }

    /**
     * @return The stack of checked located objects.
     */
    public List<LocatedObject<?>> getStack() {
        return stack;
    }

    /**
     * @return The stack size.
     */
    public final int size() {
        return getStack().size();
    }

    /**
     * Returns the located element at an index.
     *
     * @param <O> The object type.
     * @param objectClass The object class.
     * @param index The index of the searched element.
     * @return The element at index.
     */
    public final <O> LocatedObject<? extends O> get(Class<O> objectClass,
                                                    int index) {
        return getStack().get(index).cast(objectClass);
    }

    /**
     * Creates a new context by appending an element to stack.
     *
     * @param <O> The object type.
     * @param element The element to push.
     * @return A new CheckContext with element pushed at end.
     */
    public <O> CheckContext push(LocatedObject<? extends O> element) {
        Checks.isNotNull(element, "element");

        final List<LocatedObject<?>> list = new ArrayList<>(this.stack);
        list.add(element);
        return new CheckContext(manager, list);
    }

    public static CheckContext init(SnapshotManager manager) {
        return new CheckContext(manager);
    }

    @Override
    public String toString() {
        return getStack().toString();
    }
}