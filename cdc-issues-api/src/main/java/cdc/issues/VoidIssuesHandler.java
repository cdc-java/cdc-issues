package cdc.issues;

/**
 * Implementation of {@link IssuesHandler} that does nothing.
 *
 * @author Damien Carbonne
 */
public class VoidIssuesHandler implements IssuesHandler {
    public static final VoidIssuesHandler INSTANCE = new VoidIssuesHandler();

    protected VoidIssuesHandler() {
        // Ignore
    }

    @Override
    public void issue(Issue issue) {
        // Ignore
    }

    @Override
    public void issues(Iterable<Issue> issues) {
        // Ignore
    }

    @Override
    public void issues(Issue... issues) {
        // Ignore
    }
}