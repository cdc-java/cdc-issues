package cdc.issues.core.io;

import java.io.IOException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleConfig;
import cdc.issues.rules.RuleId;
import cdc.util.lang.CollectionUtils;
import cdc.util.strings.StringUtils;

final class XmlIo {
    private XmlIo() {
    }

    private static String toCase(String s) {
        return Names.XML_CASE_CONVERTER.splitAndConvert(s);
    }

    static final String ANCHOR = toCase(Names.ANCHOR);
    static final String ANSWER = toCase(Names.ANSWER);
    static final String ASSIGNEE = toCase(Names.ASSIGNEE);
    static final String AUTHOR = toCase(Names.AUTHOR);
    static final String COMMENT = toCase(Names.COMMENT);
    static final String COMMENTS = toCase(Names.COMMENTS);
    static final String CREATED = toCase(Names.CREATED);
    static final String CUSTOMIZED_SEVERITY = toCase(Names.CUSTOMIZED_SEVERITY);
    static final String DATE = toCase(Names.DATE);
    static final String DESCRIPTION = toCase(Names.DESCRIPTION);
    static final String DOMAIN = toCase(Names.DOMAIN);
    static final String ENABLED = toCase(Names.ENABLED);
    static final String ID_HASH = toCase(Names.ID_HASH);
    static final String INFOS = toCase(Names.INFOS);
    static final String ISSUE = toCase(Names.ISSUE);
    static final String ISSUES = toCase(Names.ISSUES);
    static final String ISSUES_HASH = toCase(Names.ISSUES_HASH);
    static final String LABELS = toCase(Names.LABELS);
    static final String LOCATION = toCase(Names.LOCATION);
    static final String LOCATIONS = toCase(Names.LOCATIONS);
    static final String META = toCase(Names.META);
    static final String METAS = toCase(Names.METAS);
    static final String MODIFIED = toCase(Names.MODIFIED);
    static final String NAME = toCase(Names.NAME);
    static final String NEW_SEVERITY = toCase(Names.NEW_SEVERITY);
    static final String PARAM = toCase(Names.PARAM);
    static final String PARAMS = toCase(Names.PARAMS);
    static final String PATH = toCase(Names.PATH);
    static final String PROFILE = toCase(Names.PROFILE);
    static final String PROFILE_CONFIGURATION = toCase(Names.PROFILE_CONFIGURATION);
    static final String PROJECT = toCase(Names.PROJECT);
    static final String PROJECT_SNAPSHOT = toCase(Names.PROJECT_SNAPSHOT);
    static final String RESOLUTION = toCase(Names.RESOLUTION);
    static final String RULE = toCase(Names.RULE);
    static final String RULES = toCase(Names.RULES);
    static final String SEVERITY = toCase(Names.SEVERITY);
    static final String SNAPSHOT = toCase(Names.SNAPSHOT);
    static final String STATUS = toCase(Names.STATUS);
    static final String TAG = toCase(Names.TAG);
    static final String TEXT = toCase(Names.TEXT);
    static final String TIMESTAMP = toCase(Names.TIMESTAMP);
    static final String TITLE = toCase(Names.TITLE);
    static final String VALUE = toCase(Names.VALUE);

    static void writeMetas(XmlWriter writer,
                           Metas metas) throws IOException {
        writeNameValueMap(writer, metas, METAS, META);
    }

    static void writeParams(XmlWriter writer,
                            Params params) throws IOException {
        writeNameValueMap(writer, params, PARAMS, PARAM);
    }

    private static void writeNameValueMap(XmlWriter writer,
                                          NameValueMap map,
                                          String plural,
                                          String singular) throws IOException {
        if (!map.isEmpty()) {
            writer.beginElement(plural);
            for (final String name : map.getSortedNames()) {
                writer.beginElement(singular);
                writer.addAttribute(NAME, name);
                writer.addAttribute(VALUE, map.getValue(name));
                writer.endElement(); // singular
            }
            writer.endElement(); // plural
        }
    }

    static void writeLabels(XmlWriter writer,
                            Labels labels) throws IOException {
        if (!labels.isEmpty()) {
            writer.beginElement(LABELS);
            final String content = labels.getSorted()
                                         .stream()
                                         .collect(Collectors.joining(" "));
            writer.addElementContent(content);
            writer.endElement();
        }
    }

    private static <M extends NameValueMap, B extends NameValueMap.Builder<M, B>>
            M parseNameValueMap(AbstractStAXParser<?> parser,
                                Supplier<B> supplier,
                                String singular,
                                String plural) throws XMLStreamException {
        final String ctx = "parseNameValueMap()";
        parser.expectStartElement(ctx, plural);

        final B builder = supplier.get();
        parser.nextTag();
        while (parser.isStartElement(singular)) {
            final String name = parser.getAttributeValue(NAME);
            final String value = StringUtils.nullify(parser.getAttributeValue(VALUE));
            builder.entry(name, value);
            parser.nextTag();
            parser.expectEndElement(ctx, singular);
            parser.nextTag();
        }
        parser.expectEndElement(ctx, plural);
        return builder.build();
    }

    static Metas parseMetas(AbstractStAXParser<?> parser) throws XMLStreamException {
        return parseNameValueMap(parser, Metas::builder, META, METAS);
    }

    static Params parseParams(AbstractStAXParser<?> parser) throws XMLStreamException {
        return parseNameValueMap(parser, Params::builder, PARAM, PARAMS);
    }

    static Metas parseOptMetas(AbstractStAXParser<?> parser) throws XMLStreamException {
        final Metas metas;
        if (parser.isStartElement(METAS)) {
            metas = parseMetas(parser);
            parser.nextTag();
        } else {
            metas = Metas.NO_METAS;
        }
        return metas;
    }

    static Params parseOptParams(AbstractStAXParser<?> parser) throws XMLStreamException {
        final Params params;
        if (parser.isStartElement(PARAMS)) {
            params = parseParams(parser);
            parser.nextTag();
        } else {
            params = Params.NO_PARAMS;
        }
        return params;
    }

    static Labels parseLabels(AbstractStAXParser<?> parser) throws XMLStreamException {
        final String ctx = "parseLabels()";
        parser.expectStartElement(ctx, LABELS);
        // Warning, calling getElementText() moved on end tag
        final String content = parser.getReader().getElementText();
        final String[] words = content.split(" ");
        final Labels labels = Labels.of(words);
        // No need to call nextTag()
        parser.expectEndElement(ctx, LABELS);
        return labels;
    }

    static Labels parseOptLabels(AbstractStAXParser<?> parser) throws XMLStreamException {
        final Labels labels;
        if (parser.isStartElement(LABELS)) {
            labels = parseLabels(parser);
            parser.nextTag();
        } else {
            labels = Labels.NO_LABELS;
        }
        return labels;
    }

    static void writeProfile(XmlWriter writer,
                             Profile profile) throws IOException {
        writer.beginElement(PROFILE);
        if (profile != null) {
            writer.addAttribute(NAME, profile.getName());
            writeMetas(writer, profile.getMetas());
            writeLabels(writer, profile.getLabels());
            writer.addElement(DESCRIPTION, profile.getDescription());
            writer.beginElement(RULES);
            for (final Rule rule : CollectionUtils.toSortedList(profile.getRules(), Rule.ID_COMPARATOR)) {
                writer.beginElement(RULE);
                writer.addAttribute(DOMAIN, rule.getDomain());
                writer.addAttribute(NAME, rule.getName());
                if (!StringUtils.isNullOrEmpty(rule.getTitle())) {
                    writer.addAttribute(TITLE, rule.getTitle());
                }
                writer.addAttribute(SEVERITY, rule.getSeverity());
                if (profile.getCustomizedSeverity(rule).isPresent()) {
                    writer.addAttribute(CUSTOMIZED_SEVERITY, profile.getCustomizedSeverity(rule).orElseThrow());
                }
                writer.addAttribute(ENABLED, profile.isEnabled(rule));
                writeParams(writer, profile.getParams(rule));
                writeMetas(writer, rule.getMetas());
                writeLabels(writer, rule.getLabels());
                writer.addElement(DESCRIPTION, rule.getDescription());
                writer.endElement(); // RULE
            }
            writer.endElement(); // RULES
        }
        writer.endElement(); // PROFILE
    }

    static void writeProfileConfig(XmlWriter writer,
                                   ProfileConfig config) throws IOException {
        writer.beginElement(PROFILE_CONFIGURATION);
        for (final RuleId ruleId : CollectionUtils.toSortedList(config.getRuleIds())) {
            final RuleConfig ruleConfig = config.getRuleConfig(ruleId);
            writer.beginElement(RULE);
            writer.addAttribute(DOMAIN, ruleId.getDomain());
            writer.addAttribute(NAME, ruleId.getName());
            if (ruleConfig.getCustomizedSeverity().isPresent()) {
                writer.addAttribute(CUSTOMIZED_SEVERITY, ruleConfig.getCustomizedSeverity().orElseThrow());
            }
            writer.addAttribute(ENABLED, ruleConfig.isEnabled());
            writeParams(writer, ruleConfig.getParams());
            writer.endElement(); // RULE
        }
        writer.endElement(); // RULES
    }

    static Profile parseProfile(AbstractStAXParser<?> parser) throws XMLStreamException {
        final XMLStreamReader reader = parser.getReader();
        final String ctx = "parseProfile()";
        parser.expectStartElement(ctx, XmlIo.PROFILE);

        final String name = parser.getAttributeValue(XmlIo.NAME);

        final ProfileImpl profile = new ProfileImpl(name);

        parser.nextTag();
        while (reader.isStartElement()) {
            if (parser.isStartElement(XmlIo.DESCRIPTION)) {
                final String description = reader.getElementText();
                profile.setDescription(description);
                parser.expectEndElement(ctx, XmlIo.DESCRIPTION);
            } else if (parser.isStartElement(XmlIo.METAS)) {
                final Metas metas = parseMetas(parser);
                profile.setMetas(metas);
            } else if (parser.isStartElement(XmlIo.LABELS)) {
                final Labels labels = parseLabels(parser);
                profile.setLabels(labels);
            } else if (parser.isStartElement(XmlIo.RULES)) {
                parseRules(parser, profile);
                parser.expectEndElement(ctx, XmlIo.RULES);
            } else {
                throw parser.unexpectedEvent();
            }
            parser.nextTag();
        }
        parser.expectEndElement(ctx, XmlIo.PROFILE);

        return profile;
    }

    static void parseRules(AbstractStAXParser<?> parser,
                           ProfileImpl profile) throws XMLStreamException {
        final String ctx = "parseRules()";
        parser.expectStartElement(ctx, XmlIo.RULES);
        parser.nextTag();
        while (parser.isStartElement(XmlIo.RULE)) {
            final String domain = parser.getAttributeValue(XmlIo.DOMAIN);
            final String name = parser.getAttributeValue(XmlIo.NAME);
            final String title = parser.getAttributeValue(XmlIo.TITLE, null);
            final IssueSeverity severity = parser.getAttributeAsEnum(XmlIo.SEVERITY, IssueSeverity.class);
            final IssueSeverity customizedSeverity =
                    parser.getAttributeAsEnum(XmlIo.CUSTOMIZED_SEVERITY, IssueSeverity.class, null);
            final boolean enabled = parser.getAttributeAsBoolean(XmlIo.ENABLED, true);
            parser.nextTag();
            final Params params = parseOptParams(parser);
            final Metas metas = parseOptMetas(parser);
            final Labels labels = parseOptLabels(parser);
            parser.expectStartElement(ctx, XmlIo.DESCRIPTION);
            final String description = parser.getReader().getElementText();
            parser.expectEndElement(ctx, XmlIo.DESCRIPTION);
            parser.nextTag();
            parser.expectEndElement(ctx, XmlIo.RULE);
            final Rule rule =
                    Rule.builder()
                        .domain(domain)
                        .name(name)
                        .title(title)
                        .description(description)
                        .severity(severity)
                        .metas(metas)
                        .labels(labels)
                        .build();
            profile.add(rule);
            profile.setEnabled(rule, enabled);
            profile.setCustomizedSeverity(rule, customizedSeverity);
            profile.setParams(rule, params);
            parser.nextTag();
        }
        parser.expectEndElement(ctx, XmlIo.RULES);
    }
}