package cdc.issues;

import java.util.Collection;

/**
 * Interface implemented by builders that have {@link Labels}.
 *
 * @param <B> The builder class.
 */
public interface LabelsBuilding<B> {
    /**
     * Sets the labels.
     *
     * @param labels The labels.
     * @return This builder.
     */
    public B labels(Labels labels);

    /**
     * Sets the labels.
     *
     * @param labels The labels.
     * @return This builder.
     */
    public default B labels(String... labels) {
        return labels(Labels.of(labels));
    }

    /**
     * Sets the labels.
     *
     * @param labels The labels.
     * @return This builder.
     */
    public default B labels(Collection<String> labels) {
        return labels(Labels.of(labels));
    }
}