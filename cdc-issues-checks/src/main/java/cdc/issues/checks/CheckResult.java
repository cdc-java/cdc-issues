package cdc.issues.checks;

/**
 * Enumeration of possible test results.
 *
 * @author Damien Carbonne
 */
public enum CheckResult {
    /**
     * The rule is disabled.
     */
    DISABLED,

    /**
     * The rule is enabled, applicable to object, passed on object, successful.
     */
    SUCCESS,

    /**
     * The rule is enabled, applicable to object, passed on object, failed.
     */
    FAILURE,

    /**
     * The rule is enabled, applicable to object, passed on object and the checker raised an exception.
     * There is a bug in checker implementation.
     */
    EXCEPTION,

    /**
     * The rule is enabled, applicable to object, but could not be passed because it is misconfigured.
     */
    MISCONFIGURED,

    /**
     * The rule is enabled, not applicable to object, skipped.
     */
    SKIPPED,

    /**
     * There is no associated rule.
     * <p>
     * This should be used with checkers that are not specializations of {@link AbstractRuleChecker}.
     */
    IGNORE;

    /**
     * @return An array of values that are related to rules.
     */
    public static CheckResult[] getRuleValues() {
        return new CheckResult[] { DISABLED, SUCCESS, FAILURE, EXCEPTION, MISCONFIGURED, SKIPPED };
    }

    /**
     * @return {@code true} when this result is related to a rule.
     */
    public boolean isRuleResult() {
        return this != IGNORE;
    }
}