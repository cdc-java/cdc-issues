package cdc.issues;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Interface describing a snapshot.
 *
 * @author Damien Carbonne
 */
public interface Snapshot {

    /**
     * @return The project to which this snapshot belongs.
     */
    public Optional<Project> getProject();

    /**
     * @return The timestamp of this snapshot.
     */
    public Instant getTimestamp();

    /**
     * @return The name of this snapshot.
     */
    public String getName();

    /**
     * @return The description of this snapshot.
     */
    public String getDescription();

    /**
     * @return The meta data associated to this snapshot.
     */
    public Metas getMetas();

    /**
     * @return The labels associated to this snapshot.
     */
    public Labels getLabels();

    /**
     * @return The issues of this snapshot.
     */
    public List<Issue> getIssues();

    /**
     * @return The issues hash.
     */
    public default String getIssuesHash() {
        return IssueUtils.getHash(getIssues());
    }
}