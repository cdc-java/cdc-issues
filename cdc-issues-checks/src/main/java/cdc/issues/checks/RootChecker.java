package cdc.issues.checks;

import java.io.PrintStream;

import cdc.issues.locations.LocatedObject;
import cdc.util.debug.Printables;

public class RootChecker<O> extends CompositeChecker<O> {
    private final LocatedObject<? extends O> root;

    public RootChecker(Class<O> objectClass,
                       LocatedObject<? extends O> root) {
        super(objectClass);
        this.root = root;
    }

    @SafeVarargs
    public RootChecker(Class<O> objectClass,
                       LocatedObject<? extends O> root,
                       AbstractChecker<? super O>... checkers) {
        this(objectClass,
             root);
        for (final AbstractChecker<? super O> checker : checkers) {
            add(checker);
        }
    }

    public final void check(SnapshotManager manager) {
        initAll(manager);
        check(CheckContext.init(manager).push(root), root);
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("ROOT<" + getObjectClass().getSimpleName() + ">");
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
        for (final AbstractChecker<? super O> child : getComponents()) {
            child.print(out, manager, level + 1);
        }
    }
}