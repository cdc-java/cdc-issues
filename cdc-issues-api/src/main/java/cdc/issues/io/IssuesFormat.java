package cdc.issues.io;

import java.io.File;

import cdc.office.ss.WorkbookKind;
import cdc.util.files.Files;
import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of possible formats for export and import of issues.
 * <p>
 * A format may be unsupported.
 *
 * @author Damien Carbonne
 */
public enum IssuesFormat {
    CSV,
    ODS,
    XLS,
    XLSM,
    XLSX,
    JSON,
    XML;

    /**
     * @return The corresponding {@link WorkbookKind}.
     * @throws UnexpectedValueException when no corresponding format can be provided.
     */
    public WorkbookKind getWorkbookKind() {
        switch (this) {
        case CSV:
            return WorkbookKind.CSV;
        case ODS:
            return WorkbookKind.ODS;
        case XLS:
            return WorkbookKind.XLS;
        case XLSX:
            return WorkbookKind.XLSX;
        case XLSM:
            return WorkbookKind.XLSM;
        default:
            throw new UnexpectedValueException(this);
        }
    }

    /**
     * @return {@code true} if this format is supported for export.
     */
    @SuppressWarnings("static-method")
    public boolean isExportFormat() {
        return true;
    }

    /**
     * @return {@code true} if this format is supported for import.
     */
    @SuppressWarnings("static-method")
    public boolean isImportFormat() {
        return true;
    }

    /**
     * @param file The file.
     * @return The format associated to {@code file} or {@code null}.
     */
    public static IssuesFormat from(File file) {
        final String ext = Files.getExtension(file);
        for (final IssuesFormat format : values()) {
            if (format.name().equalsIgnoreCase(ext)) {
                return format;
            }
        }
        return null;
    }
}