package cdc.issues.answers;

import java.util.Objects;

import cdc.issues.Issue;
import cdc.util.lang.Checks;

/**
 * (Issue, IssueAnswer) pair.
 *
 * @author Damien Carbonne
 */
public class AnsweredIssue {
    private final Issue issue;
    private final IssueAnswer answer;

    public AnsweredIssue(Issue issue,
                         IssueAnswer answer) {
        this.issue = Checks.isNotNull(issue, "issue");
        this.answer = answer;
    }

    public Issue getIssue() {
        return issue;
    }

    public IssueAnswer getAnswer() {
        return answer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(issue,
                            answer);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        final AnsweredIssue other = (AnsweredIssue) object;
        return Objects.equals(this.issue, other.issue)
                && Objects.equals(this.answer, other.answer);
    }

    @Override
    public String toString() {
        return "[" + getIssue() + " " + getAnswer() + "]";
    }
}