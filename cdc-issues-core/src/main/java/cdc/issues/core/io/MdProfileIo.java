package cdc.issues.core.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.StructuredDescription;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.io.ProfileIoFeatures.Hint;
import cdc.issues.rules.Profile;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.util.lang.CollectionUtils;

public class MdProfileIo {
    private static final Pattern NL_PATTERN = Pattern.compile("(\\R)");
    private final ProfileIoFeatures features;

    MdProfileIo(ProfileIoFeatures features) {
        this.features = features;
    }

    public void save(Profile profile,
                     File file) throws IOException {
        try (final PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            out.print("# ");
            out.println(profile.getName());
            dumpMetas(out, "##", profile.getMetas());
            dumpLabels(out, "##", profile.getLabels());
            if (profile.getDescription() != null) {
                out.println(escapeMd(profile.getDescription()));
            }

            for (final Rule rule : CollectionUtils.toSortedList(profile.getRules(),
                                                                Rule.ID_COMPARATOR)) {
                final StructuredDescription descr = RuleDescription.builder().text(rule.getDescription()).build();
                out.println();
                out.println();
                out.print("## ");
                if (features.isEnabled(Hint.SHOW_RULE_DOMAIN)) {
                    out.print(rule.getDomain() + "/");
                }
                out.println(rule.getName() + (rule.getTitle() == null ? "" : ": " + rule.getTitle()));

                dumpParams(out, "###", profile.getParams(rule));

                if (features.isEnabled(Hint.SHOW_RULE_ENABLING)) {
                    if (profile.isEnabled(rule)) {
                        out.println("Enabled");
                    } else {
                        out.println("Disabled");
                    }
                }
                dumpSeverities(out, profile, rule);
                dumpMetas(out, "###", rule.getMetas());
                dumpLabels(out, "###", rule.getLabels());

                // Description header
                out.println(escapeMd(descr.getHeader()));

                // Description sections
                if (features.getSections().isEmpty()) {
                    for (final String section : descr.getSections()) {
                        dumpSection(out, descr, section);
                    }
                } else {
                    for (final String section : features.getSections()) {
                        dumpSection(out, descr, section);
                    }
                }
            }
        }
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    public static String wrap(String text,
                              Pattern pattern,
                              String pre,
                              String post) {
        int lastIndex = 0;
        final StringBuilder builder = new StringBuilder();
        final Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            builder.append(text, lastIndex, matcher.start())
                   .append(pre)
                   .append(matcher.group(1))
                   .append(post);
            lastIndex = matcher.end();
        }
        if (lastIndex < text.length()) {
            builder.append(text, lastIndex, text.length());
        }
        return builder.toString();
    }

    public static String replace(String text,
                                 Pattern pattern,
                                 String by) {
        int lastIndex = 0;
        final StringBuilder builder = new StringBuilder();
        final Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            builder.append(text, lastIndex, matcher.start())
                   .append(by);
            lastIndex = matcher.end();
        }
        if (lastIndex < text.length()) {
            builder.append(text, lastIndex, text.length());
        }
        return builder.toString();
    }

    /**
     * Inserts 2 spaces before each new line.
     *
     * @param text The text.
     * @return The transformed text.
     */
    public static String mdReplaceNL(String text) {
        return replace(text, NL_PATTERN, "  \n");
    }

    private String escapeMd(String text) {
        text = features.getItemConverter().apply(text);
        return text;
    }

    private void dumpSeverities(PrintStream out,
                                Profile profile,
                                Rule rule) {
        if (features.isEnabled(Hint.SHOW_RULE_SEVERITY)) {
            if (profile.getCustomizedSeverity(rule).isPresent()) {
                out.println("- " + profile.getEffectiveSeverity(rule) + " (" + rule.getSeverity() + ")");
            } else {
                out.println("- " + profile.getEffectiveSeverity(rule));
            }
            out.println();
        }
    }

    private static void dumpNameValueMap(PrintStream out,
                                         String tag,
                                         String title,
                                         NameValueMap map) {
        if (!map.isEmpty()) {
            out.println(tag + " " + title);
            for (final String name : map.getSortedNames()) {
                out.println("- **" + name + "**: " + validate(map.getValue(name)));
            }
            out.println();
        }
    }

    private static void dumpMetas(PrintStream out,
                                  String tag,
                                  Metas metas) {
        dumpNameValueMap(out, tag, "Metas", metas);
    }

    private static void dumpParams(PrintStream out,
                                   String tag,
                                   Params params) {
        dumpNameValueMap(out, tag, "Params", params);
    }

    private static void dumpLabels(PrintStream out,
                                   String tag,
                                   Labels labels) {
        if (!labels.isEmpty()) {
            out.println(tag + " Labels");
            for (final String label : labels.getSorted()) {
                out.println("- **" + label + "**");
            }
            out.println();
        }
    }

    private void dumpSection(PrintStream out,
                             StructuredDescription descr,
                             String section) {
        if (features.isEnabled(Hint.SHOW_EMPTY_SECTIONS) || descr.hasSection(section)) {
            out.println();
            out.print("### ");
            out.println(section);

            if (descr.hasSection(section)) {
                dumpSectionItems(out, descr, section);
            } else {
                out.println("N/A");
            }
        }
    }

    private void dumpSectionItems(PrintStream out,
                                  StructuredDescription descr,
                                  String section) {
        final List<String> items = descr.getSectionItems(section);
        if (!items.isEmpty()) {
            for (final String item : items) {
                out.print("- ");
                out.println(escapeMd(item));
            }
        }
    }
}