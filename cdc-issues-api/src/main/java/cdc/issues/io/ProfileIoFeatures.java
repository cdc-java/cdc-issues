package cdc.issues.io;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.UnaryOperator;

import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.lang.Checks;

/**
 * Features used to configure {@link ProfileIo}.
 *
 * @author Damien Carbonne
 */
public final class ProfileIoFeatures {
    public static final ProfileIoFeatures BEST =
            builder().workbookWriterFeatures(WorkbookWriterFeatures.builder()
                                                                   .enable(WorkbookWriterFeatures.Feature.AUTO_FILTER_COLUMNS)
                                                                   .enable(WorkbookWriterFeatures.Feature.AUTO_SIZE_COLUMNS)
                                                                   .enable(WorkbookWriterFeatures.Feature.AUTO_SIZE_ROWS)
                                                                   .enable(WorkbookWriterFeatures.Feature.TRUNCATE_CELLS)
                                                                   .build())
                     .hint(Hint.PRETTY_PRINT)
                     .build();

    public static final ProfileIoFeatures FASTEST =
            builder().workbookWriterFeatures(WorkbookWriterFeatures.builder()
                                                                   .enable(WorkbookWriterFeatures.Feature.AUTO_FILTER_COLUMNS)
                                                                   .enable(WorkbookWriterFeatures.Feature.TRUNCATE_CELLS)
                                                                   .build())
                     .hint(Hint.PRETTY_PRINT)
                     .hint(Hint.POI_STREAMING)
                     .build();

    private final WorkbookWriterFeatures workbookWriterFeatures;
    private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
    private final List<String> sections;
    private final UnaryOperator<String> itemConverter;

    private ProfileIoFeatures(Builder builder) {
        this.workbookWriterFeatures = builder.workbookWriterFeatures;
        this.hints.addAll(builder.hints);
        this.sections = List.copyOf(builder.sections);
        this.itemConverter = Checks.isNotNull(builder.itemConverter, "itemConverter");
    }

    public enum Hint {
        /**
         * If enabled, pretty prints result.
         */
        PRETTY_PRINT,

        /**
         * If enabled, uses Fast ODS.
         * <p>
         * Specific to Workbooks.<br>
         * <b>WARNING:</b> This is still experimental and incomplete. Do not use.
         */
        ODS_FAST,

        /**
         * If enabled, uses ODF Toolkit simple API.
         * <p>
         * Specific to Workbooks.<br>
         * <b>WARNING:</b> This is very slow, but seems to work.
         */
        ODS_SIMPLE,

        /**
         * If enabled, uses POI Streaming API.
         * <p>
         * Specific to Workbooks.<br>
         * This requires less memory, but is not compliant with certain features.
         */
        POI_STREAMING,

        /**
         * If enabled, when a section is empty, show them.
         * <p>
         * Meaningful for {@link ProfileFormat#MD} and {@link ProfileFormat#HTML}.
         */
        SHOW_EMPTY_SECTIONS,

        /**
         * If enabled, show severities of rules.
         * <p>
         * Meaningful for {@link ProfileFormat#MD} and {@link ProfileFormat#HTML}.
         */
        SHOW_RULE_SEVERITY,

        /**
         * If enabled, show enabling of rules.
         * <p>
         * Meaningful for {@link ProfileFormat#MD} and {@link ProfileFormat#HTML}.
         */
        SHOW_RULE_ENABLING,

        /**
         * If enabled, show domain of rules.
         * <p>
         * Meaningful for {@link ProfileFormat#MD} and {@link ProfileFormat#HTML}.
         */
        SHOW_RULE_DOMAIN
    }

    public WorkbookWriterFeatures getWorkbookWriterFeatures() {
        return workbookWriterFeatures;
    }

    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public List<String> getSections() {
        return sections;
    }

    public UnaryOperator<String> getItemConverter() {
        return itemConverter;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private WorkbookWriterFeatures workbookWriterFeatures =
                WorkbookWriterFeatures.builder()
                                      .enable(WorkbookWriterFeatures.Feature.AUTO_FILTER_COLUMNS)
                                      .enable(WorkbookWriterFeatures.Feature.AUTO_SIZE_COLUMNS)
                                      .enable(WorkbookWriterFeatures.Feature.AUTO_SIZE_ROWS)
                                      .enable(WorkbookWriterFeatures.Feature.TRUNCATE_CELLS)
                                      .build();
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private final List<String> sections = new ArrayList<>();
        private UnaryOperator<String> itemConverter = UnaryOperator.identity();
        // TODO charset, separator (CSV)

        protected Builder() {
            super();
        }

        public Builder workbookWriterFeatures(WorkbookWriterFeatures features) {
            this.workbookWriterFeatures = features;
            return this;
        }

        public Builder hint(Hint hint) {
            hints.add(hint);
            return this;
        }

        /**
         * Sets the sections to dump.
         *
         * @param sections The sections.
         * @return This builder.
         */
        public Builder sections(List<String> sections) {
            this.sections.clear();
            this.sections.addAll(sections);
            return this;
        }

        /**
         * Sets the sections to dump.
         *
         * @param sections The sections.
         * @return This builder.
         */
        public Builder sections(String... sections) {
            return sections(List.of(sections));
        }

        /**
         * Sets the item wrapper.
         * <p>
         * It is used to modify items.
         *
         * @param itemConverter The item converter.
         * @return This builder.
         */
        public Builder itemConverter(UnaryOperator<String> itemConverter) {
            this.itemConverter = itemConverter;
            return this;
        }

        public ProfileIoFeatures build() {
            return new ProfileIoFeatures(this);
        }
    }
}