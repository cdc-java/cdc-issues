package cdc.issues.checks;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

import cdc.issues.locations.LocatedObject;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleId;
import cdc.util.debug.PrintableWithContext;
import cdc.util.lang.Checks;

/**
 * Base abstract class of checkers.
 *
 * @author Damien Carbonne
 *
 * @param <O> The checked object type.
 */
public abstract class AbstractChecker<O> implements PrintableWithContext<SnapshotManager> {
    /** The class of checked objects. */
    private final Class<O> objectClass;

    private record CRN(CheckResult result,
                       String notes) {
    }

    private static final Map<CheckResult, CRN> DEFAULT_CRN = new EnumMap<>(CheckResult.class);
    static {
        for (final CheckResult result : CheckResult.values()) {
            DEFAULT_CRN.put(result, new CRN(result, null));
        }
    }

    private static CRN toCRN(CheckResult result) {
        return DEFAULT_CRN.get(result);
    }

    private static CRN toCRN(CheckResult result,
                             String notes) {
        return new CRN(result, notes);
    }

    private static <O> CRN invoke(AbstractChecker<? super O> checker,
                                  CheckContext context,
                                  O object,
                                  Location location) {
        try {
            if (checker.isEnabled(context.getManager())) { // Will probably not throw any exception
                if (checker.accepts(object)) { // Might throw an exception
                    if (checker.isCorrectlyConfigured(context.getManager())) {
                        return toCRN(checker.check(context, object, location)); // Might throw an exception
                    } else {
                        return toCRN(CheckResult.MISCONFIGURED);
                    }
                } else {
                    return toCRN(CheckResult.SKIPPED);
                }
            } else {
                return toCRN(CheckResult.DISABLED);
            }
        } catch (final RuntimeException e) {
            return toCRN(CheckResult.EXCEPTION, e.getMessage());
        }
    }

    /**
     * Invokes a checker and stores (if relevant) the returned result.
     *
     * @param <O> The checked object type.
     * @param checker The checker.
     * @param context The context.
     * @param object The object to check.
     * @param location The object location.
     * @return The check result.
     */
    protected static <O> CheckResult checkAndStat(AbstractChecker<? super O> checker,
                                                  CheckContext context,
                                                  O object,
                                                  Location location) {
        final CRN crn = invoke(checker, context, object, location);
        checker.addStat(context, location, crn.result, crn.notes);
        return crn.result;
    }

    /**
     * Create a checker.
     *
     * @param objectClass The object class.
     */
    protected AbstractChecker(Class<O> objectClass) {
        this.objectClass = Checks.isNotNull(objectClass, "objectClass");
    }

    /**
     * @return A list of checkers referenced by this one.
     */
    protected abstract List<AbstractChecker<?>> getNext();

    /**
     * @param manager The manager.
     * @return A list of checkers referenced by this one.
     *         {@code manager} can be used to resolve referenced checker.
     *         This is the case with {@link LazyChecker}.
     */
    protected abstract List<AbstractChecker<?>> getNext(SnapshotManager manager);

    /**
     * Method that is called before execution of checker.
     * <p>
     * It can be used to register a checker into the used manager.
     * Default implementation does nothing.
     *
     * @param manager The manager.
     */
    protected void init(SnapshotManager manager) {
        // Ignore
    }

    /**
     * Method that must be called to initialize all checkers.
     *
     * @param manager The manager.
     */
    public final void initAll(SnapshotManager manager) {
        traverse(c -> c.init(manager));
    }

    /**
     * @return The class of checked objects.
     */
    public final Class<O> getObjectClass() {
        return objectClass;
    }

    /**
     * @param manager The manager.
     * @return {@code true} if this checker is enabled.
     */
    public abstract boolean isEnabled(SnapshotManager manager);

    /**
     * Returns {@code true} if this checker is correctly configured.
     * <p>
     * This can be overridden by checkers that depend on a correct configuration.
     *
     * @param manager The manager.
     * @return {@code true} if this checker is correctly configured.
     */
    public abstract boolean isCorrectlyConfigured(SnapshotManager manager);

    /**
     * Add statistics entry if its is possible and relevant.
     * <ul>
     * <li>Statistics of {@link AbstractRuleChecker} are saved,
     * <li>if {@code result} is {@link CheckResult#EXCEPTION EXCEPTION}, then an entry is added under {@link RuleId#NONE},
     * <li>others are ignored.
     * </ul>
     * This may be useful when there is an implementation error, typically with a {@link LazyChecker} that
     * cannot be resolved correctly.
     *
     * @param context The check context
     * @param location The checked object.
     * @param result The check result.
     * @param notes The check notes.
     */
    private void addStat(CheckContext context,
                         Location location,
                         CheckResult result,
                         String notes) {
        if (context.getManager().hasStats()) {
            if (this instanceof final AbstractRuleChecker<?> rc) {
                context.getManager().getStats().orElseThrow().add(location, rc.getRule().getId(), result, notes);
            } else if (result == CheckResult.EXCEPTION) {
                context.getManager().getStats().orElseThrow().add(location, RuleId.NONE, result, notes);
            }
        }
    }

    /**
     * Returns {@code true} if the passed object must be checked.
     * <p>
     * A check may be applied to an object only if some conditions are met.<br>
     * This may be overridden to define such conditions.<br>
     * The default implementation always returns {@code true}.
     * <p>
     * <b>Note:</b> configuration of checker shall NOT be taken into account.
     *
     * @param object The object to test.
     * @return {@code true} if this checker accepts {@code object}.
     */
    public boolean accepts(O object) {
        return true;
    }

    /**
     * Method that must be specialized to check an object.
     * <p>
     * It is this method responsibility to store detected issues into the associated manager.
     * <p>
     * <b>Note:</b> specializations of {@link AbstractRuleChecker} should return
     * {@link CheckResult#SUCCESS SUCCESS} or {@link CheckResult#FAILURE FAILURE}.<br>
     * Other results ({@link CheckResult#DISABLED DISABLED}, {@link CheckResult#SKIPPED SKIPPED},
     * {@link CheckResult#EXCEPTION EXCEPTION} and {@link CheckResult#MISCONFIGURED MISCONFIGURED})
     * are handled by the framework.
     *
     * @param context The context.
     * @param object The object to check.
     * @param location The object location.
     * @return The check result.
     */
    public abstract CheckResult check(CheckContext context,
                                      O object,
                                      Location location);

    /**
     * Check a raw object.
     *
     * @param context The context.
     * @param object The object to check.
     * @param location The object location.
     * @return The check result.
     * @throws ClassCastException When {@code object} can not be cast to {@code objectClass}.
     */
    public final CheckResult checkRaw(CheckContext context,
                                      Object object,
                                      Location location) {
        final O o = objectClass.cast(object);
        return check(context, o, location);
    }

    /**
     * Check a located object.
     *
     * @param context The check context.
     * @param data The located object.
     * @return The check result.
     */
    public final CheckResult check(CheckContext context,
                                   LocatedObject<? extends O> data) {
        return check(context,
                     data.getObject(),
                     data.getLocation());
    }

    private static void traverse(AbstractChecker<?> checker,
                                 Set<AbstractChecker<?>> visited,
                                 Consumer<AbstractChecker<?>> consumer) {
        consumer.accept(checker);
        visited.add(checker);
        for (final AbstractChecker<?> succ : checker.getNext()) {
            if (!visited.contains(succ)) {
                traverse(succ, visited, consumer);
            }
        }
    }

    /**
     * Traverse all referenced checkers once.
     * <p>
     * <b>WARNING:</b> Order of traversal is unspecified.
     * <p>
     * The difference with {@link #traverse(Consumer, SnapshotManager)} is that manager can not be used
     * to retrieve next checkers. That should not be an issue.
     * This is used internally to initialize all checkers.
     *
     * @param consumer The checker consumer.
     */
    private void traverse(Consumer<AbstractChecker<?>> consumer) {
        final Set<AbstractChecker<?>> visited = new HashSet<>();
        traverse(this, visited, consumer);
    }

    private static void traverse(SnapshotManager manager,
                                 AbstractChecker<?> checker,
                                 Set<AbstractChecker<?>> visited,
                                 Consumer<AbstractChecker<?>> consumer) {
        consumer.accept(checker);
        visited.add(checker);
        for (final AbstractChecker<?> succ : checker.getNext(manager)) {
            if (!visited.contains(succ)) {
                traverse(manager, succ, visited, consumer);
            }
        }
    }

    /**
     * Traverse all referenced checkers once.
     * <p>
     * <b>WARNING:</b> Order of traversal is unspecified.
     *
     * @param consumer The checker consumer.
     * @param manager The manager.
     */
    public void traverse(Consumer<AbstractChecker<?>> consumer,
                         SnapshotManager manager) {
        final Set<AbstractChecker<?>> visited = new HashSet<>();
        traverse(manager, this, visited, consumer);
    }

    /**
     * Returns a list of checkers that are instance of a checker class and match a predicate.
     *
     * @param <X> The checker type.
     * @param checkerClass The checker class.
     * @param manager The manager.
     * @param predicate The checker predicate.
     * @return A list of checkers that are instances of {@code checkerClass} and match {@code predicate}.
     */
    public <X extends AbstractChecker<?>> List<X> collect(Class<X> checkerClass,
                                                          SnapshotManager manager,
                                                          Predicate<? super X> predicate) {
        final List<X> list = new ArrayList<>();
        traverse(c -> {
            if (checkerClass.isInstance(c) && predicate.test(checkerClass.cast(c))) {
                list.add(checkerClass.cast(c));
            }
        },
                 manager);
        return list;
    }

    /**
     * Returns a list of checkers that are instance of a checker class.
     *
     * @param <X> The checker type.
     * @param checkerClass The checker class.
     * @param manager The manager.
     * @return A list of checkers that are instances of {@code checkerClass}.
     */
    public <X extends AbstractChecker<?>> List<X> collect(Class<X> checkerClass,
                                                          SnapshotManager manager) {
        return collect(checkerClass, manager, x -> true);
    }

    /**
     * @param manager The manager.
     * @return A list of checked rules (Sorted and without duplicates).
     */
    public List<Rule> getCheckedRules(SnapshotManager manager) {
        return collect(AbstractRuleChecker.class,
                       manager).stream()
                               .map(AbstractRuleChecker::getRule)
                               .sorted(Rule.ID_COMPARATOR)
                               .distinct()
                               .toList();
    }
}