package cdc.issues.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import cdc.issues.Issue;
import cdc.issues.IssueId;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.util.lang.Checks;

/**
 * Default implementation of {@link IssuesAndAnswers}.
 *
 * @author Damien Carbonne
 */
public class IssuesAndAnswersImpl implements IssuesAndAnswers {
    private final List<Issue> issues = new ArrayList<>();
    private final Map<IssueId, IssueAnswerImpl> map = new HashMap<>();
    private final Set<IssueAnswerImpl> answers = new HashSet<>();

    public IssuesAndAnswersImpl() {
        super();
    }

    public IssuesAndAnswersImpl addIssue(Issue issue) {
        Checks.isNotNull(issue, "issue");
        issues.add(issue);
        return this;
    }

    public IssuesAndAnswersImpl addIssues(List<? extends Issue> issues) {
        this.issues.addAll(issues);
        return this;
    }

    public IssuesAndAnswersImpl addAnswer(IssueAnswerImpl answer) {
        Checks.isNotNull(answer, "answer");
        final IssueAnswer current = map.get(answer.getIssueId());
        answers.remove(current);
        answers.add(answer);
        map.put(answer.getIssueId(), answer);
        return this;
    }

    @Override
    public List<Issue> getIssues() {
        return issues;
    }

    @Override
    public Set<IssueAnswerImpl> getAnswers() {
        return answers;
    }

    @Override
    public Optional<IssueAnswerImpl> getAnswer(IssueId id) {
        return Optional.ofNullable(map.get(id));
    }

    @Override
    public int hashCode() {
        // Skip map on purpose
        return Objects.hash(issues,
                            answers);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IssuesAndAnswersImpl)) {
            return false;
        }
        final IssuesAndAnswersImpl other = (IssuesAndAnswersImpl) object;
        // Skip map on purpose
        return Objects.equals(issues, other.issues)
                && Objects.equals(answers, other.answers);
    }
}