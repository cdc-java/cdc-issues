package cdc.issues.io;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.EnumSet;
import java.util.Set;

import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.ss.WorkbookWriterFeatures.Feature;

public final class IssuesIoFactoryFeatures {
    public static final IssuesIoFactoryFeatures UTC_BEST =
            builder().workbookWriterFeatures(WorkbookWriterFeatures.builder()
                                                                   .enable(Feature.AUTO_FILTER_COLUMNS)
                                                                   .enable(Feature.AUTO_SIZE_COLUMNS)
                                                                   .enable(Feature.AUTO_SIZE_ROWS)
                                                                   .enable(Feature.TRUNCATE_CELLS)
                                                                   .build())
                     .hint(Hint.PRETTY_PRINT)
                     .zoneId(ZoneOffset.UTC)
                     .hint(Hint.POI_STREAMING)
                     .hint(Hint.ZIP32)
                     .build();

    public static final IssuesIoFactoryFeatures UTC_BEST_VERBOSE =
            builder().set(UTC_BEST)
                     .hint(Hint.VERBOSE)
                     .build();

    public static final IssuesIoFactoryFeatures UTC_FASTEST =
            builder().workbookWriterFeatures(WorkbookWriterFeatures.builder()
                                                                   .enable(Feature.AUTO_FILTER_COLUMNS)
                                                                   .enable(Feature.TRUNCATE_CELLS)
                                                                   .build())
                     .hint(Hint.PRETTY_PRINT)
                     .zoneId(ZoneOffset.UTC)
                     .hint(Hint.POI_STREAMING)
                     .hint(Hint.ZIP32)
                     .build();

    public static final IssuesIoFactoryFeatures UTC_FASTEST_VERBOSE =
            builder().set(UTC_FASTEST)
                     .hint(Hint.VERBOSE)
                     .build();

    private final WorkbookWriterFeatures workbookWriterFeatures;
    private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
    /** ZoneId used to print timestamp. */
    private final ZoneId zoneId;
    private final String password;

    private IssuesIoFactoryFeatures(Builder builder) {
        this.workbookWriterFeatures = builder.workbookWriterFeatures;
        this.hints.addAll(builder.hints);
        this.zoneId = builder.zoneId;
        this.password = builder.password;
    }

    public enum Hint {
        VERBOSE,

        /**
         * If enabled, pretty prints result.
         */
        PRETTY_PRINT,

        /**
         * If enabled, uses Fast ODS.
         * <p>
         * Specific to Workbooks.<br>
         * <b>WARNING:</b> This is still experimental and incomplete. Do not use.
         */
        ODS_FAST,

        /**
         * If enabled, uses ODF Toolkit simple API.
         * <p>
         * Specific to Workbooks.<br>
         * <b>WARNING:</b> This is very slow, but seems to work.
         */
        ODS_SIMPLE,

        /**
         * If enabled, uses POI Streaming API.
         * <p>
         * Specific to Workbooks.<br>
         * This requires less memory, but is not compliant with certain features.
         */
        POI_STREAMING,

        /**
         * If enabled, use ZIP32 with streaming.
         * <p>
         * Specific to Excel Workbooks.<br>
         */
        ZIP32
    }

    public WorkbookWriterFeatures getWorkbookWriterFeatures() {
        return workbookWriterFeatures;
    }

    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public String getPassword() {
        return password;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private WorkbookWriterFeatures workbookWriterFeatures = WorkbookWriterFeatures.STANDARD_FAST;
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private ZoneId zoneId = ZoneOffset.UTC;
        private String password = null;

        protected Builder() {
            super();
        }

        public Builder set(IssuesIoFactoryFeatures other) {
            workbookWriterFeatures(other.getWorkbookWriterFeatures());
            zoneId(other.getZoneId());
            password(other.getPassword());
            hints.addAll(other.hints);
            return this;
        }

        public Builder workbookWriterFeatures(WorkbookWriterFeatures features) {
            this.workbookWriterFeatures = features;
            return this;
        }

        public Builder hint(Hint hint) {
            hints.add(hint);
            return this;
        }

        public Builder zoneId(ZoneId zoneId) {
            this.zoneId = zoneId;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public IssuesIoFactoryFeatures build() {
            return new IssuesIoFactoryFeatures(this);
        }
    }
}