package cdc.issues.checks;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.locations.LocatedObject;
import cdc.issues.rules.Rule;
import cdc.util.debug.Printables;

/**
 * Abstract leaf ({@link Rule}) checker.
 *
 * @author Damien Carbonne
 *
 * @param <O> The checked object type.
 */
public abstract class AbstractRuleChecker<O> extends AbstractChecker<O> {
    private final Rule rule;

    protected AbstractRuleChecker(Class<O> objectClass,
                                  Rule rule) {
        super(objectClass);
        this.rule = rule;
    }

    @Override
    protected final List<AbstractChecker<?>> getNext() {
        return Collections.emptyList();
    }

    @Override
    protected final List<AbstractChecker<?>> getNext(SnapshotManager manager) {
        return Collections.emptyList();
    }

    public Rule getRule() {
        return rule;
    }

    /**
     * @param object The object.
     * @return A string designating the checked object or object part.
     */
    protected abstract String getHeader(O object);

    protected final String getHeader(LocatedObject<? extends O> data) {
        return getHeader(data.getObject());
    }

    @Override
    public final boolean isEnabled(SnapshotManager manager) {
        if (manager.getProfile().isPresent()) {
            return manager.getProfile().orElseThrow().isEnabled(rule);
        } else {
            return true;
        }
    }

    @Override
    public boolean isCorrectlyConfigured(SnapshotManager manager) {
        return true;
    }

    /**
     * @param manager The manager.
     * @return The effective severity to use for this rule.
     */
    public final IssueSeverity getEffectiveSeverity(SnapshotManager manager) {
        if (manager.getProfile().isPresent()) {
            return manager.getProfile().orElseThrow().getEffectiveSeverity(rule);
        } else {
            return rule.getSeverity();
        }
    }

    protected Issue.Builder<?> issue(SnapshotManager manager) {
        // Set effective severity after the rule
        return Issue.builder()
                    .project(manager.getProjectName())
                    .rule(rule)
                    .severity(getEffectiveSeverity(manager));
    }

    protected static final void add(SnapshotManager manager,
                                    Issue issue) {
        manager.getIssuesCollector().issue(issue);
    }

    protected Issue.Builder<?> issue(CheckContext context) {
        return issue(context.getManager());
    }

    protected static final void add(CheckContext context,
                                    Issue issue) {
        add(context.getManager(), issue);
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("RULE<" + getObjectClass().getSimpleName() + "> "
                + rule.getName() + " '" + rule.getTitle() + "' " + (isEnabled(manager) ? "ENABLED" : "DISABLED"));
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
    }

    @Override
    public String toString() {
        return "RULE (" + rule.getName() + ")";
    }
}