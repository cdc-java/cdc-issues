package cdc.issues.rules;

import java.util.Collection;

import cdc.issues.StructuredDescription;
import cdc.util.lang.UnexpectedValueException;
import cdc.util.strings.StringUtils;

/**
 * Specialization of {@link StructuredDescription} that can be used to describe a {@link Rule}.
 * <p>
 * It supports the following sections:
 * <ul>
 * <li><b>Remarks</b> that can be used to add remarks.
 * <li><b>Sources</b> that can be used to refer to related sources.
 * <li><b>Applies to</b> that can be used to designate things to which the rule can be applied.
 * <li><b>Exceptions</b> that can be used to designate exceptions to the rule.
 * <li><b>Related to</b> that can be used to create link with other rules.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class RuleDescription extends StructuredDescription {
    public static final String SECTION_APPLIES_TO = "Applies to";
    public static final String SECTION_EXCEPTIONS = "Exceptions";
    public static final String SECTION_REMARKS = "Remarks";
    public static final String SECTION_RELATED_TO = "Related to";
    public static final String SECTION_SOURCES = "Sources";

    public static final String A = "a";
    public static final String AN = "an";
    public static final String ANY = "any";
    public static final String SOME = "some";
    public static final String THE = "the";
    public static final String ALL = "all";

    protected RuleDescription(Builder<?> builder) {
        super(builder);
    }

    private static String toCapital(String s) {
        if (StringUtils.isNullOrEmpty(s)) {
            return "";
        } else if (Character.isUpperCase(s.charAt(0))) {
            return s;
        } else if (s.length() == 1) {
            return Character.toString(Character.toUpperCase(s.charAt(0)));
        } else {
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }
    }

    private static String toLower(String s) {
        if (StringUtils.isNullOrEmpty(s)) {
            return "";
        } else {
            return s.toLowerCase();
        }
    }

    private static String toCase(String s,
                                 boolean capital) {
        return capital ? toCapital(s) : toLower(s);
    }

    private static String format(String specifier,
                                 Object arg) {
        switch (specifier) {
        case "wrap":
            return "[" + arg.toString() + "]";
        case "capital":
            if (arg == null) {
                return "";
            } else {
                return toCapital(arg.toString()) + " ";
            }
        case "lower":
            if (arg == null) {
                return "";
            } else {
                return toLower(arg.toString()) + " ";
            }
        default:
            throw new UnexpectedValueException("Invalid specifier: " + specifier);
        }
    }

    /**
     * Formats a list of arguments with a format.
     * <p>
     * The following format specifiers are supported:
     * <ul>
     * <li><b>wrap</b>: the passed argument is wrapped with '[' and ']'.
     * <li><b>lower</b>: if the passed argument is null, nothing is inserted, otherwise a space followed by the passed argument
     * converted to lower case is inserted.
     * <li><b>capital</b>: if the passed argument is null, nothing is inserted, otherwise a space followed by the passed argument
     * converted to capital case is inserted.
     * </ul>
     * Examples:
     * <ul>
     * <li><code>format("Foo{%lower}Bar", null) = "FooBar"</code>
     * <li><code>format("Foo{%lower}Bar", "AA") = "Foo aaBar"</code>
     * <li><code>format("Foo{%wrap}Bar", "AA") = "Foo[AA]Bar"</code>
     * </ul>
     *
     * @param format The format string.
     * @param args The arguments.
     * @return A string formatted for {@code format} and {@code args}.
     *
     */
    public static String format(String format,
                                Object... args) {
        return StringUtils.format(format, RuleDescription::format, args);
    }

    public static String wrap(String item) {
        return "[" + item + "]";
    }

    private static String wrap(String article,
                               String item) {
        return StringUtils.isNullOrEmpty(article)
                ? wrap(item)
                : article + " " + wrap(item);
    }

    public static String wrap(String article,
                              boolean capital,
                              String item) {
        return wrap(toCase(article, capital), item);
    }

    public static RuleDescription.Builder<?> builder() {
        return new RuleDescription.Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends StructuredDescription.Builder<B> {
        protected Builder() {
            super();
        }

        public B define(String format,
                        Object... args) {
            return text(RuleDescription.format(format, args));
        }

        public B wrap(String item) {
            return text(RuleDescription.wrap(item));
        }

        public B wrap(String article,
                      boolean capital,
                      String item) {
            return text(RuleDescription.wrap(article, capital, item));
        }

        public B remarks(String... remarks) {
            section(SECTION_REMARKS);
            return uItems(remarks);
        }

        public B remarks(Collection<String> remarks) {
            return remarks(remarks.toArray(String[]::new));
        }

        public B sources(String... sources) {
            section(SECTION_SOURCES);
            return uItems(sources);
        }

        public B sources(Collection<String> sources) {
            return sources(sources.toArray(String[]::new));
        }

        public B appliesTo(String... items) {
            section(SECTION_APPLIES_TO);
            return uItems(items);
        }

        public B appliesTo(Collection<String> items) {
            return appliesTo(items.toArray(String[]::new));
        }

        public B exceptions(String... items) {
            section(SECTION_EXCEPTIONS);
            return uItems(items);
        }

        public B exceptions(Collection<String> items) {
            return exceptions(items.toArray(String[]::new));
        }

        public B relatedTo(String... items) {
            section(SECTION_RELATED_TO);
            return uItems(items);
        }

        public B relatedTo(Collection<String> items) {
            return relatedTo(items.toArray(String[]::new));
        }

        @Override
        public RuleDescription build() {
            return new RuleDescription(this);
        }
    }
}