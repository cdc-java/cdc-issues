package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.issues.Issue;
import cdc.issues.IssueId;
import cdc.issues.IssueSeverity;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.issues.impl.IssueAnswerImpl;
import cdc.issues.impl.IssueCommentImpl;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesReader;
import cdc.issues.locations.Locations;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

/**
 * Implementation of {@link IssuesReader} dedicated to XML.
 *
 * @author Damien Carbonne
 */
public class XmlIssuesReader extends IssuesIo implements IssuesReader {
    private final File file;
    private final InputStream in;

    public XmlIssuesReader(File file,
                           IssuesIoFactoryFeatures features) {
        super(features);
        this.file = file;
        this.in = null;
    }

    public XmlIssuesReader(InputStream in,
                           IssuesFormat format,
                           IssuesIoFactoryFeatures features) {
        super(features);
        this.file = null;
        this.in = in;
    }

    @Override
    public IssuesAndAnswers load(Settings settings,
                                 ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final String systemId = file == null ? "???" : file.getPath();
        traceLoad(systemId);
        final StAXLoader loader = new StAXLoader(FailureReaction.FAIL,
                                                 this,
                                                 settings,
                                                 controller);
        final IssuesAndAnswers buffer;
        if (file == null) {
            buffer = loader.load(in, systemId);
        } else {
            buffer = loader.load(file);
        }
        chrono.suspend();
        traceLoaded(systemId, chrono);
        return buffer;
    }

    private static class StAXLoader extends AbstractStAXLoader<IssuesAndAnswers> {
        public StAXLoader(FailureReaction reaction,
                          IssuesIo owner,
                          Settings settings,
                          ProgressController controller) {
            super((reader,
                   systemId) -> new Parser(reader,
                                           systemId,
                                           reaction,
                                           owner,
                                           settings,
                                           controller));
        }

        private static class Parser extends AbstractStAXParser<IssuesAndAnswers> {
            private final IssuesIo owner;
            private final Settings settings;
            private final IssuesAndAnswersImpl buffer = new IssuesAndAnswersImpl();
            private final ProgressSupplier progress;

            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction,
                             IssuesIo owner,
                             Settings settings,
                             ProgressController controller) {
                super(reader, systemId, reaction);
                this.owner = owner;
                this.settings = settings;
                this.progress = new ProgressSupplier(controller);
            }

            @Override
            protected IssuesAndAnswers parse() throws XMLStreamException {
                final String ctx = "parse()";
                expectStartDocument(ctx);

                nextTag();
                expectStartElement(ctx);
                if (isStartElement(XmlIo.ISSUES)) {
                    parseIssues();
                } else if (isStartElement(XmlIo.PROJECT_SNAPSHOT)) {
                    parseProjectSnapshot();
                } else {
                    throw unexpectedEvent();
                }
                next();
                expectEndDocument(ctx);
                return buffer;
            }

            private void parseProjectSnapshot() throws XMLStreamException {
                final String ctx = "parseProjectSnapshot()";
                expectStartElement(ctx, XmlIo.PROJECT_SNAPSHOT);

                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(XmlIo.PROJECT)) {
                        ignoreElement();
                    } else if (isStartElement(XmlIo.SNAPSHOT)) {
                        parseSnapshot();
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }

                expectEndElement(ctx, XmlIo.PROJECT_SNAPSHOT);
            }

            private void parseSnapshot() throws XMLStreamException {
                final String ctx = "parseSnapshot()";
                expectStartElement(ctx, XmlIo.SNAPSHOT);

                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(XmlIo.ISSUES)) {
                        parseIssues();
                    } else {
                        ignoreElement();
                    }
                    nextTag();
                }

                expectEndElement(ctx, XmlIo.SNAPSHOT);
            }

            private void parseIssues() throws XMLStreamException {
                final String ctx = "parseIssues()";
                expectStartElement(ctx, XmlIo.ISSUES);

                progress.reset(-1, "load " + systemId);
                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(XmlIo.ISSUE)) {
                        parseIssue();
                        progress.incrementValue();
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
                progress.close();

                expectEndElement(ctx, XmlIo.ISSUES);
            }

            private void parseIssue() throws XMLStreamException {
                final String ctx = "parseIssue()";
                expectStartElement(ctx, XmlIo.ISSUE);

                final Issue.Builder<?> builder = Issue.builder();

                final String stimestamp = getAttributeValue(XmlIo.TIMESTAMP);
                final String domain = getAttributeValue(XmlIo.DOMAIN, null);
                final String snapshot = getAttributeValue(XmlIo.SNAPSHOT, null);
                final String name = getAttributeValue(XmlIo.NAME);
                final String title = getAttributeValue(XmlIo.TITLE, null);
                final String project = getAttributeValue(XmlIo.PROJECT, null);
                final IssueSeverity severity = getAttributeAsEnum(XmlIo.SEVERITY, IssueSeverity.class);
                builder.timestamp(owner.toInstant(stimestamp))
                       .domain(domain)
                       .name(name)
                       .title(title)
                       .project(project)
                       .snapshot(snapshot)
                       .severity(severity);

                nextTag();

                expectStartElement(ctx, XmlIo.DESCRIPTION);
                final String description = reader.getElementText();
                expectEndElement(ctx, XmlIo.DESCRIPTION);
                builder.description(description);

                nextTag();

                // Params
                builder.params(XmlIo.parseOptParams(this));

                // Locations
                expectStartElement(ctx, XmlIo.LOCATIONS);
                nextTag();
                while (isStartElement(XmlIo.LOCATION)) {
                    final String tag = getAttributeValue(XmlIo.TAG, null);
                    final String path = getAttributeValue(XmlIo.PATH);
                    final String anchor = getAttributeValue(XmlIo.ANCHOR, null);
                    builder.addLocation(Locations.build(tag, path, anchor));
                    nextTag();
                    expectEndElement(ctx, XmlIo.LOCATION);
                    nextTag();
                }
                expectEndElement(ctx, XmlIo.LOCATIONS);

                nextTag();

                // Metas
                builder.metas(XmlIo.parseOptMetas(this));
                // Labels
                builder.labels(XmlIo.parseOptLabels(this));

                final Issue issue = builder.build();
                buffer.addIssue(issue);

                // Answer
                if (isStartElement(XmlIo.ANSWER)) {
                    if (settings.getHints().contains(Settings.Hint.NO_ANSWERS)) {
                        ignoreElement();
                    } else {
                        buffer.addAnswer(parseAnswer(issue.getId()));
                    }
                    expectEndElement(ctx, XmlIo.ANSWER);
                    nextTag();
                }

                expectEndElement(ctx, XmlIo.ISSUE);
            }

            private IssueAnswerImpl parseAnswer(IssueId id) throws XMLStreamException {
                final String ctx = "parseAnswer()";
                expectStartElement(ctx, XmlIo.ANSWER);

                final IssueAnswerImpl.Builder answer = IssueAnswerImpl.builder();

                final String author = getAttributeValue(XmlIo.AUTHOR, null);
                final String screated = getAttributeValue(XmlIo.CREATED, null);
                final String smodified = getAttributeValue(XmlIo.MODIFIED, null);
                final IssueStatus status = getAttributeAsEnum(XmlIo.STATUS, IssueStatus.class, null);
                final IssueResolution resolution = getAttributeAsEnum(XmlIo.RESOLUTION, IssueResolution.class, null);
                final String assignee = getAttributeValue(XmlIo.ASSIGNEE, null);
                final IssueSeverity newSeverity = getAttributeAsEnum(XmlIo.NEW_SEVERITY, IssueSeverity.class, null);

                answer.issueId(id)
                      .author(author)
                      .creationDate(owner.toInstant(screated))
                      .modificationDate(owner.toInstant(smodified))
                      .status(status)
                      .resolution(resolution)
                      .assignee(assignee)
                      .newSeverity(newSeverity);

                nextTag();
                if (isStartElement(XmlIo.COMMENTS)) {
                    nextTag();
                    while (isStartElement(XmlIo.COMMENT)) {
                        final String cauthor = getAttributeValue(XmlIo.AUTHOR, null);
                        final String sdate = getAttributeValue(XmlIo.DATE, null);
                        final String text = getReader().getElementText();

                        final IssueCommentImpl.Builder comment =
                                IssueCommentImpl.builder()
                                                .author(cauthor)
                                                .date(owner.toInstant(sdate))
                                                .text(text);

                        answer.comment(comment.build());

                        expectEndElement(ctx, XmlIo.COMMENT);
                        nextTag();
                    }
                    expectEndElement(ctx, XmlIo.COMMENTS);
                    nextTag();
                }

                // Metas
                answer.metas(XmlIo.parseOptMetas(this));
                // Labels
                answer.labels(XmlIo.parseOptLabels(this));

                expectEndElement(ctx, XmlIo.ANSWER);

                return answer.build();
            }
        }
    }
}