package cdc.issues.rules;

import java.util.function.Consumer;

import cdc.issues.IssueSeverity;

public final class RuleUtils {
    private RuleUtils() {
    }

    public static Rule define(String domain,
                              String name,
                              String title,
                              Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                              IssueSeverity severity) {
        final RuleDescription.Builder<?> description = RuleDescription.builder();
        descriptionBuilder.accept(description);
        final Rule.Builder builder = Rule.builder()
                                         .domain(domain)
                                         .name(name)
                                         .title(title)
                                         .description(description.build().toString())
                                         .severity(severity);
        return builder.build();
    }

    @Deprecated(since = "2025-01-18", forRemoval = true)
    public static Rule define(String domain,
                              String name,
                              Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                              IssueSeverity severity) {
        return define(domain, name, null, descriptionBuilder, severity);
    }
}