package cdc.issues;

/**
 * Interface of a (name, value) pair.
 */
public interface NameValue {
    /**
     * @return The name.
     */
    public String getName();

    /**
     * @return The value (possibly {@code null}).
     */
    public String getValue();
}