package cdc.issues;

/**
 * Interface implemented by classes that can return a {@link Diagnosis}.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface HasDiagnosis {
    /**
     * @return The diagnosis.
     */
    public Diagnosis getDiagnosis();
}