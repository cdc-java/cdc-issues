package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.Location;

class LocationTest {
    @Test
    void testGetTag() {
        assertSame(null, Location.getTag(null));
        assertSame(null, Location.getTag(""));
        assertSame(null, Location.getTag("foo"));
        assertEquals("foo", Location.getTag("foo::Path"));
        assertEquals("foo", Location.getTag("foo::Path::Anchor"));
    }

    @Test
    void testGetPath() {
        assertSame(null, Location.getPath(null));
        assertSame(null, Location.getPath(""));
        assertSame(null, Location.getPath("foo"));
        assertEquals("Path", Location.getPath("foo::Path"));
        assertEquals("Path", Location.getPath("foo::Path::Anchor"));
    }

    @Test
    void testGetAnchor() {
        assertSame(null, Location.getAnchor(null));
        assertSame(null, Location.getAnchor(""));
        assertSame(null, Location.getAnchor("foo"));
        assertSame(null, Location.getAnchor("foo::Path"));
        assertEquals("", Location.getAnchor("foo::Path::"));
        assertEquals("Anchor", Location.getAnchor("foo::Path::Anchor"));
    }
}