package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ParamsTest {
    @Test
    void test() {
        assertTrue(Params.NO_PARAMS.isEmpty());
        final Params params1 = Params.builder()
                                     .param("name1", "value1")
                                     .param("name2", "value2")
                                     .build();
        final Params params2 = Params.builder()
                                     .param("name2", "value2")
                                     .param("name1", "value1")
                                     .build();
        assertSame(2, params1.getNames().size());
        assertEquals(params1, params2);
        assertEquals("value1", params1.getValue("name1"));
        assertEquals("value1", params2.getValue("name1"));
        assertEquals(params1.getNames(), params2.getNames());
        assertEquals(params1.getSortedNames(), params2.getSortedNames());
        assertEquals(params1, params1);
        assertNotEquals(params1, null);
        assertNotEquals(params1, "Hello");
        assertEquals(params1.toString(), params2.toString());
        assertEquals(params1.hashCode(), params2.hashCode());
    }
}