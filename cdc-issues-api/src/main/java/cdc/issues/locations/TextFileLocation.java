package cdc.issues.locations;

import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Implementation of Location dedicated to text files.
 *
 * @author Damien Carbonne
 */
public class TextFileLocation extends AbstractLocation {
    private final String systemId;
    private final int lineNumber;
    private final int columnNumber;

    public static final String TAG = "TextFileLocation";
    private static final String SEPARATOR = ":";

    protected TextFileLocation(String systemId,
                               int lineNumber,
                               int columnNumber) {
        this.systemId = Checks.isNotNull(systemId, "systemId");
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
        if (lineNumber < 1 && columnNumber >= 1) {
            throw new IllegalArgumentException("Non compliant numbers");
        }
    }

    public static TextFileLocation create(String systemId,
                                          String anchor) {
        return builder().systemId(systemId)
                        .anchor(anchor)
                        .build();
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return getSystemId();
    }

    @Override
    public String getAnchor() {
        if (lineNumber >= 1) {
            if (columnNumber >= 1) {
                return lineNumber + SEPARATOR + columnNumber;
            } else {
                return Integer.toString(lineNumber);
            }
        } else {
            return "";
        }
    }

    public String getSystemId() {
        return systemId;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String systemId;
        private int lineNumber = 0;
        private int columnNumber = 0;

        protected Builder() {
            super();
        }

        public Builder systemId(String systemId) {
            this.systemId = systemId;
            return this;
        }

        public Builder lineNumber(int lineNumber) {
            this.lineNumber = Math.max(0, lineNumber);
            return this;
        }

        public Builder columnNumber(int columnNumber) {
            this.columnNumber = Math.max(0, columnNumber);
            return this;
        }

        public Builder anchor(String anchor) {
            if (StringUtils.isNullOrEmpty(anchor)) {
                this.lineNumber = 0;
                this.columnNumber = 0;
            } else {
                final int pos = anchor.indexOf(SEPARATOR);
                if (pos > 0) {
                    this.lineNumber = Integer.parseInt(anchor.substring(0, pos));
                    this.columnNumber = Integer.parseInt(anchor.substring(pos + SEPARATOR.length()));
                } else {
                    this.lineNumber = Integer.parseInt(anchor);
                }
            }
            return this;
        }

        public TextFileLocation build() {
            return new TextFileLocation(systemId,
                                        lineNumber,
                                        columnNumber);
        }
    }
}