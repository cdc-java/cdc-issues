package cdc.issues.checks;

import cdc.issues.rules.Rule;
import cdc.util.lang.Checks;

/**
 * Specialization of {@link AbstractRuleChecker} that can check a single property of an object.
 * <p>
 * Location is that of the object.
 *
 * @param <O> The object type.
 * @param <P> The property type.
 */
public abstract class AbstractPropertyChecker<O, P> extends AbstractRuleChecker<O> {
    private final Class<P> propertyClass;

    protected AbstractPropertyChecker(Class<O> objectClass,
                                      Rule rule,
                                      Class<P> propertyClass) {
        super(objectClass,
              rule);
        this.propertyClass = Checks.isNotNull(propertyClass, "propertyClass");
    }

    /**
     * @return The property class.
     */
    public final Class<P> getPropertyClass() {
        return propertyClass;
    }

    /**
     * @param object The object
     * @return The property.
     */
    protected abstract P getProperty(O object);
}