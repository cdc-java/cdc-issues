package cdc.issues.locations;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import cdc.util.lang.Checks;

/**
 * (Object, Location) pair.
 *
 * @author Damien Carbonne
 *
 * @param <O> The object type.
 */
public final class LocatedObject<O> implements LocatedItem {
    private final O object;
    private final Location location;

    private LocatedObject(O object,
                          Location location) {
        Checks.isNotNull(location, "location");
        this.object = object;
        this.location = location;
    }

    /**
     * @return The data.
     */
    public O getObject() {
        return object;
    }

    /**
     * @return The data location.
     */
    @Override
    public Location getLocation() {
        return location;
    }

    public <E> LocatedObject<? extends E> cast(Class<E> cls) {
        if (cls.isInstance(getObject())) {
            @SuppressWarnings("unchecked")
            final LocatedObject<? extends E> tmp = (LocatedObject<? extends E>) this;
            return tmp;
        } else {
            throw new ClassCastException();
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(object,
                            location);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof LocatedObject)) {
            return false;
        }
        final LocatedObject<?> other = (LocatedObject<?>) object;
        return Objects.equals(this.object, other.object)
                && Objects.equals(this.location, other.location);
    }

    @Override
    public String toString() {
        return "[" + object + ", " + location + "]";
    }

    public static <O> LocatedObject<O> of(O object,
                                          Location location) {
        return new LocatedObject<>(object, location);
    }

    public static <O extends LocatedItem> LocatedObject<O> of(O object) {
        return new LocatedObject<>(object, object.getLocation());
    }

    public static <O extends LocatedItem> List<LocatedObject<? extends O>> locate(List<? extends O> list) {
        return list.stream()
                   .map(LocatedObject::of)
                   .collect(Collectors.toList());
    }
}