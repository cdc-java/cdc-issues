package cdc.issues.rules;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Params;

/**
 * Interface describing a Profile.
 * <p>
 * It is a set of {@link ConfiguredRule ConfiguredRules}, some of which are enabled.
 *
 * @author Damien Carbonne
 */
public interface Profile {
    /**
     * @return The profile name.
     */
    public String getName();

    /**
     * @return The profile description.
     */
    public String getDescription();

    /**
     * @return The meta data associated to this profile.
     */
    public Metas getMetas();

    /**
     * @return The labels associated to this profile.
     */
    public Labels getLabels();

    /**
     * @return The set of rules in this profile.
     */
    public Set<Rule> getRules();

    public default boolean hasRule(Rule rule) {
        return getRules().contains(rule);
    }

    /**
     * @return The set of rules that are enabled in this profile.
     */
    public default Set<Rule> getEnabledRules() {
        return getRules().stream()
                         .filter(this::isEnabled)
                         .collect(Collectors.toSet());
    }

    public ProfileConfig getProfileConfig();

    /**
     * @param ruleId The rule id.
     * @return {@code true} if a rule with {@code ruleId} exists in this profile.
     */
    public default boolean hasRule(RuleId ruleId) {
        return getProfileConfig().getRuleIds().contains(ruleId);
    }

    /**
     * @param ruleId The rule id.
     * @return The rule with {@code ruleId}.
     */
    public Optional<Rule> getRule(RuleId ruleId);

    /**
     * @param rule The rule.
     * @return The configured rule corresponding to {@code rule}.
     */
    public ConfiguredRule getConfiguredRule(Rule rule);

    /**
     * @param rule The rule.
     * @return The {@code rule} config.
     */
    public default RuleConfig getRuleConfig(Rule rule) {
        return getConfiguredRule(rule).getConfig();
    }

    /**
     * @param rule The rule.
     * @return {@code true} if {@code rule} is enabled in this profile.
     * @throws IllegalArgumentException When {@code rule} is {@code null} or is unknown.
     */
    public default boolean isEnabled(Rule rule) {
        return getConfiguredRule(rule).isEnabled();
    }

    /**
     *
     * @param rule The rule.
     * @return The optional customized severity.
     */
    public default Optional<IssueSeverity> getCustomizedSeverity(Rule rule) {
        return getConfiguredRule(rule).getCustomizedSeverity();
    }

    /**
     * @param rule The rule.
     * @return The effective severity of {@code rule}.
     * @throws IllegalArgumentException When {@code rule} is {@code null} or is unknown.
     */
    public default IssueSeverity getEffectiveSeverity(Rule rule) {
        return getConfiguredRule(rule).getEffectiveSeverity();
    }

    /**
     * Returns the parameters to use for a rule.
     *
     * @param rule The rule.
     * @return The parameters that are used to configure {@code rule}
     *         in this profile.
     * @throws IllegalArgumentException When {@code rule} is {@code null} or is unknown.
     */
    public default Params getParams(Rule rule) {
        return getConfiguredRule(rule).getParams();
    }
}