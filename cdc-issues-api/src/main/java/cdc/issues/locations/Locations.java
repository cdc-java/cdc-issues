package cdc.issues.locations;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

import cdc.util.lang.Checks;

/**
 * Factory of {@link Location}.
 *
 * @author Damien Carbonne
 */
public final class Locations {
    private static final Map<String, BiFunction<String, String, ? extends Location>> FACTORIES = new HashMap<>();

    static {
        register(DefaultLocation.TAG, DefaultLocation::new);
        register(TextFileLocation.TAG, TextFileLocation::create);
        register(WorkbookLocation.TAG, WorkbookLocation::create);
    }

    private Locations() {
    }

    public static void register(String tag,
                                BiFunction<String, String, ? extends Location> factory) {
        Checks.doesNotContainKey(FACTORIES, tag, "tag");
        Checks.isNotNull(factory, "factory");
        FACTORIES.put(tag, factory);
    }

    /**
     * @return The set of tags for which a factory has been registered.
     */
    public static Set<String> getTags() {
        return FACTORIES.keySet();
    }

    /**
     * Returns the factory associated to a tag.
     *
     * @param tag The tag.
     * @return The factory associated to {@code tag} or {@code null}.
     */
    public static BiFunction<String, String, ? extends Location> getFactoryOrNull(String tag) {
        return FACTORIES.get(tag);
    }

    /**
     * Creates a Location instance.
     * <p>
     * If a factory is registered for {@code tag}, uses it.
     * Otherwise, creates a {@link DefaultLocation}.
     *
     * @param tag The tag.
     * @param path The path.
     * @param anchor The anchor.
     * @return A new Location instance.
     */
    public static Location build(String tag,
                                 String path,
                                 String anchor) {
        final BiFunction<String, String, ? extends Location> factory = getFactoryOrNull(tag);
        if (factory == null) {
            return new DefaultLocation(path, anchor);
        } else {
            return factory.apply(path, anchor);
        }
    }

    /**
     * Creates a Location instance from its standard string representation.
     *
     * @param location The standard string representation of the location,
     *            as defined in {@link Location#toString(Location)}.
     * @return A new Location instance.
     * @see Location#toString(Location)
     */
    public static Location build(String location) {
        final String tag = Location.getTag(location);
        final String path = Location.getPath(location);
        final String anchor = Location.getAnchor(location);
        return build(tag, path, anchor);
    }
}