package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cdc.io.xml.XmlWriter;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.StructuredDescription;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.io.ProfileIoFeatures.Hint;
import cdc.issues.rules.Profile;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.util.lang.CollectionUtils;
import cdc.util.strings.StringUtils;

class HtmlProfileIo {
    private final ProfileIoFeatures features;

    HtmlProfileIo(ProfileIoFeatures features) {
        this.features = features;
    }

    public void save(Profile profile,
                     File file) throws IOException {
        // TODO use an HTML writer
        try (final XmlWriter writer = new XmlWriter(file)) {
            if (features.isEnabled(Hint.PRETTY_PRINT)) {
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            }
            writer.setIndentString("  ");

            writer.beginDocument();
            writer.addDTD("<!DOCTYPE html>");
            writer.beginElement("html");

            // Head
            writer.beginElement("head");

            writer.beginElement("meta");
            writer.addAttribute("charset", "utf-8");
            writer.endElement(); // meta

            writer.addElement("title", profile.getName());

            writer.endElement(); // head

            // Body
            writer.beginElement("body");

            writer.addElement("h1", profile.getName());

            dumpMetas(writer, "h2", profile.getMetas());
            dumpLabels(writer, "h2", "Labels", profile.getLabels());

            if (!StringUtils.isNullOrEmpty(profile.getDescription())) {
                writer.beginElement("p");
                addHtmlContent(writer, profile.getDescription());
                writer.endElement(); // p
            }

            for (final Rule rule : CollectionUtils.toSortedList(profile.getRules(),
                                                                Rule.ID_COMPARATOR)) {
                final StructuredDescription descr = RuleDescription.builder().text(rule.getDescription()).build();

                writer.beginElement("h2");
                if (features.isEnabled(Hint.SHOW_RULE_DOMAIN)) {
                    writer.addElementContent(rule.getDomain());
                    writer.addElementContent("/");
                }
                writer.addElementContent(rule.getName());
                if (rule.getTitle() != null) {
                    writer.addElementContent(": ");
                    writer.addElementContent(rule.getTitle());
                }
                writer.endElement(); // h2

                dumpParams(writer, "h3", profile.getParams(rule));

                if (features.isEnabled(Hint.SHOW_RULE_ENABLING)) {
                    if (profile.isEnabled(rule)) {
                        writer.addElementContent("Enabled");
                    } else {
                        writer.addElementContent("Disabled");
                    }
                }

                dumpSeverities(writer, profile, rule);
                dumpMetas(writer, "h3", rule.getMetas());
                dumpLabels(writer, "h3", "Labels", rule.getLabels());

                // Description header
                writer.beginElement("p");
                addHtmlContent(writer, descr.getHeader());
                writer.endElement(); // p

                // Description sections
                if (features.getSections().isEmpty()) {
                    for (final String section : descr.getSections()) {
                        dumpSection(writer, descr, section);
                    }
                } else {
                    for (final String section : features.getSections()) {
                        dumpSection(writer, descr, section);
                    }
                }
            }

            writer.endElement(); // body
            writer.endElement(); // html
            writer.endDocument();
        }
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    private void addHtmlContent(XmlWriter writer,
                                String text) throws IOException {
        int index = 0;
        for (final String s : StringUtils.splitLines(text)) {
            if (index > 0) {
                writer.addUnescapedElementContent("<br>");
            }
            writer.addElementContent(features.getItemConverter().apply(s));
            index++;
        }
    }

    private void dumpSeverities(XmlWriter writer,
                                Profile profile,
                                Rule rule) throws IOException {
        if (features.isEnabled(Hint.SHOW_RULE_SEVERITY)) {
            writer.beginElement("ul");
            writer.beginElement("li");
            if (profile.getCustomizedSeverity(rule).isPresent()) {
                writer.addElementContent(profile.getEffectiveSeverity(rule) + " (" + rule.getSeverity() + ")");
            } else {
                writer.addElementContent(profile.getEffectiveSeverity(rule).name());
            }
            writer.endElement(); // li
            writer.endElement(); // ul
        }
    }

    private static void dumpNameValueMap(XmlWriter writer,
                                         String tag,
                                         NameValueMap map,
                                         String title) throws IOException {
        if (!map.isEmpty()) {
            writer.addElement(tag, title);
            writer.beginElement("ul");
            for (final String name : map.getSortedNames()) {
                writer.beginElement("li");
                writer.addElementContent(""); // Force mixed content
                writer.beginElement("b");
                writer.addElementContent(name);
                writer.endElement(); // b
                writer.addElementContent(": " + validate(map.getValue(name)));
                writer.endElement(); // li
            }
            writer.endElement(); // ul
        }
    }

    private static void dumpMetas(XmlWriter writer,
                                  String tag,
                                  Metas metas) throws IOException {
        dumpNameValueMap(writer, tag, metas, "Metas");
    }

    private static void dumpParams(XmlWriter writer,
                                   String tag,
                                   Params params) throws IOException {
        dumpNameValueMap(writer, tag, params, "Params");
    }

    private static void dumpLabels(XmlWriter writer,
                                   String tag,
                                   String title,
                                   Labels labels) throws IOException {
        if (!labels.isEmpty()) {
            writer.addElement(tag, title);
            writer.beginElement("ul");
            for (final String label : labels.getSorted()) {
                writer.beginElement("li");
                writer.addElementContent(label);
                writer.endElement(); // li
            }
            writer.endElement(); // ul
        }
    }

    private void dumpSection(XmlWriter writer,
                             StructuredDescription descr,
                             String section) throws IOException {
        if (features.isEnabled(Hint.SHOW_EMPTY_SECTIONS) || descr.hasSection(section)) {
            writer.beginElement("h3");
            writer.addElementContent(section);
            writer.endElement(); // h3
            if (descr.hasSection(section)) {
                dumpSectionItems(writer, descr, section);
            } else {
                writer.addElement("p", "N/A");
            }
        }
    }

    private void dumpSectionItems(XmlWriter writer,
                                  StructuredDescription descr,
                                  String section) throws IOException {
        final List<String> items = descr.getSectionItems(section);
        if (!items.isEmpty()) {
            writer.beginElement("ul");
            for (final String item : items) {
                writer.beginElement("li");
                addHtmlContent(writer, item);
                writer.endElement(); // li
            }
            writer.endElement(); // ul
        }
    }

}