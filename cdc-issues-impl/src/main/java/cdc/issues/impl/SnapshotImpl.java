package cdc.issues.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import cdc.issues.Issue;
import cdc.issues.IssueUtils;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Project;
import cdc.issues.Snapshot;
import cdc.util.lang.Checks;

/**
 * Default implementation of {@link Snapshot}.
 *
 * @author Damien Carbonne
 */
public class SnapshotImpl implements Snapshot {
    private final ProjectImpl project;
    private String name;
    private String description;
    private Metas metas = Metas.NO_METAS;
    private Labels labels = Labels.NO_LABELS;
    private Instant timestamp = Instant.now();
    private final List<Issue> issues = new ArrayList<>();
    private String issuesHash = null;

    public SnapshotImpl() {
        this(null);
    }

    SnapshotImpl(ProjectImpl project) {
        this.project = project;
        if (project != null) {
            project.addSnapshot(this);
        }
    }

    public SnapshotImpl setName(String name) {
        this.name = name;
        return this;
    }

    public SnapshotImpl setDescription(String description) {
        this.description = description;
        return this;
    }

    public SnapshotImpl setMetas(Metas metas) {
        this.metas = Checks.isNotNull(metas, "metas");
        return this;
    }

    public SnapshotImpl setLabels(Labels labels) {
        this.labels = Checks.isNotNull(labels, "labels");
        return this;
    }

    public SnapshotImpl setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public SnapshotImpl addIssue(Issue issue) {
        issues.add(Checks.isNotNull(issue, "issue"));
        this.issuesHash = null;
        return this;
    }

    public SnapshotImpl addIssues(List<? extends Issue> issues) {
        this.issues.addAll(issues);
        this.issuesHash = null;
        return this;
    }

    @Override
    public Optional<Project> getProject() {
        return Optional.ofNullable(project);
    }

    @Override
    public Instant getTimestamp() {
        return timestamp;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Metas getMetas() {
        return metas;
    }

    @Override
    public Labels getLabels() {
        return labels;
    }

    @Override
    public List<Issue> getIssues() {
        return Collections.unmodifiableList(issues);
    }

    @Override
    public String getIssuesHash() {
        if (issuesHash == null) {
            issuesHash = IssueUtils.getHash(issues);
        }
        return issuesHash;
    }
}