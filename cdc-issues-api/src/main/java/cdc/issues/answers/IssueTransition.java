package cdc.issues.answers;

import java.util.EnumSet;
import java.util.Set;

/**
 * Enumeration of possible status/resolution transitions.
 *
 * @author Damien Carbonne
 */
public enum IssueTransition {
    CONFIRM(EnumSet.of(IssueStatus.OPEN,
                       IssueStatus.REOPENED,
                       IssueStatus.RESOLVED,
                       IssueStatus.WAITING),
            IssueStatus.CONFIRMED,
            IssueResolution.UNRESOLVED),
    WAIT(EnumSet.of(IssueStatus.CONFIRMED,
                    IssueStatus.OPEN,
                    IssueStatus.REOPENED,
                    IssueStatus.RESOLVED),
         IssueStatus.WAITING,
         IssueResolution.UNRESOLVED),
    RESOLVE_AS_FIXED(EnumSet.of(IssueStatus.CONFIRMED,
                                IssueStatus.OPEN,
                                IssueStatus.REOPENED,
                                IssueStatus.RESOLVED, // FIXED
                                IssueStatus.WAITING),
                     IssueStatus.RESOLVED,
                     IssueResolution.FIXED),
    RESOLVE_AS_WONT_FIX(EnumSet.of(IssueStatus.CONFIRMED,
                                   IssueStatus.OPEN,
                                   IssueStatus.REOPENED,
                                   IssueStatus.RESOLVED, // WON'T FIX
                                   IssueStatus.WAITING),
                        IssueStatus.RESOLVED,
                        IssueResolution.WONT_FIX),
    RESOLVE_AS_FALSE_POSITIVE(EnumSet.of(IssueStatus.CONFIRMED,
                                         IssueStatus.OPEN,
                                         IssueStatus.REOPENED,
                                         IssueStatus.RESOLVED, // WON'T FIX, FIXED
                                         IssueStatus.WAITING),
                              IssueStatus.RESOLVED,
                              IssueResolution.FALSE_POSITIVE),
    REOPEN(EnumSet.of(IssueStatus.CLOSED, // != UNRESOLVED
                      IssueStatus.RESOLVED), // FIXED
           IssueStatus.REOPENED,
           IssueResolution.UNRESOLVED),
    OPEN(EnumSet.noneOf(IssueStatus.class),
         IssueStatus.OPEN,
         IssueResolution.UNRESOLVED),
    CLOSE_AS_FIXED(EnumSet.of(IssueStatus.CONFIRMED,
                              IssueStatus.OPEN,
                              IssueStatus.REOPENED,
                              IssueStatus.RESOLVED, // FIXED
                              IssueStatus.WAITING),
                   IssueStatus.CLOSED,
                   IssueResolution.FIXED),
    CLOSE_AS_REMOVED(EnumSet.of(IssueStatus.CONFIRMED,
                                IssueStatus.OPEN,
                                IssueStatus.REOPENED,
                                IssueStatus.RESOLVED,
                                IssueStatus.WAITING),
                     IssueStatus.CLOSED,
                     IssueResolution.REMOVED),
    CLOSE_AS_WONT_FIX(EnumSet.of(IssueStatus.RESOLVED), // WON'T FIX
                      IssueStatus.CLOSED,
                      IssueResolution.WONT_FIX),
    CLOSE_AS_FALSE_POSITIVE(EnumSet.of(IssueStatus.RESOLVED), // FALSE POSITIVE
                            IssueStatus.CLOSED,
                            IssueResolution.FALSE_POSITIVE);

    private final Set<IssueStatus> sourceStatuses;
    private final IssueStatus targetStatus;
    private final IssueResolution targetResolution;

    private IssueTransition(Set<IssueStatus> sourceStatues,
                            IssueStatus targetStatus,
                            IssueResolution targetResolution) {
        this.sourceStatuses = sourceStatues;
        this.targetResolution = targetResolution;
        this.targetStatus = targetStatus;
    }

    /**
     * @return {@code true} if this transition can be triggered manually.
     */
    public boolean isManual() {
        return this == CONFIRM
                || this == REOPEN
                || this == RESOLVE_AS_FALSE_POSITIVE
                || this == RESOLVE_AS_FIXED
                || this == RESOLVE_AS_WONT_FIX
                || this == WAIT;

    }

    /**
     * @return {@code true} if this transition can be triggered automatically.
     */
    public boolean isAutomatic() {
        return this == CLOSE_AS_FALSE_POSITIVE
                || this == CLOSE_AS_FIXED
                || this == CLOSE_AS_REMOVED
                || this == CLOSE_AS_WONT_FIX
                || this == OPEN
                || this == REOPEN;
    }

    /**
     * Returns {@code true} if a (status, resolution) pair is a valid source
     * state to trigger this transition.
     *
     * @param status The status.
     * @param resolution The resolution.
     * @return {@code true} if {@code (status, resolution)} pair is a valid
     *         source state to trigger this transition
     */
    public boolean isValidSource(IssueStatus status,
                                 IssueResolution resolution) {
        if (status.isCompliantWith(resolution)
                && sourceStatuses.contains(status)) {
            // Additional checks
            switch (this) {
            case RESOLVE_AS_FIXED:
                if (status == IssueStatus.RESOLVED) {
                    return resolution != IssueResolution.FIXED;
                } else {
                    return true;
                }
            case RESOLVE_AS_WONT_FIX:
                if (status == IssueStatus.RESOLVED) {
                    return resolution != IssueResolution.WONT_FIX;
                } else {
                    return true;
                }
            case RESOLVE_AS_FALSE_POSITIVE:
                if (status == IssueStatus.RESOLVED) {
                    return resolution != IssueResolution.FALSE_POSITIVE;
                } else {
                    return true;
                }
            case REOPEN:
                if (status == IssueStatus.CLOSED) {
                    return resolution != IssueResolution.UNRESOLVED;
                } else if (status == IssueStatus.RESOLVED) {
                    return resolution == IssueResolution.FIXED;
                } else {
                    return true;
                }
            case CLOSE_AS_FIXED:
                if (status == IssueStatus.RESOLVED) {
                    return resolution == IssueResolution.FIXED;
                } else {
                    return true;
                }
            case CLOSE_AS_WONT_FIX:
                if (status == IssueStatus.RESOLVED) {
                    return resolution == IssueResolution.WONT_FIX;
                } else {
                    return true;
                }
            case CLOSE_AS_FALSE_POSITIVE:
                if (status == IssueStatus.RESOLVED) {
                    return resolution == IssueResolution.FALSE_POSITIVE;
                } else {
                    return true;
                }
            default:
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @return The status reached after applying this transition.
     */
    public IssueStatus getTargetStatus() {
        return targetStatus;
    }

    /**
     * @return The resolution reached after applying this transition.
     */
    public IssueResolution getTargetResolution() {
        return targetResolution;
    }

    /**
     * Returns a set of transitions that can be applied from a
     * given starting (status, resolution) point.
     *
     * @param status The starting status.
     * @param resolution The starting resolution.
     * @return The set of transition that can be applied from
     *         ({@code status}, {@code resolution}).
     */
    public static Set<IssueTransition> getTransitionsFrom(IssueStatus status,
                                                          IssueResolution resolution) {
        final Set<IssueTransition> set = EnumSet.noneOf(IssueTransition.class);
        for (final IssueTransition transition : IssueTransition.values()) {
            if (transition.isValidSource(status, resolution)) {
                set.add(transition);
            }
        }
        return set;
    }
}