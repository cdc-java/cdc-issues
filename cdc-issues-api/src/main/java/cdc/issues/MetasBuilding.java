package cdc.issues;

import java.util.Collection;

/**
 * Interface implemented by builders that have {@link Metas}.
 *
 * @param <B> The builder class.
 */
public interface MetasBuilding<B> {
    /**
     * @return This builder.
     */
    public B self();

    /**
     * Adds a new or replaces an existing (name, value) pair.
     *
     * @param name The meta name.
     * @param value The meta value.
     * @return This builder.
     */
    public B meta(String name,
                  String value);

    public default B meta(Meta meta) {
        return meta(meta.getName(), meta.getValue());
    }

    /**
     * Adds a new or modifies an existing (name, value) pair.
     * <p>
     * If no value is associated to {@code name}, behaves like meta(name, value).
     * Otherwise, modify the value associated to {@code name} by appending {@code separator} and {@code value}.
     *
     * @param name The meta name.
     * @param value The meta value.
     * @param separator The separator.
     * @return This builder.
     */
    public B meta(String name,
                  String value,
                  String separator);

    public default B meta(Meta meta,
                          String separator) {
        return meta(meta.getName(), meta.getValue(), separator);
    }

    /**
     * Adds a new or replaces an existing (name, value) pair if {@code value} is not {@code null}.
     *
     * @param name The meta name.
     * @param value The meta value.
     * @return This builder.
     */
    public default B metaIfNotNull(String name,
                                   String value) {
        if (value != null) {
            return meta(name, value);
        } else {
            return self();
        }
    }

    /**
     * Adds new or replaces existing (name, value) pairs.
     *
     * @param metas The meta data.
     * @return This builder.
     */
    public default B metas(Metas metas) {
        for (final String name : metas.getNames()) {
            meta(name, metas.getValue(name));
        }
        return self();
    }

    /**
     * Adds new or replaces existing (name, value) pairs.
     *
     * @param metas The meta data.
     * @return This builder.
     */
    public default B metas(Collection<Meta> metas) {
        for (final Meta meta : metas) {
            meta(meta.getName(), meta.getValue());
        }
        return self();
    }
}