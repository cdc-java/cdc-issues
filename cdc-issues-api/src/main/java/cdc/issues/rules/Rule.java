package cdc.issues.rules;

import java.util.Comparator;
import java.util.Objects;

import cdc.issues.FormalParams;
import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;
import cdc.issues.Labels;
import cdc.issues.LabelsBuilding;
import cdc.issues.Metas;
import cdc.issues.MetasBuilding;
import cdc.util.lang.Checks;

/**
 * Class used to describe a Rule.
 *
 * @author Damien Carbonne
 */
public class Rule {
    private static final String DESCRIPTION = "description";
    private static final String DOMAIN = "domain";
    private static final String LABELS = "labels";
    private static final String NAME = "name";
    private static final String PARAMS = "params";
    private static final String SEVERITY = "severity";

    private final RuleId id;
    private final String title;
    private final IssueSeverity severity;
    private final String description;
    private final FormalParams params;
    private final Metas metas;
    private final Labels labels;

    /**
     * Comparator of Rules based on their id (domain, name).
     */
    public static final Comparator<Rule> ID_COMPARATOR =
            Comparator.comparing(Rule::getId);

    private static String denullify(String s) {
        return s == null ? "" : s;
    }

    /**
     * Comparator of Rules based on their domain, title and name.
     */
    public static final Comparator<Rule> DOMAIN_TITLE_NAME_COMPARATOR =
            Comparator.comparing(Rule::getDomain)
                      .thenComparing(r -> denullify(r.getTitle()))
                      .thenComparing(r -> r.getId().getName());

    protected Rule(Builder builder) {
        this.id = new RuleId(builder.domain, builder.name);
        this.title = builder.title;
        this.severity = Checks.isNotNull(builder.severity, SEVERITY);
        this.description = Checks.isNotNull(builder.description, DESCRIPTION);
        this.params = Checks.isNotNull(builder.params, PARAMS);
        this.metas = builder.metas.build();
        this.labels = Checks.isNotNull(builder.labels, LABELS);
    }

    /**
     * @return The rule identifier.
     */
    public RuleId getId() {
        return id;
    }

    /**
     * @return The rule domain.
     */
    public String getDomain() {
        return id.getDomain();
    }

    /**
     * @return The rule name.
     */
    public String getName() {
        return id.getName();
    }

    /**
     * @param <T> The enum type.
     * @param cls The enum class.
     * @return The rule name as an enum.
     */
    public <T extends Enum<T>> T getName(Class<T> cls) {
        return id.getName(cls);
    }

    /**
     * @return The rule title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return The default rule severity.
     */
    public IssueSeverity getSeverity() {
        return severity;
    }

    /**
     * @return The rule description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return The rule formal parameters.
     */
    public FormalParams getParams() {
        return params;
    }

    /**
     * @return The rule meta data.
     */
    public Metas getMetas() {
        return metas;
    }

    /**
     * @return The rule labels.
     */
    public Labels getLabels() {
        return labels;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            title,
                            severity,
                            description,
                            params,
                            metas,
                            labels);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Rule)) {
            return false;
        }
        final Rule other = (Rule) object;
        return Objects.equals(this.id, other.id)
                && Objects.equals(this.title, other.title)
                && Objects.equals(this.severity, other.severity)
                && Objects.equals(this.description, other.description)
                && Objects.equals(this.params, other.params)
                && Objects.equals(this.metas, other.metas)
                && Objects.equals(this.labels, other.labels);
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Rule builder.
     *
     * @author Damien Carbonne
     */
    public static class Builder implements MetasBuilding<Builder>, LabelsBuilding<Builder> {
        private String domain;
        private String name;
        private String title;
        private IssueSeverity severity;
        private String description = "";
        private FormalParams params = FormalParams.NO_PARAMS;
        private final Metas.Builder metas = Metas.builder();
        private Labels labels = Labels.NO_LABELS;

        protected Builder() {
        }

        @Override
        public Builder self() {
            return this;
        }

        /**
         * Sets the rule id.
         *
         * @param id The id.
         * @return This builder.
         */
        public Builder id(RuleId id) {
            this.domain = id.getDomain();
            this.name = id.getName();
            return this;
        }

        /**
         * Sets the rule domain.
         *
         * @param domain The rule domain.
         * @return This builder.
         */
        public Builder domain(String domain) {
            Checks.isNotNull(domain, DOMAIN);

            this.domain = domain;
            return this;
        }

        /**
         * Sets the rule name.
         *
         * @param name The rule name.
         * @return This builder.
         */
        public Builder name(String name) {
            Checks.isNotNull(name, NAME);

            this.name = name;
            return this;
        }

        /**
         * Sets the rule title.
         *
         * @param title The rule title.
         * @return This builder.
         */
        public Builder title(String title) {
            this.title = title;
            return this;
        }

        /**
         * Sets the rule name and its severity if the {@code name} implements
         * {@link IssueSeverityItem} and current severity is {@code null}.
         *
         * @param name The rule name.
         * @return This builder.
         */
        public Builder name(Enum<?> name) {
            Checks.isNotNull(name, NAME);

            this.name = name.name();
            if (severity == null && name instanceof final IssueSeverityItem isi) {
                this.severity = isi.getSeverity();
            }
            return this;
        }

        /**
         * Sets the default severity of the rule.
         *
         * @param severity The rule severity.
         * @return This builder.
         */
        public Builder severity(IssueSeverity severity) {
            Checks.isNotNull(severity, SEVERITY);

            this.severity = severity;
            return this;
        }

        /**
         * Sets the rule description.
         *
         * @param description The rule description.
         * @return This builder.
         */
        public Builder description(String description) {
            Checks.isNotNull(description, DESCRIPTION);

            this.description = description;
            return this;
        }

        /**
         * Sets the rule formal parameters.
         *
         * @param params The rule formal parameters.
         * @return This builder.
         */
        public Builder params(FormalParams params) {
            Checks.isNotNull(params, PARAMS);

            this.params = params;
            return this;
        }

        @Override
        public Builder meta(String name,
                            String value) {
            this.metas.meta(name, value);
            return self();
        }

        @Override
        public Builder meta(String name,
                            String value,
                            String separator) {
            this.metas.meta(name, value, separator);
            return self();
        }

        @Override
        public Builder metas(Metas metas) {
            this.metas.metas(metas);
            return self();
        }

        @Override
        public Builder labels(Labels labels) {
            Checks.isNotNull(labels, LABELS);

            this.labels = labels;
            return this;
        }

        public Rule build() {
            return new Rule(this);
        }
    }
}