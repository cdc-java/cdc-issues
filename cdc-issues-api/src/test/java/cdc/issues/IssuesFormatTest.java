package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertSame;

import java.io.File;

import org.junit.jupiter.api.Test;

import cdc.issues.io.IssuesFormat;

class IssuesFormatTest {
    @Test
    void test() {
        for (final IssuesFormat format : IssuesFormat.values()) {
            final File file = new File("file." + format);
            assertSame(format, IssuesFormat.from(file));
        }
        assertSame(null, IssuesFormat.from(new File("hello")));

        // Coverage
        for (final IssuesFormat format : IssuesFormat.values()) {
            format.isExportFormat();
            format.isImportFormat();
        }
    }
}