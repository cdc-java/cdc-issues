package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class FormalParamsTest {
    @Test
    void test() {
        assertTrue(FormalParams.NO_PARAMS.isEmpty());
        final FormalParams params1 = FormalParams.builder()
                                                 .param("name1", "value1")
                                                 .param("name2", "value2")
                                                 .build();
        final FormalParams params2 = FormalParams.builder()
                                                 .param("name2", "value2")
                                                 .param("name1", "value1")
                                                 .build();
        assertSame(2, params1.getNames().size());
        assertEquals(params1, params2);
        assertEquals("value1", params1.getDescription("name1"));
        assertEquals("value1", params2.getDescription("name1"));
        assertEquals(params1.getNames(), params2.getNames());
        assertEquals(params1.getSortedNames(), params2.getSortedNames());
        assertEquals(params1, params1);
        assertNotEquals(params1, null);
        assertNotEquals(params1, "Hello");
        assertEquals(params1.toString(), params2.toString());
        assertEquals(params1.hashCode(), params2.hashCode());
    }
}