package cdc.issues;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementation of {@link IssuesHandler} that logs issues and delegates them to another handler..
 *
 * @author Damien Carbonne
 */
public class VerboseIssuesHandler implements IssuesHandler {
    private static final Logger LOGGER = LogManager.getLogger(VerboseIssuesHandler.class);
    private final IssuesHandler delegate;

    public VerboseIssuesHandler(IssuesHandler delegate) {
        this.delegate = delegate;
    }

    @Override
    public void issue(Issue issue) {
        if (issue.getSeverity() == IssueSeverity.INFO) {
            LOGGER.info(issue);
        } else if (issue.getSeverity() == IssueSeverity.MINOR || issue.getSeverity() == IssueSeverity.MAJOR) {
            LOGGER.warn(issue);
        } else if (issue.getSeverity() == IssueSeverity.CRITICAL) {
            LOGGER.error(issue);
        } else if (issue.getSeverity() == IssueSeverity.BLOCKER) {
            LOGGER.fatal(issue);
        }
        delegate.issue(issue);
    }
}