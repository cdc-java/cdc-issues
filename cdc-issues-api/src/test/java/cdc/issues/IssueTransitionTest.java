package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssueTransition;

class IssueTransitionTest {
    @Test
    void testGetTargetResolution() {
        assertSame(IssueResolution.FALSE_POSITIVE, IssueTransition.CLOSE_AS_FALSE_POSITIVE.getTargetResolution());
        assertSame(IssueResolution.FIXED, IssueTransition.CLOSE_AS_FIXED.getTargetResolution());
        assertSame(IssueResolution.REMOVED, IssueTransition.CLOSE_AS_REMOVED.getTargetResolution());
        assertSame(IssueResolution.WONT_FIX, IssueTransition.CLOSE_AS_WONT_FIX.getTargetResolution());
        assertSame(IssueResolution.UNRESOLVED, IssueTransition.CONFIRM.getTargetResolution());
        assertSame(IssueResolution.UNRESOLVED, IssueTransition.OPEN.getTargetResolution());
        assertSame(IssueResolution.UNRESOLVED, IssueTransition.REOPEN.getTargetResolution());
        assertSame(IssueResolution.FALSE_POSITIVE, IssueTransition.RESOLVE_AS_FALSE_POSITIVE.getTargetResolution());
        assertSame(IssueResolution.FIXED, IssueTransition.RESOLVE_AS_FIXED.getTargetResolution());
        assertSame(IssueResolution.WONT_FIX, IssueTransition.RESOLVE_AS_WONT_FIX.getTargetResolution());
        assertSame(IssueResolution.UNRESOLVED, IssueTransition.WAIT.getTargetResolution());
    }

    @Test
    void testGetTargetStatus() {
        assertSame(IssueStatus.CLOSED, IssueTransition.CLOSE_AS_FALSE_POSITIVE.getTargetStatus());
        assertSame(IssueStatus.CLOSED, IssueTransition.CLOSE_AS_FIXED.getTargetStatus());
        assertSame(IssueStatus.CLOSED, IssueTransition.CLOSE_AS_REMOVED.getTargetStatus());
        assertSame(IssueStatus.CLOSED, IssueTransition.CLOSE_AS_WONT_FIX.getTargetStatus());
        assertSame(IssueStatus.CONFIRMED, IssueTransition.CONFIRM.getTargetStatus());
        assertSame(IssueStatus.OPEN, IssueTransition.OPEN.getTargetStatus());
        assertSame(IssueStatus.REOPENED, IssueTransition.REOPEN.getTargetStatus());
        assertSame(IssueStatus.RESOLVED, IssueTransition.RESOLVE_AS_FALSE_POSITIVE.getTargetStatus());
        assertSame(IssueStatus.RESOLVED, IssueTransition.RESOLVE_AS_FIXED.getTargetStatus());
        assertSame(IssueStatus.RESOLVED, IssueTransition.RESOLVE_AS_WONT_FIX.getTargetStatus());
        assertSame(IssueStatus.WAITING, IssueTransition.WAIT.getTargetStatus());
    }

    private static void checkIsValidSource(boolean expected,
                                           IssueTransition transition,
                                           IssueStatus status,
                                           IssueResolution resolution) {
        assertSame(expected, transition.isValidSource(status, resolution), transition + " " + status + " " + resolution);
    }

    @Test
    void testIsValidSourceCloseAsFalsePositive() {
        final IssueTransition transition = IssueTransition.CLOSE_AS_FALSE_POSITIVE;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceCloseAsWontFix() {
        final IssueTransition transition = IssueTransition.CLOSE_AS_WONT_FIX;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceCloseAsFixed() {
        final IssueTransition transition = IssueTransition.CLOSE_AS_FIXED;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceCloseAsRemoved() {
        final IssueTransition transition = IssueTransition.CLOSE_AS_REMOVED;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceResolveAsFalsePositive() {
        final IssueTransition transition = IssueTransition.RESOLVE_AS_FALSE_POSITIVE;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceResolveAsWontFix() {
        final IssueTransition transition = IssueTransition.RESOLVE_AS_WONT_FIX;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceResolveAsFixed() {
        final IssueTransition transition = IssueTransition.RESOLVE_AS_FIXED;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceConfirm() {
        final IssueTransition transition = IssueTransition.CONFIRM;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.WAITING && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceWait() {
        final IssueTransition transition = IssueTransition.WAIT;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.WONT_FIX
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FALSE_POSITIVE
                        || status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.CONFIRMED && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.OPEN && resolution == IssueResolution.UNRESOLVED
                        || status == IssueStatus.REOPENED && resolution == IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }

    @Test
    void testIsValidSourceOpen() {
        final IssueTransition transition = IssueTransition.OPEN;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                checkIsValidSource(false, transition, status, resolution);
            }
        }
    }

    @Test
    void testIsValidSourceReopen() {
        final IssueTransition transition = IssueTransition.REOPEN;
        for (final IssueStatus status : IssueStatus.values()) {
            for (final IssueResolution resolution : IssueResolution.values()) {
                if (status == IssueStatus.RESOLVED && resolution == IssueResolution.FIXED
                        || status == IssueStatus.CLOSED && resolution != IssueResolution.UNRESOLVED) {
                    checkIsValidSource(true, transition, status, resolution);
                } else {
                    checkIsValidSource(false, transition, status, resolution);
                }
            }
        }
    }
}