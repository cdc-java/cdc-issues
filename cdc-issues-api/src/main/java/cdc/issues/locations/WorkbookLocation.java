package cdc.issues.locations;

import cdc.util.lang.Checks;

/**
 * Implementation of Location dedicated to workbooks.
 * <p>
 * It can be used to designate:
 * <ul>
 * <li>A workbook: (systemId),
 * <li>A sheet: (systemId, sheetName),
 * <li>A column: (systemId, sheetName, columnName),
 * <li>A row: (systemId, sheetName, rowNumber) or (systemId, sheetName, rowId),
 * <li>Or a cell: (systemId, sheetName, columnName, rowNumber)
 * or (systemId, sheetName, columnName, rowId).
 * </ul>
 * <b>Note:</b> A row can either be designated using a number
 * or a functional id, but not using both solutions.<br>
 * <b>Note:</b> A valid row number is {@code > 0}.
 *
 * @author Damien Carbonne
 */
public class WorkbookLocation extends AbstractLocation {
    private final String systemId;
    private final String sheetName;
    private final int rowNumber;
    private final String rowId;
    private final String columnName;

    public static final String TAG = "WorkbookLocation";

    private static final String OPEN = "[";
    private static final String SEP = ":";
    private static final String CLOSE = "]";

    protected WorkbookLocation(Builder builder) {
        this.systemId = Checks.isNotNull(builder.systemId, "systemId");
        this.sheetName = builder.sheetName;
        this.rowNumber = builder.rowNumber;
        this.rowId = builder.rowId;
        this.columnName = builder.columnName;
        if (builder.rowNumber > 0 && builder.rowId != null) {
            throw new IllegalArgumentException("Row number and id can not be both defined.");
        }
        if ((builder.rowNumber > 0 || builder.rowId != null || builder.columnName != null) && builder.sheetName == null) {
            throw new IllegalArgumentException("Sheet name is missing.");
        }
    }

    public static WorkbookLocation create(String systemId,
                                          String anchor) {
        return builder().systemId(systemId)
                        .anchor(anchor)
                        .build();
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return getSystemId();
    }

    @Override
    public String getAnchor() {
        if (hasSheetData()) {
            if (hasColumnData()) {
                if (hasRowIdData()) {
                    return sheetName + OPEN + columnName + SEP + rowId + CLOSE;
                } else if (hasRowNumberData()) {
                    return sheetName + OPEN + columnName + SEP + rowNumber + CLOSE;
                } else {
                    return sheetName + OPEN + columnName + CLOSE;
                }
            } else {
                if (hasRowIdData()) {
                    return sheetName + OPEN + SEP + rowId + CLOSE;

                } else if (hasRowNumberData()) {
                    return sheetName + OPEN + SEP + rowNumber + CLOSE;
                } else {
                    return sheetName;
                }
            }
        } else {
            return "";
        }
    }

    /**
     * @return The system id of this location.
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @return The sheet name, possibly {@code null}, of this location.
     */
    public String getSheetName() {
        return sheetName;
    }

    /**
     * @return The row number, possibly {@code <= 0}, of this location.
     */
    public int getRowNumber() {
        return rowNumber;
    }

    /**
     * @return The row id, possibly {@code null}, of this location.
     */
    public String getRowId() {
        return rowId;
    }

    /**
     * @return The column name, possibly {@code null}, of this location.
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @return {@code true} if this location contains sheet data.
     */
    public boolean hasSheetData() {
        return sheetName != null;
    }

    /**
     * @return {@code true} if this location contains row number data.
     */
    public boolean hasRowNumberData() {
        return rowNumber > 0;
    }

    /**
     * @return {@code true} if this location contains row id data.
     */
    public boolean hasRowIdData() {
        return rowId != null;
    }

    /**
     * @return {@code true} if this location contains row data.
     */
    public boolean hasRowData() {
        return hasRowNumberData() || hasRowIdData();
    }

    /**
     * @return {@code true} if this location contains column data.
     */
    public boolean hasColumnData() {
        return columnName != null;
    }

    /**
     * @return {@code true} if this location designates a workbook.
     */
    public boolean isWorkbookLocation() {
        return !hasSheetData() && !hasRowData() && !hasColumnData();
    }

    /**
     * @return {@code true} if this location designates a sheet.
     */
    public boolean isSheetLocation() {
        return hasSheetData() && !hasRowData() && !hasColumnData();
    }

    /**
     * @return {@code true} if this location designates a column.
     */
    public boolean isColumnLocation() {
        return !hasRowData() && hasColumnData();
    }

    /**
     * @return {@code true} if this location designates a row.
     */
    public boolean isRowLocation() {
        return hasRowData() && !hasColumnData();
    }

    /**
     * @return {@code true} if this location designates a cell.
     */
    public boolean isCellLocation() {
        return hasRowData() && hasColumnData();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String systemId;
        private String sheetName = null;
        private int rowNumber = 0;
        private String rowId = null;
        private String columnName = null;

        protected Builder() {
            super();
        }

        public Builder systemId(String systemId) {
            this.systemId = systemId;
            return this;
        }

        public Builder sheetName(String sheetName) {
            this.sheetName = sheetName;
            return this;
        }

        public Builder rowNumber(int rowNumber) {
            this.rowNumber = rowNumber;
            return this;
        }

        public Builder rowId(String rowId) {
            this.rowId = rowId;
            return this;
        }

        public Builder columnName(String columnName) {
            this.columnName = columnName;
            return this;
        }

        public Builder anchor(String anchor) {
            if (anchor == null) {
                this.sheetName = anchor;
                this.rowNumber = 0;
                this.rowId = null;
                this.columnName = null;
            } else {
                final int posOpen = anchor.indexOf(OPEN);
                if (posOpen >= 0) {
                    this.sheetName = anchor.substring(0, posOpen);
                    final int posSep = anchor.indexOf(SEP, posOpen + 1);
                    final int posClose = anchor.indexOf(CLOSE, posOpen + 1);
                    if (posSep >= 0) {
                        // Either row number or row id
                        final String r = anchor.substring(posSep + 1, posClose);
                        try {
                            this.rowNumber = Integer.parseInt(r);
                            this.rowId = null;
                        } catch (final NumberFormatException e) {
                            this.rowNumber = 0;
                            this.rowId = r;
                        }
                        if (posSep == posOpen + 1) {
                            // No column data
                            this.columnName = null;
                        } else {
                            this.columnName = anchor.substring(posOpen + 1, posSep);
                        }
                    } else {
                        this.rowNumber = 0;
                        this.rowId = null;
                        this.columnName = anchor.substring(posOpen + 1, posClose);
                    }
                } else {
                    this.sheetName = anchor;
                    this.rowNumber = 0;
                    this.rowId = null;
                    this.columnName = null;
                }
            }
            return this;
        }

        public WorkbookLocation build() {
            return new WorkbookLocation(this);
        }
    }
}