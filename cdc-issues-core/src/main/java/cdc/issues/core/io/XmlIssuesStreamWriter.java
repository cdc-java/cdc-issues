package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

import cdc.io.xml.XmlWriter;
import cdc.issues.Issue;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssueComment;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesStreamWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.SnapshotData;
import cdc.issues.locations.Location;
import cdc.issues.rules.Profile;
import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link IssuesStreamWriter} dedicated to XML.
 *
 * @author Damien Carbonne
 */
public class XmlIssuesStreamWriter extends IssuesIo implements IssuesStreamWriter {
    private final File file;
    private final OutputStream out;
    private final OutSettings settings;
    private final XmlWriter writer;
    private StreamStatus status = StreamStatus.INIT;
    private boolean hasInfos = false;

    private RuntimeException unexpectedStatus(String message) {
        throw new IllegalStateException("Unexpected status: " + status + ": " + message);
    }

    private XmlIssuesStreamWriter(File file,
                                  OutputStream out,
                                  IssuesFormat format,
                                  OutSettings settings,
                                  IssuesIoFactoryFeatures features)
            throws IOException {
        super(features);
        this.file = file;
        this.out = out;
        this.settings = settings;
        this.writer = createWriter();
    }

    public XmlIssuesStreamWriter(File file,
                                 OutSettings settings,
                                 IssuesIoFactoryFeatures features)
            throws IOException {
        this(file, null, null, settings, features);
    }

    public XmlIssuesStreamWriter(OutputStream out,
                                 IssuesFormat format,
                                 OutSettings settings,
                                 IssuesIoFactoryFeatures features)
            throws IOException {
        this(null, out, format, settings, features);
    }

    private XmlWriter createWriter() throws IOException {
        final XmlWriter w;
        if (file == null) {
            final Charset charset = features.getWorkbookWriterFeatures().getCharset();
            if (charset == null) {
                w = new XmlWriter(out);
            } else {
                w = new XmlWriter(out, charset.name());
            }
        } else {
            w = new XmlWriter(file);
        }
        if (features.isEnabled(IssuesIoFactoryFeatures.Hint.PRETTY_PRINT)) {
            w.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        }
        return w;
    }

    private void saveProjectAndProfile(SnapshotData snapshot) throws IOException {
        final Profile profile = snapshot.getProfile().orElse(null);

        writer.beginElement(XmlIo.PROJECT);
        writer.addAttribute(XmlIo.NAME, snapshot.getProjectName());
        writer.addElement(XmlIo.DESCRIPTION, snapshot.getProjectDescription());
        XmlIo.writeMetas(writer, snapshot.getProjectMetas());
        XmlIo.writeLabels(writer, snapshot.getProjectLabels());
        XmlIo.writeProfile(writer, profile);
        writer.endElement(); // PROJECT
    }

    @Override
    public void startDocument() throws IOException {
        if (status != StreamStatus.INIT) {
            throw unexpectedStatus("calling startDocument");
        }
        status = StreamStatus.DOCUMENT;
        writer.beginDocument();
    }

    @Override
    public void add(SnapshotData snapshot) throws IOException {
        if (status != StreamStatus.DOCUMENT) {
            throw unexpectedStatus("calling add'SnapshotData");
        }
        status = StreamStatus.INFOS;
        writer.beginElement(XmlIo.PROJECT_SNAPSHOT);
        saveProjectAndProfile(snapshot);

        writer.beginElement(XmlIo.SNAPSHOT);
        writer.addAttribute(XmlIo.NAME, snapshot.getSnapshotName());
        writer.addAttribute(XmlIo.TIMESTAMP, toString(snapshot.getSnapshotTimestamp()));
        writer.addAttribute(XmlIo.ISSUES_HASH, snapshot.getIssuesHash());
        writer.addElement(XmlIo.DESCRIPTION, snapshot.getSnapshotDescription());
        XmlIo.writeMetas(writer, snapshot.getSnapshotMetas());
        XmlIo.writeLabels(writer, snapshot.getSnapshotLabels());

        writer.beginElement(XmlIo.ISSUES);
        status = StreamStatus.SNAPSHOT;
        hasInfos = true;
    }

    @Override
    public void add(Issue issue,
                    IssueAnswer answer) throws IOException {
        if (status == StreamStatus.DOCUMENT) {
            // No info section
            // The document only contains issues
            writer.beginElement(XmlIo.ISSUES);
            status = StreamStatus.SNAPSHOT;
        } else if (status == StreamStatus.SNAPSHOT) {
            // Ignore
        } else {
            throw unexpectedStatus("calling add'Issue");
        }

        writer.beginElement(XmlIo.ISSUE);
        writer.addAttribute(XmlIo.TIMESTAMP, toString(issue.getTimestamp()));
        if (!settings.isEnabled(OutSettings.Hint.NO_ID_HASH_COL)) {
            writer.addAttribute(XmlIo.ID_HASH, issue.getIdHash());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_DOMAIN_COL) && !StringUtils.isNullOrEmpty(issue.getDomain())) {
            writer.addAttribute(XmlIo.DOMAIN, issue.getDomain());
        }
        writer.addAttribute(XmlIo.NAME, issue.getName());
        if (!StringUtils.isNullOrEmpty(issue.getTitle())) {
            writer.addAttribute(XmlIo.TITLE, issue.getTitle());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_PROJECT_COL) && !StringUtils.isNullOrEmpty(issue.getProject())) {
            writer.addAttribute(XmlIo.PROJECT, issue.getProject());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_SNAPSHOT_COL) && !StringUtils.isNullOrEmpty(issue.getSnapshot())) {
            writer.addAttribute(XmlIo.SNAPSHOT, issue.getSnapshot());
        }
        writer.addAttribute(XmlIo.SEVERITY, issue.getSeverity().name());

        writer.addElement(XmlIo.DESCRIPTION, issue.getDescription());

        if (!settings.isEnabled(OutSettings.Hint.NO_PARAMS_COL)) {
            XmlIo.writeParams(writer, issue.getParams());
        }

        // Locations
        writer.beginElement(XmlIo.LOCATIONS);
        for (final Location location : issue.getLocations()) {
            writer.beginElement(XmlIo.LOCATION);
            writer.addAttribute(XmlIo.TAG, location.getTag());
            writer.addAttribute(XmlIo.PATH, location.getPath());
            if (location.hasAnchor()) {
                writer.addAttribute(XmlIo.ANCHOR, location.getAnchor());
            }
            writer.endElement();
        }
        writer.endElement();

        // Meta
        if (!settings.isEnabled(OutSettings.Hint.NO_METAS_COL) && !issue.getMetas().isEmpty()) {
            XmlIo.writeMetas(writer, issue.getMetas());
        }

        // Labels
        if (!settings.isEnabled(OutSettings.Hint.NO_LABELS_COL) && !issue.getLabels().isEmpty()) {
            XmlIo.writeLabels(writer, issue.getLabels());
        }

        // Answer
        if (answer != null && !settings.isEnabled(OutSettings.Hint.NO_ANSWERS)) {
            writer.beginElement(XmlIo.ANSWER);
            if (!StringUtils.isNullOrEmpty(answer.getAuthor())) {
                writer.addAttribute(XmlIo.AUTHOR, answer.getAuthor());
            }
            if (answer.getCreationDate() != null) {
                writer.addAttribute(XmlIo.CREATED, toString(answer.getCreationDate()));
            }
            if (answer.getModificationDate() != null) {
                writer.addAttribute(XmlIo.MODIFIED, toString(answer.getModificationDate()));
            }
            writer.addAttribute(XmlIo.STATUS, answer.getStatus());
            if (answer.getResolution() != null) {
                writer.addAttribute(XmlIo.RESOLUTION, answer.getResolution());
            }
            if (!StringUtils.isNullOrEmpty(answer.getAssignee())) {
                writer.addAttribute(XmlIo.ASSIGNEE, answer.getAssignee());
            }
            if (answer.getNewSeverity() != null) {
                writer.addAttribute(XmlIo.NEW_SEVERITY, answer.getNewSeverity());
            }
            final List<? extends IssueComment> comments = answer.getComments();
            if (comments != null && !comments.isEmpty()) {
                writer.beginElement(XmlIo.COMMENTS);
                for (final IssueComment comment : comments) {
                    writer.beginElement(XmlIo.COMMENT);
                    if (!StringUtils.isNullOrEmpty(comment.getAuthor())) {
                        writer.addAttribute(XmlIo.AUTHOR, comment.getAuthor());
                    }
                    if (comment.getDate() != null) {
                        writer.addAttribute(XmlIo.DATE, toString(comment.getDate()));
                    }
                    writer.addElementContent(comment.getText());
                    writer.endElement();
                }
                writer.endElement();
            }

            if (!answer.getMetas().isEmpty()) {
                XmlIo.writeMetas(writer, answer.getMetas());
            }
            if (!answer.getLabels().isEmpty()) {
                XmlIo.writeLabels(writer, answer.getLabels());
            }

            writer.endElement();
        }
        writer.endElement();
    }

    @Override
    public void endDocument() throws IOException {
        if (status == StreamStatus.DOCUMENT) {
            // startDocument was called, and nothing after that
            // no infos, no issues
            writer.beginElement(XmlIo.ISSUES); // ISSUES
            writer.endElement(); // ISSUES
            writer.endDocument();
            status = StreamStatus.END;
        } else if (status == StreamStatus.SNAPSHOT) {
            if (hasInfos) {
                // infos, maybe issues
                writer.endElement(); // ISSUES
                writer.endElement(); // SNAPSHOT
                writer.endElement(); // PROJECT_SNAPSHOT
                writer.endDocument();
            } else {
                // no infos, issues
                writer.endElement(); // ISSUES
                writer.endDocument();
            }
            status = StreamStatus.END;
        } else {
            throw unexpectedStatus("calling endDocument");
        }
    }

    @Override
    public void flush() throws IOException {
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}