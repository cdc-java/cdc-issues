package cdc.issues;

import java.util.Spliterator;

import cdc.issues.locations.LocatedObject;

public class DataSource<T> {
    private final Class<T> dataClass;
    private final Spliterator<LocatedObject<T>> spliterator;

    public DataSource(Class<T> dataClass,
                      Spliterator<LocatedObject<T>> spliterator) {
        this.dataClass = dataClass;
        this.spliterator = spliterator;
    }

    public Class<T> getDataClass() {
        return dataClass;
    }

    public Spliterator<LocatedObject<T>> getSpliterator() {
        return spliterator;
    }
}