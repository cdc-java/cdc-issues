package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.DefaultLocation;

class DefaultLocationTest {
    @Test
    void test() {
        final DefaultLocation location0a = DefaultLocation.builder()
                                                          .path("PATH")
                                                          .build();
        final DefaultLocation location0b = new DefaultLocation("PATH");
        final DefaultLocation location1a = DefaultLocation.builder()
                                                          .path("path")
                                                          .anchor("anchor")
                                                          .build();

        final DefaultLocation location1b = DefaultLocation.builder()
                                                          .path("path")
                                                          .anchor("anchor")
                                                          .build();
        final DefaultLocation location2a = DefaultLocation.builder()
                                                          .path("PATH")
                                                          .anchor("anchor")
                                                          .build();

        final DefaultLocation location2b = new DefaultLocation(location2a);

        assertEquals("PATH", location0a.getPath());
        assertEquals(null, location0a.getAnchor());

        assertEquals("path", location1a.getPath());
        assertEquals("anchor", location1a.getAnchor());
        assertNotEquals(location1a, location0a);
        assertNotEquals(location0a, location1a);
        assertNotEquals(location0a, location2a);
        assertNotEquals(location1a, location2a);
        assertNotEquals(location2a, "Hello");
        assertEquals(location0a, location0b);
        assertEquals(location1a, location1a);
        assertEquals(location1a, location1b);
        assertEquals(location2a, location2b);

        assertEquals(location0a.hashCode(), location0b.hashCode());
        assertEquals(location0a.toString(), location0b.toString());
        assertEquals(location1a.toString(), location1b.toString());

        assertFalse(location0a.hasAnchor());
        assertTrue(location1a.hasAnchor());
    }
}
