package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class IssueSeverityTest {
    @Test
    void testIsAtLeast() {
        assertTrue(IssueSeverity.BLOCKER.isAtLeast(IssueSeverity.BLOCKER));
        assertTrue(IssueSeverity.BLOCKER.isAtLeast(IssueSeverity.CRITICAL));
        assertTrue(IssueSeverity.BLOCKER.isAtLeast(IssueSeverity.MAJOR));
        assertTrue(IssueSeverity.BLOCKER.isAtLeast(IssueSeverity.MINOR));
        assertTrue(IssueSeverity.BLOCKER.isAtLeast(IssueSeverity.INFO));

        assertFalse(IssueSeverity.CRITICAL.isAtLeast(IssueSeverity.BLOCKER));
        assertTrue(IssueSeverity.CRITICAL.isAtLeast(IssueSeverity.CRITICAL));
        assertTrue(IssueSeverity.CRITICAL.isAtLeast(IssueSeverity.MAJOR));
        assertTrue(IssueSeverity.CRITICAL.isAtLeast(IssueSeverity.MINOR));
        assertTrue(IssueSeverity.CRITICAL.isAtLeast(IssueSeverity.INFO));

        assertFalse(IssueSeverity.MAJOR.isAtLeast(IssueSeverity.BLOCKER));
        assertFalse(IssueSeverity.MAJOR.isAtLeast(IssueSeverity.CRITICAL));
        assertTrue(IssueSeverity.MAJOR.isAtLeast(IssueSeverity.MAJOR));
        assertTrue(IssueSeverity.MAJOR.isAtLeast(IssueSeverity.MINOR));
        assertTrue(IssueSeverity.MAJOR.isAtLeast(IssueSeverity.INFO));

        assertFalse(IssueSeverity.MINOR.isAtLeast(IssueSeverity.BLOCKER));
        assertFalse(IssueSeverity.MINOR.isAtLeast(IssueSeverity.CRITICAL));
        assertFalse(IssueSeverity.MINOR.isAtLeast(IssueSeverity.MAJOR));
        assertTrue(IssueSeverity.MINOR.isAtLeast(IssueSeverity.MINOR));
        assertTrue(IssueSeverity.MINOR.isAtLeast(IssueSeverity.INFO));

        assertFalse(IssueSeverity.INFO.isAtLeast(IssueSeverity.BLOCKER));
        assertFalse(IssueSeverity.INFO.isAtLeast(IssueSeverity.CRITICAL));
        assertFalse(IssueSeverity.INFO.isAtLeast(IssueSeverity.MAJOR));
        assertFalse(IssueSeverity.INFO.isAtLeast(IssueSeverity.MINOR));
        assertTrue(IssueSeverity.INFO.isAtLeast(IssueSeverity.INFO));
    }

    @Test
    void testMin() {
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.min(IssueSeverity.BLOCKER, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.min(IssueSeverity.BLOCKER, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.min(IssueSeverity.BLOCKER, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.BLOCKER, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.BLOCKER, IssueSeverity.INFO));

        assertSame(IssueSeverity.CRITICAL, IssueSeverity.min(IssueSeverity.CRITICAL, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.min(IssueSeverity.CRITICAL, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.min(IssueSeverity.CRITICAL, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.CRITICAL, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.CRITICAL, IssueSeverity.INFO));

        assertSame(IssueSeverity.MAJOR, IssueSeverity.min(IssueSeverity.MAJOR, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.min(IssueSeverity.MAJOR, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.min(IssueSeverity.MAJOR, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.MAJOR, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.MAJOR, IssueSeverity.INFO));

        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.MINOR, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.MINOR, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.MINOR, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.min(IssueSeverity.MINOR, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.MINOR, IssueSeverity.INFO));

        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.INFO, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.INFO, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.INFO, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.INFO, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.min(IssueSeverity.INFO, IssueSeverity.INFO));
    }

    @Test
    void testMax() {
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.BLOCKER, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.BLOCKER, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.BLOCKER, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.BLOCKER, IssueSeverity.MINOR));
        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.BLOCKER, IssueSeverity.INFO));

        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.CRITICAL, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.CRITICAL, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.CRITICAL, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.CRITICAL, IssueSeverity.MINOR));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.CRITICAL, IssueSeverity.INFO));

        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.MAJOR, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.MAJOR, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.max(IssueSeverity.MAJOR, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.max(IssueSeverity.MAJOR, IssueSeverity.MINOR));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.max(IssueSeverity.MAJOR, IssueSeverity.INFO));

        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.MINOR, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.MINOR, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.max(IssueSeverity.MINOR, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.max(IssueSeverity.MINOR, IssueSeverity.MINOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.max(IssueSeverity.MINOR, IssueSeverity.INFO));

        assertSame(IssueSeverity.BLOCKER, IssueSeverity.max(IssueSeverity.INFO, IssueSeverity.BLOCKER));
        assertSame(IssueSeverity.CRITICAL, IssueSeverity.max(IssueSeverity.INFO, IssueSeverity.CRITICAL));
        assertSame(IssueSeverity.MAJOR, IssueSeverity.max(IssueSeverity.INFO, IssueSeverity.MAJOR));
        assertSame(IssueSeverity.MINOR, IssueSeverity.max(IssueSeverity.INFO, IssueSeverity.MINOR));
        assertSame(IssueSeverity.INFO, IssueSeverity.max(IssueSeverity.INFO, IssueSeverity.INFO));
    }
}