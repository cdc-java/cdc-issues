package cdc.issues;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.util.lang.Checks;

/**
 * Set of effective parameters or meta data.
 * <p>
 * It is a set of (name, value) pairs.
 *
 * @author Damien Carbonne
 */
public interface Params extends NameValueMap, Comparable<Params> {
    public static final Params NO_PARAMS = new ParamsImpl(Collections.emptyMap());

    /**
     * @return A collection of {@link Param}.
     */
    public Collection<Param> getParams();

    /**
     * @return A list of {@link Param} sorted using their name.
     */
    public List<Param> getSortedParams();

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of {@link Params}.
     */
    public static final class Builder extends NameValueMap.Builder<Params, Builder> {
        private Builder() {
        }

        public Builder param(String name,
                             String value) {
            return entry(name, value);
        }

        public Builder params(Collection<Param> params) {
            return entries(params);
        }

        public Builder params(Params params) {
            return entries(params);
        }

        @Override
        public Params build() {
            if (map.isEmpty()) {
                return NO_PARAMS;
            } else {
                return new ParamsImpl(map);
            }
        }
    }
}

record ParamsImpl(Map<String, String> map) implements Params {
    ParamsImpl {
        for (final String name : map.keySet()) {
            Checks.isTrue(Param.isValidName(name), "name: '{}'", name);
        }
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public Set<String> getNames() {
        return map.keySet();
    }

    @Override
    public List<String> getSortedNames() {
        return map.keySet()
                  .stream()
                  .sorted()
                  .toList();
    }

    @Override
    public String getValue(String name) {
        return map.get(name);
    }

    @Override
    public String getValue(String name,
                           String def) {
        return map.getOrDefault(name, def);
    }

    @Override
    public Collection<Param> getParams() {
        return map.keySet()
                  .stream()
                  .map(name -> Param.of(name, getValue(name)))
                  .toList();
    }

    @Override
    public List<Param> getSortedParams() {
        return map.keySet()
                  .stream()
                  .sorted()
                  .map(name -> Param.of(name, getValue(name)))
                  .toList();
    }

    @Override
    public String toString() {
        return getSortedParams().stream()
                                .map(Param::toString)
                                .collect(Collectors.joining(", ", "[", "]"));
    }

    @Override
    public Collection<? extends NameValue> getNameValues() {
        return getParams();
    }

    @Override
    public List<? extends NameValue> getSortedNameValues() {
        return getSortedParams();
    }

    @Override
    public int compareTo(Params other) {
        return toString().compareTo(other.toString());
    }
}