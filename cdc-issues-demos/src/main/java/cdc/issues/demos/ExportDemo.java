package cdc.issues.demos;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.Params;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.impl.IssueAnswerImpl;
import cdc.issues.impl.IssueCommentImpl;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.impl.ProjectImpl;
import cdc.issues.impl.SnapshotImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.locations.DefaultLocation;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.util.events.ProgressController;

public class ExportDemo {
    private static final Logger LOGGER = LogManager.getLogger(ExportDemo.class);
    private static final String DOMAIN = "Domain1";
    private static final String PROFILE = "Profile1";
    private static final String PROJECT = "Project1";
    private static final String SNAPSHOT = "Snapshot1";
    private static final String RULE1 = "Rule1";
    private static final String RULE2 = "Rule2";

    private static final Comparator<String> CMP = new WeightComparator(-100, "meta2", "meta1");

    private static IssuesAndAnswersImpl build(boolean answers,
                                              int count) {
        final IssuesAndAnswersImpl data = new IssuesAndAnswersImpl();
        final IssueResolution[] resolutions = IssueResolution.values();
        final IssueStatus[] statuses = IssueStatus.values();
        for (int index = 0; index < count; index++) {
            final Metas metas;
            if (index % 2 == 0) {
                metas = Metas.NO_METAS;
            } else {
                metas = Metas.builder()
                             .meta("meta1", "value1")
                             .meta("meta2", "value2")
                             .build();
            }

            final Params params;
            if (index % 2 == 0) {
                params = Params.NO_PARAMS;
            } else {
                params = Params.builder()
                               .param("param1", "value1")
                               .param("param2", "value2")
                               .build();
            }

            final Issue issue =
                    Issue.builder()
                         .domain(DOMAIN)
                         .name(index % 2 == 0 ? RULE1 : RULE2)
                         .params(params)
                         .snapshot(index % 2 == 0 ? null : SNAPSHOT)
                         .project(index % 2 == 0 ? null : PROJECT)
                         .severity(IssueSeverity.INFO)
                         .description("Issue Description")
                         .addLocation(new DefaultLocation("Target " + index, "Path 1"))
                         .addLocation(new DefaultLocation("Target " + index))
                         .metas(metas)
                         .build();
            data.addIssue(issue);
            if (answers) {
                final Metas userMetas;
                if (index % 2 == 0) {
                    userMetas = Metas.NO_METAS;
                } else {
                    userMetas = Metas.builder()
                                     .meta("user-meta1", "value1")
                                     .meta("user-meta2", "value2")
                                     .build();
                }

                final IssueAnswerImpl.Builder answer =
                        IssueAnswerImpl.builder()
                                       .issueId(issue.getId())
                                       .assignee("Joe")
                                       .author("Jack");
                final int resolutionIndex = index % (resolutions.length + 1);
                answer.resolution(resolutionIndex == resolutions.length
                        ? IssueResolution.UNRESOLVED
                        : resolutions[resolutionIndex]);
                final int statusIndex = index % (statuses.length + 1);
                answer.status(statusIndex == statuses.length
                        ? IssueStatus.OPEN
                        : statuses[statusIndex]);
                if (index % 2 == 0) {
                    answer.newSeverity(IssueSeverity.MINOR);
                }

                if (index % 2 == 0) {
                    answer.comment(IssueCommentImpl.builder().author("Jude").text("A comment").build())
                          .comment(IssueCommentImpl.builder().author("Jack").text("Another comment").build());
                }
                answer.metas(userMetas);

                data.addAnswer(answer.build());
            }
        }
        return data;
    }

    private static void runFull(IssuesFormat format,
                                int count) throws IOException {
        final File file = new File("target/issues-io-demo-full." + format.name().toLowerCase());
        final File[] profilesFiles = {
                new File("target/profile.html"),
                new File("target/profile.md"),
                new File("target/profile.xlsx"),
                new File("target/profile.xml")
        };
        LOGGER.info("Generate {}", file);

        final IssuesAndAnswersImpl data1 = build(true, count);
        final ProjectImpl project = new ProjectImpl(PROJECT).setMetas(Metas.builder()
                                                                           .meta("project meta1", "value1")
                                                                           .build());
        final SnapshotImpl snapshot = project.createSnapshot()
                                             .setName(SNAPSHOT)
                                             .setMetas(Metas.builder()
                                                            .meta("snapshot arg1", "value1")
                                                            .build());
        final ProfileImpl profile = new ProfileImpl(PROFILE).setMetas(Metas.builder()
                                                                           .meta("profile meta1", "value1")
                                                                           .build())
                                                            .setDescription("Profile description");
        final Rule rule1 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE1)
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description of Rule 1")
                               .metas(Metas.builder()
                                           .meta("meta1", "value1")
                                           .build())
                               .build();
        final Rule rule2 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE2)
                               .severity(IssueSeverity.CRITICAL)
                               .description("Description of Rule 2")
                               .metas(Metas.builder()
                                           .meta("meta1", "value1")
                                           .meta("meta2", "value2")
                                           .build())
                               .build();
        profile.add(rule1);
        profile.add(rule2);
        profile.setParams(rule2,
                          Params.builder()
                                .param("param1", "value1")
                                .param("param2", "value2")
                                .build());
        snapshot.addIssues(data1.getIssues());
        project.addAnswers(data1.getAnswers());
        project.setProfile(profile);

        final OutSettings settings = OutSettings.builder()
                                                .metaComparator(CMP)
                                                .build();

        IssuesWriter.save(snapshot,
                          settings,
                          file,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);
        LOGGER.info("Generated {}", file);

        final ProfileIoFeatures pf = ProfileIoFeatures.builder()
                                                      .hint(ProfileIoFeatures.Hint.PRETTY_PRINT)
                                                      .sections(RuleDescription.SECTION_APPLIES_TO,
                                                                RuleDescription.SECTION_REMARKS)
                                                      .build();

        for (final File f : profilesFiles) {
            LOGGER.info("Generate {}", f);
            ProfileIo.save(pf, profile, f);
            LOGGER.info("Generated {}", f);
        }
    }

    private static void run(IssuesFormat format,
                            OutSettings settings,
                            int count,
                            String suffix) throws IOException {
        final File file = new File("target/issues-io-demo-"
                + "-" + suffix
                + "." + format.name().toLowerCase());
        LOGGER.info("Generate {}", file);
        final IssuesAndAnswersImpl data = build(!settings.getHints().contains(OutSettings.Hint.NO_ANSWERS), count);

        IssuesWriter.save(data,
                          settings,
                          file,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);
        LOGGER.info("Generated {}", file);
    }

    private static void runNoAnswersAuto(IssuesFormat format,
                                         int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .hint(OutSettings.Hint.NO_ANSWERS)
                           .hint(OutSettings.Hint.AUTO_METAS)
                           .hint(OutSettings.Hint.AUTO_PARAMS)
                           .hint(OutSettings.Hint.AUTO_LOCATIONS)
                           .hint(OutSettings.Hint.AUTO_USER_METAS)
                           .build();
        run(format, settings, count, "no-answers-auto");
    }

    private static void runNoAnswers(IssuesFormat format,
                                     int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .hint(OutSettings.Hint.NO_ANSWERS)
                           .build();
        run(format, settings, count, "no-answers");
    }

    private static void runNoAnswersNoOthers(IssuesFormat format,
                                             int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .hint(OutSettings.Hint.NO_ANSWERS)
                           .hint(OutSettings.Hint.NO_DOMAIN_COL)
                           .hint(OutSettings.Hint.NO_METAS_COL)
                           .hint(OutSettings.Hint.NO_PARAMS_COL)
                           .hint(OutSettings.Hint.NO_PROJECT_COL)
                           .hint(OutSettings.Hint.NO_SNAPSHOT_COL)
                           .build();
        run(format, settings, count, "no-answers-no-others");
    }

    private static void runAnswersAuto(IssuesFormat format,
                                       int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .hint(OutSettings.Hint.AUTO_METAS)
                           .hint(OutSettings.Hint.AUTO_PARAMS)
                           .hint(OutSettings.Hint.AUTO_LOCATIONS)
                           .build();
        run(format, settings, count, "answers-auto");
    }

    private static void runAnswers(IssuesFormat format,
                                   int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .build();
        run(format, settings, count, "answers");
    }

    private static void runAnswersNoOthers(IssuesFormat format,
                                           int count) throws IOException {
        final OutSettings settings =
                OutSettings.builder()
                           .hint(OutSettings.Hint.NO_DOMAIN_COL)
                           .hint(OutSettings.Hint.NO_METAS_COL)
                           .hint(OutSettings.Hint.NO_PARAMS_COL)
                           .hint(OutSettings.Hint.NO_PROJECT_COL)
                           .hint(OutSettings.Hint.NO_SNAPSHOT_COL)
                           .build();
        run(format, settings, count, "answers-no-others");
    }

    private static void runAll(IssuesFormat format) throws IOException {
        final int count = 10;
        runNoAnswersAuto(format, count);
        runNoAnswers(format, count);
        runNoAnswersNoOthers(format, count);
        runAnswersAuto(format, count);
        runAnswers(format, count);
        runAnswersNoOthers(format, count);
        runFull(format, count);
    }

    public static void main(String... args) throws IOException {
        runAll(IssuesFormat.XLSX);
        runAll(IssuesFormat.XML);
        runAll(IssuesFormat.JSON);
    }
}