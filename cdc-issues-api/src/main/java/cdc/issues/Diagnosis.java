package cdc.issues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * A Diagnosis is a list of issues.
 *
 * @author Damien Carbonne
 */
public final class Diagnosis {
    private final List<Issue> issues;

    public static final Diagnosis OK_DIAGNOSIS = new Diagnosis();

    @SafeVarargs
    protected Diagnosis(Issue... issues) {
        this(Arrays.asList(Checks.isNotNull(issues, "issues")));
    }

    private Diagnosis(Collection<Issue> issues) {
        Checks.isNotNull(issues, "issues");

        if (issues.isEmpty()) {
            this.issues = Collections.emptyList();
        } else {
            final List<Issue> tmp = new ArrayList<>();
            for (final Issue issue : issues) {
                tmp.add(issue);
            }
            this.issues = Collections.unmodifiableList(tmp);
        }
    }

    public static Diagnosis okDiagnosis() {
        final Diagnosis tmp = OK_DIAGNOSIS;
        return tmp;
    }

    @Deprecated(since = "2025-01-02", forRemoval = true)
    public static Diagnosis create(Collection<Issue> issues) {
        return of(issues);
    }

    @Deprecated(since = "2025-01-02", forRemoval = true)
    @SafeVarargs
    public static Diagnosis create(Issue... issues) {
        return of(issues);
    }

    public static Diagnosis of(Collection<Issue> issues) {
        if (issues.isEmpty()) {
            return okDiagnosis();
        } else {
            return new Diagnosis(issues);
        }
    }

    @SafeVarargs
    public static Diagnosis of(Issue... issues) {
        return of(Arrays.asList(issues));
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public boolean isOk() {
        return issues.isEmpty();
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (isOk()) {
            builder.append("OK");
        } else {
            builder.append("KO ");
            for (final Issue issue : issues) {
                builder.append(issue);
            }
        }
        return builder.toString();
    }
}