package cdc.issues.core.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.io.ProfileIoFeatures.Hint;
import cdc.issues.rules.Profile;

public class JsonProfileIo {
    private final ProfileIoFeatures features;

    JsonProfileIo(ProfileIoFeatures features) {
        this.features = features;
    }

    public void save(Profile profile,
                     File file) throws IOException {
        final Map<String, Object> config = new HashMap<>();
        if (features.isEnabled(Hint.PRETTY_PRINT)) {
            config.put(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE);
        }
        final JsonGeneratorFactory factory = Json.createGeneratorFactory(config);
        try (final JsonGenerator generator = factory.createGenerator(new BufferedWriter(new FileWriter(file)))) {
            generator.writeStartObject(); // DOCUMENT
            JsonIo.writeProfile(generator, profile);
            generator.writeEnd(); // DOCUMENT
            generator.flush();
        }
    }
}