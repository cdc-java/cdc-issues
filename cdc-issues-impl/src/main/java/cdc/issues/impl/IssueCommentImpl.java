package cdc.issues.impl;

import java.time.Instant;
import java.util.Objects;

import cdc.issues.answers.IssueComment;

/**
 * Default implementation of IssueComment.
 *
 * @author Damien Carbonne
 */
public class IssueCommentImpl implements IssueComment {
    private final String author;
    private Instant date;
    private String text;

    protected IssueCommentImpl(Builder builder) {
        this.author = builder.author;
        this.date = builder.date == null ? Instant.now() : builder.date;
        this.text = builder.text;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public Instant getDate() {
        return date;
    }

    public IssueCommentImpl setDate(Instant date) {
        this.date = date;
        return this;
    }

    @Override
    public String getText() {
        return text;
    }

    public IssueCommentImpl setText(String text) {
        this.text = text;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(author,
                            date,
                            text);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IssueCommentImpl)) {
            return false;
        }
        final IssueCommentImpl other = (IssueCommentImpl) object;
        return Objects.equals(author, other.author)
                && Objects.equals(date, other.date)
                && Objects.equals(text, other.text);
    }

    @Override
    public String toString() {
        return "[" + author + ", " + date + ", " + text + "]";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String author;
        private Instant date;
        private String text;

        protected Builder() {
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder date(Instant date) {
            this.date = date;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public IssueCommentImpl build() {
            return new IssueCommentImpl(this);
        }
    }
}