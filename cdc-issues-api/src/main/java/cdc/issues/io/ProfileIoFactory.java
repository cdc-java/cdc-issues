package cdc.issues.io;

import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Factory of {@link ProfileIo}.
 *
 * @author Damien Carbonne
 */
public final class ProfileIoFactory {
    private ProfileIoFactory() {
    }

    /**
     * Creates an instance of {@link ProfileIo} configured with {@link ProfileIoFeatures}.
     *
     * @param features The features.
     * @return An instance of {@link ProfileIo} configured with {@code features}.
     */
    public static ProfileIo create(ProfileIoFeatures features) {
        final Class<? extends ProfileIo> cls =
                Introspection.uncheckedCast(Introspection.getClass("cdc.issues.core.io.ProfileIoImpl",
                                                                   ProfileIo.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { ProfileIoFeatures.class };
        return Introspection.newInstance(cls,
                                         parameterTypes,
                                         FailureReaction.FAIL,
                                         features);
    }
}