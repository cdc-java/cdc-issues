package cdc.issues.answers;

import java.time.Instant;

/**
 * Interface describing an issue comment.
 *
 * @author Damien Carbonne
 */
public interface IssueComment {

    /**
     * @return The author of this comment.
     */
    public String getAuthor();

    /**
     * @return The date (as an Instant) of this comment.
     */
    public Instant getDate();

    /**
     *
     * @return The text of this comment.
     */
    public String getText();
}