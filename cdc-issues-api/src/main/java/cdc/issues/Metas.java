package cdc.issues;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.util.lang.Checks;

/**
 * Set of {@link Meta}, with unique names.
 */
public interface Metas extends NameValueMap {
    public static final Metas NO_METAS = new MetasImpl(Collections.emptyMap());

    /** Standard meta name to describe an item author. */
    public static final String AUTHOR = "author";
    /** Standard meta name to describe an item version. */
    public static final String VERSION = "version";
    /** Standard meta name to describe since when an item exists (date or version). */
    public static final String SINCE = "since";
    /** Standard meta name to describe when an item was created (date or version). */
    public static final String CREATED = "created";
    /** Standard meta name to describe when an item was modified (date or version). */
    public static final String MODIFIED = "modified";
    /** Standard meta name to describe badges (labels). */
    public static final String BADGES = "badges";
    /** Standard meta name to describe status. */
    public static final String STATUS = "status";

    /**
     * @return A collection of {@link Meta}.
     */
    public Collection<Meta> getMetas();

    /**
     * @return A list of {@link Meta} sorted using their name.
     */
    public List<Meta> getSortedMetas();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends NameValueMap.Builder<Metas, Builder> implements MetasBuilding<Builder> {
        private Builder() {
        }

        @Override
        public Builder self() {
            return this;
        }

        /**
         * Adds a new (name, value) pair.
         *
         * @param name The meta name.
         * @param value The meta value.
         * @return This builder.
         */
        @Override
        public Builder meta(String name,
                            String value) {
            return entry(name, value);
        }

        @Override
        public Builder meta(String name,
                            String value,
                            String separator) {
            return append(name, value, separator);
        }

        @Override
        public Metas build() {
            if (map.isEmpty()) {
                return NO_METAS;
            } else {
                return new MetasImpl(map);
            }
        }
    }
}

record MetasImpl(Map<String, String> map) implements Metas {
    MetasImpl {
        for (final String name : map.keySet()) {
            Checks.isTrue(Meta.isValidName(name), "name: '{}'", name);
        }
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public Set<String> getNames() {
        return map.keySet();
    }

    @Override
    public List<String> getSortedNames() {
        return map.keySet()
                  .stream()
                  .sorted()
                  .toList();
    }

    @Override
    public String getValue(String name) {
        return map.get(name);
    }

    @Override
    public String getValue(String name,
                           String def) {
        return map.getOrDefault(name, def);
    }

    @Override
    public Collection<Meta> getMetas() {
        return map.keySet()
                  .stream()
                  .map(name -> Meta.of(name, getValue(name)))
                  .toList();
    }

    @Override
    public List<Meta> getSortedMetas() {
        return map.keySet()
                  .stream()
                  .sorted()
                  .map(name -> Meta.of(name, getValue(name)))
                  .toList();
    }

    @Override
    public String toString() {
        return getSortedMetas().stream()
                               .map(Meta::toString)
                               .collect(Collectors.joining(", ", "[", "]"));
    }

    @Override
    public Collection<? extends NameValue> getNameValues() {
        return getMetas();
    }

    @Override
    public List<? extends NameValue> getSortedNameValues() {
        return getSortedMetas();
    }
}