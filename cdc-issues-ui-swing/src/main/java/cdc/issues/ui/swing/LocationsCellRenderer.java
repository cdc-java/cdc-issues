package cdc.issues.ui.swing;

import javax.swing.JLabel;

import cdc.issues.locations.Location;
import cdc.ui.swing.cells.BaseCellRenderer;
import cdc.ui.swing.cells.CellParams;

public class LocationsCellRenderer extends BaseCellRenderer<Location[], JLabel> {
    public LocationsCellRenderer() {
        super(Location[].class, new JLabel());

        getDelegate().setOpaque(true);
    }

    @Override
    public void configure(Location[] value,
                          CellParams params) {
        super.configure(value, params);
        final StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        boolean first = true;
        for (final Location location : value) {
            if (first) {
                first = false;
            } else {
                builder.append("<br>");
            }
            builder.append(location);
        }
        builder.append("</html>");

        getDelegate().setText(builder.toString());
    }
}