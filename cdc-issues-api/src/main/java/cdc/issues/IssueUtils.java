package cdc.issues;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Utilities dedicated to issues.
 *
 * @author Damien Carbonne
 */
public final class IssueUtils {
    private IssueUtils() {
    }

    /**
     * @param issues The collection of issues.
     * @return A set of meta names used in {@code issues}.
     */
    public static Set<String> getMetaNames(Collection<? extends Issue> issues) {
        final Set<String> set = new HashSet<>();
        for (final Issue issue : issues) {
            set.addAll(issue.getMetas().getNames());
        }
        return set;
    }

    /**
     * @param issues The collection of issues.
     * @return A set of param names used in {@code issues}.
     */
    public static Set<String> getParamNames(Collection<? extends Issue> issues) {
        final Set<String> set = new HashSet<>();
        for (final Issue issue : issues) {
            set.addAll(issue.getParams().getNames());
        }
        return set;
    }

    /**
     * Computes a hash of a collection of issues.
     * <p>
     * This is based on identifiers. Order of issues in collection does not matter.
     *
     * @param issues The issues.
     * @return A hash of issues.
     */
    public static String getHash(Collection<? extends Issue> issues) {
        final List<IssueId> ids = new ArrayList<>();
        for (final Issue issue : issues) {
            ids.add(issue.getId());
        }
        Collections.sort(ids);
        int hash = 0;
        for (final IssueId id : ids) {
            hash = hash + 31 * id.hashCode();
        }
        return Integer.toHexString(hash).toUpperCase();
    }
}