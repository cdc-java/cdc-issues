package cdc.issues.checks;

import java.io.PrintStream;
import java.util.List;

import cdc.issues.locations.Location;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Specialization of {@link AbstractChecker} that delegates checks
 * when the checked object can be cast to a target type.
 * <p>
 * <b>Note:</b> the target type is not necessarily a specialization of the original type.
 *
 * @param <O> The checked object type.
 * @param <S> The checked target type.
 */
public class SpecializationChecker<O, S> extends AbstractChecker<O> {
    private final AbstractChecker<S> delegate;

    public SpecializationChecker(Class<O> objectClass,
                                 AbstractChecker<S> delegate) {
        super(objectClass);
        this.delegate = Checks.isNotNull(delegate, "delegate");
    }

    @Override
    protected final List<AbstractChecker<?>> getNext() {
        return List.of(delegate);
    }

    @Override
    protected final List<AbstractChecker<?>> getNext(SnapshotManager manager) {
        return getNext();
    }

    /**
     * @return The delegate checker.
     */
    public AbstractChecker<S> getDelegate() {
        return delegate;
    }

    @Override
    public final boolean isEnabled(SnapshotManager manager) {
        return delegate.isEnabled(manager);
    }

    @Override
    public final boolean isCorrectlyConfigured(SnapshotManager manager) {
        return delegate.isCorrectlyConfigured(manager);
    }

    @Override
    public boolean accepts(O object) {
        if (object == null || delegate.getObjectClass().isInstance(object)) {
            return delegate.accepts(delegate.getObjectClass().cast(object));
        } else {
            return false;
        }
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        return checkAndStat(delegate,
                            context,
                            delegate.getObjectClass().cast(object),
                            location);
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("SPECIALIZATION<" + getObjectClass().getSimpleName()
                + ", " + getDelegate().getObjectClass().getSimpleName() + ">");
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
        delegate.print(out, manager, level + 1);
    }
}