package cdc.issues;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Set of labels.
 */
public interface Labels {
    public static final Labels NO_LABELS = of();

    /**
     * @return The set of labels.
     */
    public Set<String> get();

    /**
     * @return The sorted labels.
     */
    public default List<String> getSorted() {
        return get().stream().sorted().toList();
    }

    public default boolean isEmpty() {
        return get().isEmpty();
    }

    public default Labels add(String label) {
        final Set<String> set = new HashSet<>(get());
        set.add(label);
        return of(set);
    }

    public default Labels remove(String label) {
        final Set<String> set = new HashSet<>(get());
        set.remove(label);
        return of(set);
    }

    /**
     * Returns true if a string is a valid label:
     * <ul>
     * <li>It cannot be null
     * <li>It cannot be empty
     * <li>It cannot contain any space or control character
     * </ul>
     *
     * @param label The label.
     * @return {@code true} if {@code label} is a valid label.
     */
    public static boolean isValidLabel(String label) {
        return label != null
                && LabelsImpl.VALID_LABEL_PATTERN.matcher(label).matches();
    }

    public static Labels of(Collection<String> labels) {
        return new LabelsImpl(Set.copyOf(labels));
    }

    public static Labels of(String... labels) {
        return new LabelsImpl(Set.of(labels));
    }
}

record LabelsImpl(Set<String> set) implements Labels {
    /** Accept anything except spaces */
    static final Pattern VALID_LABEL_PATTERN = Pattern.compile("^[^\\h\\v\\p{Cntrl}]+$");

    LabelsImpl {
        for (final String label : set) {
            if (!Labels.isValidLabel(label)) {
                throw new IllegalArgumentException("Invalid label '" + label + "'");
            }
        }
    }

    @Override
    public Set<String> get() {
        return set;
    }

    @Override
    public String toString() {
        return set.stream().sorted().toList().toString();
    }
}
