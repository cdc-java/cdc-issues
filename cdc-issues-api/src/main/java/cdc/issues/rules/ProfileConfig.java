package cdc.issues.rules;

import java.util.Set;

public interface ProfileConfig {
    /**
     * @return The set of rule ids.
     */
    public Set<RuleId> getRuleIds();

    /**
     * @param ruleId The rule id.
     * @return The config of rule identified by {@code ruleId}.
     */
    public RuleConfig getRuleConfig(RuleId ruleId);
}