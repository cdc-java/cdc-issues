package cdc.issues.demos;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class DemosTest {
    @Test
    void testIssuesPanelDemo() throws InterruptedException {
        IssuesPanelDemo.main();
        Thread.sleep(1000);
        assertTrue(true);
    }

    @Test
    void testExportDemo() throws IOException {
        ExportDemo.main();
        assertTrue(true);
    }
}