package cdc.issues;

import java.util.regex.Pattern;

import cdc.util.lang.Checks;

/**
 * Definition of an effective parameter.
 * <p>
 * It is a (name, value) pair, where name can not be {@code null}.
 *
 * @author Damien Carbonne
 */
public interface Param extends NameValue, Comparable<Param> {
    /**
     * @param name The name.
     * @param value The value.
     * @return A new Param instance from {@code name} and {@code value}.
     * @throws IllegalArgumentException When {@code name} is not valid.
     */
    public static Param of(String name,
                           String value) {
        return new ParamImpl(name, value);
    }

    /**
     * Returns true if a string is a valid name:
     * <ul>
     * <li>It cannot be null
     * <li>It cannot be empty
     * <li>It cannot contain any space or control character
     * </ul>
     *
     * @param name The name.
     * @return {@code true} if {@code label} is a valid label.
     */
    public static boolean isValidName(String name) {
        return name != null
                && ParamImpl.VALID_NAME_PATTERN.matcher(name).matches();
    }
}

record ParamImpl(String name,
                 String value)
        implements Param {
    /** Accept anything except spaces */
    static final Pattern VALID_NAME_PATTERN = Pattern.compile("^\\w[\\w\\-/ ]*$");

    ParamImpl {
        Checks.isTrue(Meta.isValidName(name), "name");
    }

    @Override
    public int compareTo(Param other) {
        return name.compareTo(other.getName());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + name + "=" + (value == null ? "" : value) + "]";
    }
}