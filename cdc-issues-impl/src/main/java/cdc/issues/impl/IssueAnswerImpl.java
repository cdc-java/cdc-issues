package cdc.issues.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cdc.issues.IssueId;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.LabelsBuilding;
import cdc.issues.Metas;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssueTransition;
import cdc.util.lang.Checks;

/**
 * Default implementation of IssueAnswer.
 *
 * @author Damien Carbonne
 */
public class IssueAnswerImpl implements IssueAnswer {
    private final IssueId issueId;
    private String author;
    private final Instant creationDate;
    private Instant modificationDate;
    private IssueStatus status;
    private IssueResolution resolution;
    private String assignee;
    private IssueSeverity newSeverity;
    private final List<IssueCommentImpl> comments = new ArrayList<>();
    private Metas metas = Metas.NO_METAS;
    private Labels labels = Labels.NO_LABELS;

    protected IssueAnswerImpl(Builder builder) {
        this.issueId = Checks.isNotNull(builder.issueId, "issueId");
        this.author = builder.author;
        this.creationDate = builder.creationDate == null ? Instant.now() : builder.creationDate;
        this.modificationDate = builder.modificationDate == null ? this.creationDate : builder.modificationDate;
        this.status = Checks.isNotNull(builder.status, "status");
        this.resolution = Checks.isNotNull(builder.resolution, "resolution");
        this.assignee = builder.assignee;
        this.newSeverity = builder.newSeverity;
        this.comments.addAll(builder.comments);
        this.metas = builder.metas;
        this.labels = builder.labels;
    }

    @Override
    public IssueId getIssueId() {
        return issueId;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public IssueAnswerImpl setAuthor(String author) {
        this.author = author;
        return this;
    }

    @Override
    public Instant getCreationDate() {
        return creationDate;
    }

    @Override
    public Instant getModificationDate() {
        return modificationDate;
    }

    public IssueAnswerImpl setModificationDate(Instant modificationDate) {
        this.modificationDate = modificationDate;
        return this;
    }

    @Override
    public IssueStatus getStatus() {
        return status;
    }

    public IssueAnswerImpl setStatus(IssueStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public IssueResolution getResolution() {
        return resolution;
    }

    public IssueAnswerImpl setResolution(IssueResolution resolution) {
        this.resolution = resolution;
        return this;
    }

    public IssueAnswerImpl trigger(IssueTransition transition) {
        if (transition.isValidSource(status, resolution)) {
            this.status = transition.getTargetStatus();
            this.resolution = transition.getTargetResolution();
        }
        return this;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

    public IssueAnswerImpl setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    @Override
    public IssueSeverity getNewSeverity() {
        return newSeverity;
    }

    public IssueAnswerImpl setNewSeverity(IssueSeverity newSeverity) {
        this.newSeverity = newSeverity;
        return this;
    }

    @Override
    public List<IssueCommentImpl> getComments() {
        return comments;
    }

    public IssueAnswerImpl setComments(List<IssueCommentImpl> comments) {
        this.comments.clear();
        this.comments.addAll(comments);
        return this;
    }

    public IssueAnswerImpl addComment(IssueCommentImpl comment) {
        this.comments.add(comment);
        return this;
    }

    @Override
    public Metas getMetas() {
        return metas;
    }

    public IssueAnswerImpl setMetas(Metas metas) {
        this.metas = Checks.isNotNull(metas, "metas");
        return this;
    }

    @Override
    public Labels getLabels() {
        return labels;
    }

    public IssueAnswerImpl setLabels(Labels labels) {
        this.labels = Checks.isNotNull(labels, "labels");
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(issueId,
                            author,
                            creationDate,
                            modificationDate,
                            status,
                            resolution,
                            assignee,
                            newSeverity,
                            comments,
                            metas,
                            labels);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IssueAnswerImpl)) {
            return false;
        }
        final IssueAnswerImpl other = (IssueAnswerImpl) object;
        return Objects.equals(issueId, other.issueId)
                && Objects.equals(author, other.author)
                && Objects.equals(creationDate, other.creationDate)
                && Objects.equals(modificationDate, other.modificationDate)
                && Objects.equals(status, other.status)
                && Objects.equals(resolution, other.resolution)
                && Objects.equals(assignee, other.assignee)
                && Objects.equals(newSeverity, other.newSeverity)
                && Objects.equals(comments, other.comments)
                && Objects.equals(metas, other.metas)
                && Objects.equals(labels, other.labels);
    }

    @Override
    public String toString() {
        return "[" + issueId
                + ", " + author
                + ", " + creationDate
                + ", " + modificationDate
                + ", " + status
                + ", " + resolution
                + ", " + assignee
                + ", " + newSeverity
                + ", " + comments
                + ", " + metas
                + ", " + labels
                + "]";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements LabelsBuilding<Builder> {
        private IssueId issueId;
        private String author;
        private Instant creationDate;
        private Instant modificationDate;
        private IssueStatus status = IssueStatus.OPEN;
        private IssueResolution resolution = IssueResolution.UNRESOLVED;
        private String assignee;
        private IssueSeverity newSeverity;
        private final List<IssueCommentImpl> comments = new ArrayList<>();
        private Metas metas = Metas.NO_METAS;
        private Labels labels = Labels.NO_LABELS;

        protected Builder() {
        }

        public Builder issueId(IssueId issueId) {
            this.issueId = issueId;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder creationDate(Instant creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder modificationDate(Instant modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public Builder status(IssueStatus status) {
            this.status = status;
            return this;
        }

        public Builder resolution(IssueResolution resolution) {
            this.resolution = resolution;
            return this;
        }

        public Builder assignee(String assignee) {
            this.assignee = assignee;
            return this;
        }

        public Builder newSeverity(IssueSeverity newSeverity) {
            this.newSeverity = newSeverity;
            return this;
        }

        public Builder comment(IssueCommentImpl comment) {
            this.comments.add(comment);
            return this;
        }

        public Builder comments(List<IssueCommentImpl> comments) {
            this.comments.addAll(comments);
            return this;
        }

        public Builder metas(Metas metas) {
            this.metas = metas;
            return this;
        }

        @Override
        public Builder labels(Labels labels) {
            this.labels = labels;
            return this;
        }

        public IssueAnswerImpl build() {
            return new IssueAnswerImpl(this);
        }
    }
}