package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.issues.Issue;
import cdc.issues.Snapshot;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.issues.answers.SnapshotIssuesAndAnswers;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.impl.SnapshotDataImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactory;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesIoFactoryFeatures.Hint;
import cdc.issues.io.IssuesStreamWriter;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.SnapshotData;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.time.Chronometer;

/**
 * Implementation of {@link IssuesWriter} based on {@link IssuesStreamWriter}.
 * <p>
 * It supports all formats.
 *
 * @author Damien Carbonne
 */
public class IssuesWriterImpl implements IssuesWriter {
    private static final Logger LOGGER = LogManager.getLogger(IssuesWriterImpl.class);
    private final IssuesIoFactoryFeatures features;
    private final File file;
    private final OutputStream out;
    private final IssuesFormat format;

    private IssuesWriterImpl(File file,
                             OutputStream out,
                             IssuesFormat format,
                             IssuesIoFactoryFeatures features) {
        this.features = features;
        this.file = file;
        this.out = out;
        this.format = format;
    }

    public IssuesWriterImpl(File file,
                            IssuesIoFactoryFeatures features) {
        this(file, null, null, features);
    }

    public IssuesWriterImpl(OutputStream out,
                            IssuesFormat format,
                            IssuesIoFactoryFeatures features) {
        this(null, out, format, features);
    }

    private void traceGenerate(String systemId) {
        if (features.isEnabled(Hint.VERBOSE)) {
            LOGGER.info("Generate {}", systemId);
        }
    }

    private void traceGenerated(String systemId,
                                Chronometer chrono) {
        if (features.isEnabled(Hint.VERBOSE)) {
            LOGGER.info("Generated {} ({})", systemId, chrono);
        }
    }

    private String getSystemId() {
        return file == null ? "???" : file.getPath();
    }

    private IssuesStreamWriter createWriter(OutSettings settings,
                                            IssuesAndAnswers issuesAndAnswers) {
        final IssuesIoFactory factory = new IssuesIoFactory(features);

        if (file == null) {
            return factory.createIssuesStreamWriter(out, format, settings.replaceAuto(issuesAndAnswers));
        } else {
            return factory.createIssuesStreamWriter(file, settings.replaceAuto(issuesAndAnswers));
        }
    }

    @Override
    public void save(Snapshot snapshot,
                     OutSettings settings,
                     ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        traceGenerate(getSystemId());

        final ProgressSupplier progress = new ProgressSupplier(controller);
        // Add 1 for flush/close
        progress.reset(snapshot.getIssues().size() + 1L, null);

        final SnapshotData synthesis = new SnapshotDataImpl(snapshot);

        final IssuesAndAnswers issuesAndAnswers = new SnapshotIssuesAndAnswers(snapshot);

        try (IssuesStreamWriter writer = createWriter(settings, issuesAndAnswers)) {
            writer.startDocument();
            writer.add(synthesis);
            for (final Issue issue : issuesAndAnswers.getIssues()) {
                writer.add(issue, issuesAndAnswers.getAnswer(issue.getId()).orElse(null));
                progress.incrementValue();
            }
            writer.endDocument();
            writer.flush();
        } finally {
            // Here we know writer has been flushed and closed.
            progress.incrementValue();
            progress.close();
        }

        chrono.suspend();
        traceGenerated(getSystemId(), chrono);
    }

    @Override
    public void save(List<? extends Issue> issues,
                     OutSettings settings,
                     ProgressController controller) throws IOException {
        save(new IssuesAndAnswersImpl().addIssues(issues),
             settings,
             controller);
    }

    @Override
    public void save(IssuesAndAnswers issuesAndAnswers,
                     OutSettings settings,
                     ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        traceGenerate(getSystemId());

        final ProgressSupplier progress = new ProgressSupplier(controller);
        // Add 1 for flush/close
        progress.reset(issuesAndAnswers.getIssues().size() + 1L, null);

        try (IssuesStreamWriter writer = createWriter(settings, issuesAndAnswers)) {
            writer.startDocument();
            for (final Issue issue : issuesAndAnswers.getIssues()) {
                writer.add(issue, issuesAndAnswers.getAnswer(issue.getId()).orElse(null));
                progress.incrementValue();
            }
            writer.endDocument();
            // Flushing may have no effect, depending on implementation
            writer.flush();
        } finally {
            // Here we know writer has been flushed and closed.
            progress.incrementValue();
            progress.close();
        }

        chrono.suspend();
        traceGenerated(getSystemId(), chrono);
    }

    @Override
    public void save(SnapshotData synthesis,
                     List<? extends Issue> issues,
                     OutSettings settings,
                     ProgressController controller) throws IOException {
        save(synthesis,
             new IssuesAndAnswersImpl().addIssues(issues),
             settings,
             controller);
    }

    @Override
    public void save(SnapshotData synthesis,
                     IssuesAndAnswers issuesAndAnswers,
                     OutSettings settings,
                     ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        traceGenerate(getSystemId());

        final ProgressSupplier progress = new ProgressSupplier(controller);
        // Add 1 for flush/close
        progress.reset(issuesAndAnswers.getIssues().size() + 1L, null);

        try (IssuesStreamWriter writer = createWriter(settings, issuesAndAnswers)) {
            writer.startDocument();
            writer.add(synthesis);
            for (final Issue issue : issuesAndAnswers.getIssues()) {
                writer.add(issue, issuesAndAnswers.getAnswer(issue.getId()).orElse(null));
                progress.incrementValue();
            }
            writer.endDocument();
            writer.flush();
        } finally {
            // Here we know writer has been flushed and closed.
            progress.incrementValue();
            progress.close();
        }

        chrono.suspend();
        traceGenerated(getSystemId(), chrono);
    }
}