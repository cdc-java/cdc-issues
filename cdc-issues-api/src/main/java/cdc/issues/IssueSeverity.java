package cdc.issues;

/**
 * Enumeration of possible issue severities.
 *
 * @author Damien Carbonne
 */
public enum IssueSeverity {
    INFO,
    MINOR,
    MAJOR,
    CRITICAL,
    BLOCKER;

    public boolean isAtLeast(IssueSeverity severity) {
        return ordinal() >= severity.ordinal();
    }

    public static IssueSeverity min(IssueSeverity x,
                                    IssueSeverity y) {
        return x.ordinal() <= y.ordinal() ? x : y;
    }

    public static IssueSeverity max(IssueSeverity x,
                                    IssueSeverity y) {
        return x.ordinal() >= y.ordinal() ? x : y;
    }
}