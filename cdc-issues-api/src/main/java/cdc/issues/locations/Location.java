package cdc.issues.locations;

import cdc.util.strings.StringUtils;

/**
 * Base interface used to describe a location inside a resource.
 * <p>
 * It is composed of 2 parts:
 * <ul>
 * <li>external: the path to a resource,
 * <li>internal: the anchor that optionally designates something inside the resource.
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface Location extends Comparable<Location> {
    public static final Location UNDEFINED = new DefaultLocation("", null);

    /**
     * The string used to join the different parts (tag::path::anchor) of a location.
     */
    public static final String SEPARATOR = "::";

    /**
     * @return The string used to identify the implementation of this interface.
     *         It is used in serialization.
     * @see cdc.issues.locations.Locations
     */
    public default String getTag() {
        return getClass().getSimpleName();
    }

    /**
     * @return The external part of the location, the path of the designated resource.<br>
     *         It is typically a file name, an URI, a kind of 'absolute' identifier.
     */
    public String getPath();

    /**
     * @return The internal part of the location, inside the resource.
     *         It may be {@code null}.
     */
    public String getAnchor();

    /**
     * @return {@code true} if this location has a meaningful anchor.
     */
    public default boolean hasAnchor() {
        return !StringUtils.isNullOrEmpty(getAnchor());
    }

    @Override
    public default int compareTo(Location other) {
        return toString().compareTo(other.toString());
    }

    public default String toString(boolean addTag) {
        return toString(this, addTag);
    }

    /**
     * Encodes a Location to a String.
     * <p>
     * The result is encoded as {@code Tag::Path[::Anchor]} or as {@code Path[::Anchor]},*
     * depending on the value of {@code addTag}.
     * <p>
     * <b>WARRNING:</b> if tag is missing, one can not reconstruct the original location
     * from the string representation.
     *
     * @param location The location.
     * @param addTag If {@code true}, the location tag is included.
     * @return A String representation of {@code location}.
     */
    public static String toString(Location location,
                                  boolean addTag) {
        final String anchor = location.getAnchor();
        final StringBuilder builder = new StringBuilder();

        if (addTag) {
            builder.append(location.getTag())
                   .append(SEPARATOR);
        }
        builder.append(location.getPath());
        if (!StringUtils.isNullOrEmpty(anchor)) {
            builder.append(SEPARATOR)
                   .append(anchor);
        }
        return builder.toString();
    }

    /**
     * Encodes a Location to a String.
     * <p>
     * The result is encoded as {@code Tag::Path[::Anchor]}.
     *
     * @param location The location.
     * @return A String representation of {@code location}.
     */
    public static String toString(Location location) {
        return toString(location, true);
    }

    /**
     * @param location The string encoding of a Location.
     * @return The Tag part of {@code location}.
     */
    public static String getTag(String location) {
        if (location == null) {
            return null;
        } else {
            final int pos = location.indexOf(SEPARATOR);
            if (pos >= 0) {
                return location.substring(0, pos);
            } else {
                return null;
            }
        }
    }

    /**
     * @param location The string encoding of a Location.
     * @return The Path part of {@code location}.
     */
    public static String getPath(String location) {
        if (location == null) {
            return null;
        } else {
            final int pos1 = location.indexOf(SEPARATOR);
            if (pos1 >= 0) {
                final int pos2 = location.indexOf(SEPARATOR, pos1 + 2);
                if (pos2 >= 0) {
                    return location.substring(pos1 + SEPARATOR.length(), pos2);
                } else {
                    return location.substring(pos1 + SEPARATOR.length());
                }
            } else {
                return null;
            }
        }
    }

    /**
     * @param location The string encoding of a Location.
     * @return The Anchor part of {@code location}.
     */
    public static String getAnchor(String location) {
        if (location == null) {
            return null;
        } else {
            final int pos1 = location.indexOf(SEPARATOR);
            if (pos1 >= 0) {
                final int pos2 = location.indexOf(SEPARATOR, pos1 + 2);
                if (pos2 >= 0) {
                    return location.substring(pos2 + SEPARATOR.length());
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String tag = null;
        private String path;
        private String anchor = null;

        protected Builder() {
            super();
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder anchor(String anchor) {
            this.anchor = anchor;
            return this;
        }

        public Location build() {
            return Locations.build(tag, path, anchor);
        }
    }
}