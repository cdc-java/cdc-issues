package cdc.issues.io;

import java.io.File;

import cdc.office.ss.WorkbookKind;
import cdc.util.files.Files;
import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of possible formats for export and import of profiles.
 * <p>
 * A format may be unsupported.
 *
 * @author Damien Carbonne
 */
public enum ProfileFormat {
    CSV,
    ODS,
    XLS,
    XLSM,
    XLSX,
    JSON,
    XML,
    HTML,
    MD;

    /**
     * @return The corresponding {@link WorkbookKind}.
     * @throws UnexpectedValueException when no corresponding format can be provided.
     */
    public WorkbookKind getWorkbookKind() {
        switch (this) {
        case CSV:
            return WorkbookKind.CSV;
        case ODS:
            return WorkbookKind.ODS;
        case XLS:
            return WorkbookKind.XLS;
        case XLSX:
            return WorkbookKind.XLSX;
        case XLSM:
            return WorkbookKind.XLSM;
        default:
            throw new UnexpectedValueException(this);
        }
    }

    /**
     * @return {@code true} if this format is supported for profile export.
     */
    @SuppressWarnings("static-method")
    public boolean isProfileExportFormat() {
        return true;
    }

    /**
     * @return {@code true} if this format is supported for profile config export.
     */
    public boolean isProfileConfigExportFormat() {
        return this == CSV || this == ODS || this == XLS || this == XLSX || this == XLSM;
    }

    /**
     * @return {@code true} if this format is supported for profile import.
     */
    public boolean isProfileImportFormat() {
        return this == XML;
    }

    /**
     * @return {@code true} if this format is supported for profile config import.
     */
    public boolean isProfileConfigImportFormat() {
        return this == CSV || this == ODS || this == XLS || this == XLSX || this == XLSM;
    }

    /**
     * @param file The file.
     * @return The format associated to {@code file} or {@code null}.
     */
    public static ProfileFormat from(File file) {
        final String ext = Files.getExtension(file);
        for (final ProfileFormat format : values()) {
            if (format.name().equalsIgnoreCase(ext)) {
                return format;
            }
        }
        return null;
    }
}