package cdc.issues.io;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.Introspection;

/**
 * Class that can be used to create instances of:
 * <ul>
 * <li>{@link IssuesWriter}
 * <li>{@link IssuesReader}
 * </ul>
 *
 * @author Damien Carbonne
 */
public class IssuesIoFactory {
    private static final String FILE = "file";
    private static final String FORMAT = "format";
    private static final String IN = "in";
    private static final String OUT = "out";
    private static final String SETTINGS = "settings";
    private final IssuesIoFactoryFeatures features;

    /**
     * Creates a factory.
     *
     * @param features The features.
     */
    public IssuesIoFactory(IssuesIoFactoryFeatures features) {
        this.features = Checks.isNotNull(features, "features");
    }

    /**
     * @return The features associated to this factory.;
     */
    public IssuesIoFactoryFeatures getFeatures() {
        return features;
    }

    /**
     * @param file The file.
     *
     * @return {@code true} if export to {@code file} is supported.
     *         If {@code file} is {@code null} returns {@code false}.
     */
    public boolean canExportTo(File file) {
        if (file == null) {
            return false;
        } else {
            return IssuesFormat.from(file) != null;
        }
    }

    /**
     * Creates an instance of {@link IssuesStreamWriter} fitted for a file.
     *
     * @param file The file for which the writer must be created.
     * @param settings The settings.
     * @return A new instance of {@link IssuesStreamWriter} fitted for {@code file}.
     * @throws IllegalArgumentException When {@code file} or {@code settings} is {@code null},*
     *             or when {@code file} has an unrecognized extension.
     */
    public IssuesStreamWriter createIssuesStreamWriter(File file,
                                                       OutSettings settings) {
        Checks.isNotNull(file, FILE);
        Checks.isNotNull(settings, SETTINGS);

        final IssuesFormat format = IssuesFormat.from(file);
        if (format != null) {
            switch (format) {
            case CSV, ODS, XLS, XLSM, XLSX:
                return createIssuesStreamWriter("cdc.issues.core.io.WorkbookIssuesStreamWriter", file, settings);
            case JSON:
                return createIssuesStreamWriter("cdc.issues.core.io.JsonIssuesStreamWriter", file, settings);
            case XML:
                return createIssuesStreamWriter("cdc.issues.core.io.XmlIssuesStreamWriter", file, settings);
            default:
                break;
            }
        }
        throw new IllegalArgumentException("Can not create an IssuesStreamWriter for " + file);
    }

    /**
     * Creates an instance of {@link IssuesStreamWriter} fitted for an output stream and a format.
     *
     * @param out The output stream.
     * @param format The format.
     * @param settings The settings.
     * @return A new instance of {@link IssuesStreamWriter} fitted for {@code out} and {@code format}.
     * @throws IllegalArgumentException When {@code out}, {@code format} or {@code settings} is {@code null}.
     */
    public IssuesStreamWriter createIssuesStreamWriter(OutputStream out,
                                                       IssuesFormat format,
                                                       OutSettings settings) {
        Checks.isNotNull(out, OUT);
        Checks.isNotNull(format, FORMAT);
        Checks.isNotNull(settings, SETTINGS);

        switch (format) {
        case CSV, ODS, XLS, XLSM, XLSX:
            return createIssuesStreamWriter("cdc.issues.core.io.WorkbookIssuesStreamWriter", out, format, settings);
        case JSON:
            return createIssuesStreamWriter("cdc.issues.core.io.JsonIssuesStreamWriter", out, format, settings);
        case XML:
            return createIssuesStreamWriter("cdc.issues.core.io.XmlIssuesStreamWriter", out, format, settings);
        default:
            break;
        }
        throw new IllegalArgumentException("Can not create an IssuesStreamWriter for " + format);
    }

    /**
     * Creates an IssuesWriter fitted for a file.
     * <p>
     * See {@link IssuesFormat} for a list of supported formats.
     *
     * @param file The file for which an IssuesWriter must be created.
     * @return A new instance of {@link IssuesWriter} fitted for {@code file}.
     * @throws IllegalArgumentException When {@code file} is {@code null} or has an unrecognized extension.
     */
    public IssuesWriter createIssuesWriter(File file) {
        Checks.isNotNull(file, FILE);

        final IssuesFormat format = IssuesFormat.from(file);
        if (format != null) {
            switch (format) {
            case CSV, ODS, XLS, XLSM, XLSX, JSON, XML:
                return createIssuesWriter("cdc.issues.core.io.IssuesWriterImpl", file);
            default:
                break;
            }
        }
        throw new IllegalArgumentException("Can not create an IssuesWriter for " + file);
    }

    /**
     * Creates an IssuesWriter fitted for an output stream and a format.
     *
     * @param out The output stream.
     * @param format The format.
     * @return A new instance of {@link IssuesWriter} fitted for {@code out} and {@code format}.
     * @throws IllegalArgumentException When {@code out} or {@code format} is {@code null}.
     */
    public IssuesWriter createIssuesWriter(OutputStream out,
                                           IssuesFormat format) {
        Checks.isNotNull(out, OUT);
        Checks.isNotNull(format, FORMAT);

        switch (format) {
        case CSV, ODS, XLS, XLSM, XLSX, JSON, XML:
            return createIssuesWriter("cdc.issues.core.io.IssuesWriterImpl", out, format);
        default:
            break;
        }
        throw new IllegalArgumentException("Can not create an IssuesWriter for " + format);
    }

    private IssuesStreamWriter createIssuesStreamWriter(String className,
                                                        File file,
                                                        OutSettings settings) {
        final Class<? extends IssuesStreamWriter> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesStreamWriter.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { File.class, OutSettings.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls,
                                         parameterTypes,
                                         FailureReaction.FAIL,
                                         file,
                                         settings,
                                         getFeatures());
    }

    private IssuesStreamWriter createIssuesStreamWriter(String className,
                                                        OutputStream out,
                                                        IssuesFormat format,
                                                        OutSettings settings) {
        final Class<? extends IssuesStreamWriter> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesStreamWriter.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes =
                { OutputStream.class, IssuesFormat.class, OutSettings.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls, parameterTypes, FailureReaction.FAIL, out, format, settings, getFeatures());
    }

    private IssuesWriter createIssuesWriter(String className,
                                            File file) {
        final Class<? extends IssuesWriter> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesWriter.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { File.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls,
                                         parameterTypes,
                                         FailureReaction.FAIL,
                                         file,
                                         getFeatures());
    }

    private IssuesWriter createIssuesWriter(String className,
                                            OutputStream out,
                                            IssuesFormat format) {
        final Class<? extends IssuesWriter> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesWriter.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { OutputStream.class, IssuesFormat.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls, parameterTypes, FailureReaction.FAIL, out, format, getFeatures());
    }

    /**
     * @param file The file.
     *
     * @return {@code true} if import from {@code file} is supported.
     *         If {@code file} is {@code null} returns {@code false}.
     */
    public boolean canImportFrom(File file) {
        if (file == null) {
            return false;
        } else {
            return IssuesFormat.from(file) != null;
        }
    }

    /**
     * Creates an IssuesReader fitted for a file.
     *
     * @param file The file.
     * @return A new instance of {@link IssuesReader} fitted for {@code file}.
     * @throws IllegalArgumentException When {@code file} is {@code null},
     *             or when {@code file} has an unrecognized extension.
     */
    public IssuesReader createIssuesReader(File file) {
        Checks.isNotNull(file, FILE);

        final IssuesFormat format = IssuesFormat.from(file);
        if (format != null) {
            switch (format) {
            case CSV, ODS, XLS, XLSM, XLSX:
                return createIssuesReader("cdc.issues.core.io.WorkbookIssuesReader", file);
            case JSON:
                return createIssuesReader("cdc.issues.core.io.JsonIssuesReader", file);
            case XML:
                return createIssuesReader("cdc.issues.core.io.XmlIssuesReader", file);
            default:
                break;
            }
        }
        throw new IllegalArgumentException("Can not create an IssuesReader for " + file);
    }

    /**
     * Creates an IssuesReader fitted for an input stream and a format.
     *
     * @param in The input stream.
     * @param format The format.
     * @return A new instance of {@link IssuesReader} fitted for {@code in} and {@code format}.
     * @throws IllegalArgumentException When {@code in} or {@code format} is {@code null}.
     */
    public IssuesReader createIssuesReader(InputStream in,
                                           IssuesFormat format) {
        Checks.isNotNull(in, IN);
        Checks.isNotNull(format, FORMAT);

        switch (format) {
        case CSV, ODS, XLS, XLSM, XLSX:
            return createIssuesReader("cdc.issues.core.io.WorkbookIssuesReader", in, format);
        case JSON:
            return createIssuesReader("cdc.issues.core.io.JsonIssuesReader", in, format);
        case XML:
            return createIssuesReader("cdc.issues.core.io.XmlIssuesReader", in, format);
        default:
            break;
        }
        throw new IllegalArgumentException("Can not create an IssuesReader for " + format);
    }

    private IssuesReader createIssuesReader(String className,
                                            File file) {
        final Class<? extends IssuesReader> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesReader.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { File.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls, parameterTypes, FailureReaction.FAIL, file, getFeatures());
    }

    private IssuesReader createIssuesReader(String className,
                                            InputStream in,
                                            IssuesFormat format) {
        final Class<? extends IssuesReader> cls =
                Introspection.uncheckedCast(Introspection.getClass(className,
                                                                   IssuesReader.class,
                                                                   FailureReaction.FAIL));
        final Class<?>[] parameterTypes = { InputStream.class, IssuesFormat.class, IssuesIoFactoryFeatures.class };
        return Introspection.newInstance(cls, parameterTypes, FailureReaction.FAIL, in, format, getFeatures());
    }
}