package cdc.issues.checks;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cdc.issues.locations.Location;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Specialization of {@link AbstractChecker} that aggregates checkers dedicated to the same object type.
 * <p>
 * This checker does nothing by itself, it delegates checks to its components.
 *
 * @author Damien Carbonne
 *
 * @param <O> The checked object type.
 */
public class CompositeChecker<O> extends AbstractChecker<O> {
    private final List<AbstractChecker<? super O>> components = new ArrayList<>();

    /**
     * Creates an empty composite checker.
     *
     * @param objectClass The object class.
     */
    public CompositeChecker(Class<O> objectClass) {
        super(objectClass);
    }

    /**
     * Creates a composite checker with some components.
     *
     * @param objectClass The object class.
     * @param components The components (checkers).
     */
    @SafeVarargs
    public CompositeChecker(Class<O> objectClass,
                            AbstractChecker<? super O>... components) {
        this(objectClass);
        for (final AbstractChecker<? super O> component : components) {
            add(component);
        }
    }

    public CompositeChecker(Class<O> objectClass,
                            List<AbstractChecker<? super O>> components) {
        this(objectClass);
        for (final AbstractChecker<? super O> component : components) {
            add(component);
        }
    }

    @Override
    protected final List<AbstractChecker<?>> getNext() {
        return Introspection.uncheckedCast(components);
    }

    @Override
    protected final List<AbstractChecker<?>> getNext(SnapshotManager manager) {
        return getNext();
    }

    @SafeVarargs
    public static <O> AbstractChecker<? super O> of(Class<O> objectClass,
                                                    AbstractChecker<? super O>... components) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(components, "components");

        if (components.length == 0) {
            return null;
        } else if (components.length == 1) {
            return components[0];
        } else {
            return new CompositeChecker<>(objectClass,
                                          components);
        }
    }

    public static <O> AbstractChecker<? super O> of(Class<O> objectClass,
                                                    List<AbstractChecker<? super O>> components) {
        Checks.isNotNull(objectClass, "objectClass");
        Checks.isNotNull(components, "components");

        if (components.isEmpty()) {
            return null;
        } else if (components.size() == 1) {
            return components.get(0);
        } else {
            return new CompositeChecker<>(objectClass,
                                          components);
        }
    }

    /**
     * Add a component checker to this composite checker.
     *
     * @param component The component checker.
     * @return This composite checker.
     */
    protected CompositeChecker<O> add(AbstractChecker<? super O> component) {
        components.add(component);
        return this;
    }

    /**
     * @return The components.
     */
    public final List<AbstractChecker<? super O>> getComponents() {
        return components;
    }

    @Override
    public final boolean isEnabled(SnapshotManager manager) {
        return true;
    }

    @Override
    public final boolean isCorrectlyConfigured(SnapshotManager manager) {
        return true;
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   O object,
                                   Location location) {
        for (final AbstractChecker<? super O> component : components) {
            checkAndStat(component,
                         context,
                         object,
                         location);
        }
        return CheckResult.IGNORE;
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("COMPOSITE<" + getObjectClass().getSimpleName() + ">");
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
        for (final AbstractChecker<? super O> component : components) {
            component.print(out, manager, level + 1);
        }
    }

    @Override
    public String toString() {
        return "COMPOSITE<" + getObjectClass().getSimpleName() + ">";
    }
}