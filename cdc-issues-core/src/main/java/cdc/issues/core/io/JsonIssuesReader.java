package cdc.issues.core.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;

import cdc.io.json.AbstractJsonParser;
import cdc.io.json.JsonEvent;
import cdc.io.json.JsonParserWrapper;
import cdc.io.json.JsonpParser;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Meta;
import cdc.issues.Metas;
import cdc.issues.Param;
import cdc.issues.Params;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.issues.impl.IssueAnswerImpl;
import cdc.issues.impl.IssueCommentImpl;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesReader;
import cdc.issues.locations.Location;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.lang.ValueHolder;
import cdc.util.strings.StringUtils;
import cdc.util.time.Chronometer;

/**
 * Implementation of {@link IssuesReader} dedicated to JSON.
 *
 * @author Damien Carbonne
 */
public class JsonIssuesReader extends IssuesIo implements IssuesReader {
    private final File file;
    private final InputStream in;

    private static class NV {
        String name;
        String value;
    }

    private static record IssueAndAnswer(Issue issue,
                                         IssueAnswerImpl answer) {
    }

    public JsonIssuesReader(File file,
                            IssuesIoFactoryFeatures features) {
        super(features);
        this.file = file;
        this.in = null;
    }

    public JsonIssuesReader(InputStream in,
                            IssuesFormat format,
                            IssuesIoFactoryFeatures features) {
        super(features);
        this.file = null;
        this.in = in;
    }

    private final class Wrapper extends JsonParserWrapper {
        private final Settings settings;
        private final ProgressSupplier progress;

        public Wrapper(AbstractJsonParser delegate,
                       Settings settings,
                       ProgressController controller) {
            super(delegate);
            this.settings = settings;
            this.progress = new ProgressSupplier(controller);
        }

        public IssuesAndAnswers parse() {
            progress.reset(-1, "load ?");
            next();
            final IssuesAndAnswersImpl buffer = new IssuesAndAnswersImpl();
            if (isOn(JsonEvent.START_ARRAY)) {
                parseArray(this::parseIssue,
                           p -> {
                               buffer.addIssue(p.issue());
                               if (p.answer() != null) {
                                   buffer.addAnswer(p.answer());
                               }
                               progress.incrementValue();
                           });
            } else {
                parseProjectSnapshot(buffer);
            }
            progress.close();
            return buffer;
        }

        private void parseProjectSnapshot(IssuesAndAnswersImpl buffer) {
            parseObject(key -> {
                if (JsonIo.PROJECT.equals(key)) {
                    skipToEnd();
                } else if (JsonIo.SNAPSHOT.equals(key)) {
                    parseSnapshot(buffer);
                } else {
                    throw unexpectedKeyName(key);
                }
            });
        }

        private void parseSnapshot(IssuesAndAnswersImpl buffer) {
            parseObject(key -> {
                if (JsonIo.ISSUES.equals(key)) {
                    parseArray(this::parseIssue,
                               p -> {
                                   buffer.addIssue(p.issue());
                                   if (p.answer() != null) {
                                       buffer.addAnswer(p.answer());
                                   }
                                   progress.incrementValue();
                               });
                } else {
                    skipToEnd();
                }
            });
        }

        private IssueAndAnswer parseIssue() {
            final Issue.Builder<?> issue = Issue.builder();
            final ValueHolder<IssueAnswerImpl.Builder> answer = new ValueHolder<>();

            parseObject(key -> {
                if (JsonIo.TIMESTAMP.equals(key)) {
                    issue.timestamp(toInstant(getStringValue()));
                } else if (JsonIo.DOMAIN.equals(key)) {
                    issue.domain(getStringValue());
                } else if (JsonIo.SNAPSHOT.equals(key)) {
                    issue.snapshot(getStringValue());
                } else if (JsonIo.NAME.equals(key)) {
                    issue.name(getStringValue());
                } else if (JsonIo.TITLE.equals(key)) {
                    issue.title(getStringValue());
                } else if (JsonIo.PARAMS.equals(key)) {
                    issue.params(parseParams());
                } else if (JsonIo.PROJECT.equals(key)) {
                    issue.project(getStringValue());
                } else if (JsonIo.SEVERITY.equals(key)) {
                    issue.severity(getEnumValue(IssueSeverity.class));
                } else if (JsonIo.DESCRIPTION.equals(key)) {
                    issue.description(getStringValue());
                } else if (JsonIo.LOCATIONS.equals(key)) {
                    issue.locations(parseArray(this::parseLocation));
                } else if (JsonIo.METAS.equals(key)) {
                    issue.metas(parseMetas());
                } else if (JsonIo.LABELS.equals(key)) {
                    issue.labels(parseLabels());
                } else if (JsonIo.ANSWER.equals(key)) {
                    if (settings.getHints().contains(Settings.Hint.NO_ANSWERS)) {
                        skipToEnd();
                    } else {
                        answer.value = IssueAnswerImpl.builder();
                        parseAnswer(answer.value);
                    }
                }
            });

            final Issue iss = issue.build();
            if (answer.value == null) {
                return new IssueAndAnswer(iss, null);
            } else {
                answer.value.issueId(iss.getId());
                return new IssueAndAnswer(iss, answer.value.build());
            }
        }

        private Location parseLocation() {
            final Location.Builder location = Location.builder();
            parseObject(key -> {
                if (JsonIo.TAG.equals(key)) {
                    location.tag(getStringValue());
                } else if (JsonIo.PATH.equals(key)) {
                    location.path(getStringValue());
                } else if (JsonIo.ANCHOR.equals(key)) {
                    location.anchor(getStringValue());
                }
            });
            return location.build();
        }

        private Metas parseMetas() {
            final List<Meta> metas = parseArray(this::parseMeta);
            return Metas.builder()
                        .metas(metas)
                        .build();
        }

        private Meta parseMeta() {
            final NV nv = new NV();
            parseObject(key -> {
                if (JsonIo.NAME.equals(key)) {
                    nv.name = getStringValue();
                } else if (JsonIo.VALUE.equals(key)) {
                    nv.value = StringUtils.nullify(getStringValue());
                }
            });
            return Meta.of(nv.name, nv.value);
        }

        private Params parseParams() {
            final List<Param> params = parseArray(this::parseParam);
            return Params.builder()
                         .params(params)
                         .build();
        }

        private Param parseParam() {
            final NV nv = new NV();
            parseObject(key -> {
                if (JsonIo.NAME.equals(key)) {
                    nv.name = getStringValue();
                } else if (JsonIo.VALUE.equals(key)) {
                    nv.value = StringUtils.nullify(getStringValue());
                }
            });
            return Param.of(nv.name, nv.value);
        }

        private Labels parseLabels() {
            final List<String> list = parseArray(AbstractJsonParser::getStringValue);
            return Labels.of(list);
        }

        private void parseAnswer(IssueAnswerImpl.Builder answer) {
            parseObject(key -> {
                if (JsonIo.AUTHOR.equals(key)) {
                    answer.author(getStringValue());
                } else if (JsonIo.CREATED.equals(key)) {
                    answer.creationDate(toInstant(getStringValue()));
                } else if (JsonIo.MODIFIED.equals(key)) {
                    answer.modificationDate(toInstant(getStringValue()));
                } else if (JsonIo.STATUS.equals(key)) {
                    answer.status(getEnumValue(IssueStatus.class));
                } else if (JsonIo.RESOLUTION.equals(key)) {
                    answer.resolution(getEnumValue(IssueResolution.class));
                } else if (JsonIo.ASSIGNEE.equals(key)) {
                    answer.assignee(getStringValue());
                } else if (JsonIo.NEW_SEVERITY.equals(key)) {
                    answer.newSeverity(getEnumValue(IssueSeverity.class));
                } else if (JsonIo.COMMENTS.equals(key)) {
                    final List<IssueCommentImpl> comments = parseArray(this::parseComment);
                    answer.comments(comments);
                } else if (JsonIo.METAS.equals(key)) {
                    answer.metas(parseMetas());
                } else if (JsonIo.LABELS.equals(key)) {
                    answer.labels(parseLabels());
                }
            });
        }

        private IssueCommentImpl parseComment() {
            final IssueCommentImpl.Builder comment = IssueCommentImpl.builder();
            parseObject(key -> {
                if (JsonIo.AUTHOR.equals(key)) {
                    comment.author(getStringValue());
                } else if (JsonIo.DATE.equals(key)) {
                    comment.date(toInstant(getStringValue()));
                } else if (JsonIo.TEXT.equals(key)) {
                    comment.text(getStringValue());
                }
            });
            return comment.build();
        }

    }

    @Override
    public IssuesAndAnswers load(Settings settings,
                                 ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final String systemId = file == null ? "???" : file.getPath();
        traceLoad(systemId);
        final JsonParserFactory factory = Json.createParserFactory(null);

        try (final InputStream is = file == null ? in : new FileInputStream(file);
                final JsonParser parser = factory.createParser(new BufferedInputStream(is))) {
            final Wrapper wrapper = new Wrapper(new JsonpParser(parser),
                                                settings,
                                                controller);
            return wrapper.parse();
        } finally {
            chrono.suspend();
            traceLoaded(systemId, chrono);
        }
    }
}