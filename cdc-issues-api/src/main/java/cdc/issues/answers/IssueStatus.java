package cdc.issues.answers;

import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of possible issue statues.
 *
 * @author Damien Carbonne
 */
public enum IssueStatus {
    /**
     * The issue has been detected, is not yet resolved,
     * and no user action has been taken.
     */
    OPEN,

    /**
     * A user reviewed an {@link #OPEN} issue and confirmed it.<br>
     * Its resolution depends on internal actions.
     */
    CONFIRMED,

    /**
     * A user reviewed an open or confirmed issue and indicated that
     * its resolution depends on external actions.
     */
    WAITING,

    /**
     * The issue is considered as resolved.
     * <p>
     * Its resolution must be {@link IssueResolution#FIXED},
     * {@link IssueResolution#FALSE_POSITIVE}
     * or {@link IssueResolution#WONT_FIX}.
     */
    RESOLVED,

    /**
     * The issue was marked as {@link #RESOLVED}
     * and {@link IssueResolution#FIXED} by a user, but is still detected.
     */
    REOPENED,

    /**
     * The issue was marked as {@link #RESOLVED} and has disappeared.
     */
    CLOSED;

    /**
     * @param resolution The resolution.
     * @return {@code true} if {@code resolution} is compliant with this status.
     */
    public boolean isCompliantWith(IssueResolution resolution) {
        if (resolution == IssueResolution.UNRESOLVED) {
            return this == OPEN
                    || this == REOPENED
                    || this == CONFIRMED
                    || this == WAITING;
        } else if (resolution == IssueResolution.REMOVED) {
            return this == IssueStatus.CLOSED;
        } else if (resolution == IssueResolution.FIXED
                || resolution == IssueResolution.FALSE_POSITIVE
                || resolution == IssueResolution.WONT_FIX) {
            return this == CLOSED || this == RESOLVED;
        } else {
            throw new UnexpectedValueException(resolution);
        }
    }
}