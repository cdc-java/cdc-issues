package cdc.issues.locations;

import java.util.Objects;

public abstract class AbstractLocation implements Location {
    @Override
    public int hashCode() {
        return Objects.hash(getPath(),
                            getAnchor());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractLocation)) {
            return false;
        }
        final AbstractLocation other = (AbstractLocation) object;
        return Objects.equals(this.getPath(), other.getPath())
                && Objects.equals(this.getAnchor(), other.getAnchor());
    }

    @Override
    public String toString() {
        return Location.toString(this);
    }
}