package cdc.issues.answers;

/**
 * Enumeration of possible issue resolutions.
 *
 * @author Damien Carbonne
 */
public enum IssueResolution {
    /**
     * The issue is unresolved.
     */
    UNRESOLVED,

    /**
     * The issue has been fixed.
     * <p>
     * Its status must be {@link IssueStatus#RESOLVED}
     * or {@link IssueStatus#CLOSED}.
     * <p>
     * This should be set automatically or by a user.
     */
    FIXED,

    /**
     * The issue has disappeared.
     * <p>
     * Its status must be {@link IssueStatus#CLOSED}.
     * <p>
     * This should be set automatically.
     */
    REMOVED,

    /**
     * The issue is a false positive.
     * <p>
     * This may be due to the rule detection being imperfect,
     * or an inappropriate rule.
     * <p>
     * Its status must be {@link IssueStatus#RESOLVED}
     * or {@link IssueStatus#CLOSED}.
     * <p>
     * This can only be set by a user.
     */
    FALSE_POSITIVE,

    /**
     * For any reason, the issue won't be fixed.
     * <p>
     * Its status must be {@link IssueStatus#RESOLVED}
     * or {@link IssueStatus#CLOSED}.
     * <p>
     * This can only be set by a user.
     */
    WONT_FIX
}