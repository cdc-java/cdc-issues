package cdc.issues.io;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;

import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.util.lang.Checks;
import cdc.util.lang.ExceptionWrapper;

/**
 * Implementation of {@link IssuesHandler} that can directly dump issues
 * to a {@link File} or an {@link OutputStream}.
 *
 * @author Damien Carbonne
 */
public class StreamIssuesHandler implements IssuesHandler, Flushable, Closeable {
    private final IssuesStreamWriter writer;

    // TODO Use ExceptionWrapper.wrap()
    @FunctionalInterface
    private static interface Procedure {
        void invoke() throws Exception;
    }

    // TODO Use ExceptionWrapper.wrap()
    private static void wrap(Procedure procedure) {
        try {
            procedure.invoke();
        } catch (final Exception e) {
            throw ExceptionWrapper.wrap(e);
        }
    }

    protected StreamIssuesHandler(Builder builder) {
        Checks.isNotNull(builder.settings, "settings");
        Checks.isTrue(builder.file != null || builder.out != null, "File and output stream are null.");
        final IssuesIoFactory factory = new IssuesIoFactory(Checks.isNotNull(builder.features, "features"));

        if (builder.file != null) {
            this.writer = factory.createIssuesStreamWriter(builder.file, builder.settings);
        } else {
            Checks.isNotNull(builder.format, "format");
            this.writer = factory.createIssuesStreamWriter(builder.out, builder.format, builder.settings);
        }
        wrap(() -> writer.startDocument());

        if (builder.snapshot != null) {
            wrap(() -> writer.add(builder.snapshot));
        }
    }

    @Override
    public void issue(Issue issue) {
        wrap(() -> writer.add(issue));
    }

    @Override
    public void close() throws IOException {
        this.writer.endDocument();
        this.writer.close();
    }

    @Override
    public void flush() throws IOException {
        this.writer.flush();
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of StreamIssuesHandler.
     * <p>
     * One must use {@link #file(File)} or {@link #out(OutputStream)}.
     */
    public static class Builder {
        private IssuesIoFactoryFeatures features = IssuesIoFactoryFeatures.UTC_FASTEST;
        private OutSettings settings = OutSettings.ALL_DATA_NO_ANSWERS;
        private File file;
        private OutputStream out;
        private IssuesFormat format = IssuesFormat.CSV;
        private SnapshotData snapshot;

        /**
         * Sets the factory features.
         * <p>
         * Default: {@link IssuesIoFactoryFeatures#UTC_FASTEST}.
         *
         * @param features The factory features.
         * @return This Builder.
         */
        public Builder features(IssuesIoFactoryFeatures features) {
            this.features = features;
            return this;
        }

        /**
         * Sets the out settings.
         * <p>
         * Default: {@link OutSettings#ALL_DATA_NO_ANSWERS}.
         *
         * @param settings The out settings.
         * @return This Builder.
         */
        public Builder settings(OutSettings settings) {
            this.settings = settings;
            return this;
        }

        /**
         * Sets the output file.
         *
         * @param file The file.
         * @return This Builder.
         */
        public Builder file(File file) {
            this.out = null;
            this.file = file;
            return this;
        }

        /**
         * Sets the output stream.
         * <p>
         * Should be used with {@link #format(IssuesFormat)}.
         *
         * @param out The outputStream to use.
         * @return This Builder.
         */
        public Builder out(OutputStream out) {
            this.file = null;
            this.out = out;
            return this;
        }

        /**
         * Sets the output format.
         * <p>
         * Can be used with {@link #out(OutputStream)}.<br>
         * Default: {@link IssuesFormat#CSV}.
         *
         * @param format The format.
         * @return This Builder.
         */
        public Builder format(IssuesFormat format) {
            this.format = format;
            return this;
        }

        /**
         * Sets the snapshot data.
         * <p>
         * This is optional and may be {@code null}.
         *
         * @param snapshot The snapshot data.
         * @return This Builder.
         */
        public Builder snapshot(SnapshotData snapshot) {
            this.snapshot = snapshot;
            return this;
        }

        public StreamIssuesHandler build() {
            return new StreamIssuesHandler(this);
        }
    }
}