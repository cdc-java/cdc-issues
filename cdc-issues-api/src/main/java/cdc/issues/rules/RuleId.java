package cdc.issues.rules;

import java.util.Objects;

import cdc.util.lang.Checks;

/**
 * Rule identifier.
 * <p>
 * It is composed of a domain and a name (in the domain).
 *
 * @author Damien Carbonne
 */
public class RuleId implements Comparable<RuleId> {
    private static final String DOMAIN = "domain";
    private static final String NAME = "name";

    private final String domain;
    private final String name;

    private static String getDomain(String s) {
        final int loc = s.indexOf('/');
        return s.substring(0, loc);
    }

    private static String getName(String s) {
        final int loc = s.indexOf('/');
        return s.substring(loc + 1);
    }

    public static final RuleId NONE = new RuleId("???", "???");

    /**
     * Creates a RuleId.
     *
     * @param domain The domain name.
     * @param name The rule name.
     * @throws IllegalArgumentException When {@code domain} or {@code name} is {@code null}.
     */
    public RuleId(String domain,
                  String name) {
        this.domain = Checks.isNotNull(domain, DOMAIN);
        this.name = Checks.isNotNull(name, NAME);
    }

    /**
     * Creates a RuleId.
     *
     * @param s The string representation of rule id: {@code domain/name}.
     *
     */
    public RuleId(String s) {
        this(getDomain(s), getName(s));
    }

    /**
     * Creates a RuleId.
     *
     * @param domain The domain name.
     * @param name The rule name.
     * @throws IllegalArgumentException When {@code domain} or {@code name} is {@code null}.
     */
    public RuleId(String domain,
                  Enum<?> name) {
        this.domain = Checks.isNotNull(domain, DOMAIN);
        this.name = Checks.isNotNull(name, NAME).name();
    }

    /**
     * @return The rule domain name.
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @return The rule name.
     */
    public String getName() {
        return name;
    }

    public <T extends Enum<T>> T getName(Class<T> cls) {
        return Enum.valueOf(cls, name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domain,
                            name);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RuleId)) {
            return false;
        }
        final RuleId other = (RuleId) object;
        return this.domain.equals(other.domain)
                && this.name.equals(other.name);
    }

    @Override
    public int compareTo(RuleId other) {
        final int domainCmp = domain.compareTo(other.domain);
        if (domainCmp == 0) {
            return name.compareTo(other.name);
        } else {
            return domainCmp;
        }
    }

    @Override
    public String toString() {
        return domain + "/" + name;
    }
}