package cdc.issues;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.util.lang.Checks;

/**
 * Formal parameters.
 * <p>
 * It is a set of (name, description) pairs.
 *
 * @author Damien Carbonne
 */
public final class FormalParams {
    private static final String NAME = "name";

    public static final FormalParams NO_PARAMS = builder().build();

    private final Map<String, String> map = new HashMap<>();

    private FormalParams(Builder builder) {
        for (final Map.Entry<String, String> entry : builder.map.entrySet()) {
            this.map.put(entry.getKey(), entry.getValue());
        }
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    /**
     * @return The parameter names.
     */
    public Set<String> getNames() {
        return map.keySet();
    }

    /**
     * @return The sorted parameter names.
     */
    public List<String> getSortedNames() {
        return map.keySet()
                  .stream()
                  .sorted()
                  .toList();
    }

    /**
     * @param name The parameter name.
     * @return The description of parameter named {@code name}.
     */
    public String getDescription(String name) {
        return map.get(name);
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof FormalParams)) {
            return false;
        }
        final FormalParams other = (FormalParams) object;
        return this.map.equals(other.map);
    }

    @Override
    public String toString() {
        return getSortedNames().stream()
                               .map(name -> name + ":" + getDescription(name))
                               .collect(Collectors.joining(" ", "[", "]"));
    }

    /**
     * @return A new {@link Builder} of {@link FormalParams}.
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of {@link FormalParams}.
     */
    public static final class Builder {
        private final Map<String, String> map = new HashMap<>();

        private Builder() {
        }

        public Builder param(String name,
                             String description) {
            Checks.isNotNull(name, NAME);
            map.put(name, description);
            return this;
        }

        public FormalParams build() {
            return new FormalParams(this);
        }
    }
}