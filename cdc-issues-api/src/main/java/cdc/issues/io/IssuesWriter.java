package cdc.issues.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import cdc.issues.Issue;
import cdc.issues.Snapshot;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.util.events.ProgressController;

/**
 * Interface implemented by classes that can save Issues, Answers, Project, Snapshot
 * to a {@link File} or an {@link OutputStream}.
 * <p>
 * One must use {@link IssuesIoFactory#createIssuesWriter(File)} or
 * {@link IssuesIoFactory#createIssuesWriter(OutputStream, IssuesFormat)}
 * to create an instance of {@link IssuesWriter}.
 * <p>
 * There are 2 modes of generation:
 * <ol>
 * <li><b>Issues and Answers</b> mode.
 * <li><b>Project, Profile and Snapshot</b> mode.
 * </ol>
 *
 * @author Damien Carbonne
 * @see IssuesFormat
 */
public interface IssuesWriter {

    /**
     * Saves a list of issues and associated answers.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issuesAndAnswers The issues and answers.
     * @param settings The settings.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void save(IssuesAndAnswers issuesAndAnswers,
                     OutSettings settings,
                     ProgressController controller) throws IOException;

    /**
     * Saves a list of issues.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issues The issues.
     * @param settings The settings.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void save(List<? extends Issue> issues,
                     OutSettings settings,
                     ProgressController controller) throws IOException;

    /**
     * Saves a project and snapshot to a file.
     * <p>
     * <b>Project, Profile and Snapshot</b> mode.
     *
     * @param snapshot The snapshot.
     * @param settings The settings.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void save(Snapshot snapshot,
                     OutSettings settings,
                     ProgressController controller) throws IOException;

    /**
     * Saves snapshot data and a list of issues.
     * <p>
     * <b>Project, Profile and Snapshot</b> mode.
     * <p>
     * <b>WARNING</b>: no check is done that {@code synthesis} and {@code issues} are consistent.
     *
     * @param synthesis The snapshot data.
     * @param issues The issues.
     * @param settings The settings.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void save(SnapshotData synthesis,
                     List<? extends Issue> issues,
                     OutSettings settings,
                     ProgressController controller) throws IOException;

    /**
     * Saves snapshot data, a list of issues and associated answers.
     * <p>
     * <b>Project, Profile and Snapshot</b> mode.
     * <p>
     * <b>WARNING</b>: no check is done that {@code synthesis} and {@code issuesAndAnswers} are consistent.
     *
     * @param synthesis The snapshot data.
     * @param issuesAndAnswers The issues and answers.
     * @param settings The settings.
     * @param controller The progress controller.
     * @throws IOException When an IO error occurs.
     */
    public void save(SnapshotData synthesis,
                     IssuesAndAnswers issuesAndAnswers,
                     OutSettings settings,
                     ProgressController controller) throws IOException;

    /**
     * Saves a list of issues and associated answers to a file.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issuesAndAnswers The issues and answers.
     * @param settings The settings.
     * @param file The file.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(IssuesAndAnswers issuesAndAnswers,
                            OutSettings settings,
                            File file,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(file);
        writer.save(issuesAndAnswers, settings, controller);
    }

    /**
     * Saves a list of issues to a file.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issues The issues.
     * @param settings The settings.
     * @param file The file.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(List<? extends Issue> issues,
                            OutSettings settings,
                            File file,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(file);
        writer.save(issues, settings, controller);
    }

    /**
     * Saves a list of issues to a file using {@link OutSettings#ALL_DATA_NO_ANSWERS} settings.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issues The issues.
     * @param file The file.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(List<? extends Issue> issues,
                            File file,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        save(issues,
             OutSettings.ALL_DATA_NO_ANSWERS,
             file,
             controller,
             features);
    }

    /**
     * Saves a project and snapshot to a file.
     * <p>
     * <b>Project, Profile and Snapshot</b> mode.
     *
     * @param snapshot The snapshot It should belong to {@code project}.
     * @param settings The settings.
     * @param file The file.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(Snapshot snapshot,
                            OutSettings settings,
                            File file,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(file);
        writer.save(snapshot, settings, controller);
    }

    /**
     * Saves a list of issues and associated answers to an output stream.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issuesAndAnswers The issues and answers.
     * @param settings The settings.
     * @param out The output stream.
     * @param format The output format.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(IssuesAndAnswers issuesAndAnswers,
                            OutSettings settings,
                            OutputStream out,
                            IssuesFormat format,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(out, format);
        writer.save(issuesAndAnswers, settings, controller);
    }

    /**
     * Saves a list of issues to an output stream.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issues The issues.
     * @param settings The settings.
     * @param out The output stream.
     * @param format The output format.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(List<? extends Issue> issues,
                            OutSettings settings,
                            OutputStream out,
                            IssuesFormat format,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(out, format);
        writer.save(issues, settings, controller);
    }

    /**
     * Saves a list of issues to an output stream.
     * <p>
     * <b>Issues and Answers</b> mode.
     *
     * @param issues The issues.
     * @param out The output stream.
     * @param format The output format.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(List<? extends Issue> issues,
                            OutputStream out,
                            IssuesFormat format,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        save(issues,
             OutSettings.ALL_DATA_NO_ANSWERS,
             out,
             format,
             controller,
             features);
    }

    /**
     * Saves snapshot data and a list of issues to a file.
     * <p>
     * <b>Project, Profile and Snapshot</b> mode.
     *
     * @param synthesis The snapshot data.
     * @param issues The issues.
     * @param settings The settings.
     * @param file The file.
     * @param controller The progress controller.
     * @param features The features used to configure {@code file} generation.
     * @throws IOException When an IO error occurs.
     */
    public static void save(SnapshotData synthesis,
                            List<? extends Issue> issues,
                            OutSettings settings,
                            File file,
                            ProgressController controller,
                            IssuesIoFactoryFeatures features) throws IOException {
        final IssuesIoFactory factory = new IssuesIoFactory(features);
        final IssuesWriter writer = factory.createIssuesWriter(file);
        writer.save(synthesis,
                    issues,
                    settings,
                    controller);
    }
}