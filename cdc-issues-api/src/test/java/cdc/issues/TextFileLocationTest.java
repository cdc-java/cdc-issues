package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.TextFileLocation;

class TextFileLocationTest {
    @Test
    void test() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         TextFileLocation.builder()
                                         .build();
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         TextFileLocation.builder()
                                         .systemId("path")
                                         .lineNumber(0)
                                         .columnNumber(1)
                                         .build();
                     });

        final TextFileLocation location0a = TextFileLocation.builder()
                                                            .systemId("path")
                                                            .build();

        final TextFileLocation location1a = TextFileLocation.builder()
                                                            .systemId("path")
                                                            .lineNumber(1)
                                                            .columnNumber(1)
                                                            .build();

        final TextFileLocation location1b = TextFileLocation.builder()
                                                            .systemId("path")
                                                            .lineNumber(1)
                                                            .columnNumber(1)
                                                            .build();

        final TextFileLocation location2a = TextFileLocation.builder()
                                                            .systemId("path")
                                                            .lineNumber(1)
                                                            .build();
        final TextFileLocation location2b = TextFileLocation.builder()
                                                            .systemId("path")
                                                            .lineNumber(1)
                                                            .build();

        assertNotEquals(location1a, null);
        assertNotEquals(location1a, "Hello");
        assertEquals(location1a, location1a);
        assertEquals(location1a, location1b);

        assertEquals(location1a.toString(), location1b.toString());
        assertEquals(location2a.toString(), location2b.toString());
        assertEquals(location1a.hashCode(), location1b.hashCode());
        assertEquals(location0a.toString(), location0a.toString());

        assertEquals(location1a.toString(), location1b.toString());
        assertEquals("path", location1a.getPath());
        assertEquals(1, location1a.getLineNumber());
        assertEquals(1, location1a.getColumnNumber());
        assertTrue(location2a.getColumnNumber() <= 0);
        assertTrue(location0a.getColumnNumber() <= 0);
        assertTrue(location0a.getLineNumber() <= 0);
    }

    @Test
    void testCreate() {
        assertEquals(TextFileLocation.builder()
                                     .systemId("systemId")
                                     .build(),
                     TextFileLocation.create("systemId", null));
        assertEquals(TextFileLocation.builder()
                                     .systemId("systemId")
                                     .lineNumber(10)
                                     .build(),
                     TextFileLocation.create("systemId", "10"));
        assertEquals(TextFileLocation.builder()
                                     .systemId("systemId")
                                     .lineNumber(10)
                                     .columnNumber(5)
                                     .build(),
                     TextFileLocation.create("systemId", "10:5"));
    }
}