package cdc.issues.answers;

import java.time.Instant;
import java.util.List;

import cdc.issues.IssueId;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;

/**
 * Interface describing the answer that can be given to an issue.
 *
 * @author Damien Carbonne
 */
public interface IssueAnswer {
    /**
     * @return The id of associated issues.<br>
     *         Should <em>NOT</em> be {@code null}.
     */
    public IssueId getIssueId();

    /**
     * @return The author of this answer.
     */
    public String getAuthor();

    /**
     * @return The creation date (as an Instant) of this answer.
     */
    public Instant getCreationDate();

    /**
     * @return The last modification date (as an Instant) of this answer.
     */
    public Instant getModificationDate();

    /**
     * @return The status of this answer.<br>
     *         Should <em>NOT</em> be {@code null}.
     */
    public IssueStatus getStatus();

    /**
     * @return The resolution of this answer.
     *         May be {@code null}.
     */
    public IssueResolution getResolution();

    /**
     * @return The assignee of this answer.
     */
    public String getAssignee();

    /**
     * @return The new severity of this answer.<br>
     *         May be {@code null} if severity was not changed.
     */
    public IssueSeverity getNewSeverity();

    /**
     * @return The comments of this answer.
     */
    public List<? extends IssueComment> getComments();

    /**
     * @return The meta data of this answer.
     */
    public Metas getMetas();

    /**
     * @return The labels of this answer.
     */
    public Labels getLabels();
}