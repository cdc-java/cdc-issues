package cdc.issues.io;

import java.io.File;
import java.io.IOException;

import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;

/**
 * Interface describing IO for {@link Profile}.
 * <p>
 * It can be constructed with {@link ProfileIoFactory}.
 *
 * @author Damien Carbonne
 */
public interface ProfileIo {
    /**
     * @return The used {@link ProfileIoFeatures features}.
     */
    public ProfileIoFeatures getFeatures();

    /**
     * Saves a Profile to a file.
     *
     * @param profile The profile.
     * @param file The file.
     * @throws IOException When an IO error occurs.
     */
    public void save(Profile profile,
                     File file) throws IOException;

    public void save(ProfileConfig config,
                     File file) throws IOException;

    /**
     * Loads a Profile from a file.
     *
     * @param file The file.
     * @return The loaded Profile.
     * @throws IOException When an IO error occurs.
     */
    public Profile loadProfile(File file) throws IOException;

    public ProfileConfig loadProfileConfig(File file) throws IOException;

    /**
     * Saves a profile to a file using particular features.
     *
     * @param features The features.
     * @param profile The profile.
     * @param file The file.
     * @throws IOException When an IO error occurs.
     */
    public static void save(ProfileIoFeatures features,
                            Profile profile,
                            File file) throws IOException {
        final ProfileIo io = ProfileIoFactory.create(features);
        io.save(profile, file);
    }

    public static void save(ProfileIoFeatures features,
                            ProfileConfig config,
                            File file) throws IOException {
        final ProfileIo io = ProfileIoFactory.create(features);
        io.save(config, file);
    }

    /**
     *
     * @param features The features.
     * @param file The file.
     * @return A new instance of profile created by reading {@code file} and configured with {@code features}.
     * @throws IOException When an IO error occurs.
     */
    public static Profile loadProfile(ProfileIoFeatures features,
                                      File file) throws IOException {
        final ProfileIo io = ProfileIoFactory.create(features);
        return io.loadProfile(file);
    }

    public static ProfileConfig loadProfileConfig(ProfileIoFeatures features,
                                                  File file) throws IOException {
        final ProfileIo io = ProfileIoFactory.create(features);
        return io.loadProfileConfig(file);
    }
}