package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.WorkbookLocation;

class WorkbookLocationTest {
    @Test
    void test() {
        final WorkbookLocation workbooka = WorkbookLocation.builder()
                                                           .systemId("file")
                                                           .build();
        final WorkbookLocation workbookb = WorkbookLocation.builder()
                                                           .systemId("file")
                                                           .build();
        final WorkbookLocation sheeta = WorkbookLocation.builder()
                                                        .systemId("file")
                                                        .sheetName("sheet")
                                                        .build();
        final WorkbookLocation sheetb = WorkbookLocation.builder()
                                                        .systemId("file")
                                                        .sheetName("sheet")
                                                        .build();
        final WorkbookLocation cola = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .columnName("col")
                                                      .build();
        final WorkbookLocation colb = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .columnName("col")
                                                      .build();
        final WorkbookLocation rowa = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .rowNumber(1)
                                                      .build();
        final WorkbookLocation rowb = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .rowNumber(1)
                                                      .build();

        final WorkbookLocation rowc = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .rowId("rid")
                                                      .build();
        final WorkbookLocation rowd = WorkbookLocation.builder()
                                                      .systemId("file")
                                                      .sheetName("sheet")
                                                      .rowId("rid")
                                                      .build();

        final WorkbookLocation cella = WorkbookLocation.builder()
                                                       .systemId("file")
                                                       .sheetName("sheet")
                                                       .rowNumber(1)
                                                       .columnName("col")
                                                       .build();
        final WorkbookLocation cellb = WorkbookLocation.builder()
                                                       .systemId("file")
                                                       .sheetName("sheet")
                                                       .rowNumber(1)
                                                       .columnName("col")
                                                       .build();

        assertEquals("file", workbooka.getPath());
        assertEquals("file", workbooka.getSystemId());
        assertEquals(null, workbooka.getSheetName());
        assertFalse(workbooka.hasAnchor());
        assertTrue(workbooka.getRowNumber() <= 0);
        assertSame(null, workbooka.getRowId());
        assertEquals(null, workbooka.getColumnName());
        assertFalse(workbooka.isSheetLocation());
        assertFalse(workbooka.isCellLocation());
        assertFalse(workbooka.isRowLocation());
        assertFalse(workbooka.isColumnLocation());
        assertTrue(workbooka.isWorkbookLocation());

        // SystemId is missing
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WorkbookLocation.builder()
                                         .build();
                     });
        // sheet name is missing
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WorkbookLocation.builder()
                                         .systemId("file")
                                         .rowNumber(1)
                                         .build();
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WorkbookLocation.builder()
                                         .systemId("file")
                                         .rowId("rid")
                                         .build();
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WorkbookLocation.builder()
                                         .systemId("file")
                                         .columnName("col")
                                         .build();
                     });

        // Both row number and row id
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WorkbookLocation.builder()
                                         .systemId("file")
                                         .sheetName("sheet")
                                         .rowNumber(1)
                                         .rowId("rid")
                                         .build();
                     });

        assertEquals(workbooka, workbookb);
        assertEquals(workbooka.hashCode(), workbookb.hashCode());
        assertEquals(workbooka.toString(), workbookb.toString());
        assertEquals(sheeta.toString(), sheetb.toString());

        assertTrue(sheeta.isSheetLocation());
        assertFalse(sheeta.isCellLocation());
        assertFalse(sheeta.isRowLocation());
        assertFalse(sheeta.isColumnLocation());

        assertFalse(cola.isSheetLocation());
        assertFalse(cola.isCellLocation());
        assertFalse(cola.isRowLocation());
        assertTrue(cola.isColumnLocation());

        assertFalse(rowa.isSheetLocation());
        assertFalse(rowa.isCellLocation());
        assertTrue(rowa.isRowLocation());
        assertFalse(rowa.isColumnLocation());

        assertFalse(rowc.isSheetLocation());
        assertFalse(rowc.isCellLocation());
        assertTrue(rowc.isRowLocation());
        assertFalse(rowc.isColumnLocation());

        assertFalse(cella.isSheetLocation());
        assertTrue(cella.isCellLocation());
        assertFalse(cella.isRowLocation());
        assertFalse(cella.isColumnLocation());

        assertEquals(sheeta.toString(), sheetb.toString());
        assertEquals(cola.toString(), colb.toString());
        assertEquals(rowa.toString(), rowb.toString());
        assertEquals(rowc.toString(), rowd.toString());
        assertEquals(cella.toString(), cellb.toString());
    }

    @Test
    void testCreate() {
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .build(),
                     WorkbookLocation.create("systemId", null));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet"));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .columnName("Column")
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet[Column]"));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .columnName("Column")
                                     .rowNumber(10)
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet[Column:10]"));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .columnName("Column")
                                     .rowId("Row")
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet[Column:Row]"));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .rowNumber(10)
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet[:10]"));
        assertEquals(WorkbookLocation.builder()
                                     .systemId("systemId")
                                     .sheetName("Sheet")
                                     .rowId("Row")
                                     .build(),
                     WorkbookLocation.create("systemId", "Sheet[:Row]"));
    }
}
