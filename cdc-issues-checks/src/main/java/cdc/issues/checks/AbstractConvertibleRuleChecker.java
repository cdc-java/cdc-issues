package cdc.issues.checks;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import cdc.issues.StructuredDescription;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.lang.Checks;

/**
 * Specialization of {@link AbstractRuleChecker} that can check that an object is convertible to a target type,
 * and then optionally run a delegate checker on the conversion result.
 *
 * @author Damien Carbonne
 *
 * @param <O> The checked object type.
 * @param <T> The target object type.
 */
public abstract class AbstractConvertibleRuleChecker<O, T> extends AbstractRuleChecker<O> {
    private final Function<O, T> converter;
    private final Optional<AbstractChecker<T>> delegate;
    private final Class<T> targetClass;

    protected AbstractConvertibleRuleChecker(Class<O> objectClass,
                                             Function<O, T> converter,
                                             AbstractChecker<T> delegate,
                                             Rule rule) {
        super(objectClass,
              rule);
        this.converter = Checks.isNotNull(converter, "converter");
        this.delegate = Optional.of(Checks.isNotNull(delegate, "delegate"));
        this.targetClass = delegate.getObjectClass();
    }

    protected AbstractConvertibleRuleChecker(Class<O> objectClass,
                                             Function<O, T> converter,
                                             Class<T> targetClass,
                                             Rule rule) {
        super(objectClass,
              rule);
        this.converter = Checks.isNotNull(converter, "converter");
        this.delegate = Optional.empty();
        this.targetClass = Checks.isNotNull(targetClass, "targetClass");
    }

    /**
     * @return The function used to convert object to target type.
     */
    public final Function<O, T> getConverter() {
        return converter;
    }

    /**
     * @return The optional checker used to check conversion result.
     */
    public final Optional<AbstractChecker<T>> getDelegate() {
        return delegate;
    }

    /**
     * @return The target class.
     */
    public final Class<T> getTargetClass() {
        return targetClass;
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        T target = null;
        boolean converted = true;
        try {
            target = converter.apply(object);
        } catch (final RuntimeException e) {
            converted = false;
        }

        if (converted) {
            if (delegate.isPresent()) {
                checkAndStat(delegate.orElseThrow(),
                             context,
                             target,
                             location);
            }
            return CheckResult.SUCCESS;
        } else {
            final StructuredDescription.Builder<?> description = StructuredDescription.builder();

            description.header(getHeader(object))
                       .value(Objects.toString(object))
                       .violation("can not be converted to " + targetClass.getSimpleName());

            add(context.getManager(),
                issue(context.getManager()).description(description)
                                           .location(location)
                                           .build());
            return CheckResult.FAILURE;
        }
    }
}