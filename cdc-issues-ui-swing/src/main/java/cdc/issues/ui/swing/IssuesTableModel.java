package cdc.issues.ui.swing;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesHandler;
import cdc.issues.Labels;
import cdc.issues.Params;
import cdc.issues.locations.Location;
import cdc.ui.swing.SwingUtils;

public class IssuesTableModel extends AbstractTableModel implements IssuesHandler {
    private static final long serialVersionUID = 1L;
    private static final String[] COLUMN_NAMES = {
            "Timestamp",
            "Domain",
            "Name",
            "Params",
            "Project",
            "Snapshot",
            "Severity",
            "Description",
            "Locations",
            "Metas",
            "Labels" };
    private static final int COLUMN_TIMESTAMP = 0;
    private static final int COLUMN_DOMAIN = 1;
    private static final int COLUMN_NAME = 2;
    private static final int COLUMN_PARAMS = 3;
    private static final int COLUMN_PROJECT = 4;
    private static final int COLUMN_SNAPSHOT = 5;
    private static final int COLUMN_SEVERITY = 6;
    private static final int COLUMN_DESCRIPTION = 7;
    private static final int COLUMN_LOCATIONS = 8;
    private static final int COLUMN_METAS = 9;
    private static final int COLUMN_LABELS = 10;

    private final transient List<Issue> values = new ArrayList<>();

    private final int[] counts = new int[IssueSeverity.values().length];

    public IssuesTableModel() {
        super();
    }

    @Override
    public void issue(Issue issue) {
        SwingUtils.processInEDT(this::add, issue);
    }

    public void add(Issue value) {
        this.values.add(value);
        counts[value.getSeverity().ordinal()]++;
        fireTableRowsInserted(values.size() - 1, values.size() - 1);
    }

    public void add(List<Issue> values) {
        if (!values.isEmpty()) {
            final int previous = this.values.size();
            this.values.addAll(values);
            final int current = this.values.size();
            for (final Issue value : values) {
                counts[value.getSeverity().ordinal()]++;
            }
            fireTableRowsInserted(previous, current - 1);
        }
    }

    public void clear() {
        if (!values.isEmpty()) {
            final int size = this.values.size();
            this.values.clear();
            for (int index = 0; index < counts.length; index++) {
                counts[index] = 0;
            }
            fireTableRowsDeleted(0, size - 1);
        }
    }

    public int getCount(IssueSeverity severity) {
        return counts[severity.ordinal()];
    }

    @Override
    public int getRowCount() {
        return values.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        if (column >= 0 && column < getColumnCount()) {
            return COLUMN_NAMES[column];
        } else {
            return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return switch (columnIndex) {
        case COLUMN_DESCRIPTION, COLUMN_DOMAIN, COLUMN_NAME, COLUMN_PROJECT, COLUMN_SNAPSHOT -> String.class;
        case COLUMN_LOCATIONS -> Location[].class;
        case COLUMN_SEVERITY -> IssueSeverity.class;
        case COLUMN_TIMESTAMP -> Instant.class;
        case COLUMN_PARAMS, COLUMN_METAS -> Params.class;
        case COLUMN_LABELS -> Labels.class;
        default -> null;
        };
    }

    @Override
    public Object getValueAt(int rowIndex,
                             int columnIndex) {
        final Issue value = values.get(rowIndex);
        return switch (columnIndex) {
        case COLUMN_DESCRIPTION -> value.getDescription();
        case COLUMN_DOMAIN -> value.getDomain();
        case COLUMN_LABELS -> value.getLabels();
        case COLUMN_LOCATIONS -> value.getLocations();
        case COLUMN_METAS -> value.getMetas();
        case COLUMN_NAME -> value.getName();
        case COLUMN_PARAMS -> value.getParams();
        case COLUMN_PROJECT -> value.getProject();
        case COLUMN_SEVERITY -> value.getSeverity();
        case COLUMN_SNAPSHOT -> value.getSnapshot();
        case COLUMN_TIMESTAMP -> value.getTimestamp();
        default -> null;
        };
    }

    @Override
    public boolean isCellEditable(int rowIndex,
                                  int columnIndex) {
        return false;
    }
}