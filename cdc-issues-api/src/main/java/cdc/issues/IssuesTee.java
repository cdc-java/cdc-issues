package cdc.issues;

public class IssuesTee<I extends Issue> implements IssuesHandler {
    private final IssuesHandler[] delegates;

    @SafeVarargs
    public IssuesTee(IssuesHandler... delegates) {
        this.delegates = delegates.clone();
    }

    @Override
    public void issue(Issue issue) {
        for (final IssuesHandler delegate : delegates) {
            delegate.issue(issue);
        }
    }
}