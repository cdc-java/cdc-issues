package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;

class IssueStatusTest {

    private static void check(boolean expected,
                              IssueStatus status,
                              IssueResolution resolution) {
        assertSame(expected, status.isCompliantWith(resolution), status + " " + resolution);
    }

    @Test
    void testIsCompliantWith() {
        check(true, IssueStatus.CLOSED, IssueResolution.FALSE_POSITIVE);
        check(true, IssueStatus.CLOSED, IssueResolution.FIXED);
        check(true, IssueStatus.CLOSED, IssueResolution.REMOVED);
        check(false, IssueStatus.CLOSED, IssueResolution.UNRESOLVED);
        check(true, IssueStatus.CLOSED, IssueResolution.WONT_FIX);

        check(false, IssueStatus.CONFIRMED, IssueResolution.FALSE_POSITIVE);
        check(false, IssueStatus.CONFIRMED, IssueResolution.FIXED);
        check(false, IssueStatus.CONFIRMED, IssueResolution.REMOVED);
        check(true, IssueStatus.CONFIRMED, IssueResolution.UNRESOLVED);
        check(false, IssueStatus.CONFIRMED, IssueResolution.WONT_FIX);

        check(false, IssueStatus.OPEN, IssueResolution.FALSE_POSITIVE);
        check(false, IssueStatus.OPEN, IssueResolution.FIXED);
        check(false, IssueStatus.OPEN, IssueResolution.REMOVED);
        check(true, IssueStatus.OPEN, IssueResolution.UNRESOLVED);
        check(false, IssueStatus.OPEN, IssueResolution.WONT_FIX);

        check(false, IssueStatus.REOPENED, IssueResolution.FALSE_POSITIVE);
        check(false, IssueStatus.REOPENED, IssueResolution.FIXED);
        check(false, IssueStatus.REOPENED, IssueResolution.REMOVED);
        check(true, IssueStatus.REOPENED, IssueResolution.UNRESOLVED);
        check(false, IssueStatus.REOPENED, IssueResolution.WONT_FIX);

        check(true, IssueStatus.RESOLVED, IssueResolution.FALSE_POSITIVE);
        check(true, IssueStatus.RESOLVED, IssueResolution.FIXED);
        check(false, IssueStatus.RESOLVED, IssueResolution.REMOVED);
        check(false, IssueStatus.RESOLVED, IssueResolution.UNRESOLVED);
        check(true, IssueStatus.RESOLVED, IssueResolution.WONT_FIX);

        check(false, IssueStatus.WAITING, IssueResolution.FALSE_POSITIVE);
        check(false, IssueStatus.WAITING, IssueResolution.FIXED);
        check(false, IssueStatus.WAITING, IssueResolution.REMOVED);
        check(true, IssueStatus.WAITING, IssueResolution.UNRESOLVED);
        check(false, IssueStatus.WAITING, IssueResolution.WONT_FIX);
    }
}