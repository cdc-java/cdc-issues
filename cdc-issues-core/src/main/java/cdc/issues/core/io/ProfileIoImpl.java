package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;

import cdc.issues.io.ProfileFormat;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;

public class ProfileIoImpl implements ProfileIo {
    private final ProfileIoFeatures features;

    public ProfileIoImpl(ProfileIoFeatures features) {
        this.features = features;
    }

    @Override
    public ProfileIoFeatures getFeatures() {
        return features;
    }

    @Override
    public void save(Profile profile,
                     File file) throws IOException {
        final ProfileFormat format = ProfileFormat.from(file);

        switch (format) {
        case HTML:
            new HtmlProfileIo(features).save(profile, file);
            break;
        case MD:
            new MdProfileIo(features).save(profile, file);
            break;
        case XML:
            new XmlProfileIo(features).save(profile, file);
            break;
        case CSV, ODS, XLS, XLSM, XLSX:
            new WorkbookProfileIo(features).save(profile, file);
            break;
        case JSON:
            new JsonProfileIo(features).save(profile, file);
            break;
        default:
            break;
        }
    }

    @Override
    public void save(ProfileConfig config,
                     File file) throws IOException {
        final ProfileFormat format = ProfileFormat.from(file);

        switch (format) {
        // case HTML:
        // new HtmlProfileIo(features).save(config, file);
        // break;
        // case MD:
        // new MdProfileIo(features).save(config, file);
        // break;
        case XML:
            new XmlProfileIo(features).save(config, file);
            break;
        case CSV, ODS, XLS, XLSM, XLSX:
            new WorkbookProfileIo(features).save(config, file);
            break;
        // case JSON:
        // new JsonProfileIo(features).save(config, file);
        // break;
        default:
            break;
        }
    }

    @Override
    public Profile loadProfile(File file) throws IOException {
        final ProfileFormat format = ProfileFormat.from(file);

        switch (format) {
        case XML:
            return new XmlProfileIo(features).loadProfile(file);
        default:
            return null;
        }
    }

    @Override
    public ProfileConfig loadProfileConfig(File file) throws IOException {
        final ProfileFormat format = ProfileFormat.from(file);
        switch (format) {
        case CSV, ODS, XLS, XLSM, XLSX:
            return new WorkbookProfileIo(features).loadProfileConfig(file);
        default:
            return null;
        }
    }
}