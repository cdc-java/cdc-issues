package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.io.IssuesFormat;
import cdc.issues.rules.RuleId;

class RuleIdTest {
    @Test
    void test() {
        final RuleId id1 = new RuleId("Domain", "Name");
        final RuleId id2 = new RuleId("Domain", "Name");
        final RuleId id3 = new RuleId("Domain", IssuesFormat.CSV);
        final RuleId id4 = new RuleId("domain", "Name");

        assertEquals(id1, id1);
        assertEquals(id1, id2);
        assertNotEquals(id1, null);
        assertNotEquals(id1, "Hello");
        assertNotEquals(id1, id3);
        assertNotEquals(id1, id4);
        assertEquals("Domain", id1.getDomain());
        assertEquals("Name", id1.getName());
        assertEquals(IssuesFormat.CSV, id3.getName(IssuesFormat.class));

        assertEquals(id1.hashCode(), id2.hashCode());
        assertEquals(id1.toString(), id2.toString());
    }
}