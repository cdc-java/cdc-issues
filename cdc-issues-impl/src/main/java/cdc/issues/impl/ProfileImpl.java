package cdc.issues.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Params;
import cdc.issues.rules.ConfiguredRule;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleConfig;
import cdc.issues.rules.RuleId;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

/**
 * Default implementation of {@link Profile}.
 *
 * @author Damien Carbonne
 */
public class ProfileImpl implements Profile {
    private static final String DESCRIPTION = "description";
    private static final String LABELS = "labels";
    private static final String METAS = "metas";
    private static final String PARAMS = "params";
    private static final String RULE = "rule";

    /** The profile name. */
    private String name;
    /** The profile description. */
    private String description = "";
    /** The profile meta datas. */
    private Metas metas = Metas.NO_METAS;
    /** The profile labels. */
    private Labels labels = Labels.NO_LABELS;
    /** The declared rules and their associated config. */
    private final Map<RuleId, ConfiguredRule> configuredRules = new HashMap<>();

    private void checkHasRule(Rule rule) {
        Checks.isNotNull(rule, RULE);
        Checks.isTrue(hasRule(rule), "Unknown rule " + rule);
    }

    protected ProfileImpl(Builder builder) {
        this.name = Checks.isNotNull(builder.name, "name");
        this.description = Checks.isNotNull(builder.description, "description");
        this.metas = Checks.isNotNull(builder.metas, "metas");
        this.labels = Checks.isNotNull(builder.labels, "labels");
        for (final Map.Entry<RuleId, ConfiguredRule.Builder> entry : builder.configuredRules.entrySet()) {
            configuredRules.put(entry.getKey(), entry.getValue().build());
        }
    }

    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl(String name) {
        this.name = name;
    }

    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl setName(String name) {
        this.name = name;
        return this;
    }

    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl setDescription(String description) {
        this.description = Checks.isNotNull(description, DESCRIPTION);
        return this;
    }

    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl setMetas(Metas metas) {
        this.metas = Checks.isNotNull(metas, METAS);
        return this;
    }

    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl setLabels(Labels labels) {
        this.labels = Checks.isNotNull(labels, LABELS);
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Metas getMetas() {
        return metas;
    }

    @Override
    public Labels getLabels() {
        return labels;
    }

    @Override
    public Optional<Rule> getRule(RuleId ruleId) {
        if (configuredRules.containsKey(ruleId)) {
            return Optional.ofNullable(configuredRules.get(ruleId).getRule());
        } else {
            return Optional.empty();
        }
    }

    /**
     * Adds a rule if it is not already added.
     *
     * @param rule The rule.
     * @return This profile.
     * @throws IllegalArgumentException When {@code rule} is {@code null} or another rule with same id is already registered.
     */
    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl add(Rule rule) {
        Checks.isNotNull(rule, RULE);
        Checks.assertFalse(configuredRules.containsKey(rule.getId()),
                           "Duplicate rule id {} in '{}', cannot add '{}'.",
                           rule.getId(),
                           getName(),
                           rule.getTitle());

        configuredRules.put(rule.getId(),
                            ConfiguredRule.builder(rule)
                                          .enabled(true)
                                          .build());
        return this;
    }

    /**
     * Adds a rule if it is not already added.
     *
     * @param rule The rule.
     * @return This profile.
     * @throws IllegalArgumentException When {@code rule} is {@code null}.
     */
    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl addIfMissing(Rule rule) {
        Checks.isNotNull(rule, RULE);

        if (!configuredRules.containsKey(rule.getId())) {
            configuredRules.put(rule.getId(),
                                ConfiguredRule.builder(rule)
                                              .enabled(true)
                                              .build());
        }
        return this;
    }

    public ProfileImpl setConfig(Rule rule,
                                 RuleConfig config) {
        checkHasRule(rule);
        Checks.isNotNull(config, "config");

        configuredRules.put(rule.getId(),
                            ConfiguredRule.builder(rule)
                                          .config(config)
                                          .build());

        return this;
    }

    public ProfileImpl setConfig(RuleId ruleId,
                                 RuleConfig config) {
        return setConfig(getRule(ruleId).orElseThrow(), config);
    }

    /**
     * Removes a rule.
     *
     * @param rule The rule.
     * @return This object.
     */
    @Deprecated(since = "2025-01-25", forRemoval = true)
    public ProfileImpl remove(Rule rule) {
        Checks.isNotNull(rule, RULE);

        configuredRules.remove(rule.getId());
        return this;
    }

    public ProfileImpl setAllEnabled(boolean enabled) {
        for (final Rule rule : getRules()) {
            setEnabled(rule, enabled);
        }
        return this;
    }

    /**
     * Enables or disables a rule.
     * <p>
     * If the rule is not declared, declares it.
     *
     * @param rule The rule.
     * @param enabled {@code true} if the rule must be enabled.
     * @return This object.
     */
    public ProfileImpl setEnabled(Rule rule,
                                  boolean enabled) {
        checkHasRule(rule);

        configuredRules.put(rule.getId(),
                            ConfiguredRule.builder(rule)
                                          .config(configuredRules.get(rule.getId()).getConfig())
                                          .enabled(enabled)
                                          .build());
        return this;
    }

    public ProfileImpl setEnabled(RuleId ruleId,
                                  boolean enabled) {
        return setEnabled(getRule(ruleId).orElseThrow(), enabled);
    }

    public ProfileImpl setCustomizedSeverity(Rule rule,
                                             IssueSeverity customizedSeverity) {
        checkHasRule(rule);

        configuredRules.put(rule.getId(),
                            ConfiguredRule.builder(rule)
                                          .config(configuredRules.get(rule.getId()).getConfig())
                                          .customizedSeverity(customizedSeverity)
                                          .build());
        return this;
    }

    public ProfileImpl setCustomizedSeverity(RuleId ruleId,
                                             IssueSeverity customizedSeverity) {
        return setCustomizedSeverity(getRule(ruleId).orElseThrow(), customizedSeverity);
    }

    public ProfileImpl setParams(Rule rule,
                                 Params params) {
        checkHasRule(rule);
        Checks.isNotNull(params, PARAMS);

        configuredRules.put(rule.getId(),
                            ConfiguredRule.builder(rule)
                                          .config(configuredRules.get(rule.getId()).getConfig())
                                          .params(params)
                                          .build());
        return this;
    }

    public ProfileImpl setParams(RuleId ruleId,
                                 Params params) {
        return setParams(getRule(ruleId).orElseThrow(), params);
    }

    public ProfileImpl apply(ProfileConfig profileConfig) {
        for (final RuleId ruleId : profileConfig.getRuleIds()) {
            if (hasRule(ruleId)) {
                final RuleConfig config = profileConfig.getRuleConfig(ruleId);
                setConfig(ruleId, config);
            }
        }
        return this;
    }

    @Override
    public Set<Rule> getRules() {
        return configuredRules.values()
                              .stream()
                              .map(ConfiguredRule::getRule)
                              .collect(Collectors.toSet());
    }

    @Override
    public boolean hasRule(Rule rule) {
        return rule != null && configuredRules.containsKey(rule.getId());
    }

    @Override
    public ProfileConfig getProfileConfig() {
        final ProfileConfigImpl result = new ProfileConfigImpl();
        for (final ConfiguredRule cr : configuredRules.values()) {
            result.set(cr.getRule().getId(), cr.getConfig());
        }
        return result;
    }

    @Override
    public boolean hasRule(RuleId ruleId) {
        return configuredRules.containsKey(ruleId);
    }

    @Override
    public ConfiguredRule getConfiguredRule(Rule rule) {
        Checks.isNotNull(rule, RULE);
        final ConfiguredRule result = configuredRules.get(rule.getId());
        if (result == null) {
            throw new NotFoundException("Unknown rule " + rule.getId());
        } else {
            return result;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String name;
        private String description = "";
        private Metas metas = Metas.NO_METAS;
        private Labels labels = Labels.NO_LABELS;
        private final Map<RuleId, ConfiguredRule.Builder> configuredRules = new HashMap<>();

        protected Builder self() {
            return this;
        }

        protected Builder() {
            super();
        }

        public Builder name(String name) {
            this.name = name;
            return self();
        }

        public Builder description(String description) {
            this.description = description;
            return self();
        }

        public Builder metas(Metas metas) {
            this.metas = metas;
            return self();
        }

        public Builder labels(Labels labels) {
            this.labels = labels;
            return self();
        }

        public Builder rule(Rule rule) {
            Checks.assertFalse(configuredRules.containsKey(rule.getId()),
                               "Duplicate rule id {} in '{}', cannot add '{}'.",
                               rule.getId(),
                               name,
                               rule.getTitle());
            configuredRules.put(Checks.isNotNull(rule, "rule").getId(),
                                ConfiguredRule.builder(rule));
            return self();
        }

        public Builder enabled(Rule rule,
                               boolean enabled) {
            final ConfiguredRule.Builder b = configuredRules.computeIfAbsent(rule.getId(), k -> ConfiguredRule.builder(rule));
            b.enabled(enabled);
            return self();
        }

        public Builder customizedSeverity(Rule rule,
                                          IssueSeverity customizedSeverity) {
            final ConfiguredRule.Builder b = configuredRules.computeIfAbsent(rule.getId(), k -> ConfiguredRule.builder(rule));
            b.customizedSeverity(customizedSeverity);
            return self();
        }

        public Builder params(Rule rule,
                              Params params) {
            final ConfiguredRule.Builder b = configuredRules.computeIfAbsent(rule.getId(), k -> ConfiguredRule.builder(rule));
            b.params(params);
            return self();
        }

        public ProfileImpl build() {
            return new ProfileImpl(this);
        }
    }
}