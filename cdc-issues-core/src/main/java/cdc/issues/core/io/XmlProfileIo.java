package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.util.lang.FailureReaction;

class XmlProfileIo {
    private final ProfileIoFeatures features;

    XmlProfileIo(ProfileIoFeatures features) {
        this.features = features;
    }

    private static void write(XmlWriter writer,
                              Profile profile) throws IOException {
        writer.beginDocument();
        XmlIo.writeProfile(writer, profile);
        writer.endDocument();
    }

    private static void write(XmlWriter writer,
                              ProfileConfig config) throws IOException {
        writer.beginDocument();
        XmlIo.writeProfileConfig(writer, config);
        writer.endDocument();
    }

    public void save(Profile profile,
                     File file) throws IOException {
        try (XmlWriter writer = new XmlWriter(file)) {
            if (features.isEnabled(ProfileIoFeatures.Hint.PRETTY_PRINT)) {
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            }
            write(writer, profile);
        }
    }

    public void save(ProfileConfig config,
                     File file) throws IOException {
        try (XmlWriter writer = new XmlWriter(file)) {
            if (features.isEnabled(ProfileIoFeatures.Hint.PRETTY_PRINT)) {
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            }
            write(writer, config);
        }
    }

    public Profile loadProfile(File file) throws IOException {
        final StAXLoader loader = new StAXLoader(FailureReaction.FAIL);
        return loader.load(file);
    }

    private static class StAXLoader extends AbstractStAXLoader<Profile> {
        public StAXLoader(FailureReaction reaction) {
            super((reader,
                   systemId) -> new Parser(reader,
                                           systemId,
                                           reaction));
        }

        private static class Parser extends AbstractStAXParser<Profile> {
            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction) {
                super(reader, systemId, reaction);
            }

            @Override
            protected Profile parse() throws XMLStreamException {
                Profile profile = null;
                final String ctx = "parse()";
                expectStartDocument(ctx);

                nextTag();
                expectStartElement(ctx);

                if (isStartElement(XmlIo.PROFILE)) {
                    profile = XmlIo.parseProfile(this);
                } else {
                    throw unexpectedEvent();
                }
                next();
                expectEndDocument(ctx);
                return profile;
            }
        }
    }
}