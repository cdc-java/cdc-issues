package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class StructuredDescriptionTest {
    private static final Logger LOGGER = LogManager.getLogger(StructuredDescriptionTest.class);
    private static final String SECTION_1 = "Section 1";
    private static final String SECTION_2 = "Section 2";
    private static final String HEADER = "This is an intro.\nBlah blah blah.";

    private static void check(Consumer<StructuredDescription.Builder<?>> builderConsumer,
                              Consumer<StructuredDescription> descriptionConsumer) {
        final StructuredDescription.Builder<?> builder = StructuredDescription.builder();
        builderConsumer.accept(builder);
        final StructuredDescription description = builder.build();
        LOGGER.info("description:\n[{}]", description);
        descriptionConsumer.accept(description);
    }

    @Test
    void testGetHeader() {
        check(b -> {
            // Ignore
        },
              d -> {
                  assertEquals("", d.getHeader());
              });

        check(b -> {
            b.text(HEADER);
        },
              d -> {
                  assertEquals(HEADER, d.getHeader());
              });

        check(b -> {
            b.section(SECTION_1);
        },
              d -> {
                  assertEquals("", d.getHeader());
              });

        check(b -> {
            b.text(HEADER);
            b.section(SECTION_1);
        },
              d -> {
                  assertEquals(HEADER, d.getHeader());
              });
    }

    @Test
    void testGetSections() {
        check(b -> {
            // Ignore
        },
              d -> {
                  assertTrue(d.getSections().isEmpty());
              });

        check(b -> {
            b.section(SECTION_1);
        },
              d -> {
                  assertEquals(List.of(SECTION_1), d.getSections());
              });

        check(b -> {
            b.section(SECTION_1);
            b.section(SECTION_2);
        },
              d -> {
                  assertEquals(List.of(SECTION_1, SECTION_2), d.getSections());
              });
    }

    @Test
    void testHasSection() {
        check(b -> {
            // Ignore
        },
              d -> {
                  assertFalse(d.hasSection(SECTION_1));
                  assertFalse(d.hasSection(SECTION_2));
              });

        check(b -> {
            b.section(SECTION_1);
        },
              d -> {
                  assertTrue(d.hasSection(SECTION_1));
                  assertFalse(d.hasSection(SECTION_2));
              });

        check(b -> {
            b.section(SECTION_1);
            b.section(SECTION_2);
        },
              d -> {
                  assertTrue(d.hasSection(SECTION_1));
                  assertTrue(d.hasSection(SECTION_2));
              });
    }

    @Test
    void testGetSectionContent() {
        check(b -> {
            // Ignore
        },
              d -> {
                  assertEquals("", d.getSectionContent(SECTION_1));
              });

        check(b -> {
            b.section(SECTION_1);
        },
              d -> {
                  assertEquals("", d.getSectionContent(SECTION_1));
              });

        check(b -> {
            b.section(SECTION_1);
            b.section(SECTION_2);
        },
              d -> {
                  assertEquals("", d.getSectionContent(SECTION_1));
                  assertEquals("", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.section(SECTION_1);
            b.uItem("hello");
            b.section(SECTION_2);
            b.uItem("world");
        },
              d -> {
                  assertEquals("- hello", d.getSectionContent(SECTION_1));
                  assertEquals("- world", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.uItem("hello");
            b.section(SECTION_2);
            b.uItem("world");
        },
              d -> {
                  assertEquals("- hello", d.getSectionContent(SECTION_1));
                  assertEquals("- world", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.text("\nhead");
            b.uItem("hello");
            b.text("\ntail");
            b.section(SECTION_2);
            b.uItem("world");
        },
              d -> {
                  assertEquals("head\n- hello\ntail", d.getSectionContent(SECTION_1));
                  assertEquals("- world", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.uItems("hello", "world");
            b.section(SECTION_2);
            b.uItems("world", "hello");
        },
              d -> {
                  assertEquals("- hello\n- world", d.getSectionContent(SECTION_1));
                  assertEquals("- world\n- hello", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.uItems(1, "hello", "world");
            b.section(SECTION_2);
            b.uItems(1, "world", "hello");
        },
              d -> {
                  assertEquals("    - hello\n    - world", d.getSectionContent(SECTION_1));
                  assertEquals("    - world\n    - hello", d.getSectionContent(SECTION_2));
              });

        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.oItems(1, "hello", "world");
            b.section(SECTION_2);
            b.oItems(1, "world", "hello");
        },
              d -> {
                  assertEquals("1) hello\n2) world", d.getSectionContent(SECTION_1));
                  assertEquals("1) world\n2) hello", d.getSectionContent(SECTION_2));
              });
        check(b -> {
            b.text("Introduction");
            b.section(SECTION_1);
            b.oItems(1, 1, "hello", "world");
            b.section(SECTION_2);
            b.oItems(1, 1, "world", "hello");
        },
              d -> {
                  assertEquals("    1) hello\n    2) world", d.getSectionContent(SECTION_1));
                  assertEquals("    1) world\n    2) hello", d.getSectionContent(SECTION_2));
              });
    }

    @Test
    void testGetSectionItems() {
        check(b -> {
            // Ignore
        },
              d -> {
                  assertTrue(d.getSectionItems(SECTION_1).isEmpty());
              });

        check(b -> {
            b.section(SECTION_1);
        },
              d -> {
                  assertTrue(d.getSectionItems(SECTION_1).isEmpty());
              });

        check(b -> {
            b.section(SECTION_1);
            b.uItem("hello");
        },
              d -> {
                  assertEquals(List.of("hello"), d.getSectionItems(SECTION_1));
              });

        check(b -> {
            b.section(SECTION_1);
            b.uItems("hello", "world");
        },
              d -> {
                  assertEquals(List.of("hello", "world"), d.getSectionItems(SECTION_1));
              });

        check(b -> {
            b.section(SECTION_1);
            b.oItems(1, "hello", "world");
        },
              d -> {
                  assertEquals(List.of("hello", "world"), d.getSectionItems(SECTION_1));
              });
    }
}