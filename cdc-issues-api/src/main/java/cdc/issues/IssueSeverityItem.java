package cdc.issues;

/**
 * Interface implemented by classes that have a severity.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface IssueSeverityItem {
    /**
     * @return The issue severity.
     */
    public IssueSeverity getSeverity();
}