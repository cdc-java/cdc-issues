package cdc.issues.io;

import java.time.Instant;
import java.util.Optional;

import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Snapshot;
import cdc.issues.rules.Profile;

/**
 * Interface describing the synthetic data associated to a {@link Snapshot}.
 *
 * @author Damien Carbonne
 */
public interface SnapshotData {
    /**
     * @return The project name.
     */
    public String getProjectName();

    /**
     * @return The project description.
     */
    public String getProjectDescription();

    /**
     * @return The project meta data.
     */
    public Metas getProjectMetas();

    /**
     * @return The project labels.
     */
    public Labels getProjectLabels();

    /**
     * @return The project profile.
     */
    public Optional<? extends Profile> getProfile();

    /**
     * @return The snapshot name.
     */
    public String getSnapshotName();

    /**
     * @return The snapshot description.
     */
    public String getSnapshotDescription();

    /**
     * @return The snapshot meta data.
     */
    public Metas getSnapshotMetas();

    /**
     * @return The snapshot labels.
     */
    public Labels getSnapshotLabels();

    /**
     * @return The snapshot timestamp.
     */
    public Instant getSnapshotTimestamp();

    /**
     * @return The number of issues of the snapshot.
     */
    public int getNumberOfIssues();

    /**
     * @return The issues hash.
     */
    public String getIssuesHash();
}