package cdc.issues.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.issues.IssueSeverity;
import cdc.issues.Params;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.RuleConfig;
import cdc.issues.rules.RuleId;
import cdc.util.lang.Checks;

public class ProfileConfigImpl implements ProfileConfig {
    private static final String RULE_ID = "ruleId";
    private static final String RULE_CONFIG = "ruleConfig";
    private final Map<RuleId, RuleConfig> configs = new HashMap<>();

    public ProfileConfigImpl() {
        super();
    }

    public void addIfMissing(RuleId ruleId) {
        if (!configs.containsKey(ruleId)) {
            set(ruleId, RuleConfig.DEFAULT);
        }
    }

    public ProfileConfigImpl set(RuleId ruleId,
                                 RuleConfig ruleConfig) {
        Checks.isNotNull(ruleId, RULE_ID);
        Checks.isNotNull(ruleConfig, RULE_CONFIG);

        configs.put(ruleId, ruleConfig);

        return this;
    }

    public ProfileConfigImpl setEnabled(RuleId ruleId,
                                        boolean enabled) {
        addIfMissing(ruleId);
        configs.put(ruleId,
                    RuleConfig.builder()
                              .set(configs.get(ruleId))
                              .enabled(enabled)
                              .build());
        return this;
    }

    public ProfileConfigImpl setCustomizedSeverity(RuleId ruleId,
                                                   IssueSeverity customizedSeverity) {
        addIfMissing(ruleId);
        configs.put(ruleId,
                    RuleConfig.builder()
                              .set(configs.get(ruleId))
                              .customizedSeverity(customizedSeverity)
                              .build());
        return this;
    }

    public ProfileConfigImpl setParams(RuleId ruleId,
                                       Params params) {
        addIfMissing(ruleId);
        configs.put(ruleId,
                    RuleConfig.builder()
                              .set(configs.get(ruleId))
                              .params(params)
                              .build());
        return this;
    }

    @Override
    public Set<RuleId> getRuleIds() {
        return configs.keySet();
    }

    @Override
    public RuleConfig getRuleConfig(RuleId ruleId) {
        Checks.isNotNull(ruleId, RULE_ID);
        return configs.get(ruleId);
    }
}