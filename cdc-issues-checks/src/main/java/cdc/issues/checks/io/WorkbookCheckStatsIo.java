package cdc.issues.checks.io;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cdc.issues.checks.CheckResult;
import cdc.issues.checks.CheckStats;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleId;
import cdc.office.ss.SheetLoader;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.Row;
import cdc.office.tables.TableSection;

/**
 * Class used to save {@link CheckStats} to a Workbook.
 *
 * @author Damien Carbonne
 *
 * @param <I> The type of the checked or ignored item.
 */
public class WorkbookCheckStatsIo<I> {
    private final Function<? super I, String> itemToString;

    private static final String DATA = "Data";
    private static final String STATS = "Stats";
    private static final String TOTAL = "Total";

    /**
     * Creates a new instance of {@link WorkbookCheckStatsIo}.
     *
     * @param itemToString The function used to convert an item to a String.
     */
    public WorkbookCheckStatsIo(Function<? super I, String> itemToString) {
        this.itemToString = itemToString;
    }

    /**
     * Save {@link CheckStats} to a workbook file.
     *
     * @param stats The {@link CheckStats}.
     * @param file The workbook file to generate.
     * @param features The {@link WorkbookWriterFeatures features}.
     * @throws IOException When an IO error occurs.
     */
    public void save(CheckStats<I> stats,
                     File file,
                     WorkbookWriterFeatures features) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);
        try (final WorkbookWriter<?> writer = factory.create(file, features)) {
            saveData(stats, writer);
            saveStats(stats, writer);
        }
    }

    private void saveData(CheckStats<I> stats,
                          WorkbookWriter<?> writer) throws IOException {
        writer.beginSheet(DATA);
        writer.addRow(TableSection.HEADER, "Item", "Rule Id", "Rule Title", "Result", "Notes");
        for (final CheckStats.Data<I> data : stats.getDatas()) {
            writer.addRow(TableSection.DATA,
                          itemToString.apply(data.getItem()),
                          data.getRuleId().toString(),
                          data.getRuleTitle(),
                          data.getResult().name(),
                          data.getNotes());
        }
    }

    private void saveStats(CheckStats<I> stats,
                           WorkbookWriter<?> writer) throws IOException {
        final CheckResult[] values = CheckResult.getRuleValues();
        writer.beginSheet(STATS);
        writer.beginRow(TableSection.HEADER);
        writer.addCell("Rule Id");
        writer.addCell("Rule Title");
        for (final CheckResult result : values) {
            writer.addCell(result);
        }
        writer.addCell(TOTAL);
        for (final RuleId ruleId : stats.getRuleIds().stream()
                                        .sorted()
                                        .toList()) {
            writer.beginRow(TableSection.DATA);
            final Rule rule = stats.getProfile().getRule(ruleId).orElse(null);
            writer.addCell(ruleId.toString());
            if (rule == null) {
                writer.addEmptyCell();
            } else {
                writer.addCell(rule.getTitle());
            }
            for (final CheckResult result : values) {
                writer.addCell(stats.getCounts(ruleId, result));
            }
            writer.addCell(stats.getCounts(ruleId));
        }
        writer.beginRow(TableSection.DATA);
        writer.addEmptyCell();
        writer.addCell(TOTAL);
        for (final CheckResult result : values) {
            writer.addCell(stats.getCounts(result));
        }
        writer.addCell(stats.getCounts());
    }

    /**
     * Merges several workbook stats files into one.
     *
     * @param output The generated file.
     * @param inputs The input files.<br>
     *            They should have been generated with {@link #save(CheckStats, File, WorkbookWriterFeatures)}.
     * @throws IOException When an IO error occurs.
     */
    public static void merge(File output,
                             List<File> inputs) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);

        try (final WorkbookWriter<?> writer = factory.create(output, WorkbookWriterFeatures.STANDARD_FAST)) {
            mergeData(writer, inputs);
            mergeStats(writer, inputs);
            writer.flush();
        }
    }

    private static void mergeData(WorkbookWriter<?> writer,
                                  List<File> inputs) throws IOException {
        final SheetLoader loader = new SheetLoader();
        writer.beginSheet(DATA);
        boolean first = true;
        for (final File input : inputs) {
            final List<Row> rows = loader.load(input, null, DATA);
            if (first) {
                writer.addRow(TableSection.HEADER, rows.get(0));
            }
            for (final Row row : rows.subList(1, rows.size())) {
                writer.addRow(TableSection.DATA, row);
            }
            first = false;
        }
    }

    private static void mergeStats(WorkbookWriter<?> writer,
                                   List<File> inputs) throws IOException {
        final int num = 5;
        final Map<String, int[]> ruleIdToCounts = new HashMap<>();
        final Map<String, String> ruleIdToTitle = new HashMap<>();
        final SheetLoader loader = new SheetLoader();
        for (final File input : inputs) {
            final List<Row> rows = loader.load(input, null, STATS);
            rows.remove(0); // header
            rows.remove(rows.size() - 1); // totals
            for (final Row row : rows) {
                final String ruleId = row.getValue(0);
                final String ruleTitle = row.getValue(1);
                ruleIdToTitle.put(ruleId, ruleTitle);
                final int[] counts = ruleIdToCounts.computeIfAbsent(ruleId, k -> new int[num]);
                for (int i = 0; i < num; i++) {
                    counts[i] += row.getValueAsInteger(2 + i, 0);
                }
            }
        }
        writer.beginSheet(STATS);
        writer.beginRow(TableSection.HEADER);
        writer.addCell("Rule Id");
        writer.addCell("Rule Title");
        for (final CheckResult result : CheckResult.getRuleValues()) {
            writer.addCell(result);
        }
        writer.addCell(TOTAL);

        final int[] totals = new int[num];
        for (final String ruleId : ruleIdToCounts.keySet()
                                                 .stream()
                                                 .sorted()
                                                 .toList()) {
            final String ruleTitle = ruleIdToTitle.get(ruleId);

            writer.beginRow(TableSection.DATA);
            writer.addCell(ruleId);
            writer.addCell(ruleTitle);
            final int[] counts = ruleIdToCounts.get(ruleId);
            for (int i = 0; i < num; i++) {
                writer.addCell(counts[i]);
                totals[i] += counts[i];
            }
        }
        writer.beginRow(TableSection.DATA);
        writer.addEmptyCell();
        writer.addCell(TOTAL);
        for (final int total : totals) {
            writer.addCell(total);
        }
    }
}