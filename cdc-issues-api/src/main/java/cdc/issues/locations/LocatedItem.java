package cdc.issues.locations;

/**
 * Interface implemented by classes that have a {@link Location}.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface LocatedItem {
    /**
     * @return The item location.
     */
    public Location getLocation();
}