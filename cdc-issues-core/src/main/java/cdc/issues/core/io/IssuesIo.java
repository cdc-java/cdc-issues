package cdc.issues.core.io;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesIoFactoryFeatures.Hint;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;
import cdc.util.time.Chronometer;

/**
 * Internal class dedicated to IO of issues.
 *
 * @author Damien Carbonne
 */
class IssuesIo {
    protected static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;

    protected enum StreamStatus {
        INIT,
        DOCUMENT,
        INFOS,
        SNAPSHOT,
        END
    }

    protected final Logger logger = LogManager.getLogger(getClass());
    protected final IssuesIoFactoryFeatures features;

    protected IssuesIo(IssuesIoFactoryFeatures features) {
        Checks.isNotNull(features, "features");
        this.features = features;
    }

    protected LocalDateTime toLocalDateTime(Instant instant) {
        return instant == null
                ? null
                : LocalDateTime.ofInstant(instant, features.getZoneId());
    }

    protected Instant toInstant(LocalDateTime ldt) {
        return ldt == null
                ? null
                : ldt.atZone(features.getZoneId()).toInstant();
    }

    protected String toString(Instant instant) {
        return instant == null
                ? ""
                : FORMATTER.format(toLocalDateTime(instant));
    }

    public Instant toInstant(String s) {
        return StringUtils.isNullOrEmpty(s)
                ? null
                : toInstant(LocalDateTime.parse(s, FORMATTER));
    }

    public final IssuesIoFactoryFeatures getFeatures() {
        return features;
    }

    protected void traceLoad(String systemId) {
        if (features.isEnabled(Hint.VERBOSE)) {
            logger.info("Load {}", systemId);
        }
    }

    protected void traceLoaded(String systemId,
                               Chronometer chrono) {
        if (features.isEnabled(Hint.VERBOSE)) {
            logger.info("Loaded {} ({})", systemId, chrono);
        }
    }
}