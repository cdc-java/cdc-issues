package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.DefaultLocation;
import cdc.issues.locations.Location;
import cdc.issues.rules.RuleId;

class IssueIdTest {

    private static enum Type {
        A,
        B
    }

    @Test
    void test() {
        final Location location = new DefaultLocation("Path", "Anchor");

        final IssueId ida = IssueId.builder().domain("Domain").name("Name").locations(location).build();
        final IssueId idb = IssueId.builder().domain("Domain").name("Name").locations(location).build();
        final IssueId idc = IssueId.builder().domain("Domain").name("Name").locations(location).build();
        final IssueId idd = IssueId.builder().ruleId(new RuleId("Domain", "Name")).locations(location).build();
        final IssueId ide = IssueId.builder().domain("Domain").name(Type.A).locations(location).build();
        final IssueId idf = IssueId.builder().domain("Domain1").name("Name").locations(location).build();
        final IssueId idg = IssueId.builder().domain("Domain").name("Name").project("Project").locations(location).build();
        final IssueId idh = IssueId.builder().domain("Domain").name("Name").locations(location, location).build();
        final IssueId idi = IssueId.builder().domain("Domain").name("Name").build();

        assertEquals(ida, ida);
        assertEquals(ida, idb);
        assertEquals(ida, idc);
        assertEquals(ida, idd);
        assertNotEquals(ida, null);
        assertNotEquals(ida, "hello");
        assertNotEquals(ida, ide);
        assertNotEquals(ida, idf);
        assertNotEquals(ida, idg);
        assertNotEquals(ida, idh);
        assertNotEquals(ida, idi);

        assertEquals(ida.toString(), idb.toString());
        assertEquals(ida.hashCode(), idb.hashCode());
        assertEquals("A", ide.getName());
        assertEquals(Type.A, ide.getName(Type.class));
    }
}