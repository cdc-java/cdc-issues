package cdc.issues.core.io;

import cdc.util.strings.CaseConverter;
import cdc.util.strings.Splitter;

/**
 * Names of tags/columns (lower case and '_' separator).
 */
final class Names {
    private Names() {
    }

    private static final Splitter SPLITTER = new Splitter('_');
    static final CaseConverter WORKBOOKS_CASE_CONVERTER =
            CaseConverter.builder()
                         .style(CaseConverter.Style.CAPITAL)
                         .wordSeparator(" ")
                         .splitter(SPLITTER)
                         .build();
    static final CaseConverter JSON_CASE_CONVERTER =
            CaseConverter.builder()
                         .style(CaseConverter.Style.CAMEL_CASE)
                         .splitter(SPLITTER)
                         .build();
    static final CaseConverter XML_CASE_CONVERTER =
            CaseConverter.builder()
                         .style(CaseConverter.Style.LOWER_CASE)
                         .wordSeparator("-")
                         .splitter(SPLITTER)
                         .build();

    static final String ANCHOR = "anchor";
    static final String CUSTOMIZED_SEVERITY = "customized_severity";
    static final String DESCRIPTION = "description";
    static final String DOMAIN = "domain";
    static final String ENABLED = "enabled";
    static final String ID_HASH = "id_hash";
    static final String INFOS = "infos";
    static final String ISSUE = "issue";
    static final String ISSUES = "issues";
    static final String ISSUES_HASH = "issues_hash";
    static final String LABELS = "labels";
    static final String LOCATION = "location";
    static final String LOCATIONS = "locations";
    static final String META = "meta";
    static final String METAS = "metas";
    static final String NAME = "name";
    static final String PARAM = "param";
    static final String PARAMS = "params";
    static final String PROFILE = "profile";
    static final String PROFILE_CONFIGURATION = "profile_configuration";
    static final String PROFILE_DESCRIPTION = "profile_description";
    static final String PROFILE_LABELS = "profile_labels";
    static final String PROFILE_METAS = "profile_metas";
    static final String PROJECT = "project";
    static final String PROJECT_DESCRIPTION = "project_description";
    static final String PROJECT_LABELS = "project_labels";
    static final String PROJECT_METAS = "project_metas";
    static final String PROJECT_SNAPSHOT = "project_snapshot";
    static final String PATH = "path";
    static final String RULE = "rule";
    static final String RULES = "rules";
    static final String SEVERITY = "severity";
    static final String SNAPSHOT = "snapshot";
    static final String SNAPSHOT_DESCRIPTION = "snapshot_description";
    static final String SNAPSHOT_LABELS = "snapshot_labels";
    static final String SNAPSHOT_METAS = "snapshot_metas";
    static final String SNAPSHOT_TIMESTAMP = "snapshot_timestamp";
    static final String TAG = "tag";
    static final String TIMESTAMP = "timestamp";
    static final String TITLE = "title";
    static final String VALUE = "value";

    static final String ANSWER = "answer";
    static final String ASSIGNEE = "assignee";
    static final String AUTHOR = "author";
    static final String COMMENT = "comment";
    static final String COMMENTS = "comments";
    static final String CREATED = "created";
    static final String DATE = "date";
    static final String MODIFIED = "modified";
    static final String NEW_SEVERITY = "new_severity";
    static final String RESOLUTION = "resolution";
    static final String STATUS = "status";
    static final String TEXT = "text";
    static final String USER_LABELS = "user_labels";
    static final String USER_METAS = "user_metas";
}