package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.TextFileLocation;
import cdc.issues.rules.RuleId;

class IssueTest {

    private static enum Type {
        A,
        B
    }

    @Test
    void test() {
        assertThrows(IllegalArgumentException.class, () -> {
            Issue.builder()
                 .build();
        });
        assertThrows(IllegalArgumentException.class, () -> {
            Issue.builder()
                 .domain("Domain")
                 .build();
        });
        assertThrows(IllegalArgumentException.class, () -> {
            Issue.builder()
                 .domain("Domain")
                 .name("Name")
                 .build();
        });

        final Issue issuea = Issue.builder()
                                  .domain("Domain")
                                  .name("Type")
                                  .addLocation(TextFileLocation.builder()
                                                               .systemId("file")
                                                               .build())
                                  .severity(IssueSeverity.BLOCKER)
                                  .description("description")
                                  .build();
        final Issue issueb = Issue.builder()
                                  .timestamp(issuea.getTimestamp())
                                  .domain("Domain")
                                  .name("Type")
                                  .addLocation(TextFileLocation.builder()
                                                               .systemId("file")
                                                               .build())
                                  .severity(IssueSeverity.BLOCKER)
                                  .description("description")
                                  .build();

        final Issue issuec = Issue.builder()
                                  .timestamp(issuea.getTimestamp())
                                  .domain("Domain")
                                  .name(Type.A)
                                  .locations(TextFileLocation.builder()
                                                             .systemId("file")
                                                             .build())
                                  .severity(IssueSeverity.BLOCKER)
                                  .description("description")
                                  .project("Project")
                                  .build();

        final Issue issued = Issue.builder()
                                  .timestamp(issuea.getTimestamp())
                                  .ruleId(new RuleId("Domain", Type.A))
                                  .locations(TextFileLocation.builder()
                                                             .systemId("file")
                                                             .build())
                                  .severity(IssueSeverity.BLOCKER)
                                  .description("description")
                                  .project("Project")
                                  .build();

        assertEquals("Type", issuea.getName());
        assertEquals(issuea, issuea);
        assertEquals(issuea, issueb);
        assertEquals(issuec, issued);
        assertNotEquals(issuea, null);
        assertNotEquals(issuea, "hello");
        assertEquals(issuea.getId(), issueb.getId());
        assertEquals(issuea.getRuleId(), issueb.getRuleId());
        assertEquals(issuea.getDescription(), issueb.getDescription());
        assertEquals(issuea.getLocationAt(0), issueb.getLocationAt(0));
        assertEquals(issuea.getLocationAt(0, TextFileLocation.class), issueb.getLocationAt(0, TextFileLocation.class));
        assertEquals(IssueSeverity.BLOCKER, issuea.getSeverity());
        assertEquals(1, issuea.getNumberOfLocations());

        assertEquals(null, issuea.getProject());

        assertEquals("Domain", issuea.getDomain());
        assertEquals("Type", issuea.getName());
        assertEquals(Type.A, issuec.getName(Type.class));
        assertEquals("A", issuec.getName());

        assertEquals(issuea.toString(), issueb.toString());
        assertEquals(issuea.hashCode(), issueb.hashCode());
    }
}