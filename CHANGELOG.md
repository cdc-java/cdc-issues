# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- Created `StreamIssuesHandler`. #116
- Created `Meta.STATUS`. #115
- Added methods to traverse and collect checkers
- Added `AbstractChecker.getCheckedRules()`.
- Added new contruction methods to `CompositeChecker`.
- Added `AbstractChecker.checkRaw()`.
- Added `AbstractChecker.init(SnapshotManager)` that can be used to register a checker in the manager for lazy invocation. #119 

### Changed
- **Breaking:** removed `SnapshotManager` in `AbstractChecker` constructor. #119

### Removed
- **Breaking:** removed generic parameters from `IssueHandler` and its specializations.
- **Breaking:** removed generic parameters from `Diagnosis`.
- **Breaking:** removed generic parameters from `HasDiagnosis`.
- **Breaking:** removed code that was deprecated in 2024/03.


## [0.66.0] - 2025-02-23
### Added
- created `MetasBuilding` and used it in `Rule` and `Issue`.

### Changed
- Updated dependencies:
    - cdc-office-0.60.1
    - org.junit-5.12.0


## [0.65.0] - 2025-02-08
### Added
- Added a new method (with title) to `RuleUtils` (replaces the deprecated one).
- **Breaking:** added rule title to stats. A profile musynow be defined to create `CheckStats`. #109 
- Added `Issue.Builder.issue(issue)` to initialize a builder with an existing issue. #111
- Added `AbstractConvertibleChecker` that can be used to check that an object can be converted to a target type
  and, in case of success, pass the conversion result to an optional delegate. #110
- Created `Metas.metaIfNotNull()` and `NameValueMap.entryIfNotNull()` methods.

### Changed
- Enabled ZIP32 to write check stats.
- Read empty param or meta as null.
- Updated dependencies:
    - cdc-office-0.60.0
    - commons-codec-1.18.0

### Fixed
- Avoid NPE when writing null param or meta.

### Deprecated 
- Deprecated a method in `RuleUtils`.
- Deprecated some methods of `ProfileImpl`. On should use `ProfileImpl.Builder`.


## [0.64.0] - 2025-01-18
### Added
- Added `IssuesIoFactoryFeatures.Hint.ZIP32`.

### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - cdc-office-0.59.0
    - commons-codec-1.17.2
- Enabled `Hint.POI_STREAMING` and `Hint.ZIP32` in `IssuesIoFactoryFeatures.UTC_BEST` and `IssuesIoFactoryFeatures.UTC_FASTEST`.


## [0.63.0] - 2025-01-03
### Added
- Added optional title to `Rule`. #97
- Added issue id hash. It is computed and can be exported.  
  A SHA1 digest is computed from (domain, name, params, project, locations). #106
- Added new methods to `SnapshotManager`.
- Added `RuleId.NONE`.
- Added notes to stats.
- Added `ProfileImpl.addIfMissing(rule)`.

### Changed
- Updated dependencies:
    - cdc-io-0.53.1
    - cdc-office-0.58.1
    - cdc-ui-0.31.3
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3
- Replaced `Issue.Builder.locations(List<Location>)` by `Issue.Builder.locations(List<? extends Location>)`.
- Added more traces in `print(out, level)` of Checkers.
- Changed behavior of `AbstractChecker.addStat(location, result)` to facilitate detection of implementation errors.
- **Breaking:** changed behavior of `ProfileImpl.add(rule)` that now throws an exception if a rule with same id is already presnet.
- Changed the sort order of rules when outputing a `Profile`.

### Deprecated
- Deprecated `Diagnosis.create(issues)` that is replaced by `Diagnosis.of(issues)`.

### Removed
- Removed code that was deprecated in 2023.

### Fixed
- Fixed `LabelsBuilding.labels(labels)`. `...` was missing.
- Fixed a possible Optional construction issue form a null object.
- Fixed missed statistics of delegating checkers. #108


## [0.62.0] - 2024-10-12
### Added
- Added `Exceptions` section to `RuleDescription`.

### Changed
- Updated dependencies:
    - org.junit-5.11.2


## [0.61.0] - 2024-09-28
### Added
- Added `append(name, value separator)` to `NameValueMap.Builder`. 

### Changed
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-office-0.57.2
    - cdc-ui-0.31.2
    - cdc-util-0.53.0
    - org.junit-5.11.1


## [0.60.0] - 2024-07-22
### Added
- Support split and auto split of user meta data. #102

### Changed
- Updated dependencies:
    - cdc-office-0.57.1

### Fixed
- Do not format split columns (that would prevent their loading). #103


## [0.59.1] - 2024-05-18
### Changed
- Updated maven plugins.
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-office-0.55.0
    - cdc-ui-0.31.1
    - cdc-util-0.52.1


## [0.59.0] - 2024-04-14
### Added
- Added `AbstractChecker.isCorrectlyConfigured()` and `CheckResult.EXCEPTION`.
  Used when a checker throws an exception.
- Created `NameValue`, `NameValueMap`, `Meta` and `Metas`. #99

### Changed
- **Breaking:** used `Metas` instead of `Params` to return meta data. #99


## [0.58.0] - 2024-03-23
### Added
- Added `Labels` to `Rule`, `Issue`, `IssueAnswer`, `Projet` and `Snapshot`. #94
- Added `Metas` to `IssueAnswer`.
- Added `AbstractChecker.isCorrectlyConfigured()` and `CheckResult.MISCONFIGURED`.

### Changed
- Updated dependencies:
    - cdc-io-0.51.2
    - cdc-office-0.54.0
    - org.apache.log4j-2.23.1
- ̀`CheckContext` is now final as it can not be extended.  
  `SnapshotManager` must be used to pass additional parameters to checkers. #98

### Deprecated
- Deprecated `RulesCatalog`, `IssuesDetector` and their implementations.  
  Note that the deprecation date was set to 2023-10-21, as this was advertized at that date in README.  
  This code was never used contrarily to other check classes.


## [0.57.1] - 2024-03-04
### Deprecated
- Added deprecated methods (`Rule` and `RuleUtils`) that were removed by 0.57.0.


## [0.57.0] - 2024-03-03
### Added
- Added meta data to rules. #95
- Added the possibility to override the severity of a rule. #96 

### Changed
- **Breaking:** a rule has now a single severity. #96
- Harmonized Profile IO.


## [0.56.1] - 2024-02-16
### Fixed
- Fixed `StructuredDescription.getSections()`. #93


## [0.56.0] - 2024-02-11
### Added
- Added `ProfileIoFeatures.SHOW_RULE_DOMAIN`.

### Changed
- `IssuesIoFactoryFeatures` `UTC_BEST` and `UTC_FASTEST` are no more verbose.  
  Use `UTC_BEST_VERBOSE` and `UTC_FASTEST_VERBOSE` to activate traces.
- Updated dependencies:
    - org.junit-5.10.2


## [0.55.0] - 2024-01-21
### Added
- Added `Params.getSortedParams()`.
- Created `ProfileIo` in api. #92

### Changed
- Updated dependencies:
    - cdc-office-0.53.0

### Removed
- Removed `ProfileIo` in core. Use the api version now.  #92 


## [0.54.0] - 2024-01-06
### Changed
- `ProfileIo` does no more formatting for MD. The application must do it itself.


## [0.53.0] - 2024-01-05
### Added
- Created `ProfileIo`.

### Changed
- `RuleDescription.Builder` is now generic to ease its derivation.


## [0.52.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-office-0.52.0
    - cdc-ui-0.31.0
    - cdc-util-0.52.0


## [0.51.0] - 2023-12-22
### Changed
- Updated dependencies:
    - cdc-office-0.51.0
    - cdc-util-0.51.0
- Updated maven plugins.

### Added
- Added a function to print a location without tag.


## [0.50.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-office-0.50.0
    - cdc-ui-0.30.0
    - cdc-tuples-1.4.0
    - cdc-util-0.50.0


## [0.34.0] - 2023-11-18
### Added
- Implemented `self()` in `AbstractPartChecker`.
- Created `SpecializationChecker`.

### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-office-0.31.0
    - cdc-ui-0.20.15
    - cdc-tuples-1.3.0
    - cdc-util-0.33.2
    - org.junit-5.10.1


## [0.33.0] - 2023-10-22
### Added
- Created `AbstractPropertyChecker`. #91

### Changed
- Moved checks code to a dedicated module (`cdc-issues-checks`). #91
- Renamed `AbstractPartChecker` to `AbstractPartsChecker`. #91


## [0.32.0] - 2023-10-21
### Added
- Added `Issue.Builder.description(StructuredDescription.Builder)`.
- Support loading of files that contain only issues or project and snapshot. #88
- Added hash of issues to generated files in project and snapshot mode. #87
- Added an `Issue.Builder` method. #89
- Created `checks` package, moved some code (`RulesCatalog`, ...) to this package,
  and created several classes (`AbstractChecker`, `CheckContext`, `SnapshotManager`, ...) that can help checkinng data.  
  `IssuesDetector` will probably be changed or removed in the future.  
  It was never used, contrarily to new classes that come from `cdc-asd` and and `cdc-mf`. #89
  
### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-office-0.30.2
    - cdc-ui-0.20.14
    - cdc-util-0.33.1
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 
- Root element of XML files has changed when project and snapshot is generated.  
  An `<issues>` element has been added around issues in snapshot. #88
- Changed WorkbookLocation contruction. No visible effect.
- Moved `CheckResult` and `CheckStats` to `checks` package. #89

### Deprecated
- Deprecated ability to derive `Issue`. This class will be probably made `final`.  
  One should directly use Issue.  
  In order to facilitate transition, generic parameters have been kept in `IssueHandler` and some other classes.  
  But one should use `Issue` as parameter. #90


## [0.31.1] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-office-0.30.1
    - cdc-ui-0.20.13
    - cdc-util-0.33.0


## [0.31.0] - 2023-06-11
### Added
- Added `LocatedItem`.
- Added `RuleDescription`.

### Changed
- Updated dependencies:
    - cdc-io-0.26.2
    - cdc-office-0.30.0
    - cdc-util-0.32.0


## [0.30.0] - 2023-04-22
### Added
- Added description to `Snapshot` and `Project`. #86

### Changed
- Updated dependencies:
    - cdc-office-0.29.4
    - cdc-ui-0.20.12


## [0.29.0] - 2023-04-15
### Added
- Added parameter to `OutSettings` to sort meta using a user-defined order. #85

### Changed
- Updated dependencies:
    - cdc-office-0.29.3
    - cdc-ui-0.20.11


## [0.28.0] - 2023-04-01
### Added
- Created `CheckResult`, `CheckStats` and `WorkbookCheckStatsIo`.
- Added a description to `Profile`.

### Changed
- Updated dependencies:
    - cdc-io-0.26.0
    - cdc-office-0.29.2
    - org.openjdk.jol-0.17


## [0.27.0] - 2023-02-25
### Added
- Added `Issue.Builder.description(Object)`.
- Added meta data to `Project` and `Snapshot`. #84
- Created `StructuredDescription` that can be used as a base class to standardize description of rules and issues. #82 and #83

### Changed
- `Profile.isEnabled(Rule)` and `Profile.getParams(Rule)` can now throw an exception is the passed rule is unknown.
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-office-0.29.1
    - cdc-ui-0.20.10
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.26.0] - 2023-01-28
### Added
- Added `Feature.AUTO_SIZE_ROWS` to `IssuesIoFactoryFeatures.UTC_BEST`.
- Added `rule(Rule)`to `Issue.Builder`.

### Changed
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-office-0.29.0
    - cdc-ui-0.20.9
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.25.0] - 2023-01-02
### Added
- Added new `save()` methods to `IssuesWriter`. 

### Changed
- Renamed `setNulberOfIssues` to `setNumberOfIssues` in `SnapshotDataImpl`.
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-office-0.28.0
    - cdc-ui-0.20.8
    - cdc-util-0.28.2

### Removed
- Removed `IssueId` deprecated constructors (see #75).


## [0.24.0] - 2022-11-12
### Added
- Added loading of issues from input stream. #78

### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-office-0.27.0
    - cdc-ui-0.20.7
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1
- In `WorkbookIssuesStrealmWriter`, use snapshot data when issue snapshot or project is null.


## [0.23.4] - 2022-08-24
### Changed
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-office-0.24.0
    - cdc-tuples-1.2.0
    - cdc-ui-0.20.6
    - cdc-util-0.28.0
    - org.junit-5.9.0


## [0.23.3] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-office-0.23.1
    - cdc-ui-0.20.5
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.23.2] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-office-0.23.0
    - cdc-ui-0.20.4
    - cdc-util-0.26.0


## [0.23.1] - 2022-05-21
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-office-0.22.0
    - cdc-tuples-1.1.0
    - cdc-ui-0.20.3
    - cdc-util-0.25.0


## [0.23.0] - 2022-03-19
### Added
- Added new `OutSettings.Hints` to expand comments. #76

### Changed
- `OutSettings.replaceAuto()` signature was changed. #76

### Fixed
- With workbooks, header is generated even when there are no issues. #77


## [0.22.0] - 2022-03-11
### Added
- Added `Profile.getConfiguredRules` and `Profile.getEnabledConfiguredRules`. #73

### Changed
- Renamed `IssueDetector.AbstractDescriptor` to `IssueDetector.AbstractFactory`. #72
- `Issue.Description`is now initialized to empty String and does not need to be set. #71
- Empty `locations` is now possible in `IssueId`. #74
- Updated dependencies:
    - cdc-io-0.21.1
    - cdc-office-0.21.1
    - cdc-ui-0.20.2
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2

### Deprecated
- `IssueId` public constructors have been deprecated. #75


## [0.21.0] - 2022-02-13
### Added
- `RuleId`, `IssueId`, `Location` and `Params` are now Comparable. #68
- It is now possible to save issue to `OutputStream`. #65
- Created `IssuesStreamWriter` and related code. #65 

### Changed
- Renamed `IssuesDetector.Descriptor` to `IssuesDetector.Factory`.  
  Methods named `...Descriptor...` were renamed `...Factory...`. #67
- Do not repeat file in factory and writer/reader. #69
- Moved `IssuesWriter.Settings` to `OutSettings`. #65
- In the context of a `Profile`, `Rules` can now be enabled/disabled.  
  Some existing methods of `Profile` were renamed. #70
- Updated dependencies:
    - cdc-io-0.21.0
    - cdc-office-0.21.0
    - cdc-ui-0.20.1
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-05
### Added
- Added new methods to `Rule.Builder`. #61
- Added new methods to `RulesCatalog`. #60
- Added `Snapshot.getPoject()`, `ProjectImpl.createSnaphot()`. #64

### Changed
- Upgraded to Java 11
- Replaced `getDescriptorOrNull` by `getDescriptor` that returns an `Optional` in ̀`RulesCatalog`.
- `Rule.Builder.description` has now a default valid value (""). #62
- Moved `cdc.issues.IssuesFactory` to `cdc.issues.io.IssuesIoFactory`,
  and `cdc.issues.IssuesFactoryFeatures` to `cdc.issues.io.IssuesIoFactoryFeatures`. #63
- Addition of `Snapshot.getProject()` resulted in changes in `ProjectImpl` and `SnapshotImpl`.  
  Changed signature of `IssuesAndAnswers.getAnswer()`, `Project.getProfile()`, `Project.getAnswer()` that now return an `Optional`.  
  Changed signature of `IssuesWriter.save(snapshot)` because `Project` can now be access from `Snapshot`. #64
- Updated dependencies:
    - cdc-io-0.20.0
    - cdc-office-0.20.0
    - cdc-ui-0.20.0


## [0.15.1] - 2022-01-29
### Added
- It is now possible to control the param and meta prefixes. #56
- Added meta data to `Profile`. #57
- Added ability to hide Locations column and to expand locations into several columns. #55
- Name of location columns is now Location#index.Xxx instead of Xxx#index, where index is on of Tag, Path or Anchor. #59

### Changed
- Updated dependencies:
    - cdc-office-0.15.0


## [0.15.0] - 2022-01-23
### Added
- Added save methods to `IssuesWriter`.
- Added `IssuesWriter.Settings.Hint.AUTO_METAS`. #51
- Added export and import of issue params.  
  Added `IssuesWriter.Settings.Hint.NO_PARAMS_COL` and `IssuesWriter.Settings.Hint.AUTO_PARAMS`. #52
- Updated maven plugins

### Changed
- Renamed some `IssuesWriter.Hint` values.
- Moved `core` code to `core.io`. This should not have visible impact.

### Fixed
- Added missing Params column to `IssuesTabelModel`. #53
- Order of columns in import of office formats does not matter any more. #54


## [0.14.2] - 2022-01-15
### Added
- It is now possible to save a project, its profile  and a snapshot. #44
- Created module `cdc-issues-impl`.

### Changed
- Moved some code from `cdc-issues-api` to `cdc-issues-impl`.  
  Moved classed were renamed from DefaultFoo to FooImpl.

### Fixed
- Use as many sheets as necessary when saving issues to a format with limits. #50

### Security
- Updated dependencies:
    - cdc-io-0.13.2
    - cdc-office-0.14.2
    - cdc-ui-0.13.2
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1. #49


## [0.14.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-io-0.13.1
    - cdc-office-0.14.1
    - cdc-ui-0.13.1
    - cdc-util-0.14.1
    - org.apache.log4j-2.17.0. #49


## [0.14.0] - 2021-12-15
### Security
- Updated dependencies:
    - cdc-io-0.13.0
    - cdc-office-0.14.0
    - cdc-ui-0.13.0
    - cdc-util-0.14.0
    - org.apache.log4j-2.16.0. #49

### Fixed
- Fixed warnings.


## [0.13.0] - 2021-10-02
### Added
- Default implementation of `Location.getTag()`. #46
- `IssuesWriter.Settings` was added. #44
- `IssueId` has now rule parameters. #47
- Created `Snapshot`interface. #48

### Changed
- `IssuesWriter` API was simplified.  
  One must now pass `Settings` to control presence of standard
  data or meta data. #44
- Moved `Project`. #48


## [0.12.0] - 2021-10-01
### Changed
- Added `getTag()` to `Location` and created `Locations`. There is now a Location
  factory.  
  Formatting of `Location` has changed in IO and now contains `Tag`. That way, 
  location can be reconstructed as initially defined. #37


## [0.11.0] - 2021-10-02
### Added
- Added `IssueComment` and ̀`IssueAnswer`. #10
- Added new versions of save to `IssuesWriter`, that take into account answers. #10
- Added new versions of load to `IssuesReader`, that take into account answers. #10
- Created `Profile` interface and added formal parameters to `Rule`s. #36
- Created `IssueTransition`. #42
- Added new values to `IssuesFactoryFeatures.Hint` and passed them
  to `WorkbookIssuesWriter`. Writing of Excel files should now be faster and
  use less memory. #43

### Changed
- Renamed `FIXEDS` to `FIXED` in IssueResolution. #10
- Added `ProgressController` parameter to `IssuesReader`. #38
- Added `UNRESOLVED` to `IssueResolution`. #39
- Added parameters to `Rule`, created and used `ConfiguredRule`. #40
- Renamed `RulesProfile`to `Profile`.

### Fixed
- A bug in `XmlIssuesWriter` and `XmlIssuesReader`.


## [0.10.1] - 2021-07-23
### Fixed
- Invalid pom dependencies.


## [0.10.0] - 2021-07-23 [YANKED]
### Added
- Imported ui code that was located in cdc-ui.
- It is now possible to associate `metadata` to an `Issue`. #29
- It is now possible to define a `WorkbookLoaction` using a `functional row id`. #31
- Improved documentation: changed README, created several images. #32
- Created `Diagnosis`. cdc-java/cdc-applic#76
- Added snapshot to `Issue`. #34
- Added `ÌssueStatus` and `IssueResolution`. #10


### Changed
- Updated dependencies.
- A `Rule` now supports one or more severities. #33
- `IssuesDetector` now accepts a list of locations. #30
- `Issue.Builder`
- Removed `params` to `Rule` and `Issue` and added `project`. #35

### Fixed
- Better progress notification in Issues Writers.


## [0.9.0] - 2021-06-05
### Added
- Created Issues Tee

### Changed
- Added ProgressController to IssuesWriter.


## [0.8.0] - 2021-05-03
### Changed
- Updated dependencies.
    - Used **cdc-kernel-tuples** instead of **cdc-util-tuples**.
    - Used **cdc-io-x** instead of **cdc-util-x**.
    - Used **cdc-office-x** instead of **cdc-util-office-x**.
- Renamed `cdc-issues-api` to `cdc-issues`. #26

### Fixed
- Renamed `cdc/impex/api/doc-files` to `cdc/impex/doc-files`.


## [0.7.0] - 2021-04-08
### Added
- This CHANGELOG file.
- issues(I... issues) in IssuesHandler.
- AbstractIssuesDetector and AbstractDescriptor.
- CompositeRulesCatalog and DefaultRulesCatalog. #25
- More tests.

### Changed
- Replaced Location[] by Location... in Issue.Builder, IssueId.
- Renamed targetId to path, and path to anchor in Location.  
Propagated these changes.
- WorkbookLocation can now designate a sheet, a row, a column, a cell.
- RulesCatalog is now an interface.
- Improved README.md.

### Fixed
- name(Enum) in Issue.Builder and Rule.Builder.


## [0.6.0] - 2021-03-29
### Added
- Issues detection: IssuesDetector, Descriptor, LocatedData, DefaultLocatedData. #17 #24
- IssueSeverityItem
- VerboseIssuesHandler, VoidIssuesHandler. #21
- Implementations of IssuesReader for XML and JSON. #19
- Tests

### Changed
- Created new packages: io, locations, and rules.
- Moved code to `cdc.issues.api.io`:
    - IssuesFormat
    - IssuesReader
    - IssuesWriter
- Moved code to `cdc.issues.api.rules`:
    - Rule
    - RuleId
    - RulesCatalog
- Renamed code:
    - AbstractIssueLocation: AbstractLocation
    - DefaultIssueLocation: DefaultLocation
    - TextFileIssueLocation: TextFileLocation
    - WorkbookIssueLocation: WorkbookLocation
    - IssueLocation: Location
    - RuleParams: FormalParams
    - IssueParams: Params

### Removed
- Issue cause. #22


## [0.5.0] - 2021-03-21
### Added
- IssuesParams. #12
- IssuesFormat. #14
- IssuesReader and implementation for workbooks. #19
- Rule, RuleId, RuleParams, RulesCatalog. #13
- Implementation of IssuesWriter for XML. #18
- COPYING.LESSER. #15
- Report module. #20
- Tests

### Changed
- Renamed IssueLevel to IssueSeverity

### Fixed
- JsonIssuesWriter
- WorkbookIssuesWriter


## [0.4.0] - 2021-03-14
### Added
- min/max to IssueLevel. #6
- IssuesWriter.
- Implementation of IssuesWriter for workbooks and JSON. #9
- IssuesFactory. #8

### Changed
- Added a Cause parameter to Issue. #7
- IssueLocation and derived classes
- Renamed getType to getIssueType in Issue


## [0.3.0] - 2021-03-07
### Added
- Timestamp to Issue. #2

### Changed
- Support ZoneId to print timestamps. #5
- IssuesHandler.


## [0.2.0] - 2021-03-01
### Added
- IssueId
- DefaultIssueLocation

### Changed
- Issue
- TextFileIssueLocation
- WorkbookIssueLocation


## [0.1.0] - 2021-02-22
### Added
- Initial design (Issue, IssueLocation, IssuesCollector, IssuesHandler, ...)
