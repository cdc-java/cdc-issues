package cdc.issues.answers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.issues.Issue;
import cdc.issues.IssueId;
import cdc.issues.Snapshot;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link IssuesAndAnswers} based on a {@link Snapshot}.
 *
 * @author Damien Carbonne
 */
public class SnapshotIssuesAndAnswers implements IssuesAndAnswers {
    private final Snapshot snapshot;

    public SnapshotIssuesAndAnswers(Snapshot snapshot) {
        this.snapshot = Checks.isNotNull(snapshot, "snapshot");
    }

    @Override
    public List<Issue> getIssues() {
        return snapshot.getIssues();
    }

    @Override
    public Set<? extends IssueAnswer> getAnswers() {
        return snapshot.getProject().isPresent()
                ? snapshot.getProject().orElseThrow().getAnswers()
                : Collections.emptySet();
    }

    @Override
    public Optional<? extends IssueAnswer> getAnswer(IssueId id) {
        return snapshot.getProject().isPresent()
                ? snapshot.getProject().orElseThrow().getAnswer(id)
                : Optional.empty();
    }
}