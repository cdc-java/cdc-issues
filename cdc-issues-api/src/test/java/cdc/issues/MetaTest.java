package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class MetaTest {
    @Test
    void test() {
        final Meta meta = Meta.of("name", "value");
        assertEquals("name", meta.getName());
        assertEquals("value", meta.getValue());
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         Meta.of(null, null);
                     });
    }
}