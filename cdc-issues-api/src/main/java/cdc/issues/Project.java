package cdc.issues;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.issues.answers.IssueAnswer;
import cdc.issues.rules.Profile;

/**
 * Interface describing a Project.
 *
 * @author Damien Carbonne
 */
public interface Project {
    /**
     * @return The name of this project.
     */
    public String getName();

    /**
     * @return The description of this project.
     */
    public String getDescription();

    /**
     * @return The meta data associated to this project.
     */
    public Metas getMetas();

    /**
     * @return The labels associated to this project.
     */
    public Labels getLabels();

    /**
     * @return The profile of this project.
     */
    public Optional<? extends Profile> getProfile();

    /**
     * @return The data sources contained in this project.
     */
    public Iterable<DataSource<?>> getDataSources();

    /**
     * @return The associated snapshots.
     */
    public List<? extends Snapshot> getSnapshots();

    /**
     * @return The associated answers.
     */
    public Set<? extends IssueAnswer> getAnswers();

    /**
     * @param id The issue id.
     * @return The answer having an {@code id} or {@code null}.
     */
    public Optional<? extends IssueAnswer> getAnswer(IssueId id);
}