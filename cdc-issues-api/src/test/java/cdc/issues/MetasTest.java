package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

class MetasTest {
    @Test
    void testEmpty() {
        final Metas metas = Metas.builder()
                                 .build();
        assertTrue(metas.isEmpty());
        assertEquals(Metas.NO_METAS, metas);
    }

    @Test
    void test() {
        final Metas metas = Metas.builder()
                                 .meta("name", "value")
                                 .build();
        assertEquals(Set.of("name"), metas.getNames());
    }

    @Test
    void testNullName() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         Metas.builder().meta((String) null, null).build();
                     });
    }

    @Test
    void testCollection() {
        final List<Meta> list = List.of(Meta.of("name", "value"));
        final Metas metas = Metas.builder()
                                 .metas(list)
                                 .build();
        assertEquals(Set.of("name"), metas.getNames());
    }
}