package cdc.issues.ui.swing;

import javax.swing.Icon;

import cdc.issues.IssueSeverity;
import cdc.ui.swing.SwingUtils;
import cdc.ui.swing.cells.EnumUi;

public final class IssueSeverityUi {
    private IssueSeverityUi() {
    }

    public static final Settings SETTINGS = new Settings();

    public static class Settings extends EnumUi.Settings<IssueSeverity> {
        final Icon blocker = SwingUtils.loadIcon("cdc/issues/images/cdc-issue-severity-blocker.png");
        final Icon critical = SwingUtils.loadIcon("cdc/issues/images/cdc-issue-severity-critical.png");
        final Icon major = SwingUtils.loadIcon("cdc/issues/images/cdc-issue-severity-major.png");
        final Icon minor = SwingUtils.loadIcon("cdc/issues/images/cdc-issue-severity-minor.png");
        final Icon info = SwingUtils.loadIcon("cdc/issues/images/cdc-issue-severity-info.png");

        @Override
        public Icon getIcon(IssueSeverity value) {
            switch (value) {
            case BLOCKER:
                return blocker;
            case CRITICAL:
                return critical;
            case MAJOR:
                return major;
            case MINOR:
                return minor;
            case INFO:
                return info;
            default:
                return null;
            }
        }

        @Override
        public String getText(IssueSeverity value) {
            return value.name();
        }
    }
}