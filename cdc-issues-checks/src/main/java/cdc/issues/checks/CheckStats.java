package cdc.issues.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.issues.rules.Profile;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleId;
import cdc.util.lang.Checks;

/**
 * Utility designed to accumulate statistics on passed and ignored checks on items.
 *
 * @author Damien Carbonne
 *
 * @param <I> The item type, onto which rules are passed or skipped.
 */
public class CheckStats<I> {
    public static interface Data<I> {
        public I getItem();

        public RuleId getRuleId();

        public String getRuleTitle();

        public CheckResult getResult();

        public String getNotes();
    }

    private static record DataImpl<I>(I item,
                                      RuleId ruleId,
                                      String ruleTitle,
                                      CheckResult result,
                                      String notes)
            implements Data<I> {
        @Override
        public I getItem() {
            return item;
        }

        @Override
        public RuleId getRuleId() {
            return ruleId;
        }

        @Override
        public String getRuleTitle() {
            return ruleTitle;
        }

        @Override
        public CheckResult getResult() {
            return result;
        }

        @Override
        public String getNotes() {
            return notes;
        }
    }

    /** The associated profile. */
    private final Profile profile;
    /** Accumulated data. */
    private final List<Data<I>> data = new ArrayList<>();
    /** (item, List<Data>) map: data accumulated for each item **/
    private final Map<I, List<Data<I>>> itemToData = new HashMap<>();
    private final Map<RuleId, int[]> counts = new HashMap<>();

    public CheckStats(Profile profile) {
        this.profile = Checks.isNotNull(profile, "profile");
    }

    public Profile getProfile() {
        return profile;
    }

    /**
     * Clears all accumulated data.
     */
    public void clear() {
        data.clear();
        itemToData.clear();
        counts.clear();
    }

    /**
     * Accumulate data on an {@code item}.
     *
     * @param item The checked or ignored item.
     * @param ruleId The {@link RuleId} of the passed or ignored {@link Rule}.
     * @param result The obtained {@link CheckResult} that must be related to a rule.
     * @param notes The notes.
     */
    public void add(I item,
                    RuleId ruleId,
                    CheckResult result,
                    String notes) {
        Checks.isNotNull(item, "item");
        Checks.isNotNull(ruleId, "ruleId");
        Checks.isNotNull(result, "result");
        Checks.isTrue(result.isRuleResult(), "Invalid result");

        final Optional<Rule> rule = profile.getRule(ruleId);

        final Data<I> d = new DataImpl<>(item,
                                         ruleId,
                                         rule.isPresent() ? rule.orElseThrow().getTitle() : null,
                                         result,
                                         notes);

        data.add(d);

        final List<Data<I>> list = itemToData.computeIfAbsent(item, k -> new ArrayList<>());
        list.add(d);

        final int[] c = counts.computeIfAbsent(ruleId, k -> new int[CheckResult.values().length]);
        c[result.ordinal()]++;
    }

    /**
     * Accumulate data on an {@code item} (without notes).
     *
     * @param item The checked or ignored item.
     * @param ruleId The {@link RuleId} of the passed or ignored {@link Rule}.
     * @param result The obtained {@link CheckResult}.
     */
    public void add(I item,
                    RuleId ruleId,
                    CheckResult result) {
        add(item, ruleId, result, null);
    }

    /**
     * @return A list of all accumulated data.
     */
    public List<Data<I>> getDatas() {
        return data;
    }

    /**
     * @param item The item.
     * @return A list of all accumulated data for an item.
     */
    public List<Data<I>> getDatas(I item) {
        return itemToData.getOrDefault(item, Collections.emptyList());
    }

    /**
     * @return A Set of all items that were accumulated.
     */
    public Set<I> getItems() {
        return itemToData.keySet();
    }

    /**
     * @return A Set of all {@link RuleId RuleIds} that were accumulated.
     */
    public Set<RuleId> getRuleIds() {
        return counts.keySet();
    }

    /**
     * Return a Set of all {@link RuleId RuleIds} that were accumulated for a given {@code item} with a certain {@link CheckResult}.
     *
     * @param item The item.
     * @param result The {@link CheckResult}.
     * @return A Set of all {@link RuleId RuleIds} that were accumulated for {@code item} with a {@code result}.
     */
    public Set<RuleId> getRuleIds(I item,
                                  CheckResult result) {
        return getDatas(item).stream()
                             .filter(d -> d.getResult() == result)
                             .map(Data::getRuleId)
                             .collect(Collectors.toSet());
    }

    /**
     * Return the number of entries that were accumulated for a given {@link RuleId} with a certain {@link CheckResult}.
     *
     * @param ruleId The {@link RuleId}.
     * @param result The {@link CheckResult}.
     * @return The number of entries that were accumulated for {@code ruleId} with {@code result}.
     */
    public int getCounts(RuleId ruleId,
                         CheckResult result) {
        final int[] c = counts.get(ruleId);
        if (c == null) {
            return 0;
        } else {
            return c[result.ordinal()];
        }
    }

    /**
     * Return the number of entries that were accumulated for a given {@link RuleId}.
     *
     * @param ruleId The {@link RuleId}.
     * @return The number of entries that were accumulated for {@code ruleId}.
     */
    public int getCounts(RuleId ruleId) {
        final int[] c = counts.get(ruleId);
        if (c == null) {
            return 0;
        } else {
            int total = 0;
            for (final int i : c) {
                total += i;
            }
            return total;
        }
    }

    /**
     * Return the number of entries that were accumulated for a given {@link CheckResult}.
     *
     * @param result The {@link CheckResult}.
     * @return The number of entries that were accumulated for a {@code result}.
     */
    public int getCounts(CheckResult result) {
        int total = 0;
        for (final int[] c : counts.values()) {
            total += c[result.ordinal()];
        }
        return total;
    }

    /**
     * @return The number of entries that were accumulated.
     */
    public int getCounts() {
        int total = 0;
        for (final int[] c : counts.values()) {
            for (final int i : c) {
                total += i;
            }
        }
        return total;
    }
}