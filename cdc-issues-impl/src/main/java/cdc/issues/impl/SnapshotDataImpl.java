package cdc.issues.impl;

import java.time.Instant;
import java.util.Optional;

import cdc.issues.IssueUtils;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Project;
import cdc.issues.Snapshot;
import cdc.issues.io.SnapshotData;
import cdc.issues.rules.Profile;
import cdc.util.lang.Checks;

/**
 * Default implementation of {@link SnapshotData}.
 *
 * @author Damien Carbonne
 */
public class SnapshotDataImpl implements SnapshotData {
    private String projectName;
    private String projectDescription;
    private Metas projectMetas = Metas.NO_METAS;
    private Labels projectLabels = Labels.NO_LABELS;
    private Profile profile;
    private String snapshotName;
    private String snapshotDescription;
    private Metas snapshotMetas = Metas.NO_METAS;
    private Labels snapshotLabels = Labels.NO_LABELS;
    private Instant snapshotTimestamp = Instant.now();
    private int numberOfIssues = -1;
    private String issuesHash = "";

    public SnapshotDataImpl() {
        super();
    }

    public SnapshotDataImpl(Snapshot snapshot) {
        final Project project = snapshot.getProject().orElse(null);
        setProjectName(project == null ? null : project.getName());
        setProjectDescription(project == null ? null : project.getDescription());
        setProjectMetas(project == null ? Metas.NO_METAS : project.getMetas());
        setProfile(project == null ? null : project.getProfile().orElse(null));
        setSnapshotName(snapshot.getName());
        setSnapshotDescription(snapshot.getDescription());
        setSnapshotMetas(snapshot.getMetas());
        setSnapshotTimestamp(snapshot.getTimestamp());
        setNumberOfIssues(snapshot.getIssues().size());
        setIssuesHash(IssueUtils.getHash(snapshot.getIssues()));
    }

    public SnapshotDataImpl setProjectName(String name) {
        this.projectName = name;
        return this;
    }

    public SnapshotDataImpl setProjectDescription(String description) {
        this.projectDescription = description;
        return this;
    }

    public SnapshotDataImpl setProjectMetas(Metas metas) {
        this.projectMetas = Checks.isNotNull(metas, "metas");
        return this;
    }

    public SnapshotDataImpl setProjectLabels(Labels labels) {
        this.projectLabels = Checks.isNotNull(labels, "labels");
        return this;
    }

    public SnapshotDataImpl setProfile(Profile profile) {
        this.profile = Checks.isNotNull(profile, "profile");
        return this;
    }

    public SnapshotDataImpl setSnapshotName(String name) {
        this.snapshotName = name;
        return this;
    }

    public SnapshotDataImpl setSnapshotDescription(String description) {
        this.snapshotDescription = description;
        return this;
    }

    public SnapshotDataImpl setSnapshotMetas(Metas metas) {
        this.snapshotMetas = Checks.isNotNull(metas, "metas");
        return this;
    }

    public SnapshotDataImpl setSnapshotLabels(Labels labels) {
        this.snapshotLabels = Checks.isNotNull(labels, "labels");
        return this;
    }

    public SnapshotDataImpl setSnapshotTimestamp(Instant timestamp) {
        this.snapshotTimestamp = timestamp;
        return this;
    }

    public SnapshotDataImpl setNumberOfIssues(int number) {
        this.numberOfIssues = number;
        return this;
    }

    public SnapshotDataImpl setIssuesHash(String hash) {
        this.issuesHash = hash;
        return this;
    }

    @Override
    public String getProjectName() {
        return projectName;
    }

    @Override
    public String getProjectDescription() {
        return projectDescription;
    }

    @Override
    public Metas getProjectMetas() {
        return projectMetas;
    }

    @Override
    public Labels getProjectLabels() {
        return projectLabels;
    }

    @Override
    public Optional<Profile> getProfile() {
        return Optional.ofNullable(profile);
    }

    @Override
    public String getSnapshotName() {
        return snapshotName;
    }

    @Override
    public String getSnapshotDescription() {
        return snapshotDescription;
    }

    @Override
    public Metas getSnapshotMetas() {
        return snapshotMetas;
    }

    @Override
    public Labels getSnapshotLabels() {
        return snapshotLabels;
    }

    @Override
    public Instant getSnapshotTimestamp() {
        return snapshotTimestamp;
    }

    @Override
    public int getNumberOfIssues() {
        return numberOfIssues;
    }

    @Override
    public String getIssuesHash() {
        return issuesHash;
    }
}