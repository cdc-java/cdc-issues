package cdc.issues.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cdc.issues.DataSource;
import cdc.issues.IssueId;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Project;
import cdc.issues.answers.IssueAnswer;
import cdc.util.lang.Checks;

/**
 * Default implementation of {@link Project}.
 *
 * @author Damien Carbonne
 */
public class ProjectImpl implements Project {
    private String name;
    private String description;
    private Metas metas = Metas.NO_METAS;
    private Labels labels = Labels.NO_LABELS;
    private ProfileImpl profile;
    private final List<SnapshotImpl> snapshots = new ArrayList<>();
    private final Map<IssueId, IssueAnswerImpl> map = new HashMap<>();
    private final Set<IssueAnswerImpl> answers = new HashSet<>();
    private final List<DataSource<?>> dataSources = new ArrayList<>();

    public ProjectImpl(String name) {
        this.name = name;
    }

    public ProjectImpl setName(String name) {
        this.name = name;
        return this;
    }

    public ProjectImpl setDescription(String description) {
        this.description = description;
        return this;
    }

    public ProjectImpl setMetas(Metas metas) {
        this.metas = Checks.isNotNull(metas, "metas");
        return this;
    }

    public ProjectImpl setLabels(Labels labels) {
        this.labels = Checks.isNotNull(labels, "labels");
        return this;
    }

    public ProjectImpl setProfile(ProfileImpl profile) {
        this.profile = Checks.isNotNull(profile, "profile");
        return this;
    }

    void addSnapshot(SnapshotImpl snapshot) {
        snapshots.add(Checks.isNotNull(snapshot, "snapshot"));
    }

    public SnapshotImpl createSnapshot() {
        return new SnapshotImpl(this);
    }

    public ProjectImpl addAnswer(IssueAnswerImpl answer) {
        Checks.isNotNull(answer, "answer");
        final IssueAnswer current = map.get(answer.getIssueId());
        answers.remove(current);
        answers.add(answer);
        map.put(answer.getIssueId(), answer);
        return this;
    }

    public ProjectImpl addAnswers(Collection<IssueAnswerImpl> answers) {
        for (final IssueAnswerImpl answer : answers) {
            addAnswer(answer);
        }
        return this;
    }

    public ProjectImpl addDataSource(DataSource<?> dataSource) {
        Checks.isNotNull(dataSource, "dataSource");
        dataSources.add(dataSource);
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Metas getMetas() {
        return metas;
    }

    @Override
    public Labels getLabels() {
        return labels;
    }

    @Override
    public Optional<ProfileImpl> getProfile() {
        return Optional.ofNullable(profile);
    }

    @Override
    public Iterable<DataSource<?>> getDataSources() {
        return dataSources;
    }

    @Override
    public List<SnapshotImpl> getSnapshots() {
        return snapshots;
    }

    @Override
    public Set<IssueAnswerImpl> getAnswers() {
        return answers;
    }

    @Override
    public Optional<IssueAnswerImpl> getAnswer(IssueId id) {
        return Optional.ofNullable(map.get(id));
    }

}