package cdc.issues.core;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Params;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.io.ProfileIoFeatures.Hint;
import cdc.issues.rules.Profile;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.Rule;

class ProfileIoTest {
    private static final Logger LOGGER = LogManager.getLogger(ProfileIoTest.class);
    private static final String DOMAIN = "Domain1";
    // private static final String PROFILE = "Profile1";
    private static final String RULE1 = "Rule1";
    private static final String RULE2 = "Rule2";
    private static final String RULE3 = "Rule3";

    void check(ProfileIoFeatures features,
               Profile profile,
               String ext) throws IOException {
        final File file1 = new File("target", "profile-1." + ext);
        final File file1c = new File("target", "profile-config-1." + ext);
        final File file2 = new File("target", "profile-2." + ext);
        final File file2c = new File("target", "profile-config-2." + ext);

        LOGGER.info("save {}", file1);
        ProfileIo.save(features, profile, file1);
        LOGGER.info("saved {}", file1);

        LOGGER.info("save {}", file1c);
        ProfileIo.save(features, profile.getProfileConfig(), file1c);
        LOGGER.info("saved {}", file1c);

        LOGGER.info("load {}", file1);
        final Profile profile2 = ProfileIo.loadProfile(features, file1);
        LOGGER.info("loaded {}", file1);

        LOGGER.info("load {}", file1c);
        final ProfileConfig profileConfig2 = ProfileIo.loadProfileConfig(features, file1c);
        LOGGER.info("loaded {}", file1c);

        if (profile2 != null) {
            LOGGER.info("save {}", file2);
            ProfileIo.save(features, profile2, file2);
            LOGGER.info("saved {}", file2);
        }

        if (profileConfig2 != null) {
            LOGGER.info("save {}", file2c);
            ProfileIo.save(features, profileConfig2, file2c);
            LOGGER.info("saved {}", file2c);
        }
    }

    static Profile buildProfile() {
        final ProfileImpl.Builder profile =
                ProfileImpl.builder()
                           .name("Test Profile")
                           .description("Profile description")
                           .metas(Metas.builder()
                                       .meta("meta1", "value1")
                                       .build())
                           .metas(Metas.builder()
                                       .meta("key1", "value1")
                                       .meta("key2", null)
                                       .meta("key3", "value2")
                                       .build())
                           .labels(Labels.of("Label1", "Label2"));

        final Rule rule1 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE1)
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description of Rule 1")
                               .metas(Metas.builder().meta("meta1", "value1").build())
                               .labels(Labels.of("Label1"))
                               .build();
        final Rule rule2 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE2)
                               .severity(IssueSeverity.CRITICAL)
                               .description("Description of Rule 2")
                               .metas(Metas.builder().meta("meta1", "value1").meta("meta2", null).build())
                               .labels(Labels.of("Label2"))
                               .build();
        final Rule rule3 = Rule.builder()
                               .domain(DOMAIN)
                               .name(RULE3)
                               .title("Rule 3 title")
                               .severity(IssueSeverity.CRITICAL)
                               .description("Description of Rule 3")
                               .metas(Metas.builder().meta("meta1", "value1").build())
                               .labels(Labels.of("Label1", "Label2"))
                               .build();
        profile.rule(rule1)
               .enabled(rule1, false);

        profile.rule(rule2)
               .params(rule2,
                       Params.builder()
                             .param("param1", "value1")
                             .param("param2", null)
                             .param("param3", "value2")
                             .build())
               .customizedSeverity(rule2, IssueSeverity.MAJOR);

        profile.rule(rule3)
               .params(rule3,
                       Params.builder()
                             .param("param1", "value1\nline2")
                             .build());

        return profile.build();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "xml",
            "xlsx",
            "csv",
            "md",
            "html",
            "json"
    })
    void test(String ext) throws IOException {
        final Profile profile = buildProfile();
        final ProfileIoFeatures features =
                ProfileIoFeatures.builder()
                                 .hint(Hint.PRETTY_PRINT)
                                 .hint(Hint.SHOW_RULE_ENABLING)
                                 .hint(Hint.SHOW_RULE_SEVERITY)
                                 .build();
        check(features, profile, ext);
    }
}