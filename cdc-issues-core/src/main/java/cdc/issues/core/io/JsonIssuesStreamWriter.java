package cdc.issues.core.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;

import cdc.issues.Issue;
import cdc.issues.answers.IssueAnswer;
import cdc.issues.answers.IssueComment;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesStreamWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.SnapshotData;
import cdc.issues.locations.Location;
import cdc.issues.rules.Profile;
import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link IssuesStreamWriter} dedicated to JSON.
 *
 * @author Damien Carbonne
 */
public class JsonIssuesStreamWriter extends IssuesIo implements IssuesStreamWriter {
    private final File file;
    private final OutputStream out;
    private final OutSettings settings;
    private final JsonGenerator generator;
    private StreamStatus status = StreamStatus.INIT;
    private boolean hasInfos = false;

    private RuntimeException unexpectedStatus(String message) {
        throw new IllegalStateException("Unexpected status: " + status + ": " + message);
    }

    private JsonIssuesStreamWriter(File file,
                                   OutputStream out,
                                   IssuesFormat format,
                                   OutSettings settings,
                                   IssuesIoFactoryFeatures features)
            throws IOException {
        super(features);
        this.file = file;
        this.out = out;
        this.settings = settings;
        this.generator = createGenerator();
    }

    public JsonIssuesStreamWriter(File file,
                                  OutSettings settings,
                                  IssuesIoFactoryFeatures features)
            throws IOException {
        this(file, null, null, settings, features);
    }

    public JsonIssuesStreamWriter(OutputStream out,
                                  IssuesFormat format,
                                  OutSettings settings,
                                  IssuesIoFactoryFeatures features)
            throws IOException {
        this(null, out, format, settings, features);
    }

    private JsonGenerator createGenerator() throws IOException {
        final Map<String, Object> config = new HashMap<>();
        if (features.isEnabled(IssuesIoFactoryFeatures.Hint.PRETTY_PRINT)) {
            config.put(JsonGenerator.PRETTY_PRINTING, Boolean.TRUE);
        }
        final JsonGeneratorFactory factory = Json.createGeneratorFactory(config);
        if (file == null) {
            return factory.createGenerator(new BufferedWriter(new OutputStreamWriter(out)));
        } else {
            return factory.createGenerator(new BufferedWriter(new FileWriter(file)));
        }
    }

    private static String validate(String s) {
        return s == null ? "" : s;
    }

    private void saveProjectAndProfile(SnapshotData snapshot) {
        final Profile profile = snapshot.getProfile().orElse(null);

        generator.writeStartObject(JsonIo.PROJECT);
        generator.write(JsonIo.NAME, validate(snapshot.getProjectName()));
        generator.write(JsonIo.DESCRIPTION, validate(snapshot.getProjectDescription()));
        JsonIo.writeMetas(generator, snapshot.getProjectMetas());
        JsonIo.writeLabels(generator, snapshot.getProjectLabels());
        JsonIo.writeProfile(generator, profile);
        generator.writeEnd(); // PROJECT
    }

    @Override
    public void startDocument() throws IOException {
        if (status != StreamStatus.INIT) {
            throw unexpectedStatus("calling startDocument");
        }
        status = StreamStatus.DOCUMENT;
    }

    @Override
    public void add(SnapshotData snapshot) throws IOException {
        if (status != StreamStatus.DOCUMENT) {
            throw unexpectedStatus("calling add'Infos");
        }
        status = StreamStatus.INFOS;
        generator.writeStartObject(); // DOCUMENT
        saveProjectAndProfile(snapshot);

        generator.writeStartObject(JsonIo.SNAPSHOT);
        generator.write(JsonIo.NAME, validate(snapshot.getSnapshotName()));
        generator.write(JsonIo.DESCRIPTION, validate(snapshot.getSnapshotDescription()));
        JsonIo.writeMetas(generator, snapshot.getSnapshotMetas());
        JsonIo.writeLabels(generator, snapshot.getSnapshotLabels());
        generator.write(JsonIo.TIMESTAMP, toString(snapshot.getSnapshotTimestamp()));
        generator.write(JsonIo.ISSUES_HASH, snapshot.getIssuesHash());
        generator.writeStartArray(JsonIo.ISSUES);

        status = StreamStatus.SNAPSHOT;
        hasInfos = true;
    }

    @Override
    public void add(Issue issue,
                    IssueAnswer answer) throws IOException {
        if (status == StreamStatus.DOCUMENT) {
            // No info section
            // The document only contains issues
            generator.writeStartArray();
            status = StreamStatus.SNAPSHOT;
        } else if (status == StreamStatus.SNAPSHOT) {
            // Ignore
        } else {
            throw unexpectedStatus("calling add'Issue");
        }

        generator.writeStartObject();
        generator.write(JsonIo.TIMESTAMP, toString(issue.getTimestamp()));
        if (!settings.isEnabled(OutSettings.Hint.NO_ID_HASH_COL)) {
            generator.write(JsonIo.ID_HASH, issue.getIdHash());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_DOMAIN_COL) && !StringUtils.isNullOrEmpty(issue.getDomain())) {
            generator.write(JsonIo.DOMAIN, issue.getDomain());
        }
        generator.write(JsonIo.NAME, issue.getName());
        if (!StringUtils.isNullOrEmpty(issue.getTitle())) {
            generator.write(JsonIo.TITLE, issue.getTitle());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_PROJECT_COL) && !StringUtils.isNullOrEmpty(issue.getProject())) {
            generator.write(JsonIo.PROJECT, issue.getProject());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_SNAPSHOT_COL) && !StringUtils.isNullOrEmpty(issue.getSnapshot())) {
            generator.write(JsonIo.SNAPSHOT, issue.getSnapshot());
        }
        generator.write(JsonIo.SEVERITY, issue.getSeverity().name());
        generator.write(JsonIo.DESCRIPTION, validate(issue.getDescription()));

        if (!settings.isEnabled(OutSettings.Hint.NO_PARAMS_COL)) {
            JsonIo.writeParams(generator, issue.getParams());
        }

        generator.writeKey(JsonIo.LOCATIONS);
        generator.writeStartArray();
        for (final Location location : issue.getLocations()) {
            generator.writeStartObject();
            generator.write(JsonIo.TAG, location.getTag());
            generator.write(JsonIo.PATH, location.getPath());
            if (location.hasAnchor()) {
                generator.write(JsonIo.ANCHOR, location.getAnchor());
            }
            generator.writeEnd();
        }
        generator.writeEnd();

        if (!settings.isEnabled(OutSettings.Hint.NO_METAS_COL)) {
            JsonIo.writeMetas(generator, issue.getMetas());
        }
        if (!settings.isEnabled(OutSettings.Hint.NO_LABELS_COL)) {
            JsonIo.writeLabels(generator, issue.getLabels());
        }

        if (answer != null && !settings.isEnabled(OutSettings.Hint.NO_ANSWERS)) {
            generator.writeStartObject(JsonIo.ANSWER);
            if (!StringUtils.isNullOrEmpty(answer.getAuthor())) {
                generator.write(JsonIo.AUTHOR, answer.getAuthor());
            }
            if (answer.getCreationDate() != null) {
                generator.write(JsonIo.CREATED, toString(answer.getCreationDate()));
            }
            if (answer.getModificationDate() != null) {
                generator.write(JsonIo.MODIFIED, toString(answer.getModificationDate()));
            }
            if (answer.getStatus() != null) {
                generator.write(JsonIo.STATUS, answer.getStatus().name());
            }
            if (answer.getResolution() != null) {
                generator.write(JsonIo.RESOLUTION, answer.getResolution().name());
            }
            if (!StringUtils.isNullOrEmpty(answer.getAssignee())) {
                generator.write(JsonIo.ASSIGNEE, answer.getAssignee());
            }
            if (answer.getNewSeverity() != null) {
                generator.write(JsonIo.NEW_SEVERITY, answer.getNewSeverity().name());
            }
            final List<? extends IssueComment> comments = answer.getComments();
            if (comments != null && !comments.isEmpty()) {
                generator.writeStartArray(JsonIo.COMMENTS);
                for (final IssueComment comment : comments) {
                    generator.writeStartObject();
                    if (!StringUtils.isNullOrEmpty(comment.getAuthor())) {
                        generator.write(JsonIo.AUTHOR, comment.getAuthor());
                    }
                    if (comment.getDate() != null) {
                        generator.write(JsonIo.DATE, toString(comment.getDate()));
                    }
                    generator.write(JsonIo.TEXT, comment.getText());
                    generator.writeEnd();
                }
                generator.writeEnd();
            }
            if (!answer.getMetas().isEmpty()) {
                JsonIo.writeMetas(generator, answer.getMetas());
            }
            if (!answer.getLabels().isEmpty()) {
                JsonIo.writeLabels(generator, answer.getLabels());
            }

            generator.writeEnd();
        }

        generator.writeEnd();
    }

    @Override
    public void endDocument() throws IOException {
        if (status == StreamStatus.DOCUMENT) {
            // startDocument was called, and nothing after that
            generator.writeStartArray(); // ISSUES
            generator.writeEnd(); // ISSUES
            status = StreamStatus.END;
        } else if (status == StreamStatus.SNAPSHOT) {
            if (hasInfos) {
                generator.writeEnd(); // ISSUES
                generator.writeEnd(); // SNAPSHOT
                generator.writeEnd(); // DOCUMENT
            } else {
                generator.writeEnd(); // ISSUES
            }
            status = StreamStatus.END;
        } else {
            throw unexpectedStatus("calling endDocument");
        }
    }

    @Override
    public void flush() throws IOException {
        generator.flush();
    }

    @Override
    public void close() throws IOException {
        generator.close();
    }
}