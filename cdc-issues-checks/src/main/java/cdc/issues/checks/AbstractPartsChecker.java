package cdc.issues.checks;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cdc.issues.locations.LocatedObject;
import cdc.issues.locations.Location;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Specialization of {@link AbstractChecker} used to check parts of an object.
 * <p>
 * This checker does nothing by itself, it delegates checks to its part checkers.
 *
 * @author Damien Carbonne
 *
 * @param <O> The checked object type.
 * @param <P> The checked part type.
 * @param <C> The checker type.
 */
public abstract class AbstractPartsChecker<O,
                                           P,
                                           C extends AbstractPartsChecker<O, P, C>>
        extends AbstractChecker<O> {
    /** The part class. */
    private final Class<P> partClass;
    /** The part checkers. */
    private final List<AbstractChecker<? super P>> partCheckers = new ArrayList<>();

    /**
     * Creates an empty part checker.
     *
     * @param objectClass The object class.
     * @param partClass The part class.
     */
    protected AbstractPartsChecker(Class<O> objectClass,
                                   Class<P> partClass) {
        super(objectClass);
        this.partClass = Checks.isNotNull(partClass, "partClass");
    }

    /**
     * Creates part checker with some part checkers.
     *
     * @param objectClass The object class.
     * @param partClass The part class.
     * @param partCheckers The part checkers.
     */
    @SafeVarargs
    protected AbstractPartsChecker(Class<O> objectClass,
                                   Class<P> partClass,
                                   AbstractChecker<? super P>... partCheckers) {
        this(objectClass,
             partClass);
        for (final AbstractChecker<? super P> partChecker : partCheckers) {
            add(partChecker);
        }
    }

    @Override
    protected final List<AbstractChecker<?>> getNext() {
        return Introspection.uncheckedCast(partCheckers);
    }

    @Override
    protected final List<AbstractChecker<?>> getNext(SnapshotManager manager) {
        return getNext();
    }

    public final List<AbstractChecker<? super P>> getPartCheckers() {
        return partCheckers;
    }

    /**
     * @return An instance of this checker, with the right (specialized) type.
     */
    protected final C self() {
        @SuppressWarnings("unchecked")
        final C tmp = (C) this;
        return tmp;
    }

    /**
     * Add a new part checker.
     *
     * @param partChecker A part checker.
     * @return This checker.
     */
    public C add(AbstractChecker<? super P> partChecker) {
        partCheckers.add(partChecker);
        return self();
    }

    /**
     * Method that must return the parts of an object.
     *
     * @param object The object.
     * @return The object parts.
     */
    protected abstract List<LocatedObject<? extends P>> getParts(O object);

    @Override
    public final boolean isEnabled(SnapshotManager manager) {
        return true;
    }

    @Override
    public final boolean isCorrectlyConfigured(SnapshotManager manager) {
        return true;
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   O object,
                                   Location location) {
        for (final LocatedObject<? extends P> part : getParts(object)) {
            for (final AbstractChecker<? super P> partChecker : partCheckers) {
                checkAndStat(partChecker,
                             context.push(part),
                             part.getObject(),
                             part.getLocation());
            }
        }
        return CheckResult.IGNORE;
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("PARTS<" + getObjectClass().getSimpleName() + ", " + partClass.getSimpleName() + ">");
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
        for (final AbstractChecker<? super P> child : partCheckers) {
            child.print(out, manager, level + 1);
        }
    }

    @Override
    public String toString() {
        return "PARTS<" + getObjectClass().getSimpleName() + ", " + partClass.getSimpleName() + ">";
    }
}