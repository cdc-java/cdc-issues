package cdc.issues.io;

import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;

import cdc.issues.Issue;
import cdc.issues.answers.IssueAnswer;

/**
 * Interface implemented by classes that can be used to save issues at a low level.
 * <p>
 * One must use {@link IssuesIoFactory#createIssuesStreamWriter(File, OutSettings)}
 * or {@link IssuesIoFactory#createIssuesStreamWriter(OutputStream, IssuesFormat, OutSettings)}
 * to create an instance of {@link IssuesStreamWriter}.
 * <p>
 * <b>WARNING:</b> all issues should be generated together.
 * <p>
 * A typical usage would be:
 * <pre><code>
 * IssuesStreamWriter w = ...
 * w.startDocument()
 * w.add(snapshot)
 * foreach issue
 *    w.add(issue)
 * w.endDocument()
 * </code></pre>
 * <p>
 * <b>Note:</b> with formats that have limitations (typically workbook sheets have a max number of rows),
 * solutions should be provided to bypass them (using several sheets for example).
 *
 * @author Damien Carbonne
 * @see IssuesFormat
 */
public interface IssuesStreamWriter extends Flushable, Closeable {
    /**
     * Must be invoked first.
     *
     * @throws IOException When an IO error occurs.
     */
    public void startDocument() throws IOException;

    /**
     * Invoked to generate snapshot data.
     *
     * @param snapshot The snapshot data.
     * @throws IOException When an IO error occurs.
     */
    public void add(SnapshotData snapshot) throws IOException;

    /**
     * Invoked to generate issue and answer data.
     *
     * @param issue The issue.
     * @param answer The associated answer.
     * @throws IOException When an IO error occurs.
     */
    public void add(Issue issue,
                    IssueAnswer answer) throws IOException;

    /**
     * Invoked to generate issue data.
     *
     * @param issue The issue.
     * @throws IOException When an IO error occurs.
     */
    public default void add(Issue issue) throws IOException {
        add(issue, null);
    }

    /**
     * Must be invoked last.
     *
     * @throws IOException When an IO error occurs.
     */
    public void endDocument() throws IOException;
}