package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class ParamTest {
    @Test
    void testIsValidName() {
        assertFalse(Param.isValidName(null));
        assertFalse(Param.isValidName(""));
        assertFalse(Param.isValidName(" "));
        assertTrue(Param.isValidName("A"));
        assertTrue(Param.isValidName("_"));
        assertTrue(Param.isValidName("A B"));
        assertTrue(Param.isValidName("A   B"));
        assertTrue(Param.isValidName("A   B   "));
        assertTrue(Param.isValidName("A-B"));
        assertTrue(Param.isValidName("A/B"));
    }
}