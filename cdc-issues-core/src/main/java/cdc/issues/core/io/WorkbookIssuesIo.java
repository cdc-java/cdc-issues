package cdc.issues.core.io;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cdc.issues.answers.IssueComment;
import cdc.issues.impl.IssueCommentImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.office.tables.Header;
import cdc.util.strings.KeyValueSplitter;

class WorkbookIssuesIo extends IssuesIo {
    private static final String SEP = ":";
    private static final String AUTHOR_SEP = WorkbooksIo.AUTHOR + SEP;
    private static final String DATE_SEP = WorkbooksIo.DATE + SEP;
    private static final String TEXT_SEP = WorkbooksIo.TEXT + SEP;

    private static final KeyValueSplitter SPLITTER = new KeyValueSplitter(AUTHOR_SEP,
                                                                          DATE_SEP,
                                                                          TEXT_SEP);

    /**
     * Issues part of header.
     * <p>
     * Only contains names, no patterns
     */
    protected static final Header ISSUES_HEADER =
            Header.builder()
                  .names(WorkbooksIo.TIMESTAMP,
                         // WorkbooksIo.HASH,
                         WorkbooksIo.DOMAIN,
                         WorkbooksIo.NAME,
                         // WorkbooksIo.TITLE,
                         WorkbooksIo.PARAMS,
                         WorkbooksIo.PROJECT,
                         WorkbooksIo.SNAPSHOT,
                         WorkbooksIo.SEVERITY,
                         WorkbooksIo.DESCRIPTION,
                         WorkbooksIo.LOCATIONS,
                         WorkbooksIo.METAS)
                  .build();

    protected static final Header ISSUES_ANSWERS_HEADER =
            Header.builder()
                  .cells(ISSUES_HEADER)
                  .names(WorkbooksIo.AUTHOR,
                         WorkbooksIo.CREATED,
                         WorkbooksIo.MODIFIED,
                         WorkbooksIo.STATUS,
                         WorkbooksIo.RESOLUTION,
                         WorkbooksIo.ASSIGNEE,
                         WorkbooksIo.NEW_SEVERITY,
                         WorkbooksIo.COMMENTS)
                  .build();

    protected WorkbookIssuesIo(IssuesIoFactoryFeatures features) {
        super(features);
    }

    protected String format(IssueComment comment) {
        return AUTHOR_SEP + comment.getAuthor()
                + " " + DATE_SEP + toString(comment.getDate())
                + " " + TEXT_SEP + comment.getText();
    }

    protected String format(List<? extends IssueComment> comments) {
        if (comments == null || comments.isEmpty()) {
            return null;
        } else {
            return comments.stream()
                           .map(this::format)
                           .collect(Collectors.joining("\n"));
        }
    }

    protected List<IssueCommentImpl> parseComments(String s) {
        final List<IssueCommentImpl> comments = new ArrayList<>();
        if (s != null) {
            final IssueCommentImpl.Builder comment = IssueCommentImpl.builder();
            SPLITTER.reset(s);
            while (SPLITTER.hasMore()) {
                final String key = SPLITTER.getKey();
                if (AUTHOR_SEP.equals(key)) {
                    comment.author(SPLITTER.getValue().trim());
                } else if (DATE_SEP.equals(key)) {
                    comment.date(toInstant(SPLITTER.getValue().trim()));
                } else {
                    comment.text(SPLITTER.getValue().trim());
                    comments.add(comment.build());
                    comment.author(null);
                    comment.date(null);
                    comment.text(null);
                }
                SPLITTER.next();
            }
        }
        return comments;
    }
}