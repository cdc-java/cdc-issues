package cdc.issues.core.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.NameValueMap;
import cdc.issues.Params;
import cdc.issues.answers.IssueResolution;
import cdc.issues.answers.IssueStatus;
import cdc.issues.answers.IssuesAndAnswers;
import cdc.issues.impl.IssueAnswerImpl;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesFormat;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesReader;
import cdc.issues.locations.Location;
import cdc.issues.locations.Locations;
import cdc.office.ss.SheetParser;
import cdc.office.ss.SheetParserFactory;
import cdc.office.ss.WorkbookKind;
import cdc.office.tables.Header;
import cdc.office.tables.HeaderMapper;
import cdc.office.tables.Row;
import cdc.office.tables.RowLocation;
import cdc.office.tables.TablesHandler;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.function.Evaluation;
import cdc.util.strings.StringUtils;
import cdc.util.time.Chronometer;

/**
 * Implementation of {@link IssuesReader} dedicated to Workbooks.
 *
 * @author Damien Carbonne
 */
public class WorkbookIssuesReader extends WorkbookIssuesIo implements IssuesReader {
    private static final Pattern LABELS_SEPARATOR_PATTERN = Pattern.compile("[ \n]");

    private final File file;
    private final InputStream in;
    private final WorkbookKind kind;

    public WorkbookIssuesReader(File file,
                                IssuesIoFactoryFeatures features) {
        super(features);
        this.file = file;
        this.in = null;
        this.kind = null;
    }

    public WorkbookIssuesReader(InputStream in,
                                IssuesFormat format,
                                IssuesIoFactoryFeatures features) {
        super(features);
        this.file = null;
        this.in = in;
        switch (format) {
        case CSV:
            this.kind = WorkbookKind.CSV;
            break;
        case ODS:
            this.kind = WorkbookKind.ODS;
            break;
        case XLS:
            this.kind = WorkbookKind.XLS;
            break;
        case XLSM:
            this.kind = WorkbookKind.XLSM;
            break;
        case XLSX:
            this.kind = WorkbookKind.XLSX;
            break;
        default:
            this.kind = null;
            break;
        }
    }

    @Override
    public IssuesAndAnswers load(Settings settings,
                                 ProgressController controller) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final String systemId = file == null ? "???" : file.getPath();
        traceLoad(systemId);
        final SheetParserFactory factory = new SheetParserFactory();
        factory.setSeparator(features.getWorkbookWriterFeatures().getSeparator());

        final SheetParser parser = factory.create(file);
        final Handler handler = new Handler(settings, controller);
        if (file == null) {
            parser.parse(in, systemId, kind, features.getPassword(), 1, handler);
        } else {
            parser.parse(file, features.getPassword(), 1, handler);
        }
        chrono.suspend();
        traceLoaded(systemId, chrono);
        return handler.buffer;
    }

    private class Handler implements TablesHandler {
        final IssuesAndAnswersImpl buffer = new IssuesAndAnswersImpl();
        private final Settings settings;
        private final ProgressSupplier progress;
        private HeaderMapper headerMapper = null;
        private boolean canLoadAnswers = true;

        public Handler(Settings settings,
                       ProgressController controller) {
            this.settings = settings;
            this.progress = new ProgressSupplier(controller);
        }

        private int getIndex(String key) {
            return headerMapper.getActualHeader().getMatchingIndex(key);
        }

        @Override
        public void processBeginTable(String name,
                                      int numberOfRows) {
            progress.reset(numberOfRows, "load sheet " + name);
        }

        @Override
        public void processEndTables(String systemId) {
            progress.close();
        }

        @Override
        public Evaluation processHeader(Row header,
                                        RowLocation location) {
            final Header actual = Header.builder().names(header).build();
            progress.incrementValue();
            if (actual.contains(ISSUES_HEADER)) {
                // We map all columns, even answers when they should not be loaded.
                headerMapper = HeaderMapper.builder()
                                           .mandatory(ISSUES_ANSWERS_HEADER)
                                           .actual(actual)
                                           .build();
                if (!actual.contains(WorkbooksIo.STATUS)) {
                    canLoadAnswers = false;
                }
                return Evaluation.CONTINUE;
            } else {
                return Evaluation.PRUNE;
            }
        }

        @Override
        public Evaluation processData(Row data,
                                      RowLocation location) {
            final String stimestamp = data.getValue(getIndex(WorkbooksIo.TIMESTAMP));
            final String domain = data.getValue(getIndex(WorkbooksIo.DOMAIN));
            final String name = data.getValue(getIndex(WorkbooksIo.NAME));
            final String title = StringUtils.nullify(data.getValue(getIndex(WorkbooksIo.TITLE), null));
            final String sparams = data.getValue(getIndex(WorkbooksIo.PARAMS));
            final String project = StringUtils.nullify(data.getValue(getIndex(WorkbooksIo.PROJECT)));
            final String snapshot = StringUtils.nullify(data.getValue(getIndex(WorkbooksIo.SNAPSHOT)));
            final IssueSeverity severity = data.getValueAsEnum(getIndex(WorkbooksIo.SEVERITY), IssueSeverity.class, null);
            final String description = data.getValue(getIndex(WorkbooksIo.DESCRIPTION));
            final String slocations = data.getValue(getIndex(WorkbooksIo.LOCATIONS));
            final String smetas = data.getValue(getIndex(WorkbooksIo.METAS));
            final String slabels = data.getValue(getIndex(WorkbooksIo.LABELS));

            final Issue issue = Issue.builder()
                                     .timestamp(toInstant(stimestamp))
                                     .domain(domain)
                                     .name(name)
                                     .title(title)
                                     .params(toParams(sparams))
                                     .project(project)
                                     .snapshot(snapshot)
                                     .severity(severity)
                                     .description(description)
                                     .locations(toLocations(slocations))
                                     .metas(toMetas(smetas))
                                     .labels(toLabels(slabels))
                                     .build();
            buffer.addIssue(issue);

            if (!settings.getHints().contains(Settings.Hint.NO_ANSWERS) && canLoadAnswers) {
                final String author = data.getValue(getIndex(WorkbooksIo.AUTHOR), null);
                final String screated = data.getValue(getIndex(WorkbooksIo.CREATED), null);
                final String smodified = data.getValue(getIndex(WorkbooksIo.MODIFIED), null);
                final IssueStatus status = data.getValueAsEnum(getIndex(WorkbooksIo.STATUS), IssueStatus.class, null);
                final IssueResolution resolution =
                        data.getValueAsEnum(getIndex(WorkbooksIo.RESOLUTION), IssueResolution.class, null);
                final String assignee = data.getValue(getIndex(WorkbooksIo.ASSIGNEE), null);
                final IssueSeverity newSeverity =
                        data.getValueAsEnum(getIndex(WorkbooksIo.NEW_SEVERITY), IssueSeverity.class, null);
                final String scomments = data.getValue(getIndex(WorkbooksIo.COMMENTS), null);
                final String usersmetas = data.getValue(getIndex(WorkbooksIo.USER_METAS));
                final String userslabels = data.getValue(getIndex(WorkbooksIo.USER_LABELS));

                final IssueAnswerImpl.Builder answer = IssueAnswerImpl.builder();
                answer.issueId(issue.getId())
                      .author(author)
                      .creationDate(toInstant(screated))
                      .modificationDate(toInstant(smodified))
                      .status(status)
                      .resolution(resolution)
                      .assignee(assignee)
                      .newSeverity(newSeverity)
                      .comments(parseComments(scomments))
                      .metas(toMetas(usersmetas))
                      .labels(toLabels(userslabels));
                buffer.addAnswer(answer.build());
            }

            progress.incrementValue();
            return Evaluation.CONTINUE;
        }

        private static Metas toMetas(String s) {
            return toNameValueMap(s, Metas::builder, Metas.NO_METAS);
        }

        private static Params toParams(String s) {
            return toNameValueMap(s, Params::builder, Params.NO_PARAMS);
        }

        private static <M extends NameValueMap, B extends NameValueMap.Builder<M, B>>
                M toNameValueMap(String s,
                                 Supplier<B> supplier,
                                 M def) {
            if (s == null || s.isEmpty()) {
                return def;
            } else {
                final B builder = supplier.get();
                for (final String line : s.split("\n")) {
                    final int sepPos = line.indexOf(':');
                    final String name = line.substring(0, sepPos);
                    final String value = StringUtils.nullify(line.substring(sepPos + 1));
                    builder.entry(name, value);
                }
                return builder.build();
            }
        }

        private Labels toLabels(String s) {
            if (s == null || s.isEmpty()) {
                return Labels.NO_LABELS;
            } else {
                final String[] words = LABELS_SEPARATOR_PATTERN.split(s);
                return Labels.of(words);
            }
        }

        private List<Location> toLocations(String s) {
            final List<Location> locations = new ArrayList<>();
            for (final String line : s.split("\n")) {
                locations.add(Locations.build(line));
            }
            return locations;
        }
    }
}