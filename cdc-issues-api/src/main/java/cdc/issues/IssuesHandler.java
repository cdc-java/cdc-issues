package cdc.issues;

/**
 * Interface implemented by classes that can handle (collect, process, ...) issues.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface IssuesHandler {
    public static final IssuesHandler VOID = VoidIssuesHandler.INSTANCE;

    /**
     * Notifies an issue.
     *
     * @param issue The issue.
     */
    public void issue(Issue issue);

    public default void issues(Iterable<Issue> issues) {
        for (final Issue issue : issues) {
            issue(issue);
        }
    }

    public default void issues(Issue... issues) {
        for (final Issue issue : issues) {
            issue(issue);
        }
    }
}