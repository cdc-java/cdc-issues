package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.issues.io.IssuesFormat;
import cdc.issues.rules.Rule;

class RuleTest {
    @Test
    void test() {
        final Rule rule1 = Rule.builder()
                               .domain("Domain")
                               .name("Name")
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description")
                               .build();

        final Rule rule2 = Rule.builder()
                               .domain("Domain")
                               .name("Name")
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description")
                               .build();
        final Rule rule3 = Rule.builder()
                               .domain("Domain")
                               .name(IssuesFormat.CSV)
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description")
                               .build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         Rule.builder()
                             .domain("Domain")
                             .name("Name")
                             .description("Description")
                             .build();
                     });

        assertEquals(rule1, rule1);
        assertEquals(rule1, rule2);
        assertNotEquals(rule1, rule3);
        assertNotEquals(rule1, null);
        assertNotEquals(rule1, "Hello");
        assertEquals(rule1.hashCode(), rule2.hashCode());
        assertEquals(rule1.getId(), rule2.getId());

        assertEquals("Description", rule1.getDescription());
        assertEquals("Domain", rule1.getDomain());
        assertEquals("Name", rule1.getName());
        assertEquals(IssueSeverity.BLOCKER, rule1.getSeverity());
        assertEquals(IssuesFormat.CSV, rule3.getName(IssuesFormat.class));
    }

    @Test
    void testComparators() {
        final Rule rule1 = Rule.builder()
                               .domain("Domain")
                               .name("R1")
                               .title("BBB")
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description")
                               .build();
        final Rule rule2 = Rule.builder()
                               .domain("Domain")
                               .name("R2")
                               .title("AAA")
                               .severity(IssueSeverity.BLOCKER)
                               .description("Description")
                               .build();

        assertSame(0, Rule.ID_COMPARATOR.compare(rule1, rule1));
        assertTrue(Rule.ID_COMPARATOR.compare(rule1, rule2) < 0);
        assertTrue(Rule.ID_COMPARATOR.compare(rule2, rule1) > 0);

        assertSame(0, Rule.DOMAIN_TITLE_NAME_COMPARATOR.compare(rule1, rule1));
        assertTrue(Rule.DOMAIN_TITLE_NAME_COMPARATOR.compare(rule1, rule2) > 0);
        assertTrue(Rule.DOMAIN_TITLE_NAME_COMPARATOR.compare(rule2, rule1) < 0);
    }
}