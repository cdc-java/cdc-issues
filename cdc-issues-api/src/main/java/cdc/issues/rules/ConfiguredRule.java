package cdc.issues.rules;

import java.util.Objects;
import java.util.Optional;

import cdc.issues.IssueSeverity;
import cdc.issues.Params;
import cdc.util.lang.Checks;

/**
 * Association of a Rule and a RuleConfig.
 *
 * @author Damien Carbonne
 */
public final class ConfiguredRule {
    /** The rule. */
    private final Rule rule;
    private final RuleConfig config;

    private ConfiguredRule(Builder builder) {
        this.rule = builder.rule;
        this.config = builder.config.build();
    }

    /**
     * @return The rule.
     */
    public Rule getRule() {
        return rule;
    }

    /**
     * @return The rule config.
     */
    public RuleConfig getConfig() {
        return config;
    }

    /**
     * @return {@code true} if the rule is enabled.
     */
    public boolean isEnabled() {
        return config.isEnabled();
    }

    /**
     * @return The optional config severity.
     *         Used to override the rule severity.
     */
    public Optional<IssueSeverity> getCustomizedSeverity() {
        return config.getCustomizedSeverity();
    }

    /**
     * @return The effective severity that should be used for the rule.
     *         It is the config severity, when defined, the default rule severity otherwise.
     */
    public IssueSeverity getEffectiveSeverity() {
        return getCustomizedSeverity().orElse(rule.getSeverity());
    }

    /**
     * @return the parameters of the rule.
     */
    public Params getParams() {
        return config.getParams();
    }

    @Override
    public int hashCode() {
        return Objects.hash(rule,
                            config);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ConfiguredRule)) {
            return false;
        }
        final ConfiguredRule other = (ConfiguredRule) object;
        return Objects.equals(rule, other.rule)
                && Objects.equals(config, other.config);
    }

    @Override
    public String toString() {
        return "[" + getRule().getName() + ", " + getConfig() + "]";
    }

    public static Builder builder(Rule rule) {
        return new Builder(rule);
    }

    public static class Builder {
        private final Rule rule;
        private final RuleConfig.Builder config = RuleConfig.builder();

        Builder(Rule rule) {
            this.rule = Checks.isNotNull(rule, "rule");
        }

        public Builder config(RuleConfig config) {
            this.config.set(config);
            return this;
        }

        public Builder enabled(boolean enabled) {
            this.config.enabled(enabled);
            return this;
        }

        public Builder customizedSeverity(IssueSeverity customizedSeverity) {
            this.config.customizedSeverity(customizedSeverity);
            return this;
        }

        public Builder params(Params params) {
            this.config.params(params);
            return this;
        }

        public ConfiguredRule build() {
            return new ConfiguredRule(this);
        }
    }
}