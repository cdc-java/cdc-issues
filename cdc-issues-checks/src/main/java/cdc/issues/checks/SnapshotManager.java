package cdc.issues.checks;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import cdc.issues.IssueUtils;
import cdc.issues.IssuesCollector;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.io.SnapshotData;
import cdc.issues.locations.Location;
import cdc.issues.rules.Profile;
import cdc.util.lang.Checks;

/**
 * Class used to build a snapshot and associated data.
 * <p>
 * It can be extended to pass additional parameters to checkers.
 *
 * @author Damien Carbonne
 */
public class SnapshotManager implements SnapshotData {
    private final String projectName;
    private final String projectDescription;
    private final Metas projectMetas;
    private final Labels projectLabels;
    private final Profile profile;
    private final String snapshotName;
    private final String snapshotDescription;
    private final Metas snapshotMetas;
    private final Labels snapshotLabels;
    private final Instant snapshotTimestamp;

    /** The issues collector. */
    private final IssuesCollector issuesCollector;
    /** The stats collector. */
    private final Optional<CheckStats<Location>> stats;
    /** The registered checkers. */
    private final Map<String, AbstractChecker<?>> nameToChecker = new HashMap<>();
    private final Map<AbstractChecker<?>, String> checkerToName = new HashMap<>();

    protected SnapshotManager(Builder<?> builder) {
        this.projectName = builder.projectName;
        this.projectDescription = builder.projectDescription;
        this.projectMetas = Checks.isNotNull(builder.projectMetas, "projectMetas");
        this.projectLabels = Checks.isNotNull(builder.projectLabels, "projectLabels");
        this.profile = Checks.isNotNull(builder.profile, "profile");
        this.snapshotName = builder.snapshotName;
        this.snapshotDescription = builder.snapshotDescription;
        this.snapshotMetas = Checks.isNotNull(builder.snapshotMetas, "snapshotMetas");
        this.snapshotLabels = Checks.isNotNull(builder.snapshotLabels, "snapshotLabels");
        this.snapshotTimestamp = builder.snapshotTimestamp;
        this.issuesCollector = builder.issuesCollector == null ? new IssuesCollector() : builder.issuesCollector;
        this.stats = Optional.ofNullable(builder.stats);
    }

    @Override
    public final String getProjectName() {
        return projectName;
    }

    @Override
    public final String getProjectDescription() {
        return projectDescription;
    }

    @Override
    public final Metas getProjectMetas() {
        return projectMetas;
    }

    @Override
    public Labels getProjectLabels() {
        return projectLabels;
    }

    @Override
    public final Optional<Profile> getProfile() {
        return Optional.of(profile);
    }

    @Override
    public final String getSnapshotName() {
        return snapshotName;
    }

    @Override
    public final String getSnapshotDescription() {
        return snapshotDescription;
    }

    @Override
    public final Metas getSnapshotMetas() {
        return snapshotMetas;
    }

    @Override
    public Labels getSnapshotLabels() {
        return snapshotLabels;
    }

    @Override
    public final Instant getSnapshotTimestamp() {
        return snapshotTimestamp;
    }

    @Override
    public final int getNumberOfIssues() {
        return issuesCollector.getIssues().size();
    }

    @Override
    public final String getIssuesHash() {
        return IssueUtils.getHash(issuesCollector.getIssues());
    }

    /**
     * @return The {@link IssuesCollector} used to collect detected issues.
     */
    public final IssuesCollector getIssuesCollector() {
        return issuesCollector;
    }

    /**
     * @return The object used to collect statistics.
     */
    public final Optional<CheckStats<Location>> getStats() {
        return stats;
    }

    /**
     * @return {@code true} if this manager has stats.
     */
    public final boolean hasStats() {
        return getStats().isPresent();
    }

    /**
     * Registers a checker and associate to a name.
     *
     * @param <O> The checked object type.
     * @param name The name used to designate the registered checker.
     * @param checker The checker.
     */
    public final <O> void register(String name,
                                   AbstractChecker<O> checker) {
        Checks.isNotNull(name, "name");
        Checks.isNotNull(checker, "checker");
        Checks.doesNotContainKey(nameToChecker, name, "A checker is already registered as " + name);
        Checks.doesNotContainKey(checkerToName, checker, "This checker is already registered as " + checkerToName.get(checker));

        nameToChecker.put(name, checker);
        checkerToName.put(checker, name);
    }

    /**
     * Returns the checker that was registered with a name.
     *
     * @param <O> The checked object type.
     * @param objectClass The checked object class.
     * @param name The name of the checker to retrieve.
     * @return The corresponding checker.
     * @throws IllegalArgumentException When a checker has been registered as {@code name} but is not
     *             compliant with {@code objectClass}.
     */
    public final <O> Optional<AbstractChecker<O>> resolve(Class<O> objectClass,
                                                          String name) {
        final AbstractChecker<?> tmp = nameToChecker.get(name);
        if (tmp == null || tmp.getObjectClass().isAssignableFrom(objectClass)) {
            @SuppressWarnings("unchecked")
            final AbstractChecker<O> x = (AbstractChecker<O>) tmp;
            return Optional.ofNullable(x);
        } else {
            throw new IllegalArgumentException(tmp.getObjectClass().getSimpleName() + " is not compliant with "
                    + objectClass.getSimpleName());
        }
    }

    /**
     * @param checker The checker.
     * @return The name {@code checker} is registered as, or {@code null}.
     */
    public final String getRegistrationName(AbstractChecker<?> checker) {
        return checkerToName.get(checker);
    }

    /**
     * @param checker The checker.
     * @return {@code true} if {@code checker} has been registered.
     */
    public final boolean isRegistered(AbstractChecker<?> checker) {
        return checkerToName.containsKey(checker);
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    /**
     * Builder of SnapshotManager.
     *
     * @param <B> The builder type.
     */
    public static class Builder<B extends Builder<B>> {
        private String projectName;
        private String projectDescription;
        private Metas projectMetas = Metas.NO_METAS;
        private final Labels projectLabels = Labels.NO_LABELS;
        private Profile profile;
        private String snapshotName;
        private String snapshotDescription;
        private Metas snapshotMetas = Metas.NO_METAS;
        private final Labels snapshotLabels = Labels.NO_LABELS;
        private Instant snapshotTimestamp = Instant.now();
        private IssuesCollector issuesCollector;
        private CheckStats<Location> stats;

        protected Builder() {
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public final B projectName(String projectName) {
            this.projectName = projectName;
            return self();
        }

        public final B projectDescription(String projectDescription) {
            this.projectDescription = projectDescription;
            return self();
        }

        public final B projectMetas(Metas projectMetas) {
            this.projectMetas = projectMetas;
            return self();
        }

        public final B profile(Profile profile) {
            this.profile = profile;
            return self();
        }

        public final B snapshotName(String snapshotName) {
            this.snapshotName = snapshotName;
            return self();
        }

        public final B snapshotDescription(String snapshotDescription) {
            this.snapshotDescription = snapshotDescription;
            return self();
        }

        public final B snapshotMetas(Metas snapshotMetas) {
            this.snapshotMetas = snapshotMetas;
            return self();
        }

        public final B snapshotTimestamp(Instant snapshotTimestamp) {
            this.snapshotTimestamp = snapshotTimestamp;
            return self();
        }

        public final B issuesCollector(IssuesCollector issuesCollector) {
            this.issuesCollector = issuesCollector;
            return self();
        }

        public final B stats(boolean enable) {
            if (enable) {
                Checks.isNotNull(profile, "profile");
                this.stats = new CheckStats<Location>(profile);
            }
            return self();
        }

        public SnapshotManager build() {
            return new SnapshotManager(this);
        }
    }
}