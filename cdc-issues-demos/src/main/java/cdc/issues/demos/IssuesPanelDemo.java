package cdc.issues.demos;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.Labels;
import cdc.issues.Metas;
import cdc.issues.Params;
import cdc.issues.locations.DefaultLocation;
import cdc.issues.ui.swing.IssuesPanel;

final class IssuesPanelDemo extends JFrame {
    private static final long serialVersionUID = 1L;
    private final IssuesPanel wIssues = new IssuesPanel();

    private IssuesPanelDemo() {
        super();
        setTitle(getClass().getSimpleName());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(800, 400));

        getContentPane().add(wIssues);
        for (int i = 0; i < 100; i++) {
            wIssues.getModel()
                   .add(Issue.builder()
                             .domain("Domain 1")
                             .description("Description")
                             .name("Name " + (i % 5 + 1))
                             .params(i % 2 == 0 ? Params.builder()
                                                        .param("param1", "value1")
                                                        .param("param2", "value2")
                                                        .build()
                                     : Params.NO_PARAMS)
                             .project(i % 5 == 0 ? "Project" : null)
                             .snapshot(i % 5 == 0 ? "Snapshot" : null)
                             .locations(new DefaultLocation("path/to/data", "anchor1"),
                                        new DefaultLocation("path/to/data", "anchor2"))
                             .severity(IssueSeverity.values()[i % IssueSeverity.values().length])
                             .metas(i % 3 == 0 ? Metas.builder()
                                                      .meta("meta1", "value1")
                                                      .meta("meta2", "value2")
                                                      .build()
                                     : Metas.NO_METAS)
                             .labels(Labels.of("Label1", "Label2"))
                             .build());
        }
        pack();
    }

    public static void main(String... args) {
        SwingUtilities.invokeLater(() -> {
            final IssuesPanelDemo frame = new IssuesPanelDemo();
            frame.setVisible(true);
        });
    }
}