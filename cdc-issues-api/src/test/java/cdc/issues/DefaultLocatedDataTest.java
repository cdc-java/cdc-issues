package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.DefaultLocation;
import cdc.issues.locations.LocatedObject;
import cdc.issues.locations.Location;

class DefaultLocatedDataTest {
    @Test
    void test() {
        final Location location1 = new DefaultLocation("path1", "anchor");
        final Location location2 = new DefaultLocation("path2", "anchor");
        final LocatedObject<String> data0a = LocatedObject.of(null, location1);
        final LocatedObject<String> data0b = LocatedObject.of(null, location1);
        final LocatedObject<String> data1a = LocatedObject.of("Hello", location1);
        final LocatedObject<String> data1b = LocatedObject.of("Hello", location1);
        final LocatedObject<String> data2a = LocatedObject.of("Hello", location2);
        final LocatedObject<String> data2b = LocatedObject.of("Hello", location2);

        assertNotEquals(data0a, null);
        assertNotEquals(data0a, "Hello");
        assertEquals(data0a, data0a);
        assertEquals(data0a, data0b);
        assertEquals(data1a, data1b);
        assertEquals(data2a, data2b);
        assertNotEquals(data1a, data2b);
        assertNotEquals(data0a, data2b);

        assertEquals(null, data0a.getObject());
        assertEquals(location1, data0a.getLocation());
        assertEquals("Hello", data1a.getObject());

        assertEquals(data2a.hashCode(), data2b.hashCode());
        assertEquals(data2a.toString(), data2b.toString());
    }
}
