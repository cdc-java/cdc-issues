package cdc.issues;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class LabelsTest {
    @Test
    void testIsValidLabel() {
        assertFalse(Labels.isValidLabel(null));
        assertFalse(Labels.isValidLabel(""));
        assertFalse(Labels.isValidLabel(" "));
        assertFalse(Labels.isValidLabel("\n"));
        assertFalse(Labels.isValidLabel("\t"));
        assertFalse(Labels.isValidLabel("\b"));
        assertFalse(Labels.isValidLabel("\f"));
        assertFalse(Labels.isValidLabel("\r"));

        assertTrue(Labels.isValidLabel("_"));
        assertTrue(Labels.isValidLabel("/"));
        assertTrue(Labels.isValidLabel("-"));
        assertTrue(Labels.isValidLabel("*"));
        assertTrue(Labels.isValidLabel("$"));
        assertTrue(Labels.isValidLabel("()"));
        assertTrue(Labels.isValidLabel("{}[]=%µ"));

        assertTrue(Labels.isValidLabel("A"));
        assertTrue(Labels.isValidLabel("ä"));
        assertTrue(Labels.isValidLabel("Ä"));
        assertTrue(Labels.isValidLabel("a"));
        assertTrue(Labels.isValidLabel("0"));

        assertFalse(Labels.isValidLabel("A B"));
        assertTrue(Labels.isValidLabel("A-B"));
        assertTrue(Labels.isValidLabel("A/B"));
        assertTrue(Labels.isValidLabel("A_B"));
    }
}