package cdc.issues.checks;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;

import cdc.issues.locations.Location;
import cdc.util.debug.Printables;

/**
 * Specialization of {@link AbstractChecker} that lazily delegates the work to a named checker.
 * <p>
 * This must be used to define recursive checkers.<br>
 * The delegate must have been registered with {@link SnapshotManager#register(String, AbstractChecker)} before
 * this checker can be used.
 *
 * @param <O> The checked object type.
 */
public final class LazyChecker<O> extends AbstractChecker<O> {
    /** The name of the referenced checker. */
    private final String name;
    /** The referenced checker. */
    private AbstractChecker<O> delegate = null;

    public LazyChecker(Class<O> objectClass,
                       String name) {
        super(objectClass);
        this.name = name;
    }

    private AbstractChecker<O> resolve(SnapshotManager manager) {
        if (delegate == null) {
            this.delegate = manager.resolve(getObjectClass(), name).orElseThrow();
        }
        return delegate;
    }

    @Override
    protected final List<AbstractChecker<?>> getNext() {
        return Collections.emptyList();
    }

    @Override
    protected final List<AbstractChecker<?>> getNext(SnapshotManager manager) {
        return List.of(resolve(manager));
    }

    @Override
    public boolean isEnabled(SnapshotManager manager) {
        return resolve(manager).isEnabled(manager);
    }

    @Override
    public boolean isCorrectlyConfigured(SnapshotManager manager) {
        return true;
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   O object,
                                   Location location) {
        return checkAndStat(resolve(context.getManager()),
                            context,
                            object,
                            location);
    }

    @Override
    public void print(PrintStream out,
                      SnapshotManager manager,
                      int level) {
        Printables.indent(out, level);
        out.print("LAZY<" + getObjectClass().getSimpleName() + "> --> " + name);
        if (manager.isRegistered(this)) {
            out.print(" as " + manager.getRegistrationName(this));
        }
        out.println();
    }
}